from abc import abstractmethod, ABC

"""
Суть задания:
На ферме могут жить животные и птицы
И у тех и у других есть два атрибута: имя и возраст
При этом нужно сделать так, чтобы возраст нельзя было изменить извне напрямую,
но при этом можно было бы получить его значение
У каждого животного должен быть обязательный метод – ходить
А для птиц – летать
Эти методы должны выводить сообщения (контекст придумайте)
Также должны быть общие для всех классов обязательные методы – постареть -
который увеличивает возраст на 1 и метод голос
При обращении к несуществующему методу или атрибуту животного или птицы должен
срабатывать пользовательский exception NotExistException
и выводить сообщение о том, что для текущего класса (имя класса), такой метод
или атрибут не существует – (имя метода или атрибута)
На основании этих классов создать отдельные классы для Свиньи, Гуся, Курицы и
Коровы – для каждого класса метод голос должен выводить разное сообщение (хрю,
 гага, кудах, му)
После этого создать класс Ферма, в который можно передавать список животных
Должна быть возможность получить каждое животное, живущее на ферме как по
 индексу, так и через цикл for.
Также у фермы должен быть метод, который увеличивает возраст всех животных,
 живущих на ней.
"""


class Base(ABC):
    """
    Базовый класс с именем, возрастом, старением и выводом ошибки.
    """

    def __init__(self, name: str, age: int):
        """
        Экземпляры класса Base
        :param name: название
        :param age: возраст
        """
        self.name = name
        self._age = age

    def growing_up(self) -> None:
        """
        Метод старения- добавляет год
        :return: возраст + 1 год
        """
        self._age += 1

    def __getattr__(self, atr: None):
        """
        Проверка наличия атрибута класса
        :param atr: атрибут на вход
        :return: ошибка с включением атрибута
        """
        raise NotExistException(atr)

    @abstractmethod
    def voice(self):
        """
        Абстрактный метод звука животных для переопределения
        :return: ошибка
        """
        raise NotImplementedError


class Birds(Base, ABC):
    """
    Класс птиц, которые умеют летать
    """

    @staticmethod
    def fly() -> str:
        """
        Статический метод полета
        :return: сообщение для пернатых
        """
        return "Попутного ветра нам"


class Animals(Base, ABC):
    """
    Класс животных, которые ходят
    """

    @staticmethod
    def walk() -> str:
        """
        Статический метод сообщения животных
        :return: сообщение
        """
        return "Сегодня у нас пешая прогулка"


class Pig(Animals):
    """
    Класс свиньи от класса животных
    """

    def voice(self) -> str:
        """
        Метод выдачи звука. Переопределенный метод
        :return: звук строкой
        """
        return "Хрю"

    def __str__(self):
        """
        Метод вывода сообщения от свиньи
        :return: строка с сообщением
        """
        return f"Свинья {self.name} говорит {self.voice()} и {self.walk()}," \
               f" в возрасте {self._age} года."


class Goose(Birds):
    """
    Класс гуся от класса птиц
    """

    def voice(self) -> str:
        """
        Метод выдачи звука. Переопределенный метод
        :return: строку "звук"
        """
        return "Га-га"

    def __str__(self):
        """
        Метод вывода сообщения от гуся
        :return: строка с сообщением
        """
        return f"Гусь {self.name} говорит {self.voice()} и {self.fly()}, " \
               f"в возрасте {self._age} года."


class Hen(Birds):
    """
    Класс курицы от класса птиц
    """

    def voice(self) -> str:
        """
        Метод выдачи звука. Переопределенный метод
        :return: строку звука
        """
        return "Кудах"

    def __str__(self):
        """
        Метод вывода сообщения от курицы
        :return: сообщение строкой
        """
        return f"Курица {self.name} говорит {self.voice()} и {self.fly()}, " \
               f"в возрасте {self._age} года."


class Cow(Animals):
    """
    Класс коровы от класса животных
    """

    def voice(self):
        """
        Метод выдачи звука. Переопределенный метод
        :return: строку звука
        """
        return "Му"

    def __str__(self):
        """
        Метод вывода сообщения от коровы
        :return:
        """
        return f"Корова {self.name} говорит {self.voice()} и {self.walk()}, " \
               f"в возрасте {self._age} года."


class Farm:
    """
    Класс "Ферма" для возможности получения списка, поиска по индексу и по
    циклу for
    """

    def __init__(self, list_animals):
        """
        основной атрибут класса Ферма
        :param list_animals: список животных
        """
        self.list_animals = list_animals

    def __getitem__(self, item):
        """
        Метод для получения животных по индексу
        :param item: индекс
        :return: животное по индексу из списка
        """
        return self.list_animals[item]

    def all_animals(self) -> None:
        """
        Метод для получения всех животных из списка
        :return: список
        """
        for animal in self.list_animals:
            print(animal)

    def growing_up(self) -> list:
        """
        Метод, увеличивающий возраст всех животных
        :return: список животных +1 год каждому
        """
        return [animal.growing_up() for animal in self.list_animals]


class NotExistException(Exception):
    """
    Класс ошибки отсутствия атрибута
    """

    def __init__(self, func):
        """
        Метод вывода сообщения о ошибке при отсутствии атрибута
        :param func:
        """
        self.message = f"Function {func} doesn't exist"
        super(NotExistException, self).__init__(self.message)


def main():
    """
    Функция запуска
    """
    # Затаем атрибуты
    hen = Hen('Чика', 1)
    goose = Goose('Черчель', 2)
    pig = Pig('Хрюкан', 2)
    cow = Cow("Рябуха", 4)
    animals = [goose, hen, pig, cow]
    farm = Farm(animals)
    # Все животных фермы c их сообщениями
    farm.all_animals()
    # Выводим животных под индексами
    print(f'Животное под индексом [0]: {farm[0]}')
    print(f'Животное под индексом [2]: {farm[2]}')
    print(f'Животное под индексом [3]: {farm[3]}')
    # Вызов функции старения
    farm.growing_up()
    # Все животные уже с измененным возрастом
    farm.all_animals()
    # Несуществующая функция
    cow.fly()


if __name__ == "__main__":
    main()
