"""
ord_no | purch_amt | ord_date | customer_id | salesman_id
70001 | 150.5 | 2012-10-05 | 3005 | 5002
70009 | 270.65 | 2012-09-10 | 3001 | 5005
70002 | 65.26 | 2012-10-05 | 3002 | 5001
70004 | 110.5 | 2012-08-17 | 3009 |5003
70007 | 948.5 | 2012-09-10 | 3005 | 5002
70005 | 2400.6 | 2012-07-27 | 3007 | 5001
70008 | 5760 | 2012-09-10 | 3002 | 5001
70010 | 1983.43 | 2012-10-10 | 3004 | 5006
70003 | 2480.4 | 2012-10-10 | 3009 | 5003
70012 | 250.45 | 2012-06-27 | 3008 | 5002
70011 | 75.29 | 2012-08-17 | 3003 | 5007
70013 | 3045.6 | 2012-04-25 | 3002 | 5001
Заполнить таблицу данными, минимум 10 записей
Напишите следующие sql запросы:
Напечатайте номер заказа, дату заказа и количество для каждого заказа, Который
продал продавец под номером: 5002
Напечатайте уникальные id продавца(salesman_id). Используйте distinct
Напечатайте по порядку данные о дате заказа, id продавца, номер заказа,
количество
Напечатайте заказы между 70001 и 70007(используйте between, and)
"""

from mysql import connector


def db_connecting(db):
    """
    Подключение к базе данных.
    """
    cursor = db.cursor()
    print("Database connection")
    return cursor


def db_closing(db):
    """
    Функция, отключающая от базы данных.
    """
    print("Disconnect from database")
    db.close()


def make_table(name_table: str, cursor) -> None:
    """
    Функция, которая создает таблицу.
    :param name_table: название.
    :param cursor: cursor.
    :return: None.
    """
    make_table_f = f"CREATE TABLE {name_table}" \
                   f"(ord_no INT(5), purch_amt FLOAT(7,2), ord_date DATE," \
                   f" customer_id INT(4),salesman_id INT(4))"
    cursor.execute(make_table_f)
    print(f"Make table:{make_table}")


def dell_table(name_table: str, cursor) -> None:
    """
    Функция, удаляющая таблицу.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"DROP TABLE {name_table}")
    print(f"Dell table: {name_table}")


def filling_the_table(name_table: str, db, cursor) -> None:
    """
    Функция, заполняющая таблицу.
    :param name_table: название заблицы.
    :param db: база данных.
    :param cursor: cursor.
    :return: None.
    """
    for ord_no, purch_amt, ord_date, customer_id, salesman_id in [
        ("70001", "150.5", "2012-10-05", "3005", "5002"),
        ("70009", "270.65", "2012-09-10", "3001", "5005"),
        ("70002", "65.26", "2012-10-05", "3002", "5001"),
        ("70004", "110.5", "2012-08-17", "3009", "5003"),
        ("70007", "948.5", "2012-09-10", "3005", "5002"),
        ("70005", "2400.6", "2012-07-27", "3007", "5001"),
        ("70008", "5760", "2012-09-10", "3002", "5001"),
        ("70010", "1983.43", "2012-10-10", "3004", "5006"),
        ("70003", "2480.4", "2012-10-10", "3009", "5003"),
        ("70012", "250.45", "2012-06-27", "3008", "5002"),
        ("70011", "75.29", "2012-08-17", "3003", "5007"),
        ("70013", "3045.6", "2012-04-25", "3002", "5001")
    ]:
        inset_in_table = f"INSERT INTO {name_table} (" \
                         f"ord_no, purch_amt, ord_date, customer_id, " \
                         f"salesman_id) VALUES (" \
                         f"'{ord_no}', '{purch_amt}', '{ord_date}', " \
                         f"'{customer_id}', '{salesman_id}')"
        cursor.execute(inset_in_table)
    db.commit()
    print("inset in table")


def all_print(name_table: str, cursor) -> None:
    """
    Функция, выводящая дату заказа, айдишник, номер заказа и количество.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT ord_date, salesman_id, ord_no, purch_amt "
                   f"FROM {name_table}")
    for y in cursor.fetchall():
        print(y)


def salesman_id(name_table: str, cursor, id) -> None:
    """
    Функция, выводящая по айди номер заказа, дату заказа и количество.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :param id: айди.
    :return: None.
    """
    cursor.execute(f"SELECT ord_no, ord_date, purch_amt "
                   f"FROM {name_table} where salesman_id = {id}")
    for a in cursor.fetchall():
        print(a)


def unique_id(name_table: str, cursor) -> None:
    """
    Функция, выдающая уникальный айдишник.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT DISTINCT salesman_id FROM {name_table}")
    for b in cursor.fetchall():
        print(b)


def between_ord(name_table: str, cursor, ord_1: int, ord_2: int) -> None:
    """
    Функция, выводящая заказы между двумя заказами.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :param ord_1: больше.
    :param ord_2: меньше.
    :return: None.
    """
    cursor.execute(
        f"SELECT * FROM {name_table} WHERE ord_no > {ord_1} "
        f"AND ord_no < {ord_2}"
    )
    for c in cursor.fetchall():
        print(c)


if __name__ == "__main__":
    db_arg = connector.connect(
        host="localhost",
        user="root",
        password="01198925MySQL",
        database="test_db"
    )

    table_n = "Orders_table"

    # Подключение к базе данных
    connecting = db_connecting(db_arg)

    # Создание таблицы
    make_table(table_n, connecting)

    # Заполнение таблицы
    filling_the_table(table_n, db_arg, connecting)

    # Дата заказа, айдишник, номер заказа и количество.
    all_print(table_n, connecting)

    # 5001 id
    salesman_id(table_n, connecting, 5001)

    # Уникальные айдишки.
    unique_id(table_n, connecting)

    # 70000 - 70007
    between_ord(table_n, connecting, 70001, 70007)

    # Dell
    dell_table(table_n, connecting)

    # Disconnect
    db_closing(db_arg)
