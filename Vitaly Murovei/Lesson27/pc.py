"""
PRO_ID | PRO_NAME | PRO_PRICE | PRO_COM
101 | Mother Board | 3200 | 15
102 | Key Board | 450 | 16
103 | ZIP drive | 250 | 14
104 | Speaker | 550 | 16
105 | Monitor | 5000 | 11
106 | DVD drive | 900 | 12
107 | CD drive | 800 | 2
108 | Printer | 2600 | 13
109 | Refill cartridge | 350 | 13
110 | Mouse | 250 | 12
Заполнить таблицу данными, минимум 10 записей
Напишите следующие sql запросы:
1. Напечатайте общую стоимость компонентов
2. Напечатайте среднюю стоимость компонентов
3. Напечатайте компоненты с ценой между 200 и 600
4. Напечатайте название компонента у которого самая высокая цена
5. Напечатайте название компонента у которого низкая высокая цена
"""
from mysql import connector


def db_connecting(db):
    """
    Подключение к базе данных.
    """
    cursor = db.cursor()
    print("Database connection")
    return cursor


def db_closing(db):
    """
    Функция, отключающая от базы данных.
    """
    print("Disconnect from database")
    db.close()


def make_table(name_table: str, cursor) -> None:
    """
    Функция, которая создает таблицу.
    :param name_table: название.
    :param cursor: cursor.
    :return: None.
    """
    make_table_f = f"CREATE TABLE {name_table}" \
                   f"(PRO_ID INT(3), PRO_NAME VARCHAR(255)," \
                   f" PRO_PRICE INT(5), PRO_COM INT(2))"
    cursor.execute(make_table_f)
    print(f"Make table:{make_table}")


def dell_table(name_table: str, cursor) -> None:
    """
    Функция, удаляющая таблицу.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"DROP TABLE {name_table}")
    print(f"Dell table: {name_table}")


def filling_the_table(name_table: str, db, cursor) -> None:
    """
    Функция, заполняющая таблицу.
    :param name_table: название заблицы.
    :param db: база данных.
    :param cursor: cursor.
    :return: None.
    """
    for PRO_ID, PRO_NAME, PRO_PRICE, PRO_COM in [
        ("101", "Mother Board", "3200", "15"),
        ("102", "Key Board", "450", "16"),
        ("103", "ZIP drive", "250", "14"),
        ("104", "Speaker", "550", "16"),
        ("105", "Monitor", "5000", "11"),
        ("106", "DVD drive", "900", "12"),
        ("107", "CD drive", "800", "2"),
        ("108", "Printer", "2600", "13"),
        ("109", "Refill cartridge", "350", "13"),
        ("110", "Mouse", "250", "12"),
    ]:
        insert_in_table = f"INSERT INTO {name_table} (" \
                          f"PRO_ID, PRO_NAME, PRO_PRICE, PRO_COM) VALUES " \
                          f"('{PRO_ID}', '{PRO_NAME}', '{PRO_PRICE}', " \
                          f"'{PRO_COM}')"
        cursor.execute(insert_in_table)
    db.commit()
    print("insert in table")


def summ_price(name_table: str, cursor) -> None:
    """
    Функция, осуществляющая вывод стоимости всех компонентов.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT SUM(PRO_PRICE) FROM {name_table}")
    print(cursor.fetchall())


def avg_price(name_table: str, cursor) -> None:
    """
    Функция, осуществляющая вывод средней стоимости.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT AVG(PRO_PRICE) FROM {name_table}")
    print(cursor.fetchall())


def between_price(name_table: str, cursor, price_1: int, price_2: int) -> None:
    """
    Функция, осуществляющая вывод цен в промежудке между 200 и 600.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :param price_1: макс.
    :param price_2: мин.
    :return: None.
    """
    cursor.execute(f"SELECT * FROM {name_table} WHERE PRO_PRICE > {price_1} "
                   f"AND PRO_PRICE < {price_2}")
    for a in cursor.fetchall():
        print(a)


def max_price(name_table: str, cursor) -> None:
    """
    Функция, осуществляющая вывод самой высокой цены.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT PRO_NAME FROM {name_table} WHERE PRO_PRICE = "
                   f"(SELECT MAX(PRO_PRICE) FROM {name_table})")
    print(cursor.fetchall())


def min_price(name_table: str, cursor) -> None:
    """
    Функция, осуществляющая вывод самой низкой цены.
    :param name_table: название таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT PRO_NAME FROM {name_table} WHERE PRO_PRICE = "
                   f"(SELECT MIN(PRO_PRICE) FROM {name_table})")
    print(cursor.fetchall())


if __name__ == "__main__":
    db_arg = connector.connect(
        host="localhost",
        user="root",
        password="01198925MySQL",
        database="test_db"
    )

    table_n = "PC_table_HW"
    connecting = db_connecting(db_arg)
    make_table(table_n, connecting)
    filling_the_table(table_n, db_arg, connecting)
    summ_price(table_n, connecting)
    avg_price(table_n, connecting)
    between_price(table_n, connecting, 200, 600)
    max_price(table_n, connecting)
    min_price(table_n, connecting)
    dell_table(table_n, connecting)
    db_closing(db_arg)
