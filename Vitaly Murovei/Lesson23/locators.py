"""
Для следующего приложения: https://www.saucedemo.com/
необходимо найти все локаторы для функциональных элементов, т.е. иконки,
кнопки, поля ввода и т.д.
Нужно найти локаторы для окна Login
Залогиниться и найти все элементы в каталоге (standard_user)
Вывести информацию по всем товарам, которая возвращает их название и цену
Для всех остальных элементов создать методы, которые находят их на странице и
возвращают элемент
* Предусмотреть логирование выполненных действий
Создать Pull Request.
"""
from selenium.webdriver import Chrome
import time


def driver_chrome():
    """
    Функция инициализации драйвера Chrome.
    :return: драйвер.
    """
    driver = Chrome(executable_path="C:/Users/acer/Downloads/chromedriver.exe")
    return driver


def page_of_login(driver_arg, link_arg: str) -> str:
    """
    Функция, переходящая на страницу логина.
    :param driver_arg: драйвер.
    :param link_arg: ссылка на страницу.
    :return: строка с наименованием страницы.
    """
    driver_arg.get(link_arg)
    return driver_arg.title


def find_username(driver_arg):
    """
    Функция, осуществляющая поиск поля имени.
    :param driver_arg: драйвер.
    :return: элемент.
    """
    return driver_arg.find_element_by_xpath("//input[@name='user-name']")


def find_password(driver_arg):
    """
    Функция, осуществляющая поиск поля пароля.
    :param driver_arg: драйвер.
    :return: элемент.
    """
    return driver_arg.find_element_by_xpath("//input[@name='password']")


def find_button_login(driver_arg):
    """
    Функция, осуществляющая поиск кнопки login.
    :param driver_arg: драйвер.
    :return: искомый элемент.
    """
    return driver_arg.find_element_by_xpath("//input[@name='login-button']")


def authorization(username_arg, password_arg, login_arg, username_str_arg: str,
                  password_str_arg: str) -> None:
    """
    Функция авторизации пользователя. Введение имени, пароля и нажатие кнопки.
    :param username_arg: поле username.
    :param password_arg: поле password.
    :param login_arg: кнопка login.
    :param username_str_arg: вводимые параметры в поле имени.
    :param password_str_arg: вводимые параметры в поле пароля.
    :return: None.
    """
    username_arg.send_keys(username_str_arg)
    password_arg.send_keys(password_str_arg)
    login_arg.click()


def find_goods(driver_arg) -> None:
    """
    Функция, осуществляющая поиск товаров и их цену.
    :param driver_arg: драйвер.
    :return: None.
    """
    goods = driver_arg.find_elements_by_xpath(
        "//div[@class='inventory_item_name']")
    cost = driver_arg.find_elements_by_xpath(
        "//div[@class='inventory_item_price']")
    print("Goods and costs:")
    for y in range(len(goods)):
        print(f"{goods[y].text} - {cost[y].text}")


def elements_without_click(driver_arg) -> list:
    """
    Функция, осуществляющая поиск элементов без возможности кликать.
    :param driver_arg: драйвер.
    :return: список элементов.
    """
    elements_without = []
    logo_main = driver_arg.find_element_by_xpath("//div[@class='app_logo']")
    title = driver_arg.find_element_by_xpath("//span[@class='title']")
    picture_title = driver_arg.find_element_by_xpath("//div[@class='peek']")
    footer_copy = driver_arg.find_element_by_xpath("//div[@class='footer_"
                                                   "copy']")
    footer_robot = driver_arg.find_element_by_xpath("//img[@class='footer_"
                                                    "robot']")
    description = driver_arg.find_elements_by_xpath("//div[@class='inventory_"
                                                    "item_desc']")
    elements_without.extend([logo_main, title, picture_title, footer_copy,
                             footer_robot, description])
    return elements_without


def find_clickable_elements(driver_arg) -> list:
    """
    Функция, осуществляющая поиск элементов, которые можно кликать.
    :param driver_arg: драйвер.
    :return: список элементовю
    """
    elements_with_click = []
    burger = driver_arg.find_element_by_xpath("//div[@class='bm-burger-"
                                              "button']")
    shopping_cart = driver_arg.find_element_by_xpath("//a[@class='shopping_"
                                                     "cart_link']")
    active_option = driver_arg.find_element_by_xpath("//span[@class='active_"
                                                     "option']")
    link_twit = driver_arg.find_element_by_xpath("//a[@href='https://"
                                                 "twitter.com/saucelabs']")
    link_face = driver_arg.find_element_by_xpath("//a[@href='https://"
                                                 "www.facebook.com/"
                                                 "saucelabs']")
    link_linkedin = driver_arg.find_element_by_xpath("//a[@href='https://"
                                                     "www.linkedin.com/company"
                                                     "/sauce-labs/']")
    buttons = driver_arg.find_element_by_xpath("//button[@class="
                                               "'btn btn_primary btn_small"
                                               " btn_inventory']")
    goods_picture = driver_arg.find_element_by_xpath("//img[@class="
                                                     "'inventory_item_img']")
    elements_with_click.extend([burger, shopping_cart, active_option,
                                link_twit, link_face, link_linkedin, buttons,
                                goods_picture])
    return elements_with_click


def driver_chrome_close(driver_arg) -> None:
    """
    Функция, осуществляющая закрытие драйвера.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.close()


def main_all_function(link_arg, username_str_arg, password_str_arg) -> None:
    """
    Функция, осуществляющая запуск всех функций + логирование.
    :param link_arg: ссылка.
    :param username_str_arg: логин.
    :param password_str_arg: пароль.
    :return: None.
    """
    driver_all = driver_chrome()
    print(f"{time.strftime('%H:%M:%S')}" f"Start:{driver_all}")

    """
    Выполнений всех функций по порядку(открытие страницы логина, поиск полей
    для заполнения, поиск кнопки логирования, ввод данных и логирование, вывод
    всех товаров и их цены, поиск кликабельных и некликабельных элементов и
    закрытие драйвера) и указание времени(часы,минуты, секунды) выполнения
    каждой.
    """
    # Страница логина
    title_page = page_of_login(driver_all, link_arg)
    print(f"{time.strftime('%H:%M:%S')}" f"Open login page:{title_page}")

    # Поле логина
    username = find_username(driver_all)
    print(f"{time.strftime('%H:%M:%S')}" f"Username:{username}")

    # Поле пароля
    password = find_password(driver_all)
    print(f"{time.strftime('%H:%M:%S')}" f"Password:{password}")

    # Кнопка логирования
    login = find_button_login(driver_all)
    print(f"{time.strftime('%H:%M:%S')}" f"Button login:{login}")

    # Логирование
    authorization(username, password, login, username_str_arg,
                  password_str_arg)
    print(f"{time.strftime('%H:%M:%S')}" f"Login successful.")

    # Товары
    find_goods(driver_all)
    print(f"{time.strftime('%H:%M:%S')}" f"Goods and costs")

    # Некликабельные элементы
    not_clickable = elements_without_click(driver_all)
    print(f"{time.strftime('%H:%M:%S')}" f"Elements without click:"
          f"{not_clickable}")

    # Кликабельные элементы
    clickable = find_clickable_elements(driver_all)
    print(f"{time.strftime('%H:%M:%S')}" f"Clickable elements: {clickable}")

    # Закрытие
    driver_chrome_close(driver_all)
    print(f"{time.strftime('%H:%M:%S')}" f"Close driver: {driver_all}")


if __name__ == "__main__":
    link = "https://www.saucedemo.com/"
    username_str = "standard_user"
    password_str = "secret_sauce"

    main_all_function(link, username_str, password_str)
