# Task 1 (перевод строки в массив)

x = "Robin Singh"

y = "I love arrays they are my favorite"

x = x.split()

y = y.split()

print(list(x))

print(list(y))
# Task 2 (переводим список в строку и объединяем строки)

a = ['Ivan', 'Ivanov']

b = "Minsk"

c = "Belarus"

a = ' '.join(a)

print(f"Привет,{a}!Добро пожаловать в {b}{c}")

# Task 3 (переводим список в строку с помощью ''.join())

sp = ["I", "love", "arrays", "they", "are", "my", "favorite"]

sp = ' '.join(sp)

print(sp)

# Task 4 (Добавляем элемент в нужную позицию .insert()  и удаляем по индексу

ls1 = [12, 'O', 17, 23, 68, 'wq', 'sr', 234, 345, 'dfg']

ls1.insert(2, "wow")

print(ls1)

del ls1[6]

print(ls1)

# Task 5

a = {'a': 1, 'b': 2, 'c': 3}

b = {'c': 3, 'd': 4, 'e': ''}

# Создайте словарь, который будет содержать в себе все элементы обоих словарей

c = {**a, **b}

print(c)

# Обновление словарей

a.update(b)

print(a)

# Проверить что все значения в словаре “a” не пустые либо не равны нулю

e = all(a.values())

print(e)

# Проверить что есть хотя бы одно пустое значение (результат выполнения

# должен быть True)

f = not all(a.values())

print(f)

# Отсортировать словарь по алфавиту в обратном порядке

y = sorted(a, reverse=True)

print(y)

#  Изменить значение под одним из ключей и вывести все значения

a['a'] = 13

for g in a.values():
    print(g)

# Task 6

list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]

# Вывести только уникальные значения и сохранить их в отдельную переменную

list_b = set(list_a)

print(list_b)

# Добавить в полученный объект значение 22

list_b.add(22)

print(list_b)

# Сделать list_a неизменяемым

list_a = tuple(list_a)

print(list_a)

# Измерить его длинну

print(len(list_a))

# Задачи на закрепление форматирования

a = 10

b = 25

summ = a + b

diff = a - b

print("Summ is {summ}, and diff = {diff}".format(summ=summ, diff=diff))

print(f"Summ is {summ}, and diff = {diff}")

# 2

list_of_children = ['Sasha', 'Vasia', 'Nikalai']

Sasha = list_of_children[0]

Vasia = list_of_children[1]

Nikalai = list_of_children[2]

print("First child is {sasha}, second is {vasia}, and last one - {nikalai}"

      .format(sasha=Sasha, vasia=Vasia, nikalai=Nikalai))

print(f"First child is {Sasha}, second is {Vasia}, and last one - {Nikalai}")
