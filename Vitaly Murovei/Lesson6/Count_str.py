# Task 2 подсчет количества букв
# Вызываем модуль
import collections

# используем Counter
s = input("Введите строку:")
c = collections.Counter(s)
# Собираем строку
print(''.join('{}{}'.format(key, val) for key, val in c.items()))
