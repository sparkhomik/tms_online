# Task 8
# написать функцию, которая проверяет есть ли в списке объект,
# которые можно вызвать
def call(list_1: list) -> bool:
    """
    :param list_1: список, который пишем для проверки
    :return:булевое значение
    """
    for i in list_1:
        if callable(i) is True:
            return True


print(call([int, str, dict, 1, 2, 3]), '\n')
