# Task 6

# Функция проверяющая тип данных:

def my_function(a: list) -> dict:
    """
    Функция подсчета количества данных по типу в списке.
    :param a: список.
    :return:dict: словарь, где ключ - тип, значение - количество.
    """
    dict1 = {}
    types1 = []
    for i in a:
        types1.append(type(i))
    for j in types1:
        dict1[j] = dict1.get(j, 0) + 1
    return dict1


list1 = [1, 2, 3, 'a', (1, 2), 'b']

print(my_function(list1))
