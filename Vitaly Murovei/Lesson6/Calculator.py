def cal(action: str, x: int, y: int):
    """
    Данное задание выполняем при помощи if, elif,else в теле функции с
    вводными данными "action","x","y"
    """
    if action == '1':
        action1 = x + y
        result = (print('Сумма:', action1))
    elif action == '2':
        action1 = x - y
        result = (print('Разность:', action1))
    elif action == '3':
        action1 = x * y
        result = (print('Произведение:', action1))
    elif action == '4':
        if y == 0:
            result = (print('Вы пытаетесь делить на 0'))
        else:
            result1 = x // y
            result2 = x % y
            result = (print('Частное:', result1, 'Остаток:', result2))
    else:
        result = (print('Выберете из 4 ех вариантов'))
    return result


# Выводим условия и поля для ввода данных
while True:
    print("1.Сложение")
    print("2.Вычитание")
    print("3.Умножение")
    print("4.Деление")
    print("5.Выход")
    a = input('Введите номер пункта меню:')
    if a == "5":
        break
    b = int(input('Введите первое число:'))
    c = int(input('Введите второе число:'))
    cal(a, b, c)
