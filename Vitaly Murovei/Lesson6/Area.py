# Task 5
# Работа с областями видимости

def func(list1: list) -> list:
    """
    Функция добавления элемента в список.
    :param list1: список  уровня модуля.
    :return: измененный список.
    """
    new_list = list1[:]
    # исправил при помощи среза. Можно было еще при помощи .copy().
    new_list.append('a')
    return new_list


my_list = [1, 2, 3]

print(func(my_list))
print(my_list)
