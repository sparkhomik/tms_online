"""
тест2:
Заполните поле "Name"
Нажмите на кнопку "Submit"
Проверьте наличие сообщения об ошибке
тест3:
Заполните поле "Message"
Нажмите на кнопку "Submit"
Проверьте наличие сообщения об ошибке
"""
import pytest
from selenium.webdriver import Chrome


@pytest.fixture(scope='session')
def driver_chrome():
    """
    Фикстура инициализации драйвера и закрытия его.
    :return: драйвер.
    """
    driver = Chrome(executable_path="C:/Users/acer"
                                    "/Downloads/chromedriver.exe")
    yield driver
    driver.quit()


@pytest.fixture()
def opening_by_link(driver_chrome):
    """
    Фикстура, открывающая окно по ссылке.
    :param driver_chrome: драйвер.
    :return: драйвер.
    """
    driver_chrome.get("https://ultimateqa.com/filling-out-forms/")
    return driver_chrome


@pytest.fixture(params=[("some_name", "", "Fill in fields:Message"),
                        ("", "some_message", "Fill in fields:Name"),
                        ("some_name", "some_message", "Thanks for job")],
                ids=["no_message", "no_name", "successful"])
def enter_data(opening_by_link, request) -> tuple:
    """
    Фикстура ввода имени и сообщения. Фикстура параметризованная и имеет 3
    3 варианта для ввода.
    :param opening_by_link: драйвер.
    :param request: запрос.
    :return: ожидаемы и фактический результат в tuple.
    """
    params = request.param
    opening_by_link.implicitly_wait(10)
    name_form = opening_by_link.find_element_by_xpath("//input[@id='et_pb"
                                                      "_contact_name_0']")
    message_form = opening_by_link.find_element_by_xpath("//textarea[@id='et"
                                                         "_pb_contact"
                                                         "_message_0']")
    submit_button = opening_by_link.find_element_by_xpath("//button[@name='et"
                                                          "_builder_submit"
                                                          "_button']")
    name_form.send_keys(params[0])
    message_form.send_keys(params[1])
    submit_button.click()
    message = opening_by_link.find_element_by_xpath("//div[@class='et-pb-"
                                                    "contact-message']/p").text
    message_add = opening_by_link("//div[@class='et-pb-contact-message']"
                                  "/ul/li")
    if message_add:
        result = message + message_add[0].text
    else:
        result = message

    expected = params[2]
    return expected, result
