"""
Откройте страницу: https://ultimateqa.com/complicated-page/
Нажмите на 2-ю сверху кнопку во втором столбце используя:
XPATH
CSS selector
class name
"""
from selenium.webdriver import Chrome


def chrome_driver():
    """
    Функция инициализации драйвера.
    :return:драйвер.
    """
    driver = Chrome(executable_path="C:/Users/acer/Downloads/chromedriver.exe")
    return driver


def following_a_link(driver_data, link_data) -> None:
    """
    Функция перехода по ссылке, где будет производиться поиск элементов + клик.
    :param driver_data: драйвер.
    :param link_data: ссылка.
    :return: None.
    """
    driver_data.get(link_data)


def opening_xpath(driver_data) -> None:
    """
    Функция, осуществляющая поиск по XPATH + клик.
    :param driver_data: драйвер.
    :return: None.
    """
    driver_data.find_element_by_xpath("//a[@class='et_pb_button et_pb_button_4"
                                      " et_pb_bg_layout_light']")
    driver_data.click()


def opening_css(driver_data) -> None:
    """
    Функция, осуществляющая поиск по CSS + клик.
    :param driver_data: драйвер.
    :return: None.
    """
    driver_data.find_element_by_css_selector(".et_pb_button.et_pb_button_"
                                             "4.et_pb_bg_layout_light")
    driver_data.click()


def opening_class_name(driver_data) -> None:
    """
    Функция, осуществляющая поиск по CLASS + клик.
    :param driver_data: драйвер.
    :return: None.
    """
    driver_data.find_element_by_class_name("et_pb_button.et_pb_button_"
                                           "4.et_pb_bg_layout_light")
    driver_data.click()


def exit_driver(driver_data) -> None:
    """
    Функция закрытия драйвера.
    :param driver_data: драйвер.
    :return: None.
    """
    driver_data.close()


if __name__ == "__main__":
    link = "https://ultimateqa.com/complicated-page/"
    driver_start = chrome_driver()
    following_a_link(driver_start, link)
    # 3 способа нажатия кнопки
    opening_xpath(driver_start)
    opening_css(driver_start)
    opening_class_name(driver_start)
    exit_driver(driver_start)
