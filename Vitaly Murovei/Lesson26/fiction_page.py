from base_page import BasePage
from fiction_page_locators import FictionPageLocators


class FictionPage(BasePage):

    def get_title_fiction(self):
        """
        Метод получения заголовка страницы.
        :return:заголовок.
        """
        return self.driver.title

    def header_fiction(self):
        """
        Метод получения текста на странице.
        :return: элемент.
        """
        element_get = self.find_element(FictionPageLocators.HEADER_LOCATORS)
        return element_get

    def get_site_tree_fiction(self):
        """
        Метод получения текста в древе сайта.
        :return: элемент.
        """
        element_tree = self.find_element(FictionPageLocators.
                                         FICTION_TREE_LOCATORS)
        return element_tree
