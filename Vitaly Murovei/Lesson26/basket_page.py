from base_page import BasePage
from basket_page_locators import BasketPageLocators


class BasketPage(BasePage):
    """
    Класс страницы корзины.
    """

    def get_title_fiction(self):
        """
        Метод получения заголовка страницы.
        :return: заголовок.
        """
        return self.driver.title

    def open_basket(self):
        """
        Метод поиска ссылки перехода в корзину.
        :return: элемент.
        """
        basket = self.find_element(BasketPageLocators.BASKET_PAGE_REF)
        return basket

    def header_basket(self):
        """
        Метод поиска текста на странице.
        :return: элемент.
        """
        header_basket = self.find_element(
            BasketPageLocators.BASKET_HEADER_LOCATORS)
        return header_basket

    def find_empty_basket(self):
        """
        Метод, который ищет элемент, указывающий на пустую корзину.
        :return: элемент.
        """
        empty_basket = self.find_element(
            BasketPageLocators.CLICKABLE_BASKET_LOCATOR)
        return empty_basket

    def find_tree_basket(self):
        """
        Метод, который ищет элемент в древе страницы.
        :return: элемент.
        """
        tree_basket = self.find_element(
            BasketPageLocators.BASKET_TREE_LOCATOR)
        return tree_basket
