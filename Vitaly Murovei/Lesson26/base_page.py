from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    """
    Базовый класс.
    """

    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open(self):
        """
        Метод открытия окна.
        """
        self.driver.get(self.url)

    def find_element(self, locator, timeout=10):
        """
        Метод поиска элемента.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: найденный элемент.
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(
                EC.presence_of_element_located(locator)
            )
            return element
        except TimeoutException:
            return None

    def find_clickable_element(self, locator, timeout=10):
        """
        Метод поиска активного элемента.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: найденный элемент(его активность).
        """
        try:
            WebDriverWait(self.driver, timeout).until(
                EC.element_to_be_clickable(locator))
            return True
        except TimeoutException:
            return False
