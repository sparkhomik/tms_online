from selenium.webdriver.common.by import By


class BasketPageLocators:
    """
    Класс с локаторами страницы корзины.
    """

    BASKET_PAGE_REF = (By.PARTIAL_LINK_TEXT, "View basket")
    BASKET_TREE_LOCATOR = (By.XPATH, "//ul[@class='breadcrumb']")
    BASKET_HEADER_LOCATORS = (By.XPATH, "//div[@class='page-header action']")
    CLICKABLE_BASKET_LOCATOR = (By.PARTIAL_LINK_TEXT, "Continue shopping")
