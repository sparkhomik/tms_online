from main_page import MainPage


def test_get_title(driver):
    """
    Функция. проверяющая заголовок.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    books_page = page.open_books_page()
    assert "Books" in books_page.get_title(), "No Books in title"


def test_books_header(driver):
    """
    Функция, проверяющая наличие текста на странице.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    books_page = page.open_books_page()
    assert "Books" in books_page.get_header().text, "No Books in header"


def test_get_site_tree(driver):
    """
    Функция, проверяющая наличие элемента в древе сайта.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    book_page = page.open_books_page()
    assert book_page.get_site_tree().text == "Books", "No Books in site tree"
