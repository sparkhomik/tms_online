from basket_page import BasketPage
from basket_page_locators import BasketPageLocators
from book_page import BooksPage
from books_page_locatos import BookPageLocators
from fiction_page import FictionPage
from fiction_page_locators import FictionPageLocators
from main_page_locators import MainPageLocators

from base_page import BasePage


class MainPage(BasePage):
    """
    Класс главной страницы.
    """
    URL = "http://selenium1py.pythonanywhere.com/en-gb/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title_main(self):
        """
        Метод получения заголовка страницы.
        :return: заголовок.
        """
        return self.driver.title

    def get_oscar_reference(self):
        """
        Метод поиска элементов главной страницы.
        :return: элемент.
        """
        reference = self.find_element(MainPageLocators.OSCAR_REF_LOCATORS)
        return reference

    def find_oscar_text(self):
        """
        Метод поиска текста на главной странице.
        :return: элемент.
        """
        ref_welcome = self.find_element(MainPageLocators.MAIN_REF_WELCOME)
        return ref_welcome

    def clickable_element_on_oscar(self):
        """
        Метод поиска кликабельного элемента на странице.
        :return: элемент.
        """
        click_element = self.find_element(MainPageLocators.CLICKABLE_REF_PAGE)
        return click_element

    def open_books_page(self):
        """
        Метод поиска ссылки книг и переключения.
        """
        books_link = self.find_element(BookPageLocators.OPEN_BOOKS_LOCATOR)
        books_link.click()
        return BooksPage(self.driver, self.driver.current_url)

    def open_fiction_page(self):
        """
        Метод поиска ссылки на fiction и переключение.
        """
        books_link = self.find_element(BookPageLocators.OPEN_BOOKS_LOCATOR)
        books_link.click()
        fiction_link = self.find_element(FictionPageLocators.FICTION_PAGE_REF)
        fiction_link.click()
        return FictionPage(self.driver, self.driver.current_url)

    def clickable_button_basket(self):
        """
        Метод поиска кликабельных элементов на главной странице(basket).
        :return:элемент.
        """
        click_basket = self.find_element(BasketPageLocators.BASKET_PAGE_REF)
        return click_basket

    def open_basket_page(self):
        """
        Метод поиска ссылки и перехода на страницу корзины.
        """
        basket_link = self.find_element(BasketPageLocators.BASKET_PAGE_REF)
        basket_link.click()
        return BasketPage(self.driver, self.driver.current_url)
