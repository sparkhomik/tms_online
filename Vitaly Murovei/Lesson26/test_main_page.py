from main_page import MainPage


def test_oscar_ref_exists(driver):
    page = MainPage(driver)
    page.open()
    assert page.get_oscar_reference(), "No reference to Oscar"


def test_oscar_ref(driver):
    page = MainPage(driver)
    page.open()
    assert page.find_oscar_text(), "No link on  start page"


def test_clickable_ref(driver):
    page = MainPage(driver)
    page.open()
    assert page.clickable_element_on_oscar(), "No clickable ref"
