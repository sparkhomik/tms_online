from base_page import BasePage
from books_page_locatos import BookPageLocators


class BooksPage(BasePage):

    def get_title(self):
        """
        Метод получения заголовка страницы.
        :return: заголовок.
        """
        return self.driver.title

    def get_header(self):
        """
        Метод получения текста на странице.
        :return: элемент.
        """
        element = self.find_element(BookPageLocators.HEADER_LOCATORS)
        return element

    def get_site_tree(self):
        """
        Метод получения текста в древе страницы.
        :return: элемент.
        """
        element = self.find_element(BookPageLocators.SITE_TREE_LOCATORS)
        return element
