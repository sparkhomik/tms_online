import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def driver():
    """
    Фикстура старта и закрытия драйвера
    :return: драйвер.
    """
    driver = webdriver.Chrome(executable_path="C:/Users/acer/Downloads"
                                              "/chromedriver.exe")
    yield driver
    driver.quit()
