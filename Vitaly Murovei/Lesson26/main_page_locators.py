from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Локаторы главной страницы
    """
    MAIN_REF_WELCOME = (By.XPATH,
                        "//a[@href="
                        "'http://latest.oscarcommerce.com/gateway/']")
    OSCAR_REF_LOCATORS = (By.XPATH, "//a[@href='/en-gb/']")
    CLICKABLE_REF_PAGE = (By.XPATH, "//a[@href='http://latest."
                                    "oscarcommerce.com/gateway/']")
