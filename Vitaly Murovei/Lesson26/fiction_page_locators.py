from selenium.webdriver.common.by import By


class FictionPageLocators:
    """
    Локаторы страницы  fiction.
    """

    FICTION_PAGE_REF = (By.PARTIAL_LINK_TEXT, "Fiction")
    FICTION_TREE_LOCATORS = (By.XPATH, "//li[@class='active']")
    HEADER_LOCATORS = (By.XPATH, "//div[@class='page-header action']")
