from main_page import MainPage


def test_clickable_button_basket(driver):
    """
    Тестовая функция, проверяющая наличие активного элемента (basket).
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    button_click = page.clickable_button_basket()
    assert "View basket" in button_click.text, "No that text of button"


def test_get_title_basket(driver):
    """
    Функция, проверяющая заголовок страницы.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    basket = page.open_basket_page()
    assert "Basket" in basket.get_title_fiction(), "No Fiction in title"


def test_header_basket(driver):
    """
    Функция, проверяющая наличие указанного текста на странице.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    basket_page = page.open_basket_page()
    assert "Basket" in basket_page.header_basket().text, "No Basket in header"


def test_empty_basket(driver):
    """
    Функция, проверяющая наличие элемента, указывающего на пустую корзину.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    basket_empty = page.open_basket_page()
    assert "Continue shopping" in basket_empty.find_empty_basket().text, \
        "Basket not empty"


def test_find_tree_basket(driver):
    """
    Функция, осуществляющая поиск элемента в древе страницы.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    tree_basket = page.open_basket_page()
    assert "Basket" in tree_basket.find_tree_basket().text, "No Basket in tree"
