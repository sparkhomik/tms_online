from selenium.webdriver.common.by import By


class BookPageLocators:
    """
    Локаторы страницы books.
    """
    HEADER_LOCATORS = (By.XPATH, "//div[@class='page-header action']")
    SITE_TREE_LOCATORS = (By.XPATH, "//li[@class='active']")
    OPEN_BOOKS_LOCATOR = (By.XPATH,
                          "//a[@href='/en-gb/catalogue/category/books_2/']")
