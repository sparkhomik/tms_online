from main_page import MainPage


def test_get_title(driver):
    """
    Функция, проверяющая текст заголовка.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    fiction = page.open_fiction_page()
    assert "Fiction" in fiction.get_title_fiction(), "No Fiction in title"


def test_header_fiction(driver):
    """
    Функция, проверяющая наличие элемента на странице.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    fiction = page.open_fiction_page()
    assert "Fiction" in fiction.header_fiction().text, "No Fiction in header"


def test_tree_fiction(driver):
    """
    Функция, проверяющая наличие элемента в древе сайта.
    :param driver: драйвер.
    """
    page = MainPage(driver)
    page.open()
    fiction = page.open_fiction_page()
    assert "Fiction" in fiction.get_site_tree_fiction().text, \
        "No Fiction in header"
