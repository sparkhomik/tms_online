"""
Написать класс для работы с текущей операционной системой:
Должно быть три метода:
- получить имя операционной системы
- релиз операционной системы
- версию OS

"""
import platform


class Os:
    """
    Класс операционной системы.
    """

    @staticmethod
    def get_name() -> str:
        """
        Метод определения ОС через спец команду.
        :return: строка, имя ос.
        """
        return platform.system()

    @staticmethod
    def get_release() -> str:
        """
        Метод определения релиза операционной системы.
        :return: строка, релиз ос.
        """
        return platform.release()

    @staticmethod
    def get_version() -> str:
        """
        Метод определения версии операционной системы.
        :return: строка, версия ос.
        """
        if platform.system() == 'Darwin':
            return platform.mac_ver()[0]
        else:
            return platform.version()


if __name__ == '__main__':
    os = Os()
    print(os.get_name())
    print(os.get_release())
    print(os.get_version())
