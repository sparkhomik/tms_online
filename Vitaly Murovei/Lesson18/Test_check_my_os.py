import unittest
from Check_my_os import Os


class TestCheck(unittest.TestCase):
    """
    Класс тестирования модуля с помощью unittest.
    """

    def test_get_name(self):
        """
        Метод тестирования имени ОС
        """
        self.assertEqual(Os.get_name(), 'Windows')

    def test_get_release(self):
        """
        Метод тетирования релиза
        """
        self.assertEqual(Os.get_release(), '10')

    def test_get_version(self):
        """
        Метод тестирования версии
        """
        self.assertEqual(Os.get_version(), '10.0.19041')

    @unittest.expectedFailure
    def test_get_name_fail(self):
        """
        Метод ошибочного тестирования (ожидаемая ошибка).
        """
        self.assertEqual(Os.get_name(), 'Darwin')

    @unittest.expectedFailure
    def test_get_release_fail(self):
        """
        Метод ошибочного тестирования (ожидаемая ошибка).
        """
        self.assertEqual(Os.get_release(), '9')

    @unittest.expectedFailure
    def test_get_version_fail(self):
        """
        Метод ошибочного тестирования (ожидаемая ошибка).
        """
        self.assertEqual(Os.get_version(), '9')
