# Task 1
"""
Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
зарезервирована ли книги или нет. Создайте класс пользователь который может
брать книгу, возвращать, бронировать. Если другой пользователь хочет взять
зарезервированную книгу(или которую уже кто-то читает - надо ему
про это сказать)
"""


class Book:
    """
    Класс книга с экземплярами класса, согласно условиям задачи
    """

    def __init__(self, title, author, count_pages, book_isbn):
        """
        :param title: название книги
        :param author: автор книги
        :param count_pages: количество страниц
        :param book_isbn: код ISBN
        """
        self.book_title = title
        self.book_author = author
        self.book_count_pages = count_pages
        self.book_isbn = book_isbn
        self.reserved = False
        self.taken = False
        self.free = True

    def book_reserved(self):
        """
        Условие зарезервированной книги
        :return: булевое значение
        """
        self.reserved = True
        self.free = False

    def book_taken(self):
        """
        Условие взятой книги
        :return: булевое значение
        """
        self.taken = True
        self.free = False

    def book_free(self):
        """
        Условие свободной книги
        :return: булевое значение
        """
        self.reserved = False
        self.taken = False
        self.free = True


class Human:
    """
    Класс пользователя для создания условий статуса книг и
    информацией о пользователях
    """

    def __init__(self, name):
        """
        Стартовые значения для подставления данных
        :param name: имя
        """
        self.name = name
        self.taken_book = None
        self.reserved_book = None

    def take_book(self, any_book):
        """
        Функция взятия книги
        :param any_book: любая книга
        :return: состояние книги (взята или зарезервирована)
        """
        if any_book.free:
            self.taken_book = any_book
            any_book.book_taken()
            return f'Книга {any_book.book_title} взята человеком ' \
                   f'по имени : {self.name}'
        else:
            return 'Книга взята или зарезервирована другим пользователем'

    def res_book(self, any_book):
        """
        Функция резервирования книги
        :param any_book: любая книга
        :return: состояние книги (зарезервирована, взята или зарезервирована)
        """
        if any_book.free:
            self.reserved_book = any_book
            any_book.book_reserved()
            return f'Книга {any_book.book_title} зарезервирована' \
                   f' человеком по имени {self.name}'
        else:
            return f'Книга {any_book.book_title} взята или зарезервирована' \
                   ' другим человеком'

    def free_book(self, any_book):
        """
        Свободная книга
        :param any_book: любая книга
        :return: состояние книги (возвращена)
        """
        any_book.book_free()
        return f'Книга {any_book.book_title} возвращена человеком по ' \
               f'имени: {self.name}'


# Книги, согласно условиям задачи
book_1 = Book(title='Dogs', author='Author1', count_pages=110, book_isbn=12112)
book_2 = Book(title='Cats', author='Author2', count_pages=234, book_isbn=1236)
book_3 = Book(title='Humans', author='Author3', count_pages=345, book_isbn=234)
book_4 = Book(title='Space', author='Author4', count_pages=345, book_isbn=453)
book_5 = Book(title='Moon', author='Author5', count_pages=120, book_isbn=432)

# Пользователи(имена)
human_1 = Human(name='Anton')
human_2 = Human(name='Don')
human_3 = Human(name='Jak')


def main():
    """
    Функция сборщик
    :return: принтует состояние книг
    """
    print(human_2.res_book(book_2))
    print(human_2.take_book(book_2))


if __name__ == "__main__":
    main()
