# Task 2
"""
Создайте класс инвестиция. Который содержит необходимые поля и методы,
например сумма инвестиция и его срок. Пользователь делает инвестиция в размере
N рублей сроком на R лет под 10% годовых (инвестиция с возможностью
ежемесячной капитализации - это означает, что проценты прибавляются к сумме
инвестиции ежемесячно).Написать класс Bank, метод deposit принимает аргументы
N и R, и возвращает сумму, которая будет на счету пользователя
"""


class InvestmentForPeople:
    """
    Основной класс, содержащий сумму и срок вклада
    """

    def __init__(self, summ, term):
        """
        Экземпляры класс InvestmentForPeople
        :param summ: сумма
        :param term: срок
        """
        self.summ = summ
        self.term = term


class Bank:
    """
    Класс, где производятся рассчеты
    """

    @staticmethod
    def dep(n: str, r: str) -> float:
        """
        Функция, производящая основные рассчеты по банковской формуле
        :param n: сумма вклада
        :param r: срок вклада
        :return: округленну сумму на счету вкладчика к концу срока вклада
        """
        # использовал фомулу (FV = PV * (1+R)n)
        count = int(n) * (1 + 0.1 / 12) ** (12 * int(r))
        return round(count, 3)

    @staticmethod
    def valid():
        """
        Функция по валидации ввода и записи переменных
        :return: записываем переменные в функцию рассчета класса Bank
        """
        while True:
            x = input('Сумма вклада: \n')
            if x.isdigit():
                break

        while True:
            y = input('Срок вклада (лет): \n')
            if y.isdigit():
                break
        invest = InvestmentForPeople(x, y)
        return Bank.dep(invest.summ, invest.term)


def main():
    """
    Функция, принимающая входные пораметры и производящие рассчеты
    :return: сумму на счете к окончанию срока депозита
    """
    return Bank.valid()


if __name__ == "__main__":
    print(main())
else:
    print("You are not running the file directly")
