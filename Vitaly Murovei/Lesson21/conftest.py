import pytest
from selenium.webdriver import Chrome


@pytest.fixture(scope="session")
def chrome_driver():
    """
    Фикстура инициализации драйвера.
    :return: Инициализированный драйвер Chrome.
    """
    driver = Chrome(executable_path="C:/Users/acer/Downloads/chromedriver.exe")
    yield driver
    driver.quit()


@pytest.fixture(scope="session")
def form_authentication(chrome_driver):
    """
    Фикстура открытия формы аутентификации.
    """
    chrome_driver.get("http://the-internet.herokuapp.com/login")
    return chrome_driver


@pytest.fixture()
def checkboxes_form(chrome_driver):
    """
    Фикстура открытия  чекбоксов.
    """
    chrome_driver.get("http://the-internet.herokuapp.com/checkboxes")
    return chrome_driver


@pytest.fixture()
def multiple_windows(chrome_driver):
    """
    Фикстура открытия Multiple Windows.
    """
    chrome_driver.get("http://the-internet.herokuapp.com/windows")
    return chrome_driver


@pytest.fixture()
def add_remove(chrome_driver):
    """
    Фикстура открытия Add/Remove.
    """
    chrome_driver.get("http://the-internet.herokuapp.com/add_remove_elements/")
    return chrome_driver


@pytest.fixture(params=[("tomsmith", "tomsmith", "Your password is invalid!"),
                        ("vitaly", "Password!", "Your username is"
                                                "invalid"),
                        ("tomsmith", "SuperSecretPassword!",
                         "You logged into a secure area!")],
                ids=["invalid pass", "invalid username", "successful"])
def login(form_authentication, request) -> tuple:
    """
    Фикстура логирования.
    Находит поля и вводит соответствующие данные.
    Находит кнопку Login и кликает ее.
    Находит сообщение о состоянии логирования.
    :return: тапл (полученный результат, ожидаемый результат)
    """
    params = request.param
    form_authentication.implicitly_wait(15)
    login_field = form_authentication.find_element_by_xpath(
        "//*[@id='username']")
    login_field.send_keys(params[0])
    pas_field = form_authentication.find_element_by_xpath(
        "//*[@id='password']")
    pas_field.send_keys(params[1])
    login_button = form_authentication.find_element_by_css_selector(
        "button.radius")
    login_button.click()
    result_authentication = form_authentication.find_element_by_xpath(
        "//*[@id='flash']")
    result = params[2]
    return result_authentication, result


@pytest.fixture()
def checkboxes(checkboxes_form) -> list:
    """
    Фикстура выбирающая чекбоксы.
    :return: состояния чекбоксов.
    """
    checkboxes = checkboxes_form.find_element_by_xpath(
        "//input[@type='checkbox']")
    result_check = []
    for check in checkboxes:
        check.click()
        result_check.append(check.is_selected())
    return result_check


@pytest.fixture()
def windows(multiple_windows) -> str:
    """
    Фикстура для открытия нового окна.
    Находит ссылку и кликает на нее.
    :return: строка с title.
    """
    new_window = multiple_windows.find_element_by_xpath(
        "//*[@id='content']/div/a")
    new_window.click()
    multiple_windows.close()
    multiple_windows.switch_to.window(multiple_windows.window_handles[0])
    text_window = multiple_windows.title
    return text_window


@pytest.fixture()
def add_elements(add_remove) -> list:
    """
    Фикстура, добавляющая элементы. Находит кнопку добавления и проверяет
    созданные элементы.
    :return: созданные элементы.
    """
    add_button = add_remove.find_element_by_xpath(
        "//*[@id='content']/div/button")
    add_button.click()
    delete_button = add_remove.find_element_by_xpath(
        "//*[@id='elements']/button")
    return delete_button


@pytest.fixture()
def delete_elements(add_remove, add_elements) -> list:
    """
    Фикстура, удаляющая элементы. Кликает на созданные элементы
    add_element, находит созданные элементы.
    :param add_remove: фикстура входная add_remove
    :param add_elements: список созданных элементов.
    :return: список созданных элементов.
    """
    for button in add_elements:
        button.click()
    delete_button = add_remove.find_element_by_xpath(
        "//*[@id='elements']/button")
    return delete_button
