def test_login(login):
    """
    Фикстура, сравнивающая тексты в строках результата.
    """
    assert login[1] in login[0].text


def test_checkboxes(checkboxes):
    """
    Фикстура, сравнивающая полученные значения с ожидаемыми.
    """
    assert checkboxes == [True, False]


def test_windows(windows):
    """
    Фикстура, сравнивающая полученный title с ожидаемым.
    """
    assert windows == "New Window"


def test_add_elements(add_elements):
    """
    Фикстура, проверяющая наличие созданного элемента.
    """
    assert bool(add_elements) is True


def test_delete_elements(delete_elements):
    """
    Фикстура, проверяющая наличие удаленных элементов.
    """
    assert bool(delete_elements) is False
