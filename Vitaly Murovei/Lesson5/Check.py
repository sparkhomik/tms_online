# Task 5 (check)
x = [1, 1, 1]
z = (all(y == x[0] for y in x))
a = [1, 2, 1]
b = (all(o == a[0] for o in a))
str_1 = ['a', 'a', 'a']
str_2 = (all(str_3 == str_1[0] for str_3 in str_1))
st_1 = []
st_2 = (all(st_3 == st_1[0] for st_3 in st_1))
print(x, "==", z)
print(a, "==", b)
print(str_1, "==", str_2)
print(st_1, "==", st_2)
