import string
# Task 2 (like)
while True:
    names = input("Введите имена через запятую и пробел")
    list_names = names.split()
    if all(a in string.digits for a in names):
        print("Необходимо вводить буквы латиницей!")
    elif len(list_names) == 0:
        print("Пустое поле ввода! Введите имена через запятую и пробел")
    else:
        if len(list_names) == 1:
            list_names = ''.join(list_names)
            print(f"{list_names}like this")
        elif len(list_names) == 2:
            print(f"{list_names[0]}and{list_names[1]}like this")
        elif len(list_names) == 3:
            print(f"{list_names[0]},{list_names[1]}and{list_names[2]}"
                  f"like this")
        elif len(list_names) > 3:
            other = int(len(list_names)) - 2
            print(f"{list_names[0]},{list_names[1]}and{other}others like this")
        else:
            print("No one likes this")
