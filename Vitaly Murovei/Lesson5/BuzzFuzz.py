# Task 3 (BuzzFuzz)
n = 1
while n < 101:
    if n % 3 == 0 and n % 5 == 0:
        print("FuzzBuzz")
    elif n % 3 == 0:
        print("Fuzz")
    elif n % 5 == 0:
        print("Buzz")
    else:
        print(n)
    n += 1
