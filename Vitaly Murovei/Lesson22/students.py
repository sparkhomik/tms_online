"""
Task 3
работа с json файлом
Разработайте поиск учащихся в одном классе, посещающих одну секцию.
Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)
Файл: students.json
"""

import json


def get_file_students() -> dict:
    """
    Функция, получающая содержимое файла students.json.
    :return: содержимое файла в словаре.
    """
    file_students = open("students.json", "r")
    content = json.loads(file_students.read())
    file_students.close()
    return content


def class_filter(content: dict, class_arg: str) -> list:
    """
    Функция, фильтрирующая по классу.
    :param content: содержимое файла в словаре.
    :param class_arg: класс.
    :return: список словарей.
    """
    students_list = []
    for x in content:
        if x["Class"] == class_arg:
            students_list.append(x)
    return students_list


def filter_section(content: dict, club: str) -> list:
    """
    Функция, фильтрирующая по секциям.
    :param club: секция.
    :param content: содержимое файла в словаре.
    :return: список словарей.
    """
    students_list = []
    for x in content:
        if x["Club"] == club:
            students_list.append(x)
    return students_list


def filter_by_gender(content: dict, gender: str) -> list:
    """
    Функция, фильтрирующая по гендеру.
    :param content: содержание файла в словаре.
    :param gender: половая принадлежность.
    :return: список словарей.
    """
    students_list = []
    for x in content:
        if x["Gender"] == gender:
            students_list.append(x)
    return students_list


def find_by_name(content: dict, name: str) -> list:
    """
    Функция, осуществляющая поиск по имени.
    :param content: содержание файла в словаре.
    :param name: имя.
    :return: список словарей.
    """
    students_list = []
    for x in content:
        if name in x["Name"]:
            students_list.append(x)
    return students_list


def list_json(list_arg: list) -> None:
    """
    Функция, выводящее содержимое списка.
    :param list_arg: список.
    :return: None.
    """
    for a in list_arg:
        print(a)


if __name__ == "__main__":
    content_students = get_file_students()

    # Ученики, отфильтрованные по классу.
    print("Фильтр по классу:")
    list_json(class_filter(content_students, "1a"))
    print("++++++++++")

    # Ученики, отфильтрованные по полу.
    print("Фильтр по полу:")
    list_json(filter_by_gender(content_students, "W"))
    print("++++++++++")

    # Ученики, отфильтрованные по секции.
    print("Фильтр по секции:")
    list_json(filter_section(content_students, "Football"))
    print("++++++++++")

    # Ученики, отфильтрованные по имени.
    print("Фильтр по имени:")
    list_json(find_by_name(content_students, "Yuki"))
