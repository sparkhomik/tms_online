"""
Task 2
работа с xml файлом
Разработайте поиск книги в библиотеке по ее автору(часть имени)/цене/заголовку/
описанию.
Файл: library.xml
"""
from xml.etree import ElementTree


def get_books():
    """
    Функция, получающая содерживмое файла library.xml.
    :return: содержимое файла, понятное Python.
    """
    xml_file = open("library.xml", "r")
    content = ElementTree.fromstring(xml_file.read())
    xml_file.close()
    return content


def search_author(root_arg, name: str) -> list:
    """
    Функция, осуществляющая поиск по имени (части имени) автора.
    :param root_arg: файл.
    :param name: часть имени/имя.
    :return: список.
    """
    books = []
    for y in root_arg:
        tag_author = y.find("author")
        if name in tag_author.text:
            books.append(y.attrib["id"])
    return books


def search_price(root_arg, price) -> list:
    """
    Функция, осуществляющая поиск книги по цене.
    :param root_arg: файл.
    :param price: цена.
    :return: список.
    """
    books = []
    for y in root_arg:
        tag_price = y.find("price")
        if str(price) == tag_price.text:
            books.append(y.attrib["id"])
    return books


def search_title(root_arg, title: str) -> list:
    """
    Функция, осуществляющая поиск по названию.
    :param root_arg: файл.
    :param title: название книги.
    :return: список.
    """
    books = []
    for y in root_arg:
        tag_title = y.find("title")
        if title in tag_title.text:
            books.append(y.attrib["id"])
    return books


def search_description(root_arg, description: str) -> list:
    """
    Функция, осуществляющая поиск по описанию.
    :param root_arg: файл.
    :param description: описание книги.
    :return: список.
    """
    books = []
    for y in root_arg:
        tag_description = y.find("description")
        if description in tag_description.text:
            books.append(y.attrib["id"])
    return books


def list_of_library(list_arg: list) -> None:
    """
    Функция, осуществляющая вывод списка.
    :param list_arg: список.
    :return: None.
    """
    for a in list_arg:
        print(a)


if __name__ == "__main__":
    root = get_books()

    # Вывод при поиске по имени
    print("Поиск по автору:")
    list_of_library(search_author(root, "Gam"))
    print("+++++++")

    # Вывод при поиске по цене
    print("Поиск по цене:")
    list_of_library(search_price(root, "5.95"))
    print("+++++++")

    # Вывод при поиске по названию
    print("Поиск по названию книги:")
    list_of_library(search_title(root, "Rain"))
    print("+++++++")

    # Вывод при поиске по описанию
    print("Поиск по описанию:")
    list_of_library(search_description(root, "look"))
