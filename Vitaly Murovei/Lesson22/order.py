"""
Task 4
работа с yaml файлом
* Напечатайте номер заказа
* Напечатайте адрес отправки
* Напечатайте описание посылки, ее стоимость и кол-во
* Сконвертируйте yaml файл в json
* Создайте свой yaml файл
Файл: order.yaml
"""
import yaml
import json
from request import get_authors


def get_order() -> dict:
    """
    Функция, получающая содержимое order.yaml.
    :return: словарь.
    """
    file_yaml = open("order.yaml", "r")
    content_py = yaml.safe_load(file_yaml)
    file_yaml.close()
    return content_py


def get_number(content_py: dict) -> str:
    """
    Функция, получающая номер заказа.
    :param content_py: содержимое из файла order.yaml.
    :return: строка.
    """
    return content_py["invoice"]


def get_address(content_py: dict) -> dict:
    """
    Функция, получающая адрес.
    :param content_py: содержимое из файла order.yaml.
    :return: словарь.
    """
    return content_py["bill-to"]["address"]


def get_contains(content_py: dict) -> tuple:
    """
    Функция, получающая список содержимого.
    :param content_py: содержимое из файла order.yaml.
    :return: тапл.
    """
    quantity = 0
    products = []
    for product in content_py["product"]:
        quantity += product["quantity"]
        products.append(product["sku"] + " " + product["description"])
    return products, quantity


def get_total(content_py: dict) -> float:
    """
    Функция, получающая стоимость.
    :param content_py: содержимое из файла order.yaml.
    :return: Флоат.
    """
    return content_py["total"]


def yaml_json(content_py: dict) -> None:
    """
    Функция, конвертирующая yaml в json.
    :param content_py: содержимое из файла order.yaml.
    :return: None.
    """
    file_json = open("order.json", "w+")
    content_py["date"] = str(content_py["date"])
    json.dump(content_py, file_json)
    file_json.close()


def create_yaml(content_py: dict) -> None:
    """
    Функция, создающая файл yaml и записывающая в него content_py.
    :param content_py: содержимое из файла order.yaml.
    :return: None.
    """
    file = open("test.yaml", "w+")
    yaml.dump(content_py, file)
    file.close()


if __name__ == "__main__":
    content_all = get_order()

    # Номер заказа
    print("Номер заказа:")
    print(get_number(content_all))
    print("++++++++++")

    # Адрес
    print("Адрес:")
    print(get_address(content_all))
    print("++++++++++")

    # Список содержимого
    print("Содержимое:")
    print(get_contains(content_all))
    print("++++++++++")

    # Количество
    print("Количество")
    print(get_contains(content_all)[-1])
    print("++++++++++")

    # Стоимость
    print("Стоимость:")
    print(get_total(content_all))

    # yaml in json
    yaml_json(content_all)

    # Создание и запись.
    authors_link = "https://fakerestapi.azurewebsites.net/api/v1/Authors"
    yaml_content = get_authors(authors_link)
    create_yaml(yaml_content)
