"""
Task 1
Используя сервис https://fakerestapi.azurewebsites.net/index.html и requests
библиотеку, написать запросы:
GET:
Получение списка авторов
Получение конкретного автора по его id
POST:
Добавить новую книгу
Добавить нового пользователя
PUT:
Обновить данные для книги под номером 10
DELETE:
Удалить пользователя под номером 4
"""
import json
import requests


def get_authors(arg_address: str) -> dict:
    """
    Функция get запроса для получения всех авторов.
    :param arg_address: адрес запроса.
    :return: dict из json
    """
    res = requests.get(arg_address)
    return json.loads(res.content)


def get_authors_name(arg_authors: dict) -> list:
    """
    Функция, получающая список из авторов.
    :param arg_authors: авторы в словаре.
    :return: список из авторов.
    """
    list_author = []
    for author in arg_authors:
        list_author.append(author["firstName"] + " " + author["lastName"])
    return list_author


def get_id(arg_authors: dict, arg_id: int) -> list:
    """
    Функция, получающая автора по id.
    :param arg_authors: словарь из авторов.
    :param arg_id: id авторов.
    :return: список имени и фамилии авторов.
    """
    for author in arg_authors:
        if arg_id == author["id"]:
            return [author["firstName"], author["lastName"]]


def post_book(address: str, arg_id: int, title: str, description: str,
              page_count: int, excerpt: str, publish_date: str):
    """
    Функция для добавления книги.
    :param address: адресс.
    :param arg_id: id книги.
    :param title: Наименование произведения.
    :param description: краткое описание.
    :param page_count: количество страниц.
    :param excerpt: отрывок из книги.
    :param publish_date: дата публикации.
    :return: ответ сервера.
    """
    response = requests.post(address, json={"id": arg_id, "title": title,
                                            "description": description,
                                            "pageCount": page_count, "excerpt":
                                                excerpt, "publishDate":
                                                publish_date})
    return response


def post_user(address: str, arg_id: int, user_name: str, password: str):
    """
    Функция, добавляющая пользователя при помощи post.
    :param address: адрес.
    :param arg_id: id пользователя.
    :param user_name: имя пользователя.
    :param password: пароль пользователя.
    :return: ответ сервера.
    """
    response = requests.post(address, json={"id": arg_id,
                                            "userName": user_name,
                                            "password": password})
    return response


def put_book(address: str, arg_id: int, title: str, description: str,
             page_count: int, excerpt: str, publish_date: str):
    """
    Функция обновления данных по книге.
    :param address: адресс.
    :param arg_id: id книги.
    :param title: Наименование произведения.
    :param description: краткое описание.
    :param page_count: количество страниц.
    :param excerpt: отрывок из книги.
    :param publish_date: дата бубликации.
    :return: ответ сервера.
    """
    real_address = address + "/" + str(arg_id)
    response = requests.put(real_address, json={"title": title,
                                                "description": description,
                                                "pageCount": page_count,
                                                "excerpt": excerpt,
                                                "publishDate": publish_date})
    return response


def delete(address: str, arg_id: int):
    """
    Функция, удаляющая пользователя.
    :param address: адресс.
    :param arg_id: id пользователя.
    :return: ответ сервера.
    """
    real_address = address + "/" + str(arg_id)
    response = requests.delete(real_address)
    return response


if __name__ == "__main__":
    # ссылки запросов
    users_link = "https://fakerestapi.azurewebsites.net/api/v1/Users"
    authors_link = "https://fakerestapi.azurewebsites.net/api/v1/Authors"
    books_link = "https://fakerestapi.azurewebsites.net/api/v1/Books"

    # авторы
    authors = get_authors(authors_link)
    for y in get_authors_name(authors):
        print(y)

    # id (вывод по id)
    print(*(get_id(authors, 456)))

    # Ответ сервера на Post
    print(post_book(books_link, 3456, "test", "test", 56, "test", "2021-10-08T"
                                                                  "13:33:34."
                                                                  "799Z"))
    # Ответ сервера при добавлени пользователя
    print(post_user(users_link, 34, "test", "123456"))

    # Ответ сервера при обновлении книги
    print(put_book(books_link, 35, "test_1", "test_1", 3, "test_1",
                   "2021-10-08T14:19:34.994Z"))

    # Ответ сервера при удалении пользователя
    print(delete(users_link, 4))
