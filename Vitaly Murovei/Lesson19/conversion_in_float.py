"""
2ая – переводит переданное число в тип float, если передана строка вызывает
 пользовательскую ошибку InputTypeError (создать ее самому)
"""


class InputTypeError(Exception):
    """
    Класс ошибки. Пишет сообщение в методе о том, что аргумент не int.
    """

    def __init__(self, arg):
        """
        Метод класса, который собирает строку ошибку.
        :param arg: аргумент (любой тип).
        """
        self.message = f"InputTypeError, argument {arg} is not a int."
        super().__init__(self.message)


def conversion_in_float(arg) -> float:
    """
    Функция вне класса, для перевода int в float и, если тип не int -
    вызов ошибки.
    :param arg: вводимый аргумент
    :return: аргумент в float
    """
    if type(arg) != int:
        raise InputTypeError(arg)
    else:
        return float(arg)


if __name__ == "__main__":
    print(conversion_in_float(12))
    print(conversion_in_float("float"))
