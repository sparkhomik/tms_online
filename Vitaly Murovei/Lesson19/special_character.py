"""
3яя – проверяет, является ли переданный аргумент спец символом
"""


def special_character(arg) -> bool:
    """
    Функция, проверяющая наличие специального символа
    :param arg: вводимый аргумент
    :return: bool (True или False)
    """
    return any([str(arg) in "!@#$%^&*(){}<>,.:`/[]+-_"])


if __name__ == "__main__":
    print(special_character(3))
    print(special_character("/"))
