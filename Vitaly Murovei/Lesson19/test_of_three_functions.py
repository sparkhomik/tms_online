"""
Для каждой функции создать параметризированный тест. В качестве параметров
тест принимает значение, которое должна обработать функция и ожидаемый
результат ее выполнения.
Каждый тест должен быть вызван по 3 раза
Для 2-ой функции написать отдельный тест, который будет ожидать ошибку
InputTypeError, и если эта ошибка не произошла, падать!
"""

import pytest
from lower_bool import lower_word
from conversion_in_float import conversion_in_float
from conversion_in_float import InputTypeError
from special_character import special_character
import time


@pytest.mark.usefixtures("time_start_stop_fixture", "file_name_fixture")
class TestClass:
    """
    Класс для объенинения тестов
    """

    @pytest.mark.parametrize("word, result", [("WORD", False), ("word", True),
                                              ("Word", False)])
    def test_lower_bool(self, word: str, result: bool):
        """
        Метод проверки lower_bool. Сравнивает проверяемое слово с ожидаемым
        результатом.
        :param word:слово, которое проверяем.
        :param result:ожидаемый результат
        """
        time.sleep(1)
        assert lower_word(word) == result

    @pytest.mark.parametrize("arg, result", [(-12, -12.0), (25, 25.0),
                                             (3, 3.0)])
    def test_conversation_in_float(self, arg, result: float):
        """
        Метод, проверяющий работу conversion_in_float. Сравнивает аргумент с
        результатом.
        :param arg: аргумент
        :param result: ожидаемый результат.
        """
        time.sleep(1)
        assert conversion_in_float(arg) == result

    @pytest.mark.parametrize("arg", ["test", "2", "-3"])
    def test_conversion_in_float_error(self, arg):
        """
        Метод проверки вызываемой ошибки, если тип аргумента не int.
        :param arg:аргумент
        """
        time.sleep(1)
        with pytest.raises(InputTypeError):
            conversion_in_float(arg)

    @pytest.mark.parametrize("arg, result", [(7, False), ("/", True),
                                             ("Word", False)])
    def test_special_character(self, arg, result):
        """
        Метод проверки special_character. Проверка спецсимвол ли аргумент.
        :param arg: аргумент.
        :param result: результат проверки.
        """
        time.sleep(1)
        assert special_character(arg) == result
