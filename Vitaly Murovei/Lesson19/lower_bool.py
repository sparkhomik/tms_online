"""
1ая – проверяет что все буквы в переданном слове в нижнем регистре и
возвращает True либо False
"""


def lower_word(word: str) -> bool:
    """
    Функция, проверяющая регистр букв, переданных в слове (нижний регистр).
    :param word: переданное слово
    :return: Bool (True или False)
    """
    return word.islower()


if __name__ == "__main__":
    print(lower_word("Day"))
    print(lower_word("day"))
    print(lower_word("DAY"))
