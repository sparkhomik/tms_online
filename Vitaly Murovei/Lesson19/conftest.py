"""
Написать фикстуру, которая будет выводить время начала запуска всех тестов и
когда все тесты отработали. По итогу она должна отработать один раз!
Написать фикстуру, которая будут выводить имя файла, из которого запущен тест.
Если тесты находятся в одном файле, для них фикстура должна отработать
один раз!
Написать фикстуру, которая для каждого теста будет выводить следующую
информацию:
А) Информацию о фикстурах, которые используются в этом тесте
Б) Информацию о том, сколько времени прошло между запуском сессии и
запуском текущего теста.
"""

import pytest
import time
import sys


@pytest.fixture(scope="session")
def time_start_stop_fixture():
    """
    Фикстура вывода времени сетапа и тирдауна.
    :return:ЧЧ:ММ:СС
    """
    start_session_time = int(time.strftime("%S"))
    print("\n----START TIME:", time.strftime("%X"), "----")
    yield start_session_time
    print("\n----STOP TIME:", time.strftime("%X"), "----")


@pytest.fixture(scope="session")
def file_name_fixture():
    """
    Фикстура вывода имени файла с запуском теста на этапе сетап.
    :return: Имена файлов.
    """
    print("\n----TEST FILE NAME:", sys.argv[-1].split("/")[-1], "----")


@pytest.fixture(scope="function", autouse=True)
def info_fixture(time_start_stop_fixture):
    """
    Фикстура, выводящая имена используемых фикстур в тексте, время между
    началом сессии и запуском теста.
    :return:имена фикстур, время между началом сессии и запуском теста.
    """

    print("\n----START FIXTURE:", time_start_stop_fixture(), "----")
    yield
    stop_test_time = int(time.strftime("%S"))
    print("\n----TIME:", stop_test_time - time_start_stop_fixture,
          "sec ----")
    print("------------------------------------------------")
