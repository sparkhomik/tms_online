name = {0: 'zero', 1: 'one', 2: 'two',
        3: 'three', 4: 'four', 5: 'five',
        6: 'six', 7: 'seven', 8: 'eight',
        9: 'nine', 10: 'ten', 11: 'eleven',
        12: 'twelve', 13: 'thirteen', 14: 'fourteen',
        15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
        18: 'eighteen', 19: 'nineteen'}


def decorator(func):
    """
    Функция декоратор, собирающая список в нужной последовательности
    :param func: функция
    :return: результат wrapper
    """

    def wrapper(*args):
        """
        Функция-обертка
        :param args: принимает список неименованных аргументов
        :return: Возвращает сортированный список
        """
        a = {name[a]: a for a in args}
        a = [a[1] for a in sorted(a.items())]
        return func(a)

    return wrapper


@decorator
def main(*args):
    """
    Функция под декоратором, возвращающая список
    :param args: неименованные аргументы
    :return: список
    """
    return args


if __name__ == "__main__":
    print(*main(5, 3, 12, 4, 18, 0))
