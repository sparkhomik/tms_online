# Task 1 Напишите декоратор, который проверял бы тип параметров
# функции, конвертировал их если надо и складывал:
from functools import reduce


def typed(type):
    """
    Функция, определяющая тип данных
    :param type: тип данных
    :return:функцию add
    """

    def add(func):
        """
        Функция под оберткой
        :param func:под оберткой
        :return:wrapper
        """

        def wrapper(*args):
            """
            Распаковка неименованных аркументов
            :param args:аргументы
            :return:функция распаковки
            """
            new = list(map(type, args))
            return func(*new)

        return wrapper

    return add


def plus(a, b):
    """
    Функция сложения аргументов
    :param a: аргумент
    :param b: аргумент
    :return: сумма аргументов
    """
    return a + b


@typed(type=str)
def func_1(*args):
    """
    Функция с ипользование reduce, которая применяет все к элементам
    последовательности
    :param аргументы
    :return: сумму аргументов в списке
    """
    gen = reduce(plus, args)
    return gen


if __name__ == "__main__":
    print(func_1('3', 23, 'AAA', 1.0555))
