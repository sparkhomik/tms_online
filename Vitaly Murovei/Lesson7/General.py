"""
Общий модуль, который позволяет выбирать игру и считает неверные попытки,
после чего рандомно запускает игру.
"""
from Cipher import Cip
from Tic_tac_toe import Tic
from random import randint


def gen_all() -> str:
    # Выбор опции
    print('1 - Крестики-нолики\n2 - Шифр Цезаря\n3 - Выход')
    action = input('Введите желаемый номер: ')
    count = 3
    # Задаем желаемые варианты ввода и отнимаем попытки при неправильном вводе
    # и запускаем рандомно программу
    while action not in ('1', '2', '3'):
        print('Выберете цифры от 1 до 3.')
        action = input('Введите желаемый номер: ')
        count -= 1
        if count == 0:
            action = str(randint(1, 3))
    return action


def start():
    user = gen_all()
    while True:
        if user == '1':
            print('Крестики-нолики')
            Tic.main()
        elif user == '2':
            print('Шифр Цезаря')
            Cip.func()

        elif user == '3':
            break


if __name__ == "__main__":
    start()
