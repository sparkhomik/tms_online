
"""
Модуль игры Шифр Цезаря, который реализует шифровку и дешифровку слов на
английском с пробелами, но без учета регистра.
"""

alphabet = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'


def func():
    """
    Функция шифр Цезаря (смещает заданную строку по заданному ключу,
    есть возможность выбора опций кодирования, декодирования и выхода
    из функции)
    :return: возвращает зашифрованную строку
    """
    while True:
        # Выбор операции
        print('1 - Encode\n2 - Decode\n3 - Exit')
        # Введенное строку в нижний регистр
        men = input('>>> ').lower()
        if men == '3':
            break
        elif not (men == '1' or men == '2'):
            continue
        output = ''
        # Ввод строки и шага
        message = input('Enter string: ').lower()
        key = int(input('Enter key: '))
        if men == '2':
            key *= -1
            # Проходимся по строке и сдвигаем на шаг, заданный заранее
        for letter in message:
            if letter in alphabet:
                t = alphabet.find(letter)
                new_key = (t + key) % len(alphabet)
                output += alphabet[new_key]
            else:
                output += letter
        print('Result: ' + output)
    return func()


if __name__ == '__main__':
    func()
