"""
Этот модуль игры Крестики-нолики. Данная игра выполнена посредством выбора
ячейки по номеру. Есть возможность выбора первого хода, начала игры, вывод
ошибок при неправильном вводе.
General
"""

X = "X"
Zero = "O"
EMPTY = " "
TIE = "TIE"
NUM_SQUARES = 9


def display_instruct():
    """
    Функция ознакамливает с разметкой поля игры и спрашивает о начале
    :return: игровое поле
    """
    print(
        """
    Welcome to the game! Today you are playing against the computer! Remember
                        data entry field!

                    0 | 1 | 2
                    ---------
                    3 | 4 | 5
                    ---------
                    6 | 7 | 8

                  Let's start? \n
      """)


def ask_yes_no(question):
    """
    Функция-вопрос для отсеивания неверных вариантов ввода
    :param question: строки "y", "n".
    :return: результат ввода в зависомости от символа
    """
    """Задаем вопрос да или нет."""
    response = None
    while response not in ("y", "n"):
        response = input(question).lower()
    return response


def ask_number(que, low, high):
    """
    Функция отсеивающая введенные данные от 0 до 8 включительно
    :param que: вопрос (ввести номер в диапазоне)
    :param low: минимальний
    :param high: максимальный
    :return:
    """
    """Спрашиваем номер в диапазоне."""
    response = None
    while response not in range(low, high):
        response = int(input(que))
    return response


def pieces():
    """
    Функция, опередляющая очередность того, кто идет первым(решил добавить)
    :return: очередность ходов (первый человек и второй компьютер или наоборот)
    """
    """Определяем кто идет первым."""
    go_first = ask_yes_no("Do you want to start first? (y/n): ")
    if go_first == "y":
        print("\nWhen do you take the first step?")
        human = X
        computer = Zero
    else:
        print("\nI'll go first!")
        computer = X
        human = Zero
    return computer, human


def new_board():
    """
    Функция, отображающая доску с ходами
    :return: визуализация доски
    """
    """Создаем новую игровую доску."""
    board = []
    for square in range(NUM_SQUARES):
        board.append(EMPTY)
    return board


def display_board(board):
    """
    Визуализация доски
    :param board: пространства ввода
    :return: доску
    """
    """Отображаем игровое поле на доске."""
    print("\n\t", board[0], "|", board[1], "|", board[2])
    print("\t", "---------")
    print("\t", board[3], "|", board[4], "|", board[5])
    print("\t", "---------")
    print("\t", board[6], "|", board[7], "|", board[8])


def legal_moves(board):
    """
    Функция, собирающая ходы для определения свободных ячеек
    :param board: пустые ячейки
    :return: возможные ходы
    """
    """Список ходов."""
    moves = []
    for square in range(NUM_SQUARES):
        if board[square] == EMPTY:
            moves.append(square)
    return moves


def winner(board):
    """
    Функция определяющая победителя по списку ходов
    :param board: определяем непустые пространства
    :return: победитель
    """
    """Определяем победителя игры."""
    ways_to_win = ((0, 1, 2),
                   (3, 4, 5),
                   (6, 7, 8),
                   (0, 3, 6),
                   (1, 4, 7),
                   (2, 5, 8),
                   (0, 4, 8),
                   (2, 4, 6))

    for row in ways_to_win:
        if board[row[0]] == board[row[1]] == board[row[2]] != EMPTY:
            winner_1 = board[row[0]]
            return winner_1

    if EMPTY not in board:
        return TIE

    return None


def human_move(board, _):
    """
    Функция, отмечающая ходы человека
    :param board: пространства для ввода
    :param _ : значение не будет использовано
    :return: ход
    """
    """Ход человека."""
    legal = legal_moves(board)
    move = None
    while move not in legal:
        move = ask_number("Where will you put the cross? (0 - 8):", 0,
                          NUM_SQUARES)
        if move not in legal:
            print(
                "\nThis square is occupied - you need to choose"
                " another one!\n")
    print("Good...")
    return move


def computer_move(board, computer, human):
    """
    Функция ходов компьютера
    :param board: пространства ввода
    :param computer: возможные ходы компьютера
    :param human: ход человека
    :return: ход компьютера
    """
    """Генерируем ход компьютера."""
    # сделаем копию для работы, так как функция будет изменять список
    board = board[:]
    # лучшие позиции
    BEST_MOVES = (4, 0, 2, 6, 8, 1, 3, 5, 7)

    print("I will choose a number", end=" ")

    # если компьютер может выиграть, запускаем
    for move in legal_moves(board):
        board[move] = computer
        if winner(board) == computer:
            print(move)
            return move
        # закончим проверку и отменим
        board[move] = EMPTY

    # если человек может победить- заблокировать ход
    for move in legal_moves(board):
        board[move] = human
        if winner(board) == human:
            print(move)
            return move
        # закончили проверку и отменили
        board[move] = EMPTY

    # если никто не выиграл - следующий ход
    for move in BEST_MOVES:
        if move in legal_moves(board):
            print(move)
            return move


def next_turn(turn):
    """
    Функция переключатель нолика и крестика
    :param turn: вводится в объединяющей функции
    :return: символ ходов
    """
    """Переключатель."""
    if turn == X:
        return Zero
    else:
        return X


def congrat_winner(the_winner, computer, human):
    """
    Функция, выводящая результат
    :param the_winner: из функции winner (комбинация)
    :param computer: комбинация ходов компьютера
    :param human: комбинация ходов человека
    :return: результат сравнения комбинаций
    """
    """Поздравление победителя."""
    if the_winner != TIE:
        print(the_winner, "Won!\n")
    else:
        print("It's a draw!\n")

    if the_winner == computer:
        print("Won the computer")

    elif the_winner == human:
        print("Man defeated")

    elif the_winner == TIE:
        print("Draw")


def main():
    """
    Функция, запускающая остальные функции поочередно
    :return: доску с ходами и победиделя
    """
    display_instruct()
    computer, human = pieces()
    turn = X
    board = new_board()
    display_board(board)

    while not winner(board):
        if turn == human:
            move = human_move(board, human)
            board[move] = human
        else:
            move = computer_move(board, computer, human)
            board[move] = computer
        display_board(board)
        turn = next_turn(turn)

    the_winner = winner(board)
    congrat_winner(the_winner, computer, human)


if __name__ == '__main__':
    main()
