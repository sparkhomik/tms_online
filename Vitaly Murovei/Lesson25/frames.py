"""
Открыть iFrame
Проверить, что текст внутри параграфа равен “Your content goes here.”
"""

from selenium.webdriver import Chrome
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


def driver_chrome():
    """
    Функция инициализации драйвера.
    :return: драйвер.
    """
    driver = Chrome(executable_path="C:/Users/acer/Downloads/chromedriver.exe")
    return driver


def following_a_link(driver_data, link_data: str) -> None:
    """
    Функция, осуществляющая переход по ссылке.
    :param driver_data: драйвер.
    :param link_data: ссылка.
    :return: None.
    """
    driver_data.get(link_data)


def find_frame_link(driver_data):
    """
    Функция, осуществляющая поиск ссылки на айфрэйм.
    :param driver_data: драйвер.
    :return:элемент.
    """
    return driver_data.find_element_by_xpath("//a[@href='/iframe']")


def following_a_link_frame(link_data) -> None:
    """
    Функция, осуществляющая переход по ссылке.
    :param link_data: ссылка.
    :return: None.
    """
    link_data.click()


def search_frame_on_page(driver_data):
    """
    Функция, осуществляющая поиск фрэйма на странице.
    :param driver_data: драйвер.
    :return: Элемент.
    """
    return WebDriverWait(driver_data, 10).until(
        EC.visibility_of_element_located((By.XPATH, "//iframe")))


def switch_to_frame(driver_data, frame_data) -> None:
    """
    Функция, осуществляющая переход на окно фрэйма.
    :param driver_data: драйвер.
    :param frame_data: элемент (фрэйм).
    :return: None.
    """
    driver_data.switch_to.frame(frame_data)


def find_text_in_frame(driver_data):
    """
    Функция, осуществляющая поиск текста.
    :param driver_data: драйвер.
    :return: элемент с текстом.
    """
    return driver_data.find_element_by_xpath("//p[text()="
                                             "'Your content goes here.']")


def exit_driver(driver_data) -> None:
    """
    Функция, отключающая драйвер.
    :param driver_data: драйвер.
    :return:None.
    """
    driver_data.close()


if __name__ == "__main__":
    driver_all = driver_chrome()

    link = "http://the-internet.herokuapp.com/frames"

    following_a_link(driver_all, link)

    following_a_link_frame(find_frame_link(driver_all))

    switch_to_frame(driver_all, search_frame_on_page(driver_all))

    print(find_text_in_frame(driver_all).text == "Your content goes here.")

    exit_driver(driver_all)
