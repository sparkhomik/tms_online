"""
Task 1
Найти чекбокс
Нажать на кнопку
Дождаться надписи “It’s gone”
Проверить, что чекбокса нет
Найти инпут
Проверить, что он disabled
Нажать на кнопку
Дождаться надписи “It's enabled!”
Проверить, что инпут enabled
"""
from selenium.webdriver import Chrome
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


def driver_chrome():
    """
    Функция инициализации драйвера.
    :return: драйвер.
    """
    driver = Chrome(executable_path="C:/Users/acer/Downloads/chromedriver.exe")
    return driver


def following_a_link(driver_data, link_data: str) -> None:
    """
    Функция, осуществляющая переход по ссылке.
    :param driver_data: драйвер.
    :param link_data: ссылка.
    :return: None.
    """
    driver_data.get(link_data)


def find_checkbox(driver_data):
    """
    Функция, осуществляющая поиск чекбокса.
    :param driver_data: драйвер.
    :return: элемент чекбокса.
    """
    return driver_data.find_element_by_xpath("//input[@type='checkbox']")


def find_button(driver_data) -> tuple:
    """
    Функция поиска кнопки(2 элемента).
    :param driver_data: драйвер.
    :return:кнопки (тапл).
    """
    return driver_data.find_element_by_xpath(
        "//button[@onclick='swapCheckbox()']"
    ), driver_data.find_element_by_xpath(
        "//button[@onclick='swapInput()']")


def click_on(button_data) -> None:
    """
    Функция клика на кнопки.
    :param button_data: драйвер.
    :return: None.
    """
    button_data.click()


def wait_message(driver_data) -> str:
    """
    Функция ожидания сообщения.
    :param driver_data:драйвер.
    :return: текск сообщения(элемента).
    """
    return WebDriverWait(driver_data, 10). \
        until(EC.visibility_of_element_located((By.XPATH, "//form[@id='"
                                                          "checkbox-example']"
                                                          "/p"))).text


def absenting_checkbox(driver_data, checkbox_data) -> str:
    """
    Функция проверки отсутствия элемента.
    :return: строку, при выполнении условия.
    """
    if WebDriverWait(driver_data, 5).until(EC.invisibility_of_element_located(
            checkbox_data)):
        return "No checkbox"


def find_input(driver_data):
    """
    Функция поиска инпута.
    :param driver_data: драйвер.
    :return: элемент.
    """
    return driver_data.find_element_by_xpath("//form[@id='input-example']"
                                             "/input")


def disable_input(driver_data):
    """
    Функция, проверяющая неактивность поля инпута.
    :param driver_data: драйвер.
    :return: сообщение, при выполнении условий.
    """
    if WebDriverWait(driver_data, 5).until_not(EC.element_to_be_clickable((
            By.XPATH, "//form[@id='input-example']/input"))) is False:
        return "No input"


def message_input_wait(driver_data) -> str:
    """
    Фуннкция, ожидающая появление сообщения.
    :param driver_data: драйвер.
    :return: текст сообщения(элемента).
    """
    return WebDriverWait(driver_data, 10).until(
        EC.visibility_of_element_located((
            By.XPATH, "//form[@id='input-example']/p"))).text


def active_input(driver_data) -> str:
    """
    Функция, проверяющая активность инпута.
    :param driver_data: драйвер.
    :return: сообщение, при выполнении условий.
    """
    if WebDriverWait(driver_data, 5).until(EC.element_to_be_clickable(
            (By.XPATH, "//form[@id='input-example']/input"))):
        return "Input ok"


def exit_driver(driver_data) -> None:
    """
    Функция, отключающая драйвер.
    :param driver_data: драйвер.
    :return:None.
    """
    driver_data.close()


if __name__ == "__main__":
    driver_all = driver_chrome()

    link = "http://the-internet.herokuapp.com/dynamic_controls"

    following_a_link(driver_all, link)

    checkbox = find_checkbox(driver_all)

    button_of_checkbox = find_button(driver_all)[0]

    click_on(button_of_checkbox)

    print(wait_message(driver_all))

    print(absenting_checkbox(driver_all, checkbox))

    fin_input = find_input(driver_all)

    print(disable_input(driver_all))

    button = find_button(driver_all)[1]

    click_on(button)

    print(message_input_wait(driver_all))

    print(active_input(driver_all))

    exit_driver(driver_all)
