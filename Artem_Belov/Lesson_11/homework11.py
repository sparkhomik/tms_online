# На ферме могут жить животные и птицы
# И у тех и у других есть два атрибута: имя и возраст
# При этом нужно сделать так, чтобы возраст нельзя было изменить
# извне напрямую, но при этом можно было бы получить его значение
# У каждого животного должен быть обязательный метод – ходить
# А для птиц – летать
# Эти методы должны выводить сообщения (контекст придумайте)
# Также должны быть общие для всех классов обязательные методы – постареть -
# который увеличивает возраст на 1 и метод голос
# При обращении к несуществующему методу или атрибуту животного или птицы
# должен срабатывать пользовательский exception NotExistException
# и выводить сообщение о том, что для текущего класса (имя класса), такой метод
# или атрибут не существует – (имя метода или атрибута)
from abc import ABC, abstractmethod


class Farm(ABC):

    """класс ферм с общими особенностями подклассов"""
    def __init__(self, name: str, age: int):
        self.name = name
        """возраст недоступный для изменения извне"""
        self._age = age

    def change_age(self):
        """функция изменения возраста"""
        self._age += 1

    def __getattr__(self, atr: None):
        """функция вызова пользовательского exception при обращении к
        несуществующему аттрибуту, по заданию"""
        raise NotExistException(atr)

    @staticmethod
    def walk() -> str:
        return "Ходит"

    @abstractmethod
    def voice(self) -> None:
        """функция перенаправления в голос животного"""
        raise NotImplementedError


class Mammals(Farm, ABC):
    """класс млекопитающих"""
    pass


class Birds(Farm, ABC):
    """класс птиц с функцией для общих особенностей"""

    @staticmethod
    def fly() -> str:
        return "Летает"


class Pig(Mammals):
    """класс Свиньи"""
    def voice(self) -> str:
        return "Хрю"

    def __str__(self) -> str:
        return f'Представитель Свиней с именем {self.name} и возрастом '\
               f'{self._age}. {self.walk()} и говорит {self.voice()}.'


class Cow(Mammals):
    """класс Коровы"""
    def voice(self) -> str:
        return "Мууу"

    def __str__(self) -> str:
        """"""
        return f'Представитель Коров с именем {self.name} и возрастом '\
               f'{self._age}. {self.walk()} и говорит {self.voice()}.'


class Chicken(Birds):
    """класс Курицы"""
    def voice(self) -> str:
        return "Кукареку"

    def __str__(self) -> str:
        return f'Представитель Куриц с именем {self.name} и возрастом '\
               f'{self._age}. {self.fly()}, но и {self.walk()} ' \
               f'и говорит {self.voice()}.'


class Goose(Birds):
    """класс Гуся"""
    def voice(self) -> str:
        return "Га-га"

    def __str__(self) -> str:
        return f'Представитель Гусей с именем {self.name} и возрастом '\
               f'{self._age}. {self.fly()}, но и {self.walk()} ' \
               f'и говорит {self.voice()}.'


class NotExistException(Exception):
    """класс кастом ошибки при несуществующем аттрибуте класса"""
    def __init__(self, func):
        """функция вывода сообщения об ошибке"""
        self.message = f'{func} - функции не существует'
        super(NotExistException, self).__init__(self.message)


class FarmFunctions:

    def __init__(self, creatures_list):
        self.creatures_list = creatures_list

    def __iter__(self):
        return iter(self.creatures_list)

    def __getitem__(self, atr):
        return self.creatures_list[atr]

    def all_creatures(self) -> None:
        """функция получения всех животных"""
        for creature in self.creatures_list:
            print(creature)

    def change_age(self) -> None:
        """функция увеличения возраста"""
        [animal.change_age() for animal in self.creatures_list]


def show_me():
    """функция запуска программы"""
    pig = Pig('Афина', 1)
    cow = Cow('Платонида', 2)
    chicken = Chicken('Спартак', 3)
    goose = Goose('Юлиан', 4)
    creatures = [pig, cow, chicken, goose]
    farm = FarmFunctions(creatures)
    farm.all_creatures()
    print(f'Первое животное: {farm[0]}')
    print(f'Второе животное: {farm[1]}')
    print(f'Третье животное: {farm[2]}')
    print(f'Четвертое животное: {farm[3]}')
    farm.change_age()
    cow.love()


show_me()
