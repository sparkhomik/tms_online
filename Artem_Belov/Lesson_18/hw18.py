import platform


class SystemInfo:
    """OS class"""
    @staticmethod
    def get_sys_name() -> str:
        """function to get system name"""
        sys_name = platform.system()
        return sys_name

    @staticmethod
    def get_sys_release() -> str:
        """function to get system version"""
        sys_ver = platform.release()
        return sys_ver

    @staticmethod
    def get_sys_version() -> str:
        """function to get system release"""
        sys_rls = platform.version()
        return sys_rls


if __name__ == "__main__":
    SystemInfo.get_sys_name()
    SystemInfo.get_sys_release()
    SystemInfo.get_sys_version()
