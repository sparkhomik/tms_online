import unittest
from hw18 import SystemInfo


class TestHw(unittest.TestCase):
    """testcase class"""

    def test_get_sys_name(self):
        self.assertEqual(SystemInfo.get_sys_name(), "Darwin")

    def test_get_sys_release(self):
        self.assertEqual(SystemInfo.get_sys_release(), "19.6.0")

    def test_get_sys_version(self):
        self.assertEqual(
            SystemInfo.get_sys_version(),
            "Darwin Kernel Version 19.6.0: "
            "Mon Aug 31 22:12:52 PDT 2020; "
            "root:xnu-6153.141.2~1/RELEASE_X86_64"
        )

    @unittest.expectedFailure
    def test_fail_get_sys_name(self):
        self.assertEqual(SystemInfo.get_sys_name(), "Android")

    @unittest.expectedFailure
    def test_fail_get_sys_version(self):
        self.assertEqual(SystemInfo.get_sys_version(), "1.0.0.")

    @unittest.expectedFailure
    def test_fail_get_sys_release(self):
        self.assertEqual(SystemInfo.get_sys_release(), "XXX")
