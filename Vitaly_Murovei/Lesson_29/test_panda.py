from panda_page import PandaPage


def test_title(driver):
    """
    Тест title.
    :param driver: драйвер.
    """
    panda_page = PandaPage(driver)
    panda_page.open()
    assert "ShiningPanda" in panda_page.get_title(), \
        "No element"


def test_name(driver):
    """
    Тест на поис элемента.
    :param driver: драйвер.
    """
    panda_page = PandaPage(driver)
    panda_page.open()
    assert panda_page.get_name().text == "ShiningPanda", \
        "No element"
