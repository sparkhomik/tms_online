from selenium.webdriver.common.by import By


class PandaPageLocators:
    """
    Класс с локатором страницы, согласно условию ДЗ.
    """
    LOCATOR_PAGE_TITLE = (By.XPATH, "//h1[@class='title']")
