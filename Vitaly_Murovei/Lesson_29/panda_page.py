from base_page import BasePage
from panda_page_locators import PandaPageLocators


class PandaPage(BasePage):
    """
    Класс страницы.
    """
    URL = "https://plugins.jenkins.io/shiningpanda"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title(self):
        """
        Получение title.
        :return: title.
        """
        return self.driver.title

    def get_name(self):
        """
        Поиск элемента.
        :return: name
        """
        return self.find_element(PandaPageLocators.LOCATOR_PAGE_TITLE)
