import random
# 1.)
a = 10
b = 23
a, b = b, a
print(a)
print(b)
# 2.)
a = a + 3
b = b - 3
print(a)
print(b)
# 3.)
float(a)
str(b)
# 4.)
print(round(a / 11, 3))
# 5.)
b = float(b)
c = b
print(c ** 3)
# 6.)
list = [3, 6, 9, 12, 15, 18, 21]
print(random.choice(list))
# 7.)
print((100**0.5)**4)
# 8.)
print('Hi guys' * 3 + 'ToDay')
# 9.)
len('Hi guysHi guysHi guysToDay')
# 10.)
task10_1 = 'Hi guysHi guysHi guysToDay'[21:26:]
task10_2 = 'Hi guysHi guysHi guysToDay'[:20:-1]
print(task10_1)
print(task10_2)
# 11.)
task11_1 = 'Hi guysHi guysHi guysToDay'[1::2]
task11_2 = 'Hi guysHi guysHi guysToDay'[-2::-2]
print(task11_1)
print(task11_2)
# 12.)
task12_1 = 'Task 10: {pr1}, {obr1}, Task 11: {pr2}, {obr2}'\
    .format(pr1=task10_1, obr1=task10_2, pr2=task11_1, obr2=task11_2)
print(task12_1)
# 13.)
print(task12_1.title())
print(task12_1.upper())
print(task12_1.lower())
# 14.)
print(task12_1.count('Task'))
