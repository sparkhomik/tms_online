# 1.)
# Перевести строку в массив
s_1 = "Robin Singh"
l_1 = s_1.split()
s_1_2 = "I love arrays they are my favorite"
l_1_2 = s_1_2.split()
print(l_1)
print(l_1_2)
# 2.)
# Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
l_2 = ['Ivan', 'Ivanov']
s_2 = 'Minsk'
s_2_1 = 'Belarus'
j_2 = ' '.join(l_2)
print('Привет, {name_surname}! Добро пожаловать в {city} {country}'
      .format(name_surname=j_2, city=s_2, country=s_2_1))
# 3.)
# Сделать из списка строку
l_3 = ['I', 'love', 'arrays', 'they', 'are', 'my', 'favorite']
j_3 = ' '.join(l_3)
print(j_3)
# 4.)
# Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
# удалите элемент из списка под индексом 6
l_4 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
l_4.insert(2, 'y')
del l_4[6]
print(l_4)
# 5.)
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ""}
# Создайте словарь, который будет содержать в себе все элементы обоих словарей
ab = {**a, **b}
# Обновите словарь “a” элементами из словаря “b”
a.update(b)
a_1 = a.values()
# Проверить что все значения в словаре “a” не пустые либо не равны нулю
print(all(a_1))
# Проверить что есть хотя бы одно пустое значение (результат выполнения
# должен быть True)
print(not all(a_1))
# Отсортировать словарь по алфавиту в обратном порядке
a_2 = a.items()
a_3 = sorted(a_2)
a_3.reverse()
print(a_3)
# 6.)
# Создать список из элементов
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
# Вывести только уникальные значения и сохранить их в отдельную переменную
unique_numbers = list(set(list_a))
list_b = unique_numbers
print(unique_numbers)
# Добавить в полученный объект значение 22
list_b.append(22)
# Сделать list_a неизменяемым
list_b = str(list_b)
# Измерить его длинну
print(len(list_b))
# Задание на закрепление форматирования
# 1.)
a = 10
b = 25
# Первый способ
print('Summ is {summ_a_b} and diff = {diff_a_b}'
      .format(summ_a_b=a + b, diff_a_b=a - b))
# Второй способ
summ_a_b = a + b
diff_a_b = a - b
print(f"Summ is {summ_a_b} and diff = {diff_a_b}")
# 2.)
list_of_children = ['Sasha', 'Vasia', 'Nikalai']
# Первый способ
a = list_of_children[0]
b = list_of_children[1]
c = list_of_children[2]
print('First child is "{first_name}", second is “{second_name}”, '
      'and last one – "{third_name}"'
      .format(first_name=a, second_name=b, third_name=c))
# Второй способ
first_name = list_of_children[0]
second_name = list_of_children[1]
third_name = list_of_children[2]
print(f'First child is "{first_name}", second is '
      f'“{second_name}”, and last one – "{third_name}"')
