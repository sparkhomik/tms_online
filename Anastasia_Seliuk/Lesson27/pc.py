import mysql.connector as mysql
from logzero import logger


def connect_db():
    """
    Подключение к БД homework27_db.
    :return: БД и объект cursor для взаимодействия с ней
    """
    db = mysql.connect(
        host="localhost",
        user="root",
        passwd="123456",
        database='homework27_db'
    )

    cursor = db.cursor()
    return db, cursor


def close_db(db):
    """
    Закрытие БД.
    :param db: БД
    """
    db.close()
    logger.info("Database is closed.")


def create_table(cursor) -> None:
    """
    Создание таблицы pc.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute(
        "CREATE TABLE pc (PRO_ID INT(3), PRO_NAME VARCHAR(255), PRO_PRICE INT,"
        " PRO_COM INT)")
    logger.info("Table 'pc' is created.")


def insert_into_table(db, cursor) -> None:
    """
    Вставка данных в таблицу pc.
    :param db: БД
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute(
        "INSERT INTO pc (PRO_ID, PRO_NAME, PRO_PRICE, PRO_COM) VALUES "
        "(101, 'Mother Board', 3200, 15), "
        "(102, 'Key Board', 450, 16), "
        "(103, 'ZIP drive', 250, 14), "
        "(104, 'Speaker', 550, 16), "
        "(105, 'Monitor', 5000, 11), "
        "(106, 'DVD drive', 900, 12), "
        "(107, 'CD drive', 800, 2), "
        "(108, 'Printer', 2600, 13), "
        "(109, 'Refill cartridge', 350, 13), "
        "(110, 'Mouse', 250, 12)")
    db.commit()
    logger.info("Data is inserted into 'pc' table.")


def total_price(cursor) -> None:
    """
    Вывод общей стоимости компонентов.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute("SELECT SUM(PRO_PRICE) FROM pc")
    print(cursor.fetchall())


def avg_price(cursor) -> None:
    """
    Вывод средней стоимости компонентов.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute("SELECT AVG(PRO_PRICE) FROM pc")
    print(cursor.fetchall())


def between_price(cursor) -> None:
    """
    Вывод компонентов с ценой между 200 и 600.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute("SELECT PRO_ID, PRO_NAME FROM pc "
                   "WHERE PRO_PRICE > 200 AND PRO_PRICE < 600")
    print(cursor.fetchall())


def max_price_name(cursor) -> None:
    """
    Вывод названия компонента, у которого самая высокая цена.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute("SELECT PRO_NAME FROM pc "
                   "WHERE PRO_PRICE = (SELECT MAX(PRO_PRICE) from pc)")
    print(cursor.fetchall())


def min_price_name(cursor) -> None:
    """
    Вывод названия компонента, у которого самая низкая цена.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute("SELECT PRO_NAME FROM pc "
                   "WHERE PRO_PRICE = (SELECT MIN(PRO_PRICE) from pc)")
    print(cursor.fetchall())


if __name__ == "__main__":
    db, cursor = connect_db()
    create_table(cursor)
    insert_into_table(db, cursor)
    total_price(cursor)
    avg_price(cursor)
    between_price(cursor)
    max_price_name(cursor)
    min_price_name(cursor)
    close_db(db)
