import mysql.connector as mysql
from logzero import logger


def connect_db():
    """
    Подключение к БД homework27_db.
    :return: БД и объект cursor для взаимодействия с ней
    """
    db = mysql.connect(
        host="localhost",
        user="root",
        passwd="123456",
        database='homework27_db'
    )

    cursor = db.cursor()
    return db, cursor


def close_db(db):
    """
    Закрытие БД.
    :param db: БД
    """
    db.close()
    logger.info("Database is closed.")


def create_table(cursor) -> None:
    """
    Создание таблицы orders.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute(
        "CREATE TABLE orders (ord_no INT(5), purch_amt FLOAT(7,2), "
        "ord_date DATE, customer_id INT(4), salesman_id INT(4))")
    logger.info("Table 'orders' is created.")


def insert_into_table(db, cursor) -> None:
    """
    Вставка данных в таблицу orders.
    :param db: БД
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute(
        "INSERT INTO orders (ord_no, purch_amt, ord_date, "
        "customer_id, salesman_id) VALUES "
        "(70001, 150.5, '2012-10-05', 3005, 5002), "
        "(70009, 270.65, '2012-09-10', 3001, 5005), "
        "(70002, 65.26, '2012-10-05', 3002, 5001), "
        "(70004, 110.5, '2012-08-17', 3009, 5003), "
        "(70007, 948.5, '2012-09-10', 3005, 5002), "
        "(70005, 2400.6, '2012-07-27', 3007, 5001), "
        "(70008, 5760, '2012-09-10', 3002, 5001), "
        "(70010, 1983.43, '2012-10-10', 3004, 5006), "
        "(70003, 2480.4, '2012-10-10', 3009, 5003), "
        "(70012, 250.45, '2012-06-27', 3008, 5002)")
    db.commit()
    logger.info("Data is inserted into 'orders' table.")


def salesman_5002(cursor) -> None:
    """
    Вывод номера заказа, даты заказа и количества для каждого заказа, который
    продал продавец под номером 5002.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute(
        "SELECT ord_no, ord_date, purch_amt FROM orders "
        "WHERE salesman_id = 5002")
    print(cursor.fetchall())


def unique_id(cursor) -> None:
    """
    Вывод уникальных id продавца.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute("SELECT DISTINCT salesman_id FROM orders")
    print(cursor.fetchall())


def order_by_date(cursor) -> None:
    """
    Вывод по порядку данных о дате заказа, id продавца, номере заказа,
    количестве.
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute("SELECT ord_date, salesman_id, ord_no, purch_amt "
                   "FROM orders ORDER BY ord_date")
    print(cursor.fetchall())


def orders_between(cursor) -> None:
    """
    Вывод заказов между 70001 и 70007ю
    :param cursor: объект для взаимодействия с БД
    """
    cursor.execute("SELECT * FROM orders "
                   "WHERE ord_no > 70001 AND ord_no < 70007")
    print(cursor.fetchall())


if __name__ == "__main__":
    db, cursor = connect_db()
    create_table(cursor)
    insert_into_table(db, cursor)
    salesman_5002(cursor)
    unique_id(cursor)
    order_by_date(cursor)
    orders_between(cursor)
    close_db(db)
