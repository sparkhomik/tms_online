import random
import goslate

"""1. Быки и коровы
В классическом варианте игра рассчитана на двух игроков. Каждый из игроков
задумывает и записывает тайное 4-значное число с неповторяющимися цифрами.
Игрок, который начинает игру по жребию, делает первую попытку отгадать число.
Попытка — это 4-значное число с неповторяющимися цифрами, сообщаемое
противнику. Противник сообщает в ответ, сколько цифр угадано без совпадения
с их позициями в тайном числе (то есть количество коров) и сколько угадано
вплоть до позиции в тайном числе (то есть количество быков).
При игре против компьютера игрок вводит комбинации одну за другой, пока не
отгадает всю последовательность. Ваша задача реализовать программу, против
которой можно сыграть в "Быки и коровы"
Пример
Загадано число 3219
>>> 2310
Две коровы, один бык
>>> 3219
Вы выиграли!"""


# создаем список всех возможных ответов
def get_all_answers():
    ans = []
    for i in range(1000, 10000):
        tmp = str(i).zfill(4)
        # print(tmp)
        if len(set(map(int, tmp))) == 4:
            ans.append(list(map(int, tmp)))
    return ans


# выбираем один ответ из списка всех возможных
def get_one_answer(ans):
    num = random.choice(ans)
    return num


# запрашиваем у пользователя неповторяющиеся цифры
def input_number():
    while True:
        nums = input('Введите 4-значное число с неповторяющимися цифрами: ')
        if len(nums) != 4 or not nums.isdigit() or nums[0] == '0':
            continue
        nums = list(map(int, nums))
        if len(set(nums)) == 4:
            break
    return nums


# сравнивает числа и сообщаем количество быков и коров
def check(nums, true_nums):
    bulls, cows = 0, 0
    for i, num in enumerate(nums):
        if num in true_nums:
            if nums[i] == true_nums[i]:
                bulls += 1
            else:
                cows += 1
    return bulls, cows


# удаляем неподходящие варианты ответов из возможных
def del_bad_answers(ans, enemy_try, bull, cow):
    for num in ans[:]:
        temp_bull, temp_cow = check(num, enemy_try)
        if temp_bull != bull or temp_cow != cow:
            ans.remove(num)
    return ans


print("Игра Быки и коровы")
answers = get_all_answers()
player = input_number()
enemy = get_one_answer(answers)

while True:
    print('=' * 20, 'Ход игрока', '=' * 20)
    print('Угадайте число компьютера')
    number = input_number()
    bulls, cows = check(number, enemy)
    print('Быки: ', bulls, 'Коровы: ', cows)
    if bulls == 4:
        print('Вы победили!')
        print('Компьютер загадал: ', enemy)
        break
    print('=' * 20, 'Ход компьтера', '=' * 20)
    enemy_try = get_one_answer(answers)
    print('Компьютер считает, что Вы загадали число: ', enemy_try)
    bulls, cows = check(enemy_try, player)
    print('Быки: ', bulls, 'Коровы: ', cows)
    if bulls == 4:
        print('Победил компьютер!')
        print('Компьютер загадал число: ', enemy)
        break
    else:
        answers = del_bad_answers(answers, enemy_try, bulls, cows)

"""2. Like
Создайте программу, которая, принимая массив имён, возвращает строку
описывающая количество лайков (как в Facebook).
Примеры:
Введите имена через запятую: "Ann"
-> "Ann likes this"
Введите имена через запятую: "Ann, Alex"
-> "Ann and Alex like this"
Введите имена через запятую: "Ann, Alex, Mark"
-> "Ann, Alex and Mark like this"
Введите имена через запятую: "Ann, Alex, Mark, Max"
-> "Ann, Alex and 2 others like this"
Если ничего не вводить должен быть вывод:
-> "No one likes this
Бонусные очки
Функция работает на нескольких языках - язык ответа определяется по языку
входного массива."""

names = input('Введите имена через запятую: ')
gs = goslate.Goslate()
language_id = gs.detect(names)
if language_id == 'en':
    if not names:
        print('No one likes this')
    else:
        names = names.split(',')
        if len(names) == 1:
            print(f'{names[0]} likes this')
        elif len(names) == 2:
            print(f'{names[0]} and{names[1]} like this')
        elif len(names) == 3:
            print(f'{names[0]},{names[1]} and{names[2]} like this')
        else:
            print(f'{names[0]},{names[1]} and {len(names) - 2}'
                  f' others like this')
elif language_id == 'ru':
    if not names:
        print('Никому не понравилось это')
    else:
        names = names.split(',')
        if len(names) == 1:
            print(f'{names[0]} понравилось это')
        elif len(names) == 2:
            print(f'{names[0]} и{names[1]} понравилось это')
        elif len(names) == 3:
            print(f'{names[0]},{names[1]} и{names[2]} понравилось это')
        else:
            print(f'{names[0]},{names[1]} и {len(names) - 2}'
                  f' другим понравилось это')

"""3. BuzzFuzz
Напишите программу, которая перебирает последовательность от 1 до 100.
Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа,
а для чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5
надо печатать "FuzzBuzz". Иначе печатать число.Вывод должен быть следующим:
1
2
fuzz
4
buzz
fuzz
7
8
.."""

for i in range(1, 101):
    if i % 5 == 0 and i % 3 == 0:
        print('FuzzBuzz')
    elif i % 3 == 0:
        print('Fuzz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)

"""4. Напишите код, который возьмет список строк и пронумерует их.
Нумерация начинается с 1, имеет “:” и пробел
[] => []
["a", "b", "c"] => ["1: a", "2: b", "3: c"]"""

sp = ['a', 'b', 'c']
for i, item in enumerate(sp):
    sp[i] = str(i + 1) + ": " + sp[i]
print(sp)

"""5. Проверить, все ли элементы одинаковые
[1, 1, 1] == True
[1, 2, 1] == False
['a', 'a', 'a'] == True
[] == True"""

sp = ['a', 'a', 'a']
if len(set(sp)) == 1:
    print('True')
else:
    print('False')

"""6. Проверка строки. В данной подстроке проверить все ли буквы в строчном
регистре или нет и вернуть список не подходящих.
dogcat => [True, []]
doGCat => [False, ['G', 'C']]"""

sp = 'doGCat'
new_l = []
new_k = []
if sp != sp.lower():
    new_l.append('False')
    for i in sp:
        if i != i.lower():
            new_k.append(i)
else:
    new_l.append('True')
new_l.append(new_k)
print(new_l)

"""Сложите все числа в списке, они могут быть отрицательными,
если список пустой вернуть 0
[] == 0
[1, 2, 3] == 6
[1.1, 2.2, 3.3] == 6.6
[4, 5, 6] == 15
range(101) == 5050"""

sp = range(101)
if sp == 0:
    print(0)
else:
    print(sum(sp))
