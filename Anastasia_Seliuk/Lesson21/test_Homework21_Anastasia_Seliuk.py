def test_authentication(authentication):
    """
    Проверка аутентификации пользователя.
    """
    assert authentication[0] == authentication[1]


def test_checkboxes(checkboxes, checkbox1, checkbox2):
    """
    Проверка, что checkbox 2  - checked, а checkbox 1 - нет.
    """
    assert not checkbox1.is_selected() and checkbox2.is_selected()


def test_windows(windows, all_windows, new_window_title):
    """
    Проверка, что в браузере открыты 2 вкладки и что заголовок текущей -
    'New Window'.
    """
    assert len(all_windows) == 2 and new_window_title == 'New Window'


def test_add_element(add_remove, add_element, check_remove_existence):
    """
    Проверка наличия на странице кнопки 'Remove' после нажатия на кнопку 'Add'.
    """
    assert check_remove_existence


def test_remove_element(add_remove, add_element, remove_element,
                        check_remove_existence):
    """
    Проверка наличия на странице кнопки 'Remove' после нажатия на кнопку 'Add'.
    Фикстура add_element вызывается для добавления элемента 'Remove' с целью
    последующего его удаления.
    """
    assert not check_remove_existence
