from typing import Tuple

import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.keys import Keys
from selenium.common.exceptions import NoSuchElementException


@pytest.fixture(scope='session', autouse=True)
def driver():
    """
    Запуск и закрытие драйвера Chrome в начале и конце сессии соответственно.
    :return: экземпляр класса драйвера Chrome
    """
    driver = Chrome(executable_path="c:/driver/chromedriver")
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def main_page_driver(driver):
    """
    Открытие страницы http://the-internet.herokuapp.com/ в браузере.
    :param driver: экземпляр класса драйвера Chrome
    :return: экземпляр класса драйвера Chrome
    """
    driver.get("http://the-internet.herokuapp.com/")
    yield driver
    driver.close()


@pytest.fixture()
def authentication_driver(driver):
    """
    Открытие страницы аутентификации в браузере.
    :param driver: экземпляр класса драйвера Chrome
    :return: экземпляр класса драйвера Chrome
    """
    driver.get("http://the-internet.herokuapp.com/login")
    yield driver
    driver.close()


@pytest.fixture(params=[('tomsmith', 'SuperSecretPassword!',
                         'You logged into a secure area!'),
                        ('tomsmith', 'Password!', 'Your password is invalid!'),
                        ('tom', ' ', 'Your username is invalid!')])
def authentication(authentication_driver, request) -> Tuple:
    """
    Аутентификация пользователя.
    :param authentication_driver: экземпляр класса драйвера Chrome
    :param request: параметр для определения имени пользователя, пароля и
    конечного результата
    :return: фактическое и ожидаемое сообщение в результате аутентификации
    пользователя
    """
    params = request.param
    authentication_driver.implicitly_wait(15)
    username = authentication_driver.find_element_by_id('username')
    username.send_keys(params[0])
    username.send_keys(Keys.TAB)
    password = authentication_driver.find_element_by_id('password')
    password.send_keys(params[1])
    password.send_keys(Keys.ENTER)
    authentication_driver.implicitly_wait(15)
    authentication_result = authentication_driver.find_element_by_id('flash')
    authentication_result = authentication_result.text
    return authentication_result[:-2:], params[2]


@pytest.fixture()
def checkboxes(main_page_driver) -> None:
    """
    Переход по ссылке 'Checkboxes'.
    :param main_page_driver: экземпляр класса драйвера Chrome
    """
    main_page_driver.implicitly_wait(10)
    checkboxes_path = '//a[@href="/checkboxes"]'
    checkboxes = main_page_driver.find_element_by_xpath(checkboxes_path)
    checkboxes.click()


@pytest.fixture()
def checkbox1(main_page_driver) -> bool:
    """
    Активен первый чебокс или нет.
    :param main_page_driver: экземпляр класса драйвера Chrome
    :return: активен первый чекбокс или нет
    """
    checkbox1_xpath = "//form[@id='checkboxes']/input[1]"
    checkbox1 = main_page_driver.find_element_by_xpath(checkbox1_xpath)
    if checkbox1.get_attribute("checked"):
        checkbox1.click()
    return checkbox1


@pytest.fixture()
def checkbox2(main_page_driver) -> bool:
    """
    Активен второй чебокс или нет.
    :param main_page_driver: экземпляр класса драйвера Chrome
    :return: активен второй чекбокс или нет
    """
    checkbox2_xpath = "//form[@id='checkboxes']/input[2]"
    checkbox2 = main_page_driver.find_element_by_xpath(checkbox2_xpath)
    if not checkbox2.get_attribute("checked"):
        checkbox2.click()
    return checkbox2


@pytest.fixture()
def windows(main_page_driver) -> None:
    """
    Открытие новой вкладки после перехода по ссылкам 'Multiple Windows' >
    'Click here'.
    :param main_page_driver: экземпляр класса драйвера Chrome
    """
    main_page_driver.implicitly_wait(10)
    windows = main_page_driver.find_element_by_xpath('//a[@href="/windows"]')
    windows.click()
    windows_new_xpath = '//a[@href="/windows/new"]'
    windows_new = main_page_driver.find_element_by_xpath(windows_new_xpath)
    windows_new.click()


@pytest.fixture()
def all_windows(main_page_driver) -> list:
    """
    Получение списка открытых вкладок.
    :param main_page_driver: экземпляр класса драйвера Chrome
    :return: список открытых вкладок
    """
    all_windows = main_page_driver.window_handles
    return all_windows


@pytest.fixture()
def new_window_title(all_windows, main_page_driver) -> str:
    """
    Получение заголовка активной вкладки.
    :param all_windows: список открытых вкладок
    :param main_page_driver: экземпляр класса драйвера Chrome
    :return: заголовок активной вкладки
    """
    main_page_driver.switch_to.window(all_windows[1])
    return main_page_driver.title


@pytest.fixture()
def add_remove(main_page_driver) -> str:
    """
    Переход по ссылке Add/Remove elements.
    :param main_page_driver: экземпляр класса драйвера Chrome
    :return: путь на кнопку 'Remove'
    """
    main_page_driver.implicitly_wait(10)
    add_remove_xpath = '//a[@href="/add_remove_elements/"]'
    add_remove_driver = main_page_driver.find_element_by_xpath(
        add_remove_xpath)
    add_remove_driver.click()
    remove_element_xpath = '//button[@onclick="deleteElement()"]'
    return remove_element_xpath


@pytest.fixture()
def add_element(main_page_driver) -> None:
    """
    Нажатие на кнопку 'Add'.
    :param main_page_driver: экземпляр класса драйвера Chrome
    """
    main_page_driver.implicitly_wait(10)
    add_element_xpath = '//button[@onclick="addElement()"]'
    add_element = main_page_driver.find_element_by_xpath(add_element_xpath)
    add_element.click()


@pytest.fixture()
def remove_element(main_page_driver, add_remove) -> None:
    """
    Нажатие на кнопку 'Remove'.
    :param main_page_driver: экземпляр класса драйвера Chrome
    :param add_remove: путь на кнопку 'Remove'
    """
    remove_element = main_page_driver.find_element_by_xpath(add_remove)
    remove_element.click()


@pytest.fixture()
def check_remove_existence(main_page_driver, add_remove) -> bool:
    """
    Проверка наличия на странице кнопки 'Remove'.
    :param main_page_driver: экземпляр класса драйвера Chrome
    :param add_remove: путь на кнопку 'Remove'
    :return: есть кнопка на странице или нет
    """
    main_page_driver.implicitly_wait(10)
    try:
        main_page_driver.find_element_by_xpath(add_remove)
    except NoSuchElementException:
        return False
    return True
