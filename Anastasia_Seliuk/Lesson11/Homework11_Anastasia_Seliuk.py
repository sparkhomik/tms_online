from abc import ABC, abstractmethod
import random

"""Ферма

На ферме могут жить животные и птицы
И у тех и у других есть два атрибута: имя и возраст
При этом нужно сделать так, чтобы возраст нельзя было изменить извне напрямую,
но при этом можно было бы получить его значение
У каждого животного должен быть обязательный метод – ходить
А для птиц – летать
Эти методы должны выводить сообщения (контекст придумайте)
Также должны быть общие для всех классов обязательные методы – постареть -
который увеличивает возраст на 1 и метод голос

При обращении к несуществующему методу или атрибуту животного или птицы должен
срабатывать пользовательский exception NotExistException
и выводить сообщение о том, что для текущего класса (имя класса), такой метод
или атрибут не существует – (имя метода или атрибута)

На основании этих классов создать отдельные классы для Свиньи, Гуся, Курицы и
Коровы – для каждого класса метод голос должен выводить разное сообщение (хрю,
гага, кудах, му)

После этого создать класс Ферма, в который можно передавать список животных
Должна быть возможность получить каждое животное, живущее на ферме как по
индексу, так и через цикл for.
Также у фермы должен быть метод, который увеличивает возраст всех животных,
живущих на ней."""


class Animal(ABC):
    """
    Класс для всех животных.
    Содержит атрибуты и методы общие для всех животных.
    """

    def __init__(self, name: str, age: int) -> None:
        """
        Создание объекта класса животные.
        :param name: имя животного
        :param age:  возраст животного
        """
        self.name = name
        self.__age = age

    def __getattr__(self, atr: str) -> None:
        """
        Вызов NotExistException в случае отсутствия вызываемого
        метода/атрибута.
        :param atr: вызываемый метод/атрибут
        """
        raise NotExistException(atr, self.__class__.__name__)

    def __str__(self) -> str:
        """
        Вывод информации о животном.
        :return: имя и возраст животного
        """
        if self.__age <= 1:
            return f'{self.name} - {self.__age} year old'
        else:
            return f'{self.name} - {self.__age} years old'

    def age(self) -> int:
        """
        Увеличение возраст животного на один год.
        :return: увеличенный на один возраст животного
        """
        self.__age += 1
        return self.__age

    @abstractmethod
    def voice(self) -> None:
        """
        Объявление абстрактного класса голос для переопределения в классах
        наследниках.
        """
        raise NotImplementedError


class Pets(Animal, ABC):
    """
    Класс домашних животных. Содержит метод общий для всех домашних животных.
    """

    @staticmethod
    def go() -> str:
        """
        Метод 'ходить'.
        :return: возможность ходить
        """
        return "Let's go!"


class Birds(Animal, ABC):
    """
    Класс птиц. Содержит метод общий для всех птиц.
    """

    @staticmethod
    def fly() -> str:
        """
        Метод 'летать'.
        :return: возможность летать
        """
        return "Let's fly!"


class Pig(Pets):
    """
    Класс свиней. Содержит метод общий для всех свиней.
    """

    def voice(self) -> str:
        """
        Подача голоса свиньей.
        :return: звук издаваемый свиньей
        """
        return "Oink!"


class Cow(Pets):
    """
    Класс коров. Содержит метод общий для всех коров.
    """

    def voice(self) -> str:
        """
        Подача голоса коровой.
        :return: звук издаваемый коровой
        """
        return "Mu!"


class Goose(Birds):
    """
    Класс гусей. Содержит метод общий для всех гусей.
    """

    def voice(self) -> str:
        """
        Подача голосом гусем.
        :return: звук издаваемый гусем
        """
        return "Eider!"


class Chicken(Birds):
    """
    Класс куриц. Содержит метод общий для всех куриц.
    """

    def voice(self) -> str:
        """
        Подача голоса курицей.
        :return: звук издаваемый курицей
        """
        return "Cluck!"


class Farm:
    """
    Класс для фермы, которая может содержать в себе животных разных классов.
    """

    def __init__(self, *animals: object) -> None:
        """
        Создание объекта класса ферма.
        :param animals: животное на ферме
        """
        self.index = 0
        self.animals = list(animals)

    def __getitem__(self, index: int) -> object:
        """
        Определение животного на ферме по индексу.
        :param index: индекс
        :return: животное на ферме по заданному индексу
        """
        return self.animals[index]

    def __iter__(self) -> object:
        """
        Для 'превращения' фермы в итерируемый объект.
        :return: ферма-итератор
        """
        return self

    def __next__(self) -> object:
        """
        Возвращение следующего животного на ферме.
        :return: следующее животное на ферме
        """
        try:
            item = self.animals[self.index]
        except IndexError:
            raise StopIteration()
        self.index += 1
        return item

    def __len__(self) -> int:
        """
        Определение количества животных на ферме.
        :return: количество животных на ферме
        """
        return len(self.animals)

    def all_animals(self) -> None:
        """
        Вывод информации по всем животным на ферме, а также издаваемых ими
        звуков. Возраст животных предварительно был увеличен на один год.
        """
        print('My farm: ')
        for animal in self:
            animal.age()
            print(animal)
            print(animal.voice())

    def random_number(self) -> int:
        """
        Определение номера случайного животного на ферме.
        :return: номер случайного животного на ферме
        """
        number = random.randrange(0, len(self) - 1)
        return number


class NotExistException(Exception):
    """
    Класс для определения ошибки, которая будет выведена пользователю, в случае
    вызова несуществующего метода или атрибута.
    """

    def __init__(self, func: str, name: str) -> str:
        """
        Определение сообщения об ошибке.
        :param func: имя метода или атрибута
        :param name: имя класса, для которого этот метод или атрибут был вызван
        """
        self.message = f'Для текущего класса {name}' \
                       f' такой метод не существует – {func}.'
        super().__init__(self.message)


def main() -> None:
    """
    Создание фермы с разнообразными животными.
    Вывод информации по всем животным на ферме, возвраст которых предварительно
    был увеличен на один год.
    Вывод звуков, издаваемых каждым животным на ферме.
    Вывод инфорамции по случайно выбранному животному на ферме.
    Попытка узнать возраст курицы.
    """
    pig1, pig2 = Pig('pig1', 3), Pig('pig2', 5)
    cow1 = Cow('cow1', 6)
    goose1, goose2 = Goose('goose1', 1), Goose('goose2', 1)
    chicken1 = Chicken('chicken1', 1)
    chicken2 = Chicken('chicken2', 1)
    chicken3 = Chicken('chicken3', 1)
    my_farm = Farm(pig1, pig2, cow1, goose1, goose2,
                   chicken1, chicken2, chicken3)

    my_farm.all_animals()

    print(f'\nRandom animal from my farm: {my_farm[my_farm.random_number()]}.')

    print(chicken2.__age)


if __name__ == '__main__':
    main()
