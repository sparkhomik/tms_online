from collections import Counter

"""1. Перевести строку в массив "Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" => ["I", "love", "arrays", "they", "are",
 "my", "favorite"]"""
robin_str = 'Robin Singh'
robin_arr = robin_str.split()
love_str = 'I love arrays they are my favorite'
love_arr = love_str.split()
print(robin_arr)
print(love_arr)

"""2. Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”"""
ivan_arr = ['Ivan', 'Ivanou']
minsk_str = 'Minsk'
bel_str = 'Belarus'
print(f"Привет, {ivan_arr[0]} {ivan_arr[1]}! "
      f"Добро пожаловать в {minsk_str} {bel_str}")

"""3. Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => "I love arrays they are my favorite"""
love_arr = ["I", "love", "arrays", "they", "are", "my", "favorite"]
love_arr = ' '.join(love_arr)
print(love_arr)

"""4. Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
удалите элемент из списка под индексом 6"""
my_list = [1, 2, 3, 4, 5, 6, 7, 8, 9, 0]
my_list.insert(2, 'new')
del my_list[6]
print(my_list)

"""5. Есть 2 словаря a = { 'a': 1, 'b': 2, 'c': 3}
b = { 'c': 3, 'd': 4,'e': “”}
5.1. Создайте словарь, который будет содержать в себе все элементы обоих
словарей"""
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ""}
ab = {**a, **b}

"""5.2. Обновите словарь “a” элементами из словаря “b”"""
a.update(b)
print(a)

"""5.3. Проверить что все значения в словаре “a” не пустые либо не равны
нулю"""
print(all(a.values()))

"""5.4. Проверить что есть хотя бы одно пустое значение (результат выполнения
должен быть True)"""
print(any(a.values()))

"""5.5. Отсортировать словарь по алфавиту в обратном порядке"""
ab = list(ab.items())
ab.sort(reverse=True)
ab = dict(ab)
print(ab)

"""5.6. Изменить значение под одним из ключей и вывести все значения"""
ab['e'] = 100
print(ab.values())

"""6. Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
a. Вывести только уникальные значения и сохранить их в отдельную переменную"""
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
set_a = set(list_a)
new_list_a = list(set_a)
print(new_list_a)

"""b. Добавить в полученный объект значение 22"""
new_list_a.append(22)
print(new_list_a)

"""c. делать list_a неизменяемым"""
new_list_a = tuple(new_list_a)
print(new_list_a)

"""d. Измерить его длинну"""
print(len(new_list_a))

"""Задачи на закрепление форматирования: 1. Есть переменные a=10, b=25
Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
При решении задачи использовать оба способа форматирования"""
a = 10
b = 25
print(f"Summ is {a + b} and diff = {a - b}.")
print("Summ is {summ} and diff = {diff}.".format(summ=a + b, diff=a - b))

"""2. Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>, second is “<второе>”, and
 last one – “<третье>””"""
list_of_children = ["Sasha", "Vasia", "Nikalai"]
print(f"First child is {list_of_children[0]}, second is {list_of_children[1]}"
      f", and last one – {list_of_children[2]}")

"""*1) Вам передан массив чисел. Известно, что каждое число в этом массиве
 имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
 Напишите программу, которая будет выводить уникальное число"""
x = [1, 5, 2, 9, 2, 9, 1]
y = Counter(x)
print(min(y, key=y.get))


# еще способ:


def u(x):
    u = []
    for i in x:
        if i not in u:
            u.append(i)
        else:
            u.remove(i)
    return u


print(u(x))
