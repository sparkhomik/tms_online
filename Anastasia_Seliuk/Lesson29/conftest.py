import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def driver():
    """
    Запуск и закрытие драйвера Chrome.
    :return: экземпляр класса драйвера Chrome
    """
    chrome_options = webdriver.ChromeOptions()
    chrome_options.add_argument('--no-sandbox')
    chrome_options.add_argument('--headless')
    chrome_options.add_argument('--disable-gpu')
    driver = webdriver.Chrome(options=chrome_options)
    driver.maximize_window()
    driver.implicitly_wait(5)
    yield driver
    driver.quit()
