from selenium.webdriver.common.by import By


class BooksPageLocators:
    """
    Класс локаторов книг.
    """
    BOOKS_HEADER_LOCATORS = (By.CSS_SELECTOR, '.page-header>h1')
    BOOKS_PATH_LOCATORS = (By.CSS_SELECTOR, '.breadcrumb>.active')
    fic_xpath = '(//a[@href="/en-gb/catalogue/category/books/fiction_3/"])[2]'
    BOOKS_FICTION_LOCATORS = (By.XPATH, fic_xpath)
