from base_page import BasePage
from tools.ogin_page_locators import LoginPageLocators


class LoginPage(BasePage):
    """
    Класс страницы логина.
    """

    def get_title(self):
        return self.driver.title

    def get_login_form(self):
        return self.find_element(LoginPageLocators.LOGIN_FORM_LOCATORS)

    def get_register_form(self):
        return self.find_element(LoginPageLocators.REGISTER_FORM_LOCATORS)

    def get_header_name(self):
        return self.find_element(LoginPageLocators.LOGIN_HEADER_LOCATORS)
