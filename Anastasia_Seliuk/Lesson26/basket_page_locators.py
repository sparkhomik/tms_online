from selenium.webdriver.common.by import By


class BasketPageLocators:
    """
    Класс локаторов корзины.
    """
    BASKET_HEADER_LOCATORS = (By.CSS_SELECTOR, '.page-header>h1')
    BASKET_PATH_LOCATORS = (By.CSS_SELECTOR, '.breadcrumb>.active')
    BASKET_IS_EMPTY = (By.CSS_SELECTOR, '#content_inner>p')
