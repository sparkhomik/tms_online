from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Класс локаторов главной страницы.
    """
    MAIN_PAGE_LOCATORS = (By.CSS_SELECTOR, "#login_link")
    books_xpath = '//a[@href="/en-gb/catalogue/category/books_2/"]'
    BOOKS_PAGE_LOCATORS = (By.XPATH, books_xpath)
    BASKET_PAGE_LOCATORS = (By.XPATH, '//a[text()="View basket"]')
    SEARCH_INP_LOCATORS = (By.XPATH, '//input[@type="search"]')
    SEARCH_BT_LOCATORS = (By.XPATH, '//input[@type="submit"]')
