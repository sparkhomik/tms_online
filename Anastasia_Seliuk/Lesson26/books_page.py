from base_page import BasePage
from fiction_page import FictionPage
from tools.books_page_locators import BooksPageLocators


class BooksPage(BasePage):
    """
    Класс книг.
    """

    def get_title(self):
        """
        Получение заголовка окна.
        :return: заголовок окна
        """
        return self.driver.title

    def get_header_name(self):
        """
        Получение header.
        :return: header
        """
        return self.find_element(BooksPageLocators.BOOKS_HEADER_LOCATORS)

    def get_path_name(self):
        """
        Получение пути открытой страницы.
        :return:
        """
        return self.find_element(BooksPageLocators.BOOKS_PATH_LOCATORS)

    def open_fiction_page(self):
        """
        Открытие страницы Fiction.
        :return: открытая страница Fiction
        """
        fiction_link = self.find_element(
            BooksPageLocators.BOOKS_FICTION_LOCATORS)
        fiction_link.click()
        return FictionPage(self.driver, self.driver.current_url)
