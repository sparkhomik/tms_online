#!/bin/bash

if [[ "$1" -gt 0 ]]; then
echo "$1 положительное"
elif [[ "$1" -eq 0 ]]; then
echo "$1 равно нулю"
else
echo "$1 отрицательное"
fi
