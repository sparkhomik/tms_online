"""2ая – переводит переданное число в тип float, если передана строка,
вызывает пользовательскую ошибку InputTypeError (создать ее самому)"""


def float_number(number: any) -> float:
    """
    Преобразование числа в тип float. Если в функцию будет передана строка, то
    произойдет вызов пользовательской ошибки InputTypeError.
    :param number: переданное число
    :return: число, преобразованное в тип float
    """
    if type(number) == str:
        raise InputTypeError()
    return float(number)


class InputTypeError(Exception):
    """
    Класс для определения ошибки, которая будет выведена пользователю, в случае
    если была передана строка.
    """

    def __init__(self) -> None:
        """
        Определение сообщения об ошибке.
        """
        self.message = 'Type of number is string. It must be int or float.'
        super().__init__(self.message)


if __name__ == "__main__":
    print(float_number(15))
