import string

"""3яя – проверяет, является ли переданный аргумент спец символом"""


def check_special_characters(character: str) -> bool:
    """
    Проверка, является ли переданный аргумент спецсимволом.
    :param character: переданный аргумент
    :return: спецсимвол или нет
    """
    return character in string.punctuation


if __name__ == "__main__":
    check_special_characters('#')
