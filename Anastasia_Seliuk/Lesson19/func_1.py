"""1ая – проверяет что все буквы в переданном слове в нижнем регистре и
возвращает True либо False"""


def lowercase_word(word: str) -> bool:
    """
    Проверка того, что все буквы в слове в нижнем регистре.
    :param word:слово для проверки
    :return: в нижнем регистре все буквы в слове или нет
    """
    return word.islower()


if __name__ == "__main__":
    print(lowercase_word("word"))
