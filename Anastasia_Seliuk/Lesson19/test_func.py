import pytest

from func_1 import lowercase_word
from func_2 import float_number, InputTypeError
from func_3 import check_special_characters

"""2) Для каждой функции создать параметризированный тест. В качестве
параметров тест принимает значение, которое должна обработать функция и
ожидаемый результат ее выполнения.
3) Каждый тест должен быть вызван по 3 раза
4) Для 2-ой функции написать отдельный тест, который будет ожидать ошибку
InputTypeError, и если эта ошибка не произошла, падать!"""


@pytest.mark.parametrize('word, result', [('word', True), ('WoRd', False),
                                          ('worD', False)])
def test_lowercase_word(word: str, result: bool):
    """
    Проверка работы функции lowercase_word.
    :param word: слово для проверки
    :param result: в нижнем регистре все буквы в слове или нет
    """
    assert lowercase_word(word) == result


@pytest.mark.parametrize('number, result', [(15, 15.0), (222, 222.0),
                                            (-1, -1.0)])
def test_float_number(number: any, result: float):
    """
    Проверка работы функции float_number.
    :param number: переданное число
    :param result: число, преобразованное в тип float
    """
    assert float_number(number) == result


@pytest.mark.parametrize('number', ['a', '15', '15.0'])
def test_fail_float_number(number: str):
    """
    Проверка выдачи сообщения об ошибке при передаче строки в функцию
    float_number.
    :param number: переданное число
    """
    with pytest.raises(InputTypeError):
        float_number(number)


@pytest.mark.parametrize("character, result", [('@', True), ('0', False),
                                               ('A', False)])
def test_check_special_characters(character: str, result: bool):
    """
    Проверка работы функции check_special_characters.
    :param character: переданный аргумент
    :param result: спецсимвол или нет
    """
    assert check_special_characters(character) == result
