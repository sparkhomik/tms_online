import random
import math

'''1. a = 10, b=23, поменять значения местами, чтобы в переменную “a”
было записано значение “23”, в “b” - значение “-10”'''

# initialize a and b
a = 10
b = 23

# swap a and b
a, b = b, -a
print(a)
print(b)

'''2. значение переменной “a” увеличить в 3 раза, а значение “b” уменьшить
на 3'''

# increase a by 3 times
a *= 3

# decrease с by 3
b -= 3

'''3. преобразовать значение “a” из целочисленного в число с плавающей
точкой (float), а значение в переменной “b” в строку'''

# convert a from integer to float
a = float(a)

# convert b from integer to string
b = str(b)

'''4. Разделить значение в переменной “a” на 11 и вывести результат с точностью
3 знака после запятой'''

# divide a by 11 and print the result to 3 decimal places
print(round(a / 11, 3))

'''5. Преобразовать значение переменной “b” в число с плавающей точкой и
записать в переменную “c”. Возвести полученное число в 3-ю степень'''

# convert b from string to float and write result ro c
c = float(b)

# raise c to the 3rd degree
print(c ** 3)

'''6. Получить случайное число, кратное 3-м'''

# get a random number from 1 to 100 fold 3
print(random.randrange(0, 100, 3))

'''7. Получить случайное число, кратное 3-м
Получить квадратный корень из 100 и возвести в 4 степень'''

# get the square root of 100 and raise to the 4th power
print(math.sqrt(100) ** 4)

'''8. Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
“Hi guysHi guysHi guysToday”'''

# initialize new strings
my_string = "Hi guys"
my_string2 = "Today"

# initialize my_string3 equal my_string 3 times with addition of my_string2
my_string3 = my_string * 3 + my_string2

# print my_string3
print(my_string3)

'''9. Получить длину строки из предыдущего задания'''

# get the length of my_string3
print(len(my_string3))

'''10. Взять предыдущую строку и вывести слово “Today” в прямом и обратном
порядке'''

# initialize my_string3_desc equal my_string 3 times with addition of my_string
# 2 in reverse order
my_string3_desc = my_string * 3 + my_string2[::-1]

# print my_string3 and my_string3_desc
print(my_string3)
print(my_string3_desc)

'''11. “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву в прямом и
обратном порядке'''

# initialize commands as a separation of my_string3 to work with only letters
commands = my_string3.split()

# get the length of commands
print(len(commands))

# initialize commands_asc where every 2rd letter from commands in the
# forward order
commands_asc = [commands[0][1::2], commands[1][1::2], commands[2][1::2],
                commands[3][1::2]]

# initialize commands_desc where every 2rd letter from commands in the
# reverse order
commands_desc = list([commands[-1][-2::-2], commands[-2][-2::-2],
                      commands[-3][-2:-2], commands[-4][-2::-2]])

# print commands_asc and commands_desc with spaces
print(' '.join(commands_asc))
print(' '.join(commands_desc))

'''12. Используя форматирования подставить результаты из задания 10 и 11 в
следующую строку “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>,
<в обратном>”'''

# initialize big_string
big_string = f'Task10: {my_string3}, {my_string3_desc}' \
             f' Task11: {commands_asc}, {commands_desc}'

# print big_string
print(big_string)

'''13. Полученную строку в задании 12 вывести:
а) Каждое слово с большой буквы
б) все слова в нижнем регистре
в) все слова в верхнем регистре'''

# print big_string in which each word with a capital letter
print(big_string.title())

# print big_string in which all words are in lowercase
print(big_string.lower())

# print big_string in which all words are in uppercase
print(big_string.upper())

'''14. Посчитать сколько раз слово “Task” встречается в строке из задания 12'''

# count how many times the word "Task" occurs in big_string
print(big_string.count("Task"))
