from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def chrome_driver():
    """
    Запуск драйвера Chrome.
    :return: экземпляр класса драйвера Chrome
    """
    driver = Chrome(executable_path="c:/driver/chromedriver")
    return driver


def dynamic_controls_page(driver):
    """
    Открытие страницы Dynamic Controls в браузере.
    :param driver: экземпляр класса драйвера Chrome
    :return: экземпляр класса драйвера Chrome
    """
    driver.get("http://the-internet.herokuapp.com/dynamic_controls")
    return driver


def find_checkbox(driver):
    """
    Поиск чекбокса.
    :param driver: экземпляр класса драйвера Chrome
    :return: чекбокс
    """
    checkbox = driver.find_element_by_css_selector("[type='checkbox']")
    return checkbox


def click_on_remove_button(driver) -> None:
    """
    Поиск и нажатие кнопки Remove.
    :param driver: экземпляр класса драйвера Chrome
    """
    button = driver.find_element_by_xpath("//button[text()='Remove']")
    button.click()


def its_gone_label(driver) -> bool:
    """
    Ожидание появления надписи It’s gone!
    :param driver: экземпляр класса драйвера Chrome
    :return: результат ожидания
    """
    label = WebDriverWait(driver, 30).until(EC.text_to_be_present_in_element(
        (By.CSS_SELECTOR, "p#message"), "It's gone!"
    ))
    return label


def check_checkbox(driver, checkbox) -> bool:
    """
    Ожидание того, что чекбокс станет невидимым.
    :param driver: экземпляр класса драйвера Chrome
    :param checkbox: чекбока
    :return: результат ожидания
    """
    uv_checkbox = WebDriverWait(driver, 30).until(EC.invisibility_of_element(
        checkbox
    ))
    return uv_checkbox


def check_input(driver) -> bool:
    """
    Проверка достпуности ввода в поле ввода.
    :param driver: экземпляр класса драйвера Chrome
    :return: доступен ввод в поле или нет
    """
    input = driver.find_element_by_css_selector("input[type='text']")
    return input.is_enabled()


def click_on_enable_button(driver) -> None:
    """
    Нажатие кнопки Enable.
    :param driver: экземпляр класса драйвера Chrome
    """
    WebDriverWait(driver, 30).until(EC.element_to_be_clickable(
        (By.XPATH, "//button[text()='Enable']"),
    ))
    button = driver.find_element_by_xpath("//button[text()='Enable']")
    button.click()


def its_enabled_label(driver) -> bool:
    """
    Ожидание пояления надписи It's enabled!
    :param driver: экземпляр класса драйвера Chrome
    :return: появилась надпись или нет
    """
    label = WebDriverWait(driver, 30).until(EC.text_to_be_present_in_element(
        (By.CSS_SELECTOR, "p#message"), "It's enabled!"
    ))
    return label


if __name__ == "__main__":
    driver = chrome_driver()
    driver = dynamic_controls_page(driver)
    checkbox = find_checkbox(driver)
    click_on_remove_button(driver)
    assert its_gone_label(driver)
    assert check_checkbox(driver, checkbox)
    assert not check_input(driver)
    click_on_enable_button(driver)
    assert its_enabled_label(driver)
    assert check_input(driver)
