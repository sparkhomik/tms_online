from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


def chrome_driver():
    """
    Запуск драйвера Chrome.
    :return: экземпляр класса драйвера Chrome
    """
    driver = Chrome(executable_path="c:/driver/chromedriver")
    return driver


def frames_page(driver):
    """
    Открытие страницы Frames в браузере.
    :param driver: экземпляр класса драйвера Chrome
    :return: экземпляр класса драйвера Chrome
    """
    driver.get("http://the-internet.herokuapp.com/frames")
    return driver


def iframe_page(driver) -> None:
    """
    Поиск ссылки на страницу iFrame и перехоод на нее.
    :param driver: экземпляр класса драйвера Chrome
    """
    WebDriverWait(driver, 30).until(EC.element_to_be_clickable(
        (By.CSS_SELECTOR, "[href='/iframe']"),
    ))
    iframe = driver.find_element_by_css_selector("[href='/iframe']")
    iframe.click()


def check_content(driver) -> bool:
    """
    Проверка видимости текста 'Your content goes here.'
    :param driver: экземпляр класса драйвера Chrome
    :return: отображается текст 'Your content goes here.' или нет
    """
    driver.switch_to.frame(driver.find_element_by_tag_name("iframe"))
    WebDriverWait(driver, 30).until(EC.visibility_of_element_located(
        (By.CSS_SELECTOR, ".mce-content-body  p"),
    ))
    content = driver.find_element_by_css_selector(".mce-content-body p")
    return content.text == "Your content goes here."


if __name__ == "__main__":
    driver = chrome_driver()
    driver = frames_page(driver)
    iframe_page(driver)
    assert check_content(driver)
