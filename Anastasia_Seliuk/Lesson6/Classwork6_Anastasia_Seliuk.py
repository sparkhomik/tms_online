"""Практические задачи:
Функции:
1) Напишите функцию, которая возвращает строку: “Hello world!"""


def func():
    return 'Hello world!'


"""2) Напишите функцию, которая вычисляет сумму трех чисел и возвращает
результат в основную ветку программы"""


def sum_numbers(a, b, c):
    return a + b + c


# функции, использующие распаковку
def sum_numbers_2(*a):
    return sum(a)


def sum_numbers_3(*a):
    my_sum = 0
    for i in a:
        my_sum += i
    return my_sum


"""6) Напишите функцию, которая проверяет на то, является ли строка палиндромом
или нет. Палиндром — это слово или фраза, которые одинаково читаются слева
направо и справа налево."""


def pal(my_str):
    my_str = my_str.lower()
    my_str = ''.join(my_str.split())
    return list(my_str) == list(reversed(my_str))


"""7) У вас интернет магазин, надо написать функцию которая проверяет что
введен правильный купон и он еще действителен
def check_coupon(entered_code, correct_code, current_date, expiration_date):
    #Code here!
check_сoupon("123", "123", "July 9, 2015", "July 9, 2015")  == True
check_сoupon("123", "123", "July 9, 2015", "July 2, 2015")  == False"""


def check_coupon(entered_code, correct_code, current_date, expiration_date):
    return entered_code == correct_code and current_date == expiration_date


"""8) Фильтр. Функция принимает на вход список, проверяет есть ли эти элементы
в списке exclude, если есть удаляет их и возвращает список с оставшимися
элементами"""


def filter_list(my_list, exclude=["African", "Roman Tufted", "Toulouse",
                                  "Pilgrim", "Steinbacher"]):
    for i in exclude:
        if i in my_list:
            my_list.remove(i)
    return my_list


"""3) Придумайте программу, в которой из одной функции вызывается вторая.
При этом ни одна из них ничего не возвращает в основную ветку программы,
обе должны выводить результаты своей работы с помощью функции print()."""


def func1():
    def func2():
        print('I am function 2')

    print('I am function 1')
    func2()


"""4) Напишите функцию, которая не принимает отрицательные числа и возвращает
число наоборот. 21445 => 54421 123456789 => 987654321"""


def na(num):
    if int(num) > 0:
        return "".join(list(reversed(num)))


"""5) Напишите функцию fib(n), которая по данному целому неотрицательному n
 возвращает n-e число Фибоначчи."""


def fib(n):
    if n in (1, 2):
        return 1
    return fib(n - 1) + fib(n - 2)
