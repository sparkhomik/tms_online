import string

"""Шифр буквы в цифры"""


def phrase() -> str:
    """
    Введение пользователем фразы для шифрования.
    :return: враза для шифрования
    """
    while True:
        b, k = input("Введите фразу для шифрования:\n"), ""
        for i in b:
            k += i if not i.isdigit() and i not in string.punctuation else ""
        return k


def personal_cipher() -> str:
    """
    Шифрование фразы пользователя в цифры.
    :return: зашифрованная фраза
    """
    alp = string.ascii_lowercase
    p = phrase()
    c = [alp.index(x) + 1 for x in p.lower()]
    return ''.join(map(str, c))


if __name__ == '__main__':
    print(personal_cipher())
