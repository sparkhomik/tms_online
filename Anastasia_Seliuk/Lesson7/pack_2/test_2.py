import string

"""Шифр цезаря

Шифр Цезаря — один из древнейших шифров. При шифровании каждый символ
заменяется другим, отстоящим от него в алфавите на фиксированное число позиций.

Примеры:
hello world! -> khoor zruog!
this is a test string -> ymnx nx f yjxy xywnsl

Напишите две функции - encode и decode принимающие как параметр строку и
число - сдвиг."""


def cipher(x: str, y: str, e: str) -> str:
    """
    Шифрование или дешифрование строки с сохранением исходного регистра и
    знаков препинания.
    :param x: строка, введенная пользователем
    :param y: количество сдвигов
    :param e: номер операции, производимой над строкой (1 - шифрование,
    2 - дешифрование)
    :return: зашифрованная или расшифрованная строка строка
    """
    a, a_up, new_s = string.ascii_lowercase, string.ascii_uppercase, ''
    for i in x:
        f = a.find(i) if i in a else a_up.find(i)
        f += int(y) if e == '1' else f - int(y)
        f = f % 25 if f > 25 or f < 0 else f
        new_s += i if i in string.punctuation or i == " " else ''
        if i in a_up and e == '1':
            new_l = a_up[f]
        elif i in a and e == '1':
            new_l = a[f]
        elif i in a_up and e == '2':
            new_l = a_up[f]
        elif i in a and e == '2':
            new_l = a[f]
        new_s += new_l
    return new_s


def choice_op() -> str:
    """
    Выбор операции пользователем.
    :return: номер выбранной операции
    """
    while True:
        a = input("Выберите операцию:\n"
                  "1) Encode\n"
                  "2) Decode\n")
        if a == '1' or a == '2':
            return a
            break


def choice_sd() -> str:
    """
    Введение числа сдвигов пользователем.
    :return: число сдвигов
    """
    while True:
        b = input("Введите число сдвигов:\n")
        if b.isdigit():
            return b
            break


def code() -> str:
    """
    Шифрование/дешифрование строки с помощью шифра Цезаря.
    :return: зашифрованная или расшифрованная строка
    """
    z = choice_op()
    y = input("Введите фразу:\n")
    w = choice_sd()
    return cipher(y, w, z)


if __name__ == '__main__':
    print(code())
