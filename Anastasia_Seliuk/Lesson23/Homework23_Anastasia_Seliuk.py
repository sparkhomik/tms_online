from selenium.webdriver import Chrome
from logzero import logger

"""1.	Для следующего приложения: https://www.saucedemo.com/
2.	необходимо найти все локаторы для функциональных элементов, т.е. иконки,
кнопки, поля ввода и т.д.
a.	Нужно найти локаторы для окна Login
Залогиниться и найти все элементы в каталоге (standard_user)
b.	Вывести информацию по всем товарам, которая возвращает их название и цену
c.	Для всех остальных элементов создать методы, которые находят их на странице
и возвращают элемент
d.	* Предусмотреть логирование выполненных действий"""


def log_in(driver) -> None:
    """
    Авторизация пользователя на сайте.
    :param driver: экземпляр класса драйвера Chrome
    """
    username = driver.find_element_by_css_selector("#user-name")
    logger.debug("Username is found.")
    username.send_keys("standard_user")
    logger.debug("Username is entered.")
    password = driver.find_element_by_css_selector("#password")
    logger.debug("Password is found.")
    password.send_keys("secret_sauce")
    logger.debug("Password is entered.")
    login_button = driver.find_element_by_css_selector("#login-button")
    login_button.click()
    logger.info("User is logged in successfully!")


def all_products_info(driver) -> str:
    """
    Вывод информации по товарам, включая их название и цену.
    :param driver: экземпляр класса драйвера Chrome
    """
    names = driver.find_element_by_css_selector(".inventory_item_name")
    logger.debug("Names are found.")
    prices = driver.find_element_by_css_selector(".inventory_item_price")
    logger.debug("Prices are found.")
    all_products = list()
    for i in range(6):
        all_products.append(f"{names.text}: {prices.text}")
    logger.info("All products info are received.")
    return ", ".join(all_products)


def burger_menu_button(driver):
    """
    Поиск элемента бургер-меню.
    :param driver: экземпляр класса драйвера Chrome
    :return: элемент бургер-меню
    """
    burger_menu = driver.find_element_by_css_selector("#react-burger-menu-btn")
    logger.debug("Burger-menu is found.")
    return burger_menu


def logo(driver):
    """
    Поиск лого на странице.
    :param driver: экземпляр класса драйвера Chrome
    :return: лого
    """
    app_logo = driver.find_element_by_css_selector(".app_logo")
    logger.debug("Logo is found.")
    return app_logo


def cart(driver):
    """
    Поиск корзины.
    :param driver: экземпляр класса драйвера Chrome
    :return: корзина
    """
    shopping_cart = driver.find_element_by_css_selector(".shopping_cart_link")
    logger.debug("Shopping cart is found.")
    return shopping_cart


def header_container(driver):
    """
    Поиск заголовка страницы.
    :param driver: экземпляр класса драйвера Chrome
    :return: заголовок страницы
    """
    header = driver.find_element_by_css_selector(".header_secondary_container")
    logger.debug("Header is found.")
    return header


def product_sort(driver):
    """
    Поиск элемента сортировки.
    :param driver: экземпляр класса драйвера Chrome
    :return: элемент сортировки
    """
    sort = driver.find_element_by_css_selector(".product_sort_container")
    logger.debug("Product sort is found.")
    return sort


def add_to_cart(driver):
    """
    Поиск кнопки 'добавить в корзину'.
    :param driver: экземпляр класса драйвера Chrome
    :return: кнопка 'добавить в корзину'
    """
    to_cart = driver.find_element_by_css_selector(".btn_inventory")
    logger.debug("Add to cart button is found.")
    return to_cart


def products_images(driver):
    """
    Поиск изображения товара.
    :param driver: экземпляр класса драйвера Chrome
    :return: изображение товара
    """
    image = driver.fird_element_by_css_selector(".inventory_item_img")
    logger.debug("All products images are found.")
    return image


def twitter(driver):
    """
    Поиск кнопки перехода в твиттер.
    :param driver: экземпляр класса драйвера Chrome
    :return: кнопка перехода в твиттер
    """
    twit_xpath = "//a[@href='https://twitter.com/saucelabs']"
    twit = driver.fird_element_by_xpath(twit_xpath)
    logger.debug("Twitter button is found.")
    return twit


def facebook(driver):
    """
    Поиск кнопки перехода в фейсбук.
    :param driver: экземпляр класса драйвера Chrome
    :return: кнопка перехода в фейсбук
    """
    facebook_xpath = "//a[@href='https://facebook.com/saucelabs']"
    facebook_element = driver.fird_element_by_xpath(facebook_xpath)
    logger.debug("Facebook button is found.")
    return facebook_element


def linkedin(driver):
    """
    Поиск кнопки перехода в линкедин.
    :param driver: экземпляр класса драйвера Chrome
    :return: кнопка перехода в линкедин
    """
    lkdin_xpath = "//a[@href='https://www.linkedin.com/company/sauce-labs/']"
    linkedin_element = driver.fird_element_by_xpath(lkdin_xpath)
    logger.debug("Linkedin button is found.")
    return linkedin_element


def footer(driver):
    """
    Поиск подвала сайта.
    :param driver: экземпляр класса драйвера Chrome
    :return: подвал сайта
    """
    footer_element = driver.fird_element_by_css_selector(".footer")
    logger.debug("Footer is found.")
    return footer_element


if __name__ == "__main__":
    main_page_driver = Chrome(executable_path="c:/driver/chromedriver")
    main_page_driver.get("https://www.saucedemo.com/")
    logger.debug("Browser is open.")
    logger.debug("Main page is open.")
    main_page_driver.implicitly_wait(10)
    log_in(main_page_driver)
    print(all_products_info(main_page_driver))
    main_page_driver.quit()
    logger.debug("Browser is closed.")
