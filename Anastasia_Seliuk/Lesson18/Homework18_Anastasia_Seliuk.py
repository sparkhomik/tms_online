import platform

"""Написать класс для работы с текущей операционной системой:
Должно быть три метода:
- получить имя операционной системы
- релиз операционной системы
- версию OS"""


class Os:
    """
    Класс операционной системы.
    """
    @staticmethod
    def os_name() -> str:
        """
        Получение имени операционной системы.
        :return: имя операционной системы
        """
        return platform.system()

    @staticmethod
    def os_release() -> str:
        """
        Получение сведений о выпуске системы.
        :return: сведения о выпуске системы
        """
        return platform.release()

    @staticmethod
    def os_version() -> str:
        """
        Получение версии операционной ситемы.
        :return: версия операционной системы
        """
        return platform.version()


def main() -> None:
    """
    Вывод информации о текущей операционной системе.
    :return:
    """
    os = Os()
    print(os.os_name())
    print(os.os_release())
    print(os.os_version())


if __name__ == '__main__':
    main()
