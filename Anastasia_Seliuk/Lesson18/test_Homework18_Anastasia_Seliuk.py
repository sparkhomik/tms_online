import unittest
from Homework18_Anastasia_Seliuk import Os

"""Написать тесты для каждого метода
Должны быть написаны не только позитивные тесты, но и негативные, тем самым
показывая корректность работы программы."""


class TestOS(unittest.TestCase):
    """
    Класс тестов для операционной системы.
    """
    def test_name(self):
        """
        Проверка, что имя операционной системы - Windows.
        """
        self.assertEqual(Os.os_name(), "Windows")

    @unittest.expectedFailure
    def test_name_fail(self):
        """
        Проверка, что имя операционной системы не Darwin.
        :return:
        """
        self.assertEqual(Os.os_name(), "Darwin", "It's not Mac OS.")

    def test_release(self):
        """
        Проверка, что версия операционной системы - 10.
        """
        self.assertEqual(Os.os_release(), "10")

    @unittest.expectedFailure
    def test_release_fail(self):
        """
        Проверка, что версия операционной системы меньше 8.
        """
        self.assertLess(int(Os.os_release()), 8, "It's greater than 8"
                                                 " version.")

    def test_version(self):
        """
        Проверка, что релиз операционной системы - 10.0.14393.
        """
        self.assertEqual(Os.os_version(), "10.0.14393")

    @unittest.expectedFailure
    def test_version_fail(self):
        """
        Проверка, что релиз операционной системы больше 11.
        """
        self.assertGreater(Os.os_version(), "11.0", "It's less than 11"
                                                    " version.")
