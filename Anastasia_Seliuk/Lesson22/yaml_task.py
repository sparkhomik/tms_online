import yaml
import json

"""4)	работа с yaml файлом
* Напечатайте номер заказа
* Напечатайте адрес отправки
* Напечатайте описание посылки, ее стоимость и кол-во
* Сконвертируйте yaml файл в json
* Создайте свой yaml файл"""


def order_invoice() -> str:
    """
    Получение номера заказаю
    :return: номер заказа
    """
    with open("order.yaml") as order:
        template = yaml.safe_load(order)
    return str(template["invoice"])


def order_address() -> str:
    """
    Получение адреса отправки.
    :return: адрес отправки
    """
    with open("order.yaml") as order:
        template = yaml.safe_load(order)
    bill_to = template["bill-to"]
    return str(list(bill_to["address"].values()))[1:-1:]


def order_info() -> str:
    """
    Получение дополнительной информации о заказе: описание, ее стоимость и
    кол-во.
    :return: дополнительная информация о заказе
    """
    with open("order.yaml") as order:
        template = yaml.safe_load(order)
    products = template["product"]
    result = []
    for product in products:
        for k, v in product.items():
            if k != "sku":
                result.append((str(v)))
    return ", ".join(result)


def convert_to_json(file) -> None:
    """
    Конвертация yaml файла в json.
    """
    with open(file) as order, open("new_json_file.json", "w+") as write_file:
        template = yaml.safe_load(order)
        for keys in template:
            template[keys] = str(template[keys])
        json.dump(template, write_file)


def new_yaml_file(file_name) -> None:
    """
    Создание нового yaml-файла.
    """
    trunk_template = [
        'switchport trunk encapsulation dot1q', 'switchport mode trunk',
        'switchport trunk native vlan 999', 'switchport trunk allowed vlan'
    ]
    access_template = [
        'switchport mode access', 'switchport access vlan',
        'switchport nonegotiate', 'spanning-tree portfast',
        'spanning-tree bpduguard enable'
    ]
    template = {'trunk': trunk_template, 'access': access_template}
    with open(file_name, 'w') as write_file:
        yaml.dump(template, write_file)


if __name__ == "__main__":
    print(order_invoice())
    print(order_address())
    print(order_info())
    convert_to_json("order.yaml")
    new_yaml_file("new_yaml_file.yaml")
