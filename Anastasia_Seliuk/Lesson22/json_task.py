import json

"""3)	работа с json файлом
Разработайте поиск учащихся в одном классе, посещающих одну секцию.
Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)"""


def students_in_one_club(club: str) -> str:
    """
    Поиск учащихся в одном классе, посещающих одну секцию.
    :param club: название секции
    :return: учащиеся одного класса, посещающие одну и ту же секцию
    """
    file_obj = open("students.json", "r")
    content = file_obj.read()
    file_obj.close()
    content_json = json.loads(content)
    in_one_club_students = {}
    for student in content_json:
        if student["Club"] == club:
            in_one_club_students[student["Name"]] = student["Class"]
    result, students_list = [], []
    for cl in in_one_club_students.values():
        if cl not in result:
            result.append(cl)
        else:
            for k, v in in_one_club_students.items():
                if cl == v:
                    students_list.append(k)
    return ', '.join(students_list)


def gender_search(gender: str) -> str:
    """
    Поиск учащихся по полу.
    :param gender: пол
    :return: список учащихся искомого пола
    """
    file_obj = open("students.json", "r")
    content = file_obj.read()
    file_obj.close()
    content_json = json.loads(content)
    result = []
    for student in content_json:
        if student["Gender"] == gender:
            result.append(student["Name"])
    return ', '.join(result)


def name_search(name: str) -> str:
    """
    Поиск учащегося по имени.
    :param name: имя/часть имени учащегося
    :return: информация о найденном учощемся
    """
    file_obj = open("students.json", "r")
    content = file_obj.read()
    file_obj.close()
    content_json = json.loads(content)
    for student in content_json:
        if name in student["Name"]:
            return ', '.join(student.values())


if __name__ == "__main__":
    print(students_in_one_club("Chess"))
    print(gender_search("W"))
    print(name_search("Rio"))
