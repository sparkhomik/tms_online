import xml.etree.ElementTree as ET

"""2)	работа с xml файлом
Разработайте поиск книги в библиотеке по ее автору(часть имени)/цене/заголовку
/описанию."""


def books_search(attr: str) -> str:
    """
    Поиск книги по ее автору(часть имени)/цене/заголовку/описанию.
    :param attr: условие поиска
    :return: id книг, подходящих под условие
    """
    file = open("library.xml", "r")
    content = file.read()
    root = ET.fromstring(content)
    result = []
    for child in root:
        for grandchild in list(child):
            if attr in grandchild.text:
                result.append(child.attrib["id"])
    return ', '.join(result)


if __name__ == "__main__":
    print(books_search("Maeve Ascendant"))
