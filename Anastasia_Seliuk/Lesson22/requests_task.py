import requests
import json

"""1)	Используя сервис https://fakerestapi.azurewebsites.net/index.html и
requests библиотеку, написать запросы:
-	GET:
-	Получение списка авторов
-	Получение конкретного автора по его id
-	POST:
-	Добавить новую книгу
-	Добавить нового пользователя
-	PUT:
-	Обновить данные для книги под номером 10
-	DELETE:
-	Удалить пользователя под номером 4"""


def get_authors() -> list:
    """
    Получение списка авторов.
    :return: список авторов
    """
    address = 'https://fakerestapi.azurewebsites.net/api/v1/Authors'
    response = requests.get(address)
    content = json.loads(response.content)
    all_authors = []
    for author in content:
        all_authors.append(author["firstName"] + ' ' + author["lastName"])
    return all_authors


def get_author_search(author_id: int) -> str:
    """
    Получение конкретного автора по его id.
    :param author_id: id искомого автора
    :return: имя и фамилия автора
    """
    address = 'https://fakerestapi.azurewebsites.net/api/v1/Authors'
    response = requests.get(address)
    content = json.loads(response.content)
    info = ''
    for author in content:
        if author["id"] == author_id:
            info += author["firstName"] + ' ' + author["lastName"] + '\n'
            return info


def post_new_book() -> str:
    """
    Добавление новой книги.
    :return: ответ сервера
    """
    new_book = {"id": 555, "title": "string", "description": "string",
                "pageCount": 555, "excerpt": "string",
                "publishDate": "2021-10-23T19:42:14.750Z"}
    address = 'https://fakerestapi.azurewebsites.net/api/v1/Books/555'
    response = requests.post(address, new_book)
    return str(response)


def post_new_user() -> str:
    """
    Добавление нового пользователя.
    :return: ответ сервера
    """
    new_user = {"id": 555, "userName": "string", "password": "string"}
    address = 'https://fakerestapi.azurewebsites.net/api/v1/Users/555'
    response = requests.post(address, new_user)
    return str(response)


def put_book(book_id) -> str:
    """
    Обновить данные для книги под номером book_id.
    :param book_id: номер книги
    :return: ответ сервера
    """
    updated_book = {"id": book_id, "title": "string", "description": "string",
                    "pageCount": 555, "excerpt": "string",
                    "publishDate": "2021-10-23T19:42:14.750Z"}
    address = 'https://fakerestapi.azurewebsites.net/api/v1/Books/' \
              + str(book_id)
    response = requests.put(address, updated_book)
    return str(response)


def delete_user(user_id) -> str:
    """
    Удаление пользователя под номером user_id.
    :param user_id: номер пользователя
    :return: ответ сервера
    """
    address = 'https://fakerestapi.azurewebsites.net/api/v1/Users/' \
              + str(user_id)
    response = requests.delete(address)
    return str(response)


if __name__ == "__main__":
    print(get_authors())
    print(get_author_search(300))
    print(post_new_book())
    print(post_new_user())
    print(put_book(10))
    print(delete_user(4))
