"""Библиотека
Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
зарезервирована ли книги или нет. Создайте класс пользователь который может
брать книгу, возвращать, бронировать. Если другой пользователь хочет взять
зарезервированную книгу(или которую уже кто-то читает - надо ему про это
сказать)."""


class Book:
    all_books = dict()

    def __init__(self, title: str, author: str, pages: str, isbn: str) -> None:
        """
        Создание книги в библиотеке.
        :param title: имя книги
        :param author: автор
        :param pages: количество страниц
        :param isbn: ISBN
        """
        self.title = title
        self.author = author
        self.pages = pages
        self.isbn = isbn
        Book.all_books[title] = 'free'

    @staticmethod
    def check(atr: str) -> bool:
        """
        Проверка наличия книги в библиотеке.
        :param atr: наименование книги
        :return: книга в наличии или нет
        """
        return Book.all_books[atr] == 'free'


class User:

    def __init__(self, name: str) -> None:
        """
        Создание карточки читателя библиотеки.
        :param name: ФИО
        """
        self.name = name

    @staticmethod
    def take_book(atr: str) -> None:
        """
        Резервирование книги.
        :param atr: наименование книги
        :return: книга зарезервирована
        """
        Book.all_books[atr] = 'reserve'

    @staticmethod
    def return_book(atr: str) -> None:
        """
        Возврат книги.
        :param atr: наименование книги
        :return: книга вернулась в библиотеку и свободна для резервирования
        """
        Book.all_books[atr] = 'free'

    @staticmethod
    def reserve_book(atr: str) -> None:
        """
        Бронирование книги.
        :param atr: наименование книги
        :return: книга забронирована
        """
        Book.all_books[atr] = 'booked'


def book_title() -> str:
    """
    Получение названия книги от читателя.
    :return: название книги, с которой читатель хочет выполнить действие
    """
    a: str = ''
    b = Book.all_books
    while True:
        if a not in b.keys():
            a = input(f'Введите название книги из списка:\n{Book.all_books}\n')
            continue
        if not Book.check(a):
            print('Книга уже занята, выберите другую.')
            a = ''
        break
    return a


def book_operation() -> str:
    """
    Выбор операции, производимой читателем над выбранной книжкой.
    :return: номер операции, производимой читателем над выбранной книжкой
    """
    c = ''
    while True:
        if c not in ['1', '2', '3']:
            c = input('Введите номер операции, которую хотите '
                      'выполнить:\n1. Взять\n2. Вернуть\n'
                      '3. Забронировать\n')
            continue
        return c


def main_1() -> None:
    """
    оздание библиотеки и произведение пользователем операции над выбранной
    книгой (резервирование, возврат или бронирование)
    """
    Book('title1', 'author1', '100', '101')
    Book('title2', 'author2', '200', '202')
    Book('title3', 'author3', '300', '303')
    a = book_title()
    c = book_operation()
    if c == '1':
        User.take_book(a)
    elif c == '2':
        User.return_book(a)
    else:
        User.reserve_book(a)


"""Банковский вклад
Создайте класс инвестиция. Который содержит необходимые поля и методы,
например сумма инвестиция и его срок. Пользователь делает инвестицию в размере
N рублей сроком на R лет под 10% годовых (инвестиция с возможностью
ежемесячной капитализации - это означает, что проценты прибавляются к сумме
инвестиции ежемесячно). Написать класс Bank, метод deposit принимает аргументы
N и R, и возвращает сумму, которая будет на счету пользователя."""


class Investment:

    def __init__(self, summa: str, term: str) -> None:
        """
        Создание нового депозита.
        :param summa: сумма депозита
        :param term: количество лет, на которое оформляется депозит
        """
        self.summa = summa
        self.term = term


class Bank:

    @staticmethod
    def deposit_amount(n: str, r: str) -> float:
        """
        Расчет суммы на счету пользователя к окончанию срока депозита.
        :param n: первоначальная сумма депозита
        :param r: количество лет
        :return: сумма на счету пользователя к окончанию срока депозита
        """
        kol = int(n) * (1 + 0.1 / 12) ** (12 * int(r))
        return kol


def deposit_sum() -> str:
    """
    Определение суммы вклада пользователем.
    :return: сумма вклада
    """
    while True:
        a = input('Введите сумму вклада: \n')
        if a.isdigit():
            break
    return a


def deposit_term() -> str:
    """
    Определение срока вклада пользователем.
    :return: срок вклада
    """
    while True:
        b = input('Количество лет: \n')
        if b.isdigit():
            break
    return b


def main_2() -> float:
    """
    Расчет суммы на счету пользователя к окончанию срока депозита.
    :return: сумма на счету пользователя к окончанию срока депозита
    """
    a = deposit_sum()
    b = deposit_term()
    inv1 = Investment(a, b)
    return Bank.deposit_amount(inv1.summa, inv1.term)


if __name__ == '__main__':
    main_1()
    print(main_2())
