from unittest import TestCase
from info_my_computer import System


class TestWindows(TestCase):
    def test_name_os(self):
        # Тест метода name_os
        self.assertEqual(System.name_os(), 'Windows')

    def test_release(self):
        # Тест метода release
        self.assertEqual(System.release(), '10')

    def test_version(self):
        # Тест метода version
        self.assertEqual(System.version(), '10.0.19042')

    def test_failed_name_os(self):
        # Тест метода name_os на неравенство
        self.assertNotEqual(System.name_os(), 'Mac')

    def test_failed_release(self):
        # Тест метода release на неравенство
        self.assertNotEqual(System.release(), '1880')

    def test_failed_version(self):
        # Тест метода version на неравенство
        self.assertNotEqual(System.version(), '10.0.159042')
