import platform


class System:
    @staticmethod
    def name_os():
        """
        Возвоащет название ОС
        :return:
        """
        return platform.system()

    @staticmethod
    def release():
        """
        Возвращает номер релиза
        :return:
        """
        return platform.release()

    @staticmethod
    def version():
        """
        Возвращает номер версии
        :return:
        """
        return platform.version()


if __name__ == '__main__':
    os = System()
    print(os.name_os())
    print(os.release())
    print(os.version())
