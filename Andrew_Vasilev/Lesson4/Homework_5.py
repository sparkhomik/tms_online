"""
Task 1
Перевести строку в массив
"Robin Singh" => ["Robin”, “Singh"]
"""
str_1 = "Robin Singh"
print(str_1.split())
"""
Перевести строку в массив
"I love arrays they are my favorite" =>
["I", "love", "arrays", "they", "are", "my", "favorite"]
"""
str_2 = "I love arrays they are my favorite"
print(str_2.split())

"""
Task 2
"""
list_1 = ['Ivan', 'Ivanov']
str_3 = 'Minsk Belarus'
print(f"Привет, {' '.join(list_1)}! Добро пожаловать {str_3[:]}")
"""
Task3
Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => "I love arrays they are my favorite"
"""
list_2 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
print(' '.join(list_2))
"""
Task4
Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
удалите элемент из списка под индексом 6
"""
list_3 = ["oka", "niva", "21015", "2109", "bmw",
          "peugeot", "reno", "skoda", "lada", "priora"]
print(list_3)
list_3[2] = "tagaz"  # 3 позиция, но 2 индекс
print(list_3)
# Удаление элемента под 6 индексом
del list_3[6]
print(list_3)
"""
Task5
Есть 2 словаря:
a = { 'a': 1, 'b': 2, 'c': 3}
b = { 'c': 3, 'd': 4,'e': “”}
"""
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ""}
"""
5.1. Создайте словарь, который будет
содержать в себе все элементы обоих словарей
"""
a_b = a | b
print(a_b)

"""
5.2. Обновите словарь “a” элементами из словаря “b”
"""
a.update(b)
print(a)
"""
5.3. Проверить что все значения в словаре “a” не пустые либо не равны нулю
"""
print(all(a.values()))
"""
5.4. Проверить что есть хотя бы одно пустое значение (результат выполнения
должен быть True)
"""
print(not all(a.values()))
"""
5.5. Отсортировать словарь по алфавиту в обратном порядке
"""
print(sorted(a, reverse=True))
"""
5.6. Отсортировать словарь по алфавиту в обратном порядке
"""
a['a'] = 13
for p in a.values():
    print(p)

"""
Task 6
"""
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
# Вывести только уникальные значения и сохранить их в отдельную переменную
list_b = set(list_a)
print(list_b)
# Добавить в полученный объект значение 22
list_b.add(22)
print(list_b)
# Сделать list_a неизменяемым
list_a = tuple(list_a)
print(list_a)
# Измерить его длинну
print(len(list_a))

"""
Задачи на закрепление форматирования:
"""
a = 10
b = 25
"""
Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
При решении задачи использовать оба способа форматирования
"""
print(f"Summ is {a + b} and diff = {a - b}.")
print("Summ is {c} and diff = {d}.".format(c=a + b, d=a - b))
"""
Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>,
second is “<второе>”, and last one – “<третье>””
"""
list_of_children = ["Sasha", "Vasia", "Nikalai"]
print(f"First child is {' '.join(list_of_children[0:1])},"
      f"second is {' '.join(list_of_children[1:2])}, "
      f"last one - {' '.join(list_of_children[-1:])}")
