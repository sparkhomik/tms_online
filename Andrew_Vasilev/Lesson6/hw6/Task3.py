print('Выберите операцию:')
print('1. Сложение')
print('2. Вычитание')
print('3. Умножение')
print('4. Деление')
a = input()
a_1 = None

if a == '1':
    a_1 = input('ВВОДИТЕ ЧИСЛА ЧЕРЕЗ ЗАПЯТУЮ: ')
elif a == '2':
    a_1 = input('ВВОДИТЕ ЧИСЛА ЧЕРЕЗ ЗАПЯТУЮ: ')
elif a == '3':
    a_1 = input('ВВОДИТЕ ЧИСЛА ЧЕРЕЗ ЗАПЯТУЮ: ')
elif a == '4':
    a_1 = input('ВВОДИТЕ ЧИСЛА ЧЕРЕЗ ЗАПЯТУЮ: ')


def change_func(var_string: str) -> float:
    """
    Функция change_func - отвечает за преобразование.
    Принимает строчный элемент.
    Возвращает список с числами с плавающей точкой.
    """
    var_args_string = var_string.replace(" ", "")
    var_args = var_args_string.split(",")
    var_args_int = [float(x) for x in var_args]
    return var_args_int


"""
Блок отвечающий за сложение всех
чисел находящихся внутри списка
name_sum
"""
if a == '1':
    name_sum1 = change_func(a_1)
if a == '1':
    def name_sum(arg_sum: list) -> float:
        """
        Функция name_sum суммирует все числа
        находящиеся в списке name_sum1.
        Принимает список с числами с плавающей точкой.
        Возвращает сумму чисел находящихя в списке.
        """
        return sum(arg_sum)
    print(name_sum(name_sum1))
"""
Блок отвечающий за вычитиание всех
чисел находящихся внутри списка
name_sub1
"""

if a == '2':
    name_sub1 = change_func(a_1)
if a == '2':
    def sub_func(arg_sub: list) -> float:
        """
        Функция sub_func вычитает все числа
        находящиеся в списке name_sub1.
        Принимает список с числами с плавающей точкой.
        Возращает разность чисел находящихся в списке.
        """
        if len(arg_sub) == 0:
            return arg_sub[0]
        elif len(arg_sub) > 0:
            x = arg_sub[0] - sum(arg_sub[1:])
        return x
    print(sub_func(name_sub1))
"""
Блок отвечающий за умножение всех
чисел находящихся внутри списка
multi_func1
"""
if a == '3':
    multi_func1 = change_func(a_1)
if a == '3':
    def multi_func(arg_multi: list) -> float:
        """
        Функция multi_func - отвечает за умножене
        всех чисел находящихся внутри списка multi_func1.
        Принимает список с числами с плавающей точкой.
        Возращает произведение чисел находящихся в списке.
        """
        n = 1
        for el in arg_multi:
            n *= el
        return n
    print(multi_func(multi_func1))
"""
Блок отвечающий за деление всех
чисел находящихся внутри списка
div_func1
"""
if a == '4':
    div_func1 = change_func(a_1)
if a == '4':
    def div_func(arg_div: list) -> float:
        """
        Функция div_func - отвечает за деление
        всех чисел находящихся внутри списка
        div_func1.
        Принимает список с числами с плавающей точкой.
        Возращает частное чисел находящихся в списке.
        """
        for i in arg_div[1:]:
            arg_div[0] = arg_div[0] / i
        return arg_div[0]
    print(div_func(div_func1))
