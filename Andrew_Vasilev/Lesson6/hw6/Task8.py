"""
Написать функцию, которая проверяет есть
ли в списке объект, которые можно вызвать
"""


# Используем функцмю callable

def call_of_duty(a: list) -> bool:
    for i in a:
        if callable(i) is True:
            return True
        elif callable(i) is False:
            return False


print(call_of_duty([1, 2, "a", (1, 2), "b"]))
print(call_of_duty([str, list, int, tuple]))
