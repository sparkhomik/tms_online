from functools import reduce


def validate(code):
    # Предварительно рассчитанные результаты
    # умножения на 2 с вычетом 9 для больших цифр
    # Номер индекса равен числу, над которым
    # проводится операция
    index_1 = (0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
    code = reduce(str.__add__, filter(str.isdigit, code))
    evens = sum(int(i) for i in code[-1::-2])
    odds = sum(index_1[int(i)] for i in code[-2::-2])
    return (evens + odds) % 10 == 0


print(validate('4561 2612 1234 5467'))
print(validate('4561 2612 1234 5464'))
