from collections import Counter

"""
Используем библиотеку collections с типом данных Counter
"""

a = input('Введите слово ')

dictionary = Counter(a)
# Исользоуем форматирование с циклом.
# Цикл проходится по словарю и записываем
# в key: букву, value: количество букв
result = ''.join([f'{key}{value}' for key, value in dictionary.items()])

print(result)
