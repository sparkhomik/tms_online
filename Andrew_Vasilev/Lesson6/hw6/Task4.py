def func(a: int, *b: int, c=None, **d: str) -> dict:
    """
    Функция принимает позиционные и именные аргумента
    """
    x = {'mandatory_position_argument': a,
         "additional_position_arguments": b,
         'mandatory_named_argument': {'name': c},
         'additional_named_arguments': d}
    return x


print(func(1, 2, 3, c='man', name="Andrew", surname="Vasilev", some="Toyota"))
