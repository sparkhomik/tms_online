"""
if
"""
# Task 1.1
a = 1
if a > 0:
    print("You are rich")

b = -1
if b < 0:
    print('You are false')
# Task 1.2
a = 0
if a > 0:
    print("You are rich")
else:
    print(-1)
# Task 1.3
c = [1, 2, 3, 4, 5]
d = [1, 2, 3]
if c == d:
    c.reverse()
    print(c)
elif a > b:
    print("You are win")
else:
    print("Game over")
# Отработал блок elif, так как блок if выдал false
