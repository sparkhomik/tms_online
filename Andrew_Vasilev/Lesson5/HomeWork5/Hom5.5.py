a = [1, 1, 1]
print(all(i == a[0] for i in a))
b = [1, 2, 1]
print(all(i == b[0] for i in b))
c = ['a', 'a', 'a']
print(all(i == c[0] for i in c))
d = []
print(all(i == d[0] for i in d))
