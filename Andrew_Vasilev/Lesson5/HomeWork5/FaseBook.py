print('Введите имена через запятую:')
str_1 = input()
list_1 = str_1.split(', ')

if len(list_1) == 0:
    print("No one likes this")
else:
    if len(list_1) == 1:
        list_1 = ' '.join(list_1)
        print(f" {list_1[0]} like this")
    elif len(list_1) == 2:
        print(f"{list_1[0]} and {list_1[1]} like this")
    elif len(list_1) == 3:
        print(f"{list_1[0]}, {list_1[1]} and {list_1[2]} like this")
    elif len(list_1) > 3:
        other_1 = int(len(list_1)) - 2
        print(f"{list_1[0]}, {list_1[1]} and {other_1} others like this")
