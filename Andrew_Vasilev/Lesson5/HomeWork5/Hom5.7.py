one = []
two = [1, 2, 3]
three = [1.1, 2.2, 3.3]
for_1 = [4, 5, 6]
five = range(101)

print(sum(one))
print(sum(two))
print(sum(three))
print(sum(for_1))
print(sum(five))
