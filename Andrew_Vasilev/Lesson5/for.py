# Task 1.1
list_1 = ["red", "green", "yellow", "blue"]
list_2 = ['-']
for i in list_1:
    print(i)
# Task 1.2
for i in list_1:
    for j in i:
        j = j + ''.join(list_2)
        print(j)
# Task 1.3
list_int_1 = [1, 2, 3, 4, 5]
for i in list_int_1:
    j = float(i)
    print(j)
