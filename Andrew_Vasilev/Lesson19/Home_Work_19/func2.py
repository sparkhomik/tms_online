"""
Переводит переданное число в тип float, если передана строка вызывает
пользовательскую ошибку InputTypeError (создать ее самому)
"""


class InputType(Exception):
    pass


def function_2(number: int) -> float:
    if type(number) == str:
        raise InputType("Ты передал строку, а должен число")
    return float(number)
