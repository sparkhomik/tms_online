"""
Параметризированный тест для функции № 1
"""

import pytest


@pytest.mark.parametrize("test_input, test_out", [("word", True)])
def test_data(test_input, test_out):
    assert test_input.islower() == test_out


@pytest.mark.parametrize("test_input, test_out", [("worD", True)])
def test_data_failed(test_input, test_out):
    assert test_input.islower() == test_out
