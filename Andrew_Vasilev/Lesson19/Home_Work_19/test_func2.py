"""
Параметризированный тест для функции № 2
"""

import pytest

from func2 import function_2, InputType


def test_function_2():
    with pytest.raises(InputType):
        assert function_2("car") == float


def test_failed_function_2():
    with pytest.raises(InputType):
        assert function_2(5) == 5.0
