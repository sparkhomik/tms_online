import pytest
import time

"""
5) Написать фикстуру, которая будет
   выводить время начала запуска всех
   тестов и когда все тесты отработали.
"""


@pytest.fixture(scope="session", autouse=True)
def time_test():
    print(f"Время начало запуска теста {time.ctime(time.time())}")
    yield
    print(f"Время завершения теста {time.ctime(time.time())}")


"""
6) Написать фикстуру, которая будут выводить имя файла,
      из которого запущен тест.
"""


@pytest.fixture(autouse=True)
def info_output_from_test(request):
    print("test case ID:", request.node.nodeid)
    print("test case name:", request.node.name)
    print("test module name:", request.node.module.__name__)
    print("test file:", request.fspath)


"""
7) Написать фикстуру, которая для каждого теста
   будет выводить следующую информацию:
   А) Информацию о фикстурах, которые
      используются в этом тесте
   Б) Информацию о том, сколько времени прошло
      между запуском сессии и запуском текущего
      теста
"""


@pytest.fixture(autouse=True)
def all_info_fixture_test(request):
    tic = time.perf_counter()
    print("Информацию о фикстурах, которые используются в этом тесте")
    print(*request.fixturenames)
    yield
    toc = time.perf_counter()
    print(f"Время выполнение теста {toc - tic:0.4f} секунд")
