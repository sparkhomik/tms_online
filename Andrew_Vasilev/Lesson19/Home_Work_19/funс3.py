"""
Проверяет, является ли переданный аргумент спец символом
"""


def special_character(character):
    a = [".", "," ":", ";", "!", "_", "*",
         "-", "+", "()", "/", "/", "#", "¤",
         "%", "&"]
    for i in a:
        if i in character:
            return True
