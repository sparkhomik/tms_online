"""
Параметризированный тест для функции № 3
"""

import pytest

from funс3 import special_character


@pytest.mark.parametrize("input_data, output_data",
                         [(special_character("!"), True)])
def test_special_character(input_data, output_data):
    assert input_data == output_data


@pytest.mark.parametrize("input_data, output_data",
                         [(special_character("l"), True)])
def test_failed_special_character(input_data, output_data):
    assert input_data == output_data
