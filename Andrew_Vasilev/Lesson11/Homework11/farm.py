from abc import ABC, abstractmethod


class NotExistException(Exception):
    """
    custom exception
    """
    message = "Уважаемый пользователь, ваше действие это безумие !!!"

    def __init__(self, message):
        super().__init__(self.message)


class Bird(ABC):
    voice = None

    def __init__(self, name: str, age: int):
        """
        Метод конструктор класса Bird
        :param name: Имя будщего экзмемпляра класса
        :param age: Возраст будущего экземпляра класса
        """
        self.name = name
        self.__age = age
        # self.()
        self.voice_call()

    def __str__(self):
        n_1 = 'Имя птички: '
        a_1 = 'Сколько ей годиков: '
        c = 'Злобный кудах: '
        e = n_1 + self.name + " " + a_1 + str(self.__age) + " " + c + \
            self.voice_call()
        return e

    @property
    def age(self):
        """
        :return: Имя и возсраст экземпляра класса
        """
        return self.name, self.__age

    @staticmethod
    def fly():
        """Выполняет функионал полета птичек :)"""
        return 'I believe, I can fly'

    def get_old(self):
        """Метод делающий всех птичек старше на один год"""
        self.__age += 1

    @abstractmethod
    def voice_call(self):
        raise NotImplementedError

    def __getattr__(self, item):
        raise NotExistException('Данный метод или атрибут '
                                'не определен в классе Bird')


class Chicken(Bird):
    def voice_call(self):
        return 'Кудах'


class Goose(Bird):
    def voice_call(self):
        return 'Гага'


class Animals(ABC):
    voice = None

    def __init__(self, name: str, age: int):
        """
        Метод конструктор класса Animals
        :param name: Имя будщего экзмемпляра класса
        :param age: Возраст будущего экземпляра класса
        """
        self.name = name
        self.__age = age
        self.voice_call()

    @staticmethod
    def go():
        """Метод выполняющий функцию бега всех животных"""
        return 'Run, Vasily, Run'

    def __str__(self):
        n_1 = 'Имя зверюшки: '
        a_1 = 'Сколько ей годиков: '
        c = 'Злобный рык: '
        e = n_1 + self.name + " " + a_1 + str(self.__age) + " " + c + \
            self.voice_call()
        return e

    @property
    def age(self):
        """
        :return: Имя и возсраст экземпляра класса
        """
        return self.name, self.__age

    def get_old(self):
        """Метод делающий всех птичек старше на один год"""
        self.__age += 1

    @abstractmethod
    def voice_call(self):
        raise NotImplementedError

    def __getattr__(self, item):
        raise NotExistException('Данный метод или атрибут'
                                ' не определен в классе'
                                ' Animals')


"""Создание класса наследованного от класса Animals"""


class Pig(Animals):
    def voice_call(self):
        return 'ХРЮ'


class Cow(Animals):
    def voice_call(self):
        return 'МУ-му-МУ'


"""Создание класа Farm принимающая на себя всех животных и птичек"""


class Farm:
    def __init__(self, *animals_list):
        self.animals_list = list(animals_list)
        self.index = 0

    def __len__(self):
        return len(self.animals_list)

    def __iter__(self):
        self.index = 0
        return self

    def __next__(self) -> object:

        """
        Возвращение следующего животного на ферме.
        :return: следующее животное на ферму.
        """
        try:
            item = self[self.index]
        except IndexError:
            raise StopIteration()
        self.index += 1
        return item

    def __getitem__(self, index):
        return self.animals_list[index]

    def make_older_animal(self):
        a = []
        for i in self.animals_list:
            a.append(i.get_old())
        return a

    def watching_age(self):
        all_animals_birds = []
        for i in self.animals_list:
            all_animals_birds.append(i.age)
        print(all_animals_birds)


# Создание экземпляра класса Bird
chicken1 = Chicken('Дуся', 0)
goose1 = Goose('Зак', 0)

# Создание экземпляра класса Animals
pig1 = Pig('Фунтик', 0)
cow1 = Cow('Буренка', 0)


def main():
    farm1 = Farm(chicken1, goose1, pig1, cow1)
    # Экземпляры классов в уодбном формате
    print(chicken1)
    print(goose1)
    print(pig1)
    print(cow1)
    # Метод make_older_animal делающий всех животных старше
    farm1.make_older_animal()
    # Метод показывающий возраст и имя живтного
    farm1.watching_age()

    # Получение каждого животного живущее на ферме
    # с помощью цикла for
    for i in farm1:
        print(i)

    # Получение каждого животного с помощью индекса
    print(farm1[0])
    print(farm1[1])
    print(farm1[2])
    print(farm1[3])

    # Вызов не существующего метода
    chicken1.voice_callss()


if __name__ == "__main__":
    main()
