from collections import Counter


# task 1
# Проверить может ли такая карта существовать
# Предусмотреть защиту от ввода букв, пустой строки и т.д.
def validate(card_num: str):
    """
    Эта Функция проверяет карту на наличие любых символов, кроме цифр,
    а также на пустой объект.
    :param card_num: номер карты, которая валидируется
    :return: функция, которая запустится только при успешно пройденной первой
    валидации
    """
    if bool(card_num) and card_num.isdigit():
        return print(card_num_validate(card_num))
    return print('This is not a card name')


def card_num_validate(card_num: str) -> bool:
    """
    Эта функция проверяет может ли карта с введенным номером существовать.
    :param card_num: номер карты
    :return: булевле значение True/False
    """
    card_num_list = [int(i) for i in card_num]
    evens = card_num_list[-2::-2]
    odds = card_num_list[-1::-2]
    evens = [(i * 2 // 10) + (i * 2 % 10) for i in evens]
    return sum(evens + odds) % 10 == 0


card_num = input()
validate(card_num)


# task 2
# Подсчет количества букв
# На вход подается строка, например, "cccbba"
# результат работы программы - строка “c3b2a"
def letters_count(line: str) -> str:
    """
    Функция, которая считает количество одинаковых букв, стоящих рядом в строке
    :param line: строка, которая подается функции для обработки
    :return: итогловая строка, в которой посчитаны одинаковые, стоящие рядом
    буквы и вырезаны 1
    """
    count = 1
    new_line = ''
    for i in range(len(line) - 1):
        if line[i] == line[i + 1]:
            count += 1
        else:
            new_line += line[i] + str(count)
            count = 1
    new_line += line[-1] + str(count)
    return new_line.replace('1', '')


line = input()
print(letters_count(line))


# task 3
# Простейший калькулятор v0.1
def simple_calculator(option: int, num_1: int, num_2: int):
    """
    Функция, руализующая простейший калькулятор с 4мя возможными операциями
    :param option: операция, запрашиваемая пользователем
    :param num_1: первое число, вводимое пользователем
    :param num_2: второе число, вводимое пользователем
    :return: результат операции
    """
    if option != 1 and option != 2 and option != 3 and option != 4:
        return print('Введите код существующей операции: 1. Сложение, '
                     '2. Вычитание, 3. Умножение, 4. Деление.')
    if option == 1:
        return num_1 + num_2
    if option == 2:
        return num_1 - num_2
    if option == 3:
        return num_1 * num_2
    if option == 4:
        if num_2 == 0:
            return 'Нельзя делить на 0'
        else:
            return 'Частное:', num_1 // num_2, 'Остаток:', num_1 % num_2


option = int(input('Выберите операцию: 1. Сложение, 2. Вычитание,'
                   ' 3. Умножение, 4. Деление. Введите номер пункта меню:'))
num_1 = int(input('Введите первое число:'))
num_2 = int(input('Введите второе число:'))
print(simple_calculator(option, num_1, num_2))


# task 4
# Написать функцию с изменяемым числом входных параметров
def function(a, *args, name, **kwargs) -> dict:
    """
    Функция с изменяемым числом входных параметров
    :param a: позиционный аргумент
    :param args: несколько позиционных аргументов
    :param name: именованный аргумент, по умолчанию равен None
    :param kwargs: несколько именованных аргументов
    :return:
    """
    name = {'name': name}
    result = {'mandatory_position_argument': a,
              'additional_position_arguments': args,
              'mandatory_named_argument': name,
              'additional_named_arguments': kwargs}
    return result


result = function(1, 2, 3, name='test', surname='test2', some='something')
print(result)


# task 5
# Работа с областями видимости
def change_list_function(my_list: list) -> list:
    """
    Функция, которая принимает на вход список и добавляет в него
    элементы.
    :param my_list: список, который в функции будет изменен
    :return: измененный список
    """
    my_list = my_list.copy()
    my_list.append(4)
    return my_list


my_list = [1, 2, 3]
changed_list = change_list_function(my_list)
print(my_list)
print(changed_list)


# task 6
# Функция проверяющая тип данных


def my_function(list: list) -> dict:
    """
    Функцию, которая принимает на фход список из чисел, строк и таплов.
    :param list: список, который подается функции для подсчета элементов
    :return: словарь, с посчитанным количеством элементов приведенных данных
    """
    list_type = []
    for i in list:
        list_type.append(type(i))

    return Counter(list_type)


list = [1, 2, 'a', (1, 2), 'b']
print(my_function(list))

# task 7
# Написать пример чтобы hash от объекта 1 и 2 были одинаковые, а id разные.
a = -1
b = -2
print(id(a), id(b))
print(hash(a), hash(b))


# task 8
# написать функцию, которая проверяет есть ли в списке объект,
# которые можно вызвать

def call_me_function(list: list, result: bool) -> bool:
    """
    Функция, которая проверяет есть ли в списке объект, который можно вызвать.
    :param list: список, который подается функции
    :param result: метка, по умолчанию - False,
    если объект обнаружится - меняем на True
    :return: значение метки result
    """
    for i in list:
        if callable(i):
            result = True
            break
    return result


list = ['Hello', type('Hello'), [5, 6]]
print(call_me_function(list, result=False))
