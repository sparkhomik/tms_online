# Написать класс для работы с текущей операционной системой:
# Должно быть три метода:
# - получить имя операционной системы
# - релиз операционной системы
# - версию OS

import platform


class MyPlatform:
    """
    Класс для работы с текущей операционной системой.
    """

    @staticmethod
    def platform_system() -> str:
        """
        Статический метед класса MyPlatform. Использует функцию system()
        из импортированного модуля platform для получения имени текущей ОС.
        :return: имя ОС
        """
        return platform.system()

    @staticmethod
    def platform_release() -> str:
        """
        Статический метед класса MyPlatform. Использует функцию release()
        из импортированного модуля platform, получаем релиз текущей ОС.
        :return: релиз текущей ОС
        """
        result = platform.release()
        return result

    @staticmethod
    def platform_version() -> str:
        """
        Статический метед класса MyPlatform. Использует функцию version()
        из импортированного модуля platform, получаем версию текущей OС.
        :return: версия текущей OС.
        """
        result = platform.version()
        return result


def main():
    """
    Основная функция программы. Выводит на печать результаты методов класса.
    :return:
    """
    print(MyPlatform.platform_system())
    print(MyPlatform.platform_release())
    print(MyPlatform.platform_version())


if __name__ == "__main__":
    main()
