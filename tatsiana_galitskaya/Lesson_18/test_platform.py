# Написать тесты для каждого метода
# Должны быть написаны не только позитивные тесты, но и негативные, тем самым
# показывая корректность работы программы.

import unittest

from .myplatform import MyPlatform


class TestPlatformPositive(unittest.TestCase):
    """
    Класс для позитивных сценариев тестов.
    """

    def test_platform_system(self):
        """
        Позитивный тест для проверки функции platform_system().
        """
        self.assertEqual(MyPlatform.platform_system(), 'Darwin')

    def test_platform_release(self):
        """
        Позитивный тест для проверки функции platform_release().
        """
        self.assertEqual(MyPlatform.platform_release(), '20.6.0')

    def test_platform_version(self):
        """
        Позитивный тест для проверки функции platform_version().
        """
        self.assertEqual(MyPlatform.platform_version(),
                         'Darwin Kernel Version 20.6.0: Wed Jun 23 00:26:31'
                         ' PDT 2021; root:xnu-7195.141.2~5/RELEASE_X86_64')


class TestPlatformNegative(unittest.TestCase):
    """
    Класс для негативных сценариев тестов.
    """
    @unittest.expectedFailure
    def test_system_failure(self):
        """
        Негативный тест для проверки функции platform_system(), обернут
        декоратором unittest.expectedFailure.
        :return:
        """
        self.assertNotEqual(MyPlatform.platform_system(), 'Darwin')

    @unittest.expectedFailure
    def test_release_failure(self):
        """
        Негативный тест для проверки функции platform_release(), обернут
        декоратором unittest.expectedFailure.
        """
        self.assertNotEqual(MyPlatform.platform_release(), '20.6.0')

    @unittest.expectedFailure
    def test_version_failure(self):
        """
        Негативный тест для проверки функции platform_version(), обернут
        декоратором unittest.expectedFailure.
        """
        self.assertNotEqual(MyPlatform.platform_version(),
                            'Darwin Kernel Version 20.6.0: Wed Jun 23 00:26:31'
                            ' PDT 2021; root:xnu-7195.141.2~5/RELEASE_X86_64')
