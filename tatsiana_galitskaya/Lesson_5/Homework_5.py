# 1. Быки и коровы
num = '3219'
user_num = '0'
while num != user_num:
    cows, bulls = 0, 0
    user_num = input()
    for i in num:
        if i in user_num and num.index(i) == user_num.index(i):
            bulls += 1
        if i in user_num and num.index(i) != user_num.index(i):
            cows += 1
    print('Угадано коров:', cows, 'Угадано быков:', bulls)
print('Вы выиграли!')

# 2. Like
names = input('Введите имена через запятую:')
names_list = names.split(', ')
if names_list[0] == '':
    print('No one likes this')
elif len(names_list) == 1:
    print(f'{names} likes this')
elif len(names_list) <= 3:
    print(f'{", ".join(names_list[:-1])} and {names_list[-1]} like this')
elif len(names_list) > 3:
    print(f'{", ".join(names_list[:2])} and {len(names_list[2:])} '
          f'others like this')

# 3. BuzzFuzz
for num in range(1, 101):
    if (num % 3 == 0) and (num % 5 == 0):
        print('fuzzbuzz')
        continue
    if num % 3 == 0:
        print('fuzz')
        continue
    if num % 5 == 0:
        print('buzz')
        continue
    else:
        print(num)

# 4. Напишите код, который возьмет список строк и пронумерует их:
# 1 способ
array = input().split()
result = {}
for i in range(1, len(array) + 1):
    result[i] = result.get(i, array[i - 1])
print(result)

# 2 способ
array = input().split()
print('[', end='')
for i in range(len(array)):
    if i == len(array) - 1:
        print(f'"{i + 1}: {array[i]}"]', end=' ')
    elif i < len(array):
        print(f'"{i + 1}: {array[i]}",', end=' ')

# 3 способ с enumerate
array = input().split()
for i, value in enumerate(array):
    i += 1
    print(i, ':', value, end=', ')

# 5. Проверить, все ли элементы одинаковые:
flag = True
array = input().split()
for i in range(1, len(array)):
    if array[i - 1] != array[i]:
        flag = False
        break
print(flag)

# 2ой способ c set
array = [1, 1, 1]
print(len(set(array)) == 0 or len(set(array)) == 1)

# 6. Проверка строки:
line = list(input())
result = ''
for letter in line:
    if letter == letter.upper():
        result += letter
print(f"{''.join(line).islower()}, {list(result)}")

# 7. Сложите все числа в списке
s = [4, 5, -6]
sum_s = 0
for n in s:
    sum_s += n
print(sum_s)

# метод sum(list)
s = [7, 5, -9]
print(sum(s))
