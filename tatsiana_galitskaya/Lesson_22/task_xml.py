# Разработайте поиск книги в библиотеке по ее автору(часть имени)
# /цене/заголовку/описанию.
from xml.etree import ElementTree


def parseXML() -> ElementTree:
    """
    Функция получения содержимого xml файла
    :return: объект с содержимым xml файла
    """
    file = open("library.xml", "r")
    content = file.read()
    root = ElementTree.fromstring(content)
    return root


def get_book_by_attr(root, attr: str, value: str) -> str:
    """
    Функция поиска книг по переданным аттрибутам.
    :param root: бъект с содержимым xml файла
    :param attr: аттрибут по которому ищем
    :param value: значение которое ищем
    :return: искомая книга
    """
    for child in root:
        for next_child in list(child):
            if next_child.tag == attr and value in next_child.text:
                return child.attrib['id']


if __name__ == '__main__':
    root = parseXML()
    book1 = get_book_by_attr(root, 'author', 'Ralls')
    book2 = get_book_by_attr(root, 'price', '36.75')
    book3 = get_book_by_attr(root, 'title', 'Midnight Rain')
    book4 = get_book_by_attr(root, 'description', 'Terrifyingly Beautiful')
    print(book1)
    print(book2)
    print(book3)
    print(book4)
