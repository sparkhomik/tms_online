# Разработайте поиск учащихся в одном классе, посещающих одну секцию.
# Фильтрацию учащихся по их полу. Поиск ученика по имени(часть имени)
import json


def get_content() -> list:
    """
    Функция получения и преобразования данных json файла.
    :return: список словарей с содержимым
    """
    file_obj = open("students.json", "r")
    content = file_obj.read()
    file_obj.close()
    students = json.loads(content)
    return students


def get_students(students: list, key: str, arg: str) -> list:
    """
    Функция поиска учащихся среди соответствующих ключей по заданному аргументу
    :param students: список словарей с содержимым
    :param key: ключ
    :param arg: искомый аргумент
    :return: список с результатом поиска
    """
    result = []
    for student in students:
        if arg in student[key]:
            result.append(student)
    return result


def main():
    students = get_content()
    # поиск учащихся в одном классе
    classmates = get_students(students, "Class", "5a")
    print(*classmates)
    # поиск посещающих одну секцию
    club_mates = get_students(students, "Club", "Chess")
    print(*club_mates)
    # фильтрация учащихся по их полу
    gender = get_students(students, "Gender", "W")
    print(*gender)
    # поиск ученика по имени(часть имени)
    name = get_students(students, "Name", "Yuki")
    print(*name)


if __name__ == '__main__':
    main()
