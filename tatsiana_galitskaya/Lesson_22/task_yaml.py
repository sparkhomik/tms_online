# * Напечатайте номер заказа
# * Напечатайте адрес отправки
# * Напечатайте описание посылки, ее стоимость и кол-во
# * Сконвертируйте yaml файл в json
# * Создайте свой yaml файл
import json
import yaml


def parseYAML():
    """
    Вычитывание и получение содержимого yaml файла.
    :return: словарь с содержимым yaml файла
    """
    with open('order.yaml') as file:
        template = yaml.safe_load(file)
    return template


def get_invoice(template) -> str:
    """
    Получение номера заказа
    :param template: словарь с содержимым yaml файла
    :return: номер заказа
    """
    return template['invoice']


def get_address(template) -> str:
    """
    Функия получения адреса отправки заказа
    :param template: словарь с содержимым yaml файла
    :return: адрес отправки заказа
    """
    return template['bill-to']['address'].values()


def get_product_info(template, attr: str) -> list:
    """
    Функция получения информации об посылке: описание, кол-во
    :param template: словарь с содержимым yaml файла
    :param attr: аттрибут по которому ищем
    :return: информация о посылке найденная по переданному аттрибуту
    """
    info = []
    for i in template['product']:
        info.append(i[attr])
    return info


def get_total(template) -> int:
    """
    Функция получения стоимости всей посылки
    :param template: словарь с содержимым yaml файла
    :return: стоимость посылки
    """
    return template['total']


def convert_yaml_to_json(yaml_in, json_out):
    """
    Функция конвертации yaml файла в json
    :param yaml_in: конвертируемый yaml файл
    :param json_out: json файл в который передаются данные в json формате
    """
    with open(yaml_in, 'r') as yaml_in, open(json_out, "w") as json_out:
        yaml_object = yaml.safe_load(yaml_in)
        for keys in yaml_object:
            yaml_object[keys] = str(yaml_object[keys])
        json.dump(yaml_object, json_out)


def create_new_yaml_file(yaml_out):
    """
    Функция создания yaml файла
    :param yaml_out: yaml файл в который передаются данные
    """
    trunk_template = ['switchport trunk encapsulation dot1q']
    access_template = ['switchport mode access']
    to_yaml = {'trunk': trunk_template, 'access': access_template}
    with open(yaml_out, 'w') as f:
        yaml.dump(to_yaml, f)


def main():
    template = parseYAML()
    invoice = get_invoice(template)
    print(invoice)   # вывод номера заказа
    address = get_address(template)
    print(*address, sep='\n')   # вывод адреса заказа
    product_description = get_product_info(template, 'description')
    print(*product_description, sep=', ')   # вывод описания посылки
    product_qty = sum(get_product_info(template, 'quantity'))
    print(product_qty)   # вывод количества товаров
    total = get_total(template)
    print(total)   # вывод стоимости посылки
    convert_yaml_to_json('order.yaml', 'json_out.json')
    create_new_yaml_file('yaml_out.yaml')


if __name__ == '__main__':
    main()
