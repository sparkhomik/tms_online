# GET:
# Получение списка авторов
# Получение конкретного автора по его id
# POST:
# Добавить новую книгу
# Добавить нового пользователя
# PUT:
# Обновить данные для книги под номером 10
# DELETE:
# Удалить пользователя под номером 4
import json
import requests

link = 'https://fakerestapi.azurewebsites.net/api/v1/'


def get_authors():
    """
    Функция получения списка всех авторов
    :return: список словарей с информацией об авторах
    """
    response = requests.get(f'{link}Authors')
    response_text = json.loads(response.text)
    return response_text


def get_authors_list(response_text) -> list:
    """
    Функция получения списка всех авторов
    :param response_text: список словарей с информацией об авторах
    :return: список имен авторов
    """
    authors = []
    for author in response_text:
        authors.append(str(author['firstName'] + ' ' + author['lastName']))
    return authors


def author_by_id(response_text: list, id_arg: int) -> str:
    """
    Функция получения автора по id
    :param response_text: список словарей с информацией об авторах
    :param id_arg: аргументс id
    :return: полное имя автора
    """
    for author in response_text:
        if author['id'] == id_arg:
            return str(author['firstName'] + ' ' + author['lastName'])


def post_book():
    """
    Функция post запроса добавления новой книги
    :return: статус код ответа сервера
    """
    response = requests.post(
        f'{link}Books',
        json={'id': 900,
              'title': 'My Book',
              'description': 'My description',
              'pageCount': 500,
              'excerpt': 'some_excerpt',
              'publishDate': '2021-11-05'})
    return response


def post_user():
    """
    Функция post запроса добавления нового пользователя
    :return: статус код ответа сервера
    """
    response = requests.post(
        f'{link}Users',
        json={'id': 11,
              'userName': 'Tatsiana',
              'password': 'secret'})
    return response


def update_book_data(book_id: int):
    """
    Функция put запроса для обновления данных для книги
    :param book_id: id книги
    :return: статус код ответа сервера
    """
    response = requests.put(
        f'{link}Books/{book_id}',
        json={'title': 'My Book_1',
              'description': 'My description',
              'pageCount': 500,
              'excerpt': 'some_excerpt',
              'publishDate': '2021-11-05'})
    return response


def delete_user(user_id: int):
    """
    Функция delete запроса для обновления данных для книги
    :param user_id: id пользователя
    :return: статус код ответа сервера
    """
    request = requests.delete(
        f'{link}Users/{user_id}')
    return request


def main():
    # get
    authors = get_authors()
    authors_list = get_authors_list(authors)
    print(authors_list)
    author = author_by_id(authors, 5)
    print(author)
    # post
    post_new_book = post_book()
    print(post_new_book)
    post_new_user = post_user()
    print(post_new_user)
    # put
    response = update_book_data(10)
    print(response)
    # delete
    response = delete_user(4)
    print(response)


if __name__ == '__main__':
    main()
