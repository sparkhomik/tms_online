import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import NoSuchElementException


@pytest.fixture(scope='session', autouse=True)
def driver():
    """
    Фикстура для запуска и закрытия драйвера в начале и в конце сессии.
    """
    driver = Chrome(
        executable_path='/Users/tatsianagalitskaya/Downloads/chromedriver')
    yield driver
    driver.quit()


@pytest.fixture()
def main_page(driver):
    """
    Фикстура перехода на веб-страницу.
    :return: текущая страница
    """
    driver.get("http://the-internet.herokuapp.com/")
    return driver


@pytest.fixture()
def driver_login(driver):
    """
    Фикстура открытия страницы логина.
    :return: текущая страница
    """
    driver.get("http://the-internet.herokuapp.com/login")
    return driver


@pytest.fixture(params=[
    ('tomsmith', 'SuperSecretPassword!', 'You logged into a secure area!\n×'),
    ('tomsmith', 'SuperSecretPassword', 'Your password is invalid!\n×')
])
def login(driver_login, request):
    """
    Фикстура для работы на странице логина. Передаем 3 параметра для проверки
    успешного и неуспешного логина: логин, пароль, ожидаемый текст на странице.
    Находит поле логина и пароля, передает туда данные.
    :param request: встроенная фикстура, представляет вызывающее состояние
    фикстуры
    :return: полученный результат и ожидаемый результат
    """
    params = request.param
    xpath_username = '//*[@id="username"]'
    xpath_password = '//*[@id="password"]'
    xpath_submit = "//button[@type='submit']"
    element_username = driver_login.find_element_by_xpath(xpath_username)
    element_username.send_keys(params[0])
    element_password = driver_login.find_element_by_xpath(xpath_password)
    element_password.send_keys(params[1])
    element_login_button = driver_login.find_element_by_xpath(xpath_submit)
    element_login_button.click()
    auth_result = driver_login.find_element_by_xpath('//*[@id="flash"]')
    result = params[2]
    return auth_result, result


@pytest.fixture()
def checkboxes_page(main_page):
    """
    Фикстура для перехода с главной страницы на страницу с чекбоксами.
    :return: текущая страница с чекбоксами
    """
    checkboxes_page = main_page.find_element_by_xpath(
        '//a[@href="/checkboxes"]')
    checkboxes_page.click()
    return checkboxes_page


@pytest.fixture()
def checkboxes(checkboxes_page, main_page):
    """
    Фикстура выбора чекбоксов.
    :return: состояния чекбоксов
    """
    checkbox_1 = main_page.find_element_by_xpath(
        '//input[@type="checkbox"][1]')
    if checkbox_1.get_attribute('checked'):
        checkbox_1.click()
    checkbox_2 = main_page.find_element_by_xpath(
        '//input[@type="checkbox"][2]')
    if not checkbox_2.get_attribute('checked'):
        checkbox_2.click()
    return checkbox_1, checkbox_2


@pytest.fixture()
def multiple_windows(main_page):
    """
    Фикстура перехода и работы на странице Multiple Windows.
    :return: переменная result с заголовком страницы
    """
    windows_page = main_page.find_element_by_xpath(
        '//a[@href="/windows"]')
    windows_page.click()
    new_window = main_page.find_element_by_xpath('//a[@href="/windows/new"]')
    new_window.click()
    main_page.switch_to.window(main_page.window_handles[1])
    result = main_page.title
    return result


@pytest.fixture()
def add_remove_elements(main_page):
    """
    Фикстура перехода с главной страницы на страницу Add/Remove elements
    :return: страница Add/Remove elements
    """
    elements_page = main_page.find_element_by_xpath(
        '//a[@href="/add_remove_elements/"]')
    elements_page.click()
    return elements_page


@pytest.fixture()
def add_elements(add_remove_elements, main_page):
    """
    Фикстура добавления элементов на странице.
    :return: добавленный новый элемент
    """
    xpath_for_add = '//button[@onclick="addElement()"]'
    xpath_for_del = '//button[@class="added-manually"]'
    waiter = WebDriverWait(main_page, 15)
    waiter.until(EC.presence_of_element_located((By.XPATH, xpath_for_add)))
    button = main_page.find_element_by_xpath(xpath_for_add)
    button.click()
    added_element = main_page.find_element_by_xpath(xpath_for_del)
    return added_element


@pytest.fixture()
def remove_elements(add_elements, main_page):
    """
    Фикстура удаления элементов на странице Add/Remove elements
    :return: булевое значение, результат поиска удаленного элемента
    """
    xpath_for_del = '//button[@class="added-manually"]'
    add_elements.click()
    try:
        main_page.find_element_by_css_selector(xpath_for_del)
        return False
    except NoSuchElementException:
        return True
