

def test_elements(login):
    """
    Тестовая функция для проверки страницы логина.
    Сравнивает ожидаемый результат с текущим
    """
    assert login[1] == login[0].text


def test_checkboxes(checkboxes):
    """
    Тестовая функция для проверки чекбоксов.
    Сравнивает текущее состояние чекбоксов с ожидаемым
    """
    result = []
    for checkbox in checkboxes:
        result.append(checkbox.is_selected())
    assert result == [False, True]


def test_multiple_windows(multiple_windows):
    """
    Тестовая функция для проверки страницы Multiple Windows.
    Сравнивает текущий заголовок страницы с ожидаемым
    """
    assert multiple_windows == 'New Window'


def test_added_element(add_elements):
    """
    Тестовая функция для проверки добавления элементов на странице Add/Remove
    elements.
    Результат наличия нового добавленного элемента.
    """
    assert add_elements.is_displayed()


def test_remove_element(remove_elements):
    """
    Тестовая функция для проверки удаления элементов на странице Add/Remove
    elements.
    Результат удаления элемента.
    """
    assert remove_elements
