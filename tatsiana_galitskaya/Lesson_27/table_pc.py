# Заполнить таблицу данными, минимум 10 записей
# Напишите следующие sql запросы:
# Напечатайте общую стоимость компонентов
# Напечатайте среднюю стоимость компонентов
# Напечатайте компоненты с ценой между 200 и 600
# Напечатайте название компонента у которого самая высокая цена
# Напечатайте название компонента у которого низкая высокая цена

from mysql import connector

db = connector.connect(
    host='localhost',
    user='root',
    password='Galitskaya7u9!',
    database='tms_db'
)


def query_total_pro_price():
    """
    Функция получения общей стоимости компонентов.
    """
    cursor = db.cursor()
    query = "SELECT SUM(PRO_PRICE) from pc"
    cursor.execute(query)
    return cursor.fetchall()


def query_average_pro_price():
    """
    Функция получения средней стоимости компонентов.
    """
    cursor = db.cursor()
    query = "SELECT AVG(PRO_PRICE)  FROM pc"
    cursor.execute(query)
    return cursor.fetchall()


def query_range_pro_price():
    """
    Функция получения компонентов с ценой между 200 и 600.
    """
    cursor = db.cursor()
    query = "SELECT * FROM pc WHERE PRO_PRICE BETWEEN 200 AND 600"
    cursor.execute(query)
    return cursor.fetchall()


def query_max_pro_price():
    """
    Функция получения компонента у которого самая высокая цена.
    """
    cursor = db.cursor()
    query = "SELECT PRO_NAME FROM pc WHERE PRO_PRICE " \
            "= (SELECT MAX(PRO_PRICE) FROM pc)"
    cursor.execute(query)
    return cursor.fetchall()


def query_min_pro_price():
    """
    Функция получения компонента у которого самая низкая цена.
    """
    cursor = db.cursor()
    query = "SELECT PRO_NAME FROM pc WHERE PRO_PRICE " \
            "= (SELECT MIN(PRO_PRICE) FROM pc)"
    cursor.execute(query)
    return cursor.fetchall()


def all_queries():
    """
    Функция для вывода на печать результа всех запросов.
    """
    print(query_total_pro_price())
    print(query_average_pro_price())
    print(query_range_pro_price())
    print(query_max_pro_price())
    print(query_min_pro_price())


if __name__ == '__main__':
    all_queries()
    db.close()
