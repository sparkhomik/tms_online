# Заполнить таблицу данными, минимум 10 записей
# Напишите следующие sql запросы:
# Напечатайте номер заказа, дату заказа и количество для каждого заказа,
# Который продал продавец под номером: 5002
# Напечатайте уникальные id продавца(salesman_id). Используйте distinct
# Напечатайте по порядку данные о дате заказа, id продавца, номер заказа,
# количество
# Напечатайте заказы между 70001 и 70007(используйте between, and)

from mysql import connector

db = connector.connect(
    host='localhost',
    user='root',
    password='Galitskaya7u9!',
    database='tms_db'
)


def query_for_salesman_5002():
    """
    Функция для получения номера заказа, даты заказа и количества для каждого
    заказа, который продал продавец под номером 5002.
    """
    cursor = db.cursor()
    query = "SELECT ord_no, ord_date, purch_amt FROM orders" \
            " WHERE salesman_id = 5002"
    cursor.execute(query)
    return cursor.fetchall()


def query_for_unique_salesman_id():
    """
    Функция получения уникальных id продавца.
    """
    cursor = db.cursor()
    query = "SELECT DISTINCT salesman_id FROM orders"
    cursor.execute(query)
    return cursor.fetchall()


def query_for_data_orders():
    """
    Функция получения данных по порядку: даты заказа, id продавца,
    номера заказа, количества.
    """
    cursor = db.cursor()
    query = "SELECT ord_date, salesman_id, ord_no, purch_amt FROM orders"
    cursor.execute(query)
    return cursor.fetchall()


def query_range_orders():
    """
    Функция получения заказов между 70001 и 70007.
    """
    cursor = db.cursor()
    query = "SELECT * FROM orders WHERE ord_no BETWEEN 70001 AND 70007"
    cursor.execute(query)
    return cursor.fetchall()


def all_queries():
    """
    Функция для вывода на печать результа всех запросов.
    """
    print(query_for_salesman_5002())
    print(query_for_unique_salesman_id())
    print(query_for_data_orders())
    print(query_range_orders())


if __name__ == '__main__':
    all_queries()
    db.close()
