from mysql import connector

db = connector.connect(
    host='localhost',
    user='root',
    password='Galitskaya7u9!',
    database='tms_db'
)


def create_table_order():
    """
    Функция создания таблицы orders.
    """
    cursor = db.cursor()
    cursor.execute("CREATE TABLE orders"
                   " (ord_no VARCHAR(255),"
                   " purch_amt VARCHAR(255),"
                   " ord_date VARCHAR(255),"
                   " customer_id VARCHAR(255),"
                   " salesman_id VARCHAR(255))")


def create_table_pc():
    """
    Функция создания таблицы pc.
    """
    cursor = db.cursor()
    cursor.execute("CREATE TABLE pc"
                   " (PRO_ID VARCHAR(255),"
                   " PRO_NAME VARCHAR(255),"
                   " PRO_PRICE INT(255),"
                   " PRO_COM VARCHAR(255))")


def show_tables():
    """
    Функция просмотра существующих таблиц.
    """
    cursor = db.cursor()
    cursor.execute("SHOW TABLES")
    print(cursor.fetchall())


if __name__ == '__main__':
    create_table_order()
    create_table_pc()
    db.close()
