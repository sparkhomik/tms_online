from mysql import connector


db = connector.connect(
    host='localhost',
    user='root',
    password='Galitskaya7u9!'
)


def create_tms_db():
    """
    Функция создания БД tms_db.
    """
    cursor = db.cursor()
    cursor.execute('CREATE DATABASE tms_db')


def show_db():
    """
    Функция просомтра существующих БД.
    """
    cursor = db.cursor()
    cursor.execute("SHOW DATABASES")
    print(cursor.fetchall())


if __name__ == '__main__':
    create_tms_db()
    db.close()
