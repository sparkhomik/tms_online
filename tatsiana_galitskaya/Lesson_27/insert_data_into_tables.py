from mysql import connector

db = connector.connect(
    host='localhost',
    user='root',
    password='Galitskaya7u9!',
    database='tms_db'
)


def insert_data_into_orders():
    """
    Функция вставки данных в таблицу orders.
    """
    cursor = db.cursor()
    for ord_no, purch_amt, ord_date, customer_id, salesman_id in [
            ("70001", "150.5", "2012-10-05", "3005", "5002"),
            ("70009", "270.65", "2012-09-10", "3001", "5005"),
            ("70002", "65.26", "2012-10-05", "3002", "5001"),
            ("70004", "110.5", "2012-08-17", "3009", "5003"),
            ("70007", "948.5", "2012-09-10", "3005", "5002"),
            ("70005", "2400.6", "2012-07-27", "3007", "5001"),
            ("70008", "5760", "2012-09-10", "3002", "5001"),
            ("70010", "1983.43", "2012-10-10", "3004", "5006"),
            ("70003", "2480.4", "2012-10-10", "3009", "5003"),
            ("70012", "250.45", "2012-06-27", "3008", "5002"),
            ("70011", "75.29", "2012-08-17", "3003", "5007"),
            ("70013", "3045.6", "2012-04-25", "3002", "5001")]:
        cursor.execute(f"INSERT INTO orders (ord_no, purch_amt, ord_date,"
                       f" customer_id, salesman_id) values ('{ord_no}',"
                       f" '{purch_amt}', '{ord_date}', '{customer_id}',"
                       f" '{salesman_id}')")
        db.commit()


def insert_data_into_pc():
    """
    Функция вставки данных в таблицу pc.
    """
    cursor = db.cursor()
    for PRO_ID, PRO_NAME, PRO_PRICE, PRO_COM in [
            ("101", "Mother Board ", 3200, "15"),
            ("102", "Key Board", 450, "16"),
            ("103", "ZIP drive", 250, "14"),
            ("104", "Speaker", 550, "16"),
            ("105", "Monitor", 5000, "11"),
            ("106", "DVD drive", 900, "12"),
            ("107", "CD drive", 800, "2"),
            ("108", "Printer", 2600, "13"),
            ("109", "Refill cartridge ", 350, "13"),
            ("110", "Mouse", 250, "12")]:
        cursor.execute(f"INSERT INTO pc (PRO_ID, PRO_NAME, PRO_PRICE, PRO_COM)"
                       f" values ('{PRO_ID}', '{PRO_NAME}', '{PRO_PRICE}',"
                       f" '{PRO_COM}')")
        db.commit()


if __name__ == '__main__':
    insert_data_into_orders()
    insert_data_into_pc()
    db.close()
