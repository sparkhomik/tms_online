import time
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By


@pytest.fixture(scope='session')
def driver():
    """
    Фикстура для запуска и закрытия драйвера в начале и в конце сессии.
    """
    driver = Chrome(
        executable_path='/Users/tatsianagalitskaya/Downloads/chromedriver')
    yield driver
    driver.quit()


@pytest.fixture()
def forms_page(driver):
    """
    Фикстура перехода на страницу с формами.
    :return: текущая страница
    """
    driver.get("https://ultimateqa.com/filling-out-forms/")
    return driver


@pytest.fixture()
def fill_name_field(forms_page):
    """
    Фикстура заполнения поля Name.
    """
    name_field = forms_page.find_element(By.CSS_SELECTOR,
                                         'input[name="et_pb_contact_name_0"]')
    name_field.send_keys('Tatsiana')


@pytest.fixture()
def fill_message_field(forms_page):
    """
    Фикстура заполнения поля Message
    """
    message_field = forms_page.find_element(By.CSS_SELECTOR,
                                            '#et_pb_contact_message_0')
    message_field.send_keys('Hello!')


@pytest.fixture()
def submit(forms_page):
    """
    Фикстура нажатия кнопки Submit
    """
    submit_locator = '.et_pb_column_1 .et_pb_contact_submit'
    submit_button = forms_page.find_element(By.CSS_SELECTOR, submit_locator)
    time.sleep(3)
    submit_button.click()


@pytest.fixture()
def get_the_result(forms_page):
    """
    Фикстура нахождения сообщения с результатом после сабмита.
    :return: элемент с сообщением с результатом
    """
    result_locator = '.et_pb_column_1 .et-pb-contact-message'
    time.sleep(3)
    result = forms_page.find_element(By.CSS_SELECTOR, result_locator)
    return result


@pytest.fixture()
def get_warning(forms_page):
    """
    Фикстура нахождения сообщения об ошибке.
    :return: элемент с сообщениме об ошибке
    """
    result_locator = '.et_pb_column_1 .et-pb-contact-message'
    time.sleep(3)
    result_message = forms_page.find_element(By.CSS_SELECTOR, result_locator)
    return result_message
