# Нажмите на 2-ю сверху кнопку во втором столбце используя:
# XPATH
# CSS selector
# class name
from selenium.webdriver import Chrome


def find_the_second_button_xpath(driver):
    """
    Функция нахождения второй кнопки во втором ряду по xpath.
    """
    xpath = '//a[@class="et_pb_button et_pb_button_4 et_pb_bg_layout_light"]'
    button_by_xpath = driver.find_element_by_xpath(xpath)
    return button_by_xpath


def find_the_second_button_css_selector(driver):
    """
    Функция нахождения второй кнопки во втором ряду по css_selector.
    """
    css_selector = '.et_pb_button.et_pb_button_4'
    button_by_css = driver.find_element_by_css_selector(css_selector)
    return button_by_css


def find_the_second_button_class_name(driver):
    """
    Функция нахождения второй кнопки во втором ряду по class_name.
    """
    class_name = 'et_pb_button.et_pb_button_4.et_pb_bg_layout_light'
    button_by_class_name = driver.find_element_by_class_name(class_name)
    return button_by_class_name


def second_button():
    """
    Основная функция, запускает драйвер, открывает страницу, вызывает
    функции поиска кнопки разными способобами и кликает на нее.
    """
    driver = Chrome(
        executable_path='/Users/tatsianagalitskaya/Downloads/chromedriver')
    driver.get("https://ultimateqa.com/complicated-page/")
    find_the_second_button_xpath(driver).click()
    find_the_second_button_css_selector(driver).click()
    find_the_second_button_class_name(driver).click()
    driver.quit()


if __name__ == '__main__':
    second_button()
