# тест1:
# Заполните форму обратной связи
# Нажмите на кнопку "Submit"
# Проверьте наличие сообщения "Form filled out successfully"
# тест2:
# Заполните поле "Name"
# Нажмите на кнопку "Submit"
# Проверьте наличие сообщения об ошибке
# тест3:
# Заполните поле "Message"
# Нажмите на кнопку "Submit"
# Проверьте наличие сообщения об ошибке


def test_result(fill_name_field, fill_message_field, submit, get_the_result):
    """
    Тест проверки сообщения после успешного заполнения 2х полей.
    Сверяет с ожидаемым результатом .
    """
    assert get_the_result.text == 'Thanks for contacting us'


def test_filled_only_name_field(fill_name_field, submit, get_warning):
    """
    Тест проверки сообщения после заполнения только поля Name.
    Сверяет с ожидаемым результатом .
    """
    message = 'Please, fill in the following fields:\nMessage'
    assert get_warning.text == message


def test_filled_only_message_field(fill_message_field, submit, get_warning):
    """
    Тест проверки сообщения после заполнения только поля Message.
    Сверяет с ожидаемым результатом .
    """
    message = 'Please, fill in the following fields:\nName'
    assert get_warning.text == message
