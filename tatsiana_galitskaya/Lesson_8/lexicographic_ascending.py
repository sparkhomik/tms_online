# task 2
# Лексикографическое возрастание
number_names = {
    0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five',
    6: 'six', 7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten',
    11: 'eleven', 12: 'twelve', 13: 'thirteen', 14: 'fourteen',
    15: 'fifteen',
    16: 'sixteen', 17: 'seventeen', 18: 'eighteen', 19: 'nineteen'
}


def decorator(func):
    """
    Декоратор для передаваемой функции.
    :param func: функция вывода чисел
    :return: объект функции с отсортированными элементами
    """

    def wrapper(numbers: str) -> func:
        """
        Подстановка значений из number_names по заданным элементам-ключам,
        сортировка их и вывод по отсортированным значениям ключей.
        :param numbers: список чисел, введенных через пробел
        :return: функция с отсортированными элементами
        """
        numbers = numbers.split(' ')
        numbers_dict = {}
        for num in numbers:
            numbers_dict[int(num)] = number_names.get(int(num))
        sorted_values = sorted(numbers_dict.values())
        sorted_keys = []
        for name in sorted_values:
            for key, value in numbers_dict.items():
                if name == value:
                    sorted_keys.append(key)
        return func(sorted_keys)

    return wrapper


@decorator
def numbers(numbers: str):
    """
    Декорируемая функция вывода чисел, принимающая строку с введенными
    числами через пробел.
    :param numbers: строка с введенными числами через пробел
    :return: выводит на печать введенные числа
    """
    print(numbers)


list_num = input('Введите целые числа через пробел, каждое не меньше'
                 ' 0 и не больше 19')
numbers(list_num)
