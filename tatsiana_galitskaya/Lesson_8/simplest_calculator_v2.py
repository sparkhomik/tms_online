# task 3
# Простейший калькулятор v0.2
"""
1) программа ожидает от пользователя ввода математического выражения и
правильно его трактует
2) программа знает о приоритете операторов
"""
operators_dict = {
    '**': 1, '*': 2, '/': 3, '+': 4, '-': 5
}


def decorator(func):
    """
    Декоратор для передаваемой функции.
    :param func: передаваемая функция
    :return: объект функции - результат вычислений
    """

    def wrapper(exp: str) -> func:
        """
        Декоратор, принимает введенное выражение, делает вычисления вызывая
        соответствующие функции и учитывает приоритет операторов.
        :param exp: введенное пользователем математическое выражение в виде
        строки
        :return: функция с результатом вычислений в качестве аргумента
        """
        exp = exp.split(' ')
        if operators_dict.get(exp[1]) < operators_dict.get(exp[3]):
            x = first_exp(exp, 1, 0, 2)
            result = second_exp(exp, x, 3, 4)
        else:
            x = first_exp(exp, 3, 2, 4)
            result = third_exp(exp, x, 1, 0)
        return func(result)
    return wrapper


def first_exp(exp: list, a: int, b: int, c: int) -> int:
    """
    Функция, которая вызывается декоратором для произведения вычислений
    оператором с большим приоритетом.
    :param exp: выражение в виде списка
    :param a: индекс оператора с большим приоритетом в выражении
    :param b: индекс первого операнда
    :param c: индекс второго операнда
    :return: результат вычисления оператором с большим приоритетом
    """
    x = 0
    if exp[a] == '**':
        x = int(exp[b]) ** int(exp[c])
    elif exp[a] == '*':
        x = int(exp[b]) * int(exp[c])
    elif exp[a] == '/':
        x = int(exp[b]) / int(exp[c])
    elif exp[a] == '+':
        x = int(exp[b]) + int(exp[c])
    elif exp[a] == '-':
        x = int(exp[b]) - int(exp[c])
    return x


def second_exp(exp: list, x: int, a: int, b: int) -> int:
    """
    Функция, которая вызывается декоратором для произведения вычислений
    оператором с меньшим приоритетом, при условии что он находится справа от
    оператора с большим приоритетом.
    :param exp: выражение в виде списка
    :param x: результат вычисления оператором с большим приоритетом
    :param a: индекс оператора с меньшим приоритетом в выражении
    :param b: индекс оставшегося операнда
    :return: результат вычисления оператором с менишьм приоритетом
    """
    result = 0
    if exp[a] == '**':
        result = x ** int(exp[b])
    elif exp[a] == '*':
        result = x * int(exp[b])
    elif exp[a] == '/':
        result = x / int(exp[b])
    elif exp[a] == '+':
        result = x + int(exp[b])
    elif exp[a] == '-':
        result = x - int(exp[b])
    return result


def third_exp(exp: list, x: int, a: int, b: int) -> int:
    """
    Функция, которая вызывается декоратором для произведения вычислений
    оператором с меньшим приоритетом, при условии что он находится слева от
    оператора с большим приоритетом.
    :param exp: выражение в виде списка
    :param x: результат вычисления оператором с большим приоритетом
    :param a: индекс оператора с меньшим приоритетом в выражении
    :param b: индекс оставшегося операнда
    :return: результат вычисления оператором с менишьм приоритетом
    """
    result = 0
    if exp[a] == '**':
        result = int(exp[b]) ** x
    elif exp[a] == '*':
        result = int(exp[b]) * x
    elif exp[a] == '/':
        result = int(exp[b]) / x
    elif exp[a] == '+':
        result = int(exp[b]) + x
    elif exp[a] == '-':
        result = int(exp[b]) - x
    return result


@decorator
def calculator_v2(expression: str):
    """
    Декорируемая функция, принимающая выражение в качестве парамента.
    :param expression: введенное пользователем математическое выражение в виде
    строки
    :return: выводит на печать результат выражения
    """
    print(expression)


exp = input('Введите математическое выражение из трех чисел, разделяя числа'
            ' и операторы пробелами')
calculator_v2(exp)
