# task 1
# Декоратор типов
"""
Напишите декоратор, который проверял бы тип параметров функции,
конвертировал их если надо и складывал.
"""


def typed(type):
    """
    Декоратор функции, который принимает тип элементов, в качестве аргумента.
    :param type: тип сладываемых элементов
    :return: результат декорируемой функции, с учетом всех параметров
    """
    def decorator(func):
        """
        Декоратор для передаваемой функции.
        :param func: передаваемая в декоратор функция
        :return: объект функции с заданным типом
        """
        def wrapper(*args):
            """
            Изменение параметров функции, согласно заданному типу.
            :param args: параметры функции
            :return: функция сложения элементов с измененными типами
            """
            args = map(type, args)
            result = func(*args)
            return result
        return wrapper
    return decorator


@typed(str)
def add_two_symbols(a, b):
    """
    Функция сложения 2х элементов.
    :param a: первый
    :param b: второй
    :return: сумма
    """
    return a + b


print(add_two_symbols('3', 5))
print(add_two_symbols(5, 5))
print(add_two_symbols('a', 'b'))


@typed(int)
def add_three_symbols(a, b, c):
    """
    Функция сложения 3х элементов.
    :param a: первый
    :param b: второй
    :param c: третий
    :return: сумма
    """
    return a + b + c


print(add_three_symbols(5, 6, 7))
print(add_three_symbols('3', 5, 0))


@typed(float)
def add_three_symbols(a, b, c):
    """
    Функция сложения 3х элементов типа float.
    :param a: первый
    :param b: второй
    :param c: третий
    :return: сумма
    """
    return a + b + c


print(add_three_symbols(0.1, 0.2, 0.4))
