import random
import typing
# task 1
# Крестики-нолики
# Компьютер играет в крестики-нолики против пользователя
"""
Модель программы игры "Крестики-нолики"
Для того, чтобы сделать ход, пользователю нужно вводить числа от 0 до 8.
Числа соответствуют полям доски так, как показано на чертеже:
  0  | 1 |  2
-----|---|-----
  3  | 4 |  5
-----|---|-----
  6  | 7 |  8
"""
# глобальные константы:
X_SIGN = 'X'
O_SIGN = 'O'
EMPTY = ' '
TIE = 'Ничья'
NUM_SQUARES = 9
board = [' '] * NUM_SQUARES


def draw_the_board(board: list) -> list:
    """
    Данная функция рисует доску с отображением ходов пользователя и компьютера,
    если таковые уже были совершены в ходе игры.
    :param board: список, где индекс каждого элемента, соответствует позиции
    на доске, согласно описанию задачи
    :return: распечатывает строки списка, отображающие пустые либо занятые
    клетки игровой доски
    игроками
    """
    pattern = [f'  {board[0]}  | {board[1]} |  {board[2]}  ',
               '-----|---|-----',
               f'  {board[3]}  | {board[4]} |  {board[5]}  ',
               '-----|---|-----',
               f'  {board[6]}  | {board[7]} |  {board[8]}']
    return [print(i) for i in pattern]


def ask_to_choose_sign():
    """
    Функция спрашивает у пользователя, за какую команду он будет играть:
    крестиками или ноликами
    :return: возвращает 2 переменные, с присвоенными символами для ходов
    пользователю и компьютеру
    """
    global user_sign
    global computer_sign
    sign = input('Выберите команду: 1)Крестики, 2)Нолики')
    if sign == '1':
        user_sign, computer_sign = 'X', 'O'
    else:
        user_sign, computer_sign = 'O', 'X'
    return user_sign, computer_sign


def free_squares_for_step(board: list) -> list:
    """
    Эта функция определяет список доступных ходов.
    :param board: список, где индекс каждого элемента, соответствует позиции
    на доске
    :return: список доступных ходов (незанятых клеток)
    """
    global free_squares
    free_squares = []
    for square in range(NUM_SQUARES):
        if board[square] == EMPTY:
            free_squares.append(square)
    return free_squares


def ask_number(question: str) -> int:
    """
    Функция запрашивает о вводе числа, где числа соответствуют полям на доске
    :param question: переменная - строка, передается при вызове из user_step()
    функции, выводится на экран пользователю
    :return: номер клетки, на которую хочет походить пользователь
    """
    response = None
    while response not in range(1, 9):
        response = int(input(question))
        return response


def user_step(board: list) -> int:
    """
    Функция, расчитывает список доступных ходов, вызывает ask_number ()
    функцию, возвращает ход пользователя.
    :param board: список, где индекс каждого элемента, соответствует позиции
    на доске
    :return: ход пользователя
    """
    free_list = free_squares_for_step(board)
    step = None
    while step not in free_list:
        step = ask_number('Выберите номер поля, которое хотите занять: (1-8)')
        if step not in free_list:
            print('Это поле уже занято, выберите другое')
    return step


def check_winner(board: list) -> typing.Union[str, None]:
    """
    Данная функция вызывается после каждого хода и сверяет все поля на доске
    с выигрышными комбинациями, переданными в кортеже wins
    :param board: список, где индекс каждого элемента, соответствует позиции
    на доске
    :return: возвращает победителигры, если такой есть
    """
    wins = (
        (0, 1, 2),
        (3, 4, 5),
        (6, 7, 8),
        (0, 3, 6),
        (1, 4, 7),
        (2, 5, 8),
        (0, 4, 8),
        (2, 4, 6)
    )
    for line in wins:
        if board[line[0]] == board[line[1]] == board[line[2]] != EMPTY:
            winner = board[line[0]]
            return winner
    if EMPTY not in board:
        winner = TIE
        return winner
    return None


def computer_step(board: list) -> int:
    """
    Функция, расчитывает список доступных ходов, генерирует ход компьютера
    через randint до тех пор, пока он не будет уникален
    :param board: список, где индекс каждого элемента, соответствует позиции
    на доске
    :return: возвращает ход компьютера
    """
    free_list = free_squares_for_step(board)
    step = None
    while step not in free_list:
        step = random.randint(0, 9)
    return step


def next_turn(turn: str) -> str:
    """
    Данная функция меняет очередность ходов, принимает последний и меняет
    следующий на противоположный
    :param turn: тип хода: X или O
    :return: следующий тип хода
    """
    if turn == X_SIGN:
        return O_SIGN
    else:
        return X_SIGN


def congrats_winner(the_winner: str, user_sign: str, computer_sign: str):
    """
    Функция печатает результат игры, в зависимости от типа фишки победителя.
    :param the_winner: тип фишки победителя
    :param user_sign: тип фишки пользователя
    :param computer_sign: тип фишки компьютера
    :return:
    """
    if the_winner == TIE:
        print('Ничья!')
    if the_winner == computer_sign:
        print('Выиграл компьютер!')
    elif the_winner == user_sign:
        print('Вы выиграли!')


def continue_or_not():
    """
    Функция вызывается в конце игры после объявления победиля и спрашивает у
    пользователя желает ли он играть дальше.
    :return: запускает основную функцию, если пользователь выбрал вариант
    с продолжением игры.
    """
    option = input('Хотите сыграть еще одну партию? '
                   'Нажмите 1 - если да, и 2 - если нет')
    if option == '1':
        main()
    else:
        print('Игра окончена')


def main():
    """
    Основная функция. Запускается при запуске программы и вызывает по ходу
    игры остальные функции.
    """
    ask_to_choose_sign()
    turn = X_SIGN
    board = [' '] * NUM_SQUARES
    draw_the_board(board)
    print('Крестики начинают первые!')
    while not check_winner(board):
        if turn == user_sign:
            step = user_step(board)
            board[step] = user_sign
            print('Ваш ход:')
        else:
            step = computer_step(board)
            board[step] = computer_sign
            print('Ход компьютера:')
        draw_the_board(board)
        turn = next_turn(turn)
    the_winner = check_winner(board)
    congrats_winner(the_winner, user_sign, computer_sign)
    continue_or_not()


if __name__ == '__main__':
    main()
