from caesar_cipher import caesar_cipher_1_func
from personal_cipher import personal_cipher
from tic_tac_toe import tic_tac_toe_game
import random

# task 3
"""
Общий модуль. Предоставляет возможность выбрать одну из двух предыдущих
программ и запустить ее, также он должен позволить по окончанию выполнения
программы выбрать запуск другой!
Если пользователь совершит ошибку при выборе программы
Первые три раза программа должна сообщить ему об ошибке, а на четвертый раз
запустить одну из программ, выбранных случайно
"""


ask = 'Выберите, какую программу вы хотите запустить: ' \
      '1) Игра крестики-нолики ' \
      '2) Шифр Цезаря ' \
      '3) Персональный шифр'


def choose_the_program() -> str:
    """
    Функция выбора программы. Спрашивает у пользователя какую именно он хочет
    запустить программу 3 раза, после чего рандомно определяет номер программы.
    :return: строка с номером программы
    """
    choice = input(ask)
    counter = 1
    while choice not in ['1', '2', '3']:
        if counter < 4:
            print('Вы ввели неверный номер программы, попробуйте еще раз')
            choice = input(ask)
            counter += 1
        else:
            choice = str(random.randint(1, 4))
            print('Сейчас будет запущена рандомно выбранная программа!')
            break
    return choice


def start_the_program():
    """
    Основная функция общего модуля, внутри себя запускает функцию выбора
    программы, и в соответствии с введенными данными запускает импортируемые
    программы, до тех пор, пока пользователь не захочет остановить работу.
    """
    reason = True
    while reason:
        choice = choose_the_program()
        if choice == '1':
            print('Игра "Крестики нолики":')
            tic_tac_toe_game.main()
        elif choice == '2':
            print('Программа "Шифр Цезаря":')
            caesar_cipher_1_func.main_for_caesar_cipher()
        elif choice == '3':
            print('Программа "Персональный шифр":')
            personal_cipher.personal_cipher_start()
        answer = input('Хотите запустить еще одну программу? 1) да 2) нет')
        if answer == '2':
            print('Работа окончена.')
            reason = False


if __name__ == '__main__':
    start_the_program()
