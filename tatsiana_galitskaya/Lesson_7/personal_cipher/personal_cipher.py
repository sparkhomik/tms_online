# task 2
# Персональный шифр для операций зашифровки и дешифровки
"""
Модуль персонального шифра. Программа принимает текст для шифровки/расшифровки.
Шифр подставляет вместо каждой буквы соответствующую букву из алфавита в
обратном поряде алфавита.
"""
RU = 'абвгдежзийклмнопрстуфхцчшщъыьэюя'
RU_REVERSE = 'яюэьыъщшчцхфутсрпонмлкйизжедгвба'
EN = 'abcdefghijklmnopqrstuvwxyz'
EN_REVERSE = 'zyxwvutsrqponmlkjihgfedcba'


def personal_cipher_encode(text):
    """
    Данная функция запустится для шифрования текста.
    :param text: фраза для операции шифрования
    :return: зашифрованная фраза
    """
    text_result, punctuation = '', " .,!?-'()"
    for char in text:
        if char in punctuation or char.isdigit():
            text_result += char
        elif char in RU:
            text_result += RU_REVERSE[RU.find(char)]
        elif char in RU.upper():
            text_result += RU_REVERSE[RU.find(char.lower())].upper()
        elif char in EN:
            text_result += EN_REVERSE[EN.find(char)]
        elif char in EN.upper():
            text_result += EN_REVERSE[EN.find(char.lower())].upper()
    return print(text_result)


def personal_cipher_decode(text):
    """
    Данная функция запустится для расшифрования текста.
    :param text: фраза для операции дешифрования
    :return: дешифрованная фраза
    """
    global RU, RU_REVERSE
    RU, RU_REVERSE = RU_REVERSE, RU
    text_result, punctuation = '', " .,!?-'()"
    for char in text:
        if char in punctuation or char.isdigit():
            text_result += char
        elif char in RU:
            text_result += RU_REVERSE[RU.find(char)]
        elif char in RU.upper():
            text_result += RU_REVERSE[RU.find(char.lower())].upper()
        elif char in EN:
            text_result += EN_REVERSE[EN.find(char)]
        elif char in EN.upper():
            text_result += EN_REVERSE[EN.find(char.lower())].upper()
    return print(text_result)


def personal_cipher_start():
    """
    Основная функция, запускается при старте программы. Уточняет у пользователя
    какую операцию он хочет выполнить (шифрования/дешиврования),
    принимает текст для обработки.
    :return: запуск следующей программы, в зависимостри от выбора пользователя.
    """
    choice = input('Выберите операцию: 1) Кодировать 2) Декодировать')
    if choice not in ['1', '2']:
        print('Такого варианта нет')
    text = input('Введите фразу:')
    if choice == '1':
        personal_cipher_encode(text)
    elif choice == '2':
        personal_cipher_decode(text)


if __name__ == '__main__':
    personal_cipher_start()
