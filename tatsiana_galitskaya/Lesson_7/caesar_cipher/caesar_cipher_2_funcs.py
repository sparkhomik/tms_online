# task 2
# Программа "Шифр цезаря"
"""
Модуль шифра Цезаря. Шифрование и дешифрование реализованы двумя функциями.
"""
RU = "абвгдежзийклмнопрстуфхцчшщъыьэюя"
EN = 'abcdefghijklmnopqrstuvwxyz'
RU_NUM = 32
EN_NUM = 25


def choose_the_code():
    """
    Эта функция запускается при запуске программы, спрашивает у пользователя
    какую операцию он хочет произвести, число сдвига и текст.
    :return: Функция, которая запустится в зависимости от выбора пользователя.
    """
    choice = input('Привет! Выберите операцию: 1) Кодировать 2) Декодировать')
    if choice not in ['1', '2']:
        print('Такого варианта нет')
    shift = int(input('ведите число-сдвиг'))
    text = input('Введите фразу:')
    if choice == '1':
        return encode(shift, text)
    if choice == '2':
        return decode(shift, text)


def encode(shift: int, text: str) -> str:
    """
    Эта функция запустится, при условии что пользователь выбрал операцию
    дешифровки.
    :param shift: число-сдвиг, веденное пользователем
    :param text: фраза для кодирования
    :return: возвращает зашифрованную фразу
    """
    text_result, punctuation = '', ' .,!?-:;()'
    for char in text:
        if char in punctuation or char.isdigit():
            text_result += char
        elif char in RU:
            text_result += RU[(RU.find(char) + shift) % RU_NUM]
        elif char in EN:
            text_result += EN[(EN.find(char) + shift) % EN_NUM]
        else:  # Если заглавная
            if char in RU.upper():
                text_result += (RU[(RU.find(char) + shift) % RU_NUM]).upper()
            if char in EN.upper():
                text_result += (EN[(EN.find(char) + shift) % EN_NUM]).upper()
    return text_result


def decode(shift: int, text: str) -> str:
    """
    Эта функция запустится, при условии что пользователь выбрал операцию
     зашифровки.
    :param shift: число-сдвиг, веденное пользователем
    :param text: фраза для кодирования
    :return: возвращает дешифрованную фразу
    """
    text_result, punctuation = '', ' .,!?-:;()'
    for char in text:
        if char in punctuation or char.isdigit():
            text_result += char
        elif char in RU:
            text_result += RU[(RU.find(char) - shift) % RU_NUM]
        elif char in EN:
            text_result += EN[(EN.find(char) - shift) % EN_NUM]
        else:  # Если заглавная
            if char in RU.upper():
                text_result += (RU[(RU.find(char) - shift) % RU_NUM]).upper()
            if char in EN.upper():
                text_result += (EN[(EN.find(char) - shift) % EN_NUM]).upper()
    return text_result


if __name__ == '__main__':
    print(choose_the_code())
