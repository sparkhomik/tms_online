# task 2
# Программа "Шифр цезаря"
"""
Модуль шифра Цезаря. В данном модуле операции шифрования и дешифрования
реализуются в одной функции.
"""
RU = "абвгдежзийклмнопрстуфхцчшщъыьэюя"
EN = 'abcdefghijklmnopqrstuvwxyz'
RU_NUM = 32
EN_NUM = 25


def caesar_cipher(choice: str, shift: int, text: str) -> str:
    """
    Эта функция запускается из основной, для операций шифрования/дешифрования.
    :param choice: операция 1 - Encode или 2 - Decode, выбранная пользователем
    :param shift: число-сдвиг
    :param text: фраза для операции шифрования/дешифрования
    :return: зашифрованная или дешифрованная фраза
    """
    text_result, punctuation = '', ' .,!?-'
    shift = shift if choice == '1' else -shift
    for char in text:
        if char in punctuation or char.isdigit():
            text_result += char
        elif char in RU:
            text_result += RU[(RU.find(char) + shift) % RU_NUM]
        elif char in EN:
            text_result += EN[(EN.find(char) + shift) % EN_NUM]
        else:  # Если заглавная
            if char in RU.upper():
                text_result += (RU[(RU.find(char) + shift) % RU_NUM]).upper()
            if char in EN.upper():
                text_result += (EN[(EN.find(char) + shift) % EN_NUM]).upper()
    return text_result


def main_for_caesar_cipher():
    """
    Основная функция, которая запустит следующую функцию,
    одну для двух операций: шифрования и дешифрования.
    :return: возвращает функцию для выполнения операции
    """
    choice = None
    while choice not in ['1', '2']:
        choice = input('Выберите операцию: 1) Кодировать 2) Декодировать')
        if choice not in ['1', '2']:
            print('Такого варианта нет, попробуйте еще раз')
    shift = int(input('ведите число-сдвиг'))
    text = input('Введите фразу:')
    return caesar_cipher(choice, shift, text)


if __name__ == '__main__':
    print(main_for_caesar_cipher())
