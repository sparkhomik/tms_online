# 2) Для каждой функции создать параметризированный тест. В качестве
# параметров тест принимает значение, которое должна обработать функция
# и ожидаемый результат ее выполнения.
# 3) Каждый тест должен быть вызван по 3 раза
# 4) Для 2-ой функции написать отдельный тест, который будет ожидать
# ошибку InputTypeError, и если эта ошибка не произошла, падать!
import pytest

from lower_case import check_lower_case
from float_type import check_float_type
from special_symbols import check_special_symbols
from float_type import InputTypeError


@pytest.mark.parametrize('actual, expected',
                         [('Test', False), ('test', True), ('TEST', False)])
def test_check_lower_case(actual: str, expected: bool):
    """
    Параметризированный тест для check_lower_case() функции.
    :param actual: передаваемый тесту параметр, для обработки функции
    :param expected: ожидаемый результат выполнения функции
    """
    assert check_lower_case(actual) == expected


@pytest.mark.parametrize('actual, expected',
                         [(7, 7.0), (0, 0.0), (100, 100.0)])
def test_check_float_type(actual: int, expected: float):
    """
    Параметризированный тест для check_float_type() функции.
    :param actual: передаваемый тесту параметр, для обработки функции
    :param expected: ожидаемый результат выполнения функции
    """
    assert check_float_type(actual) == expected


@pytest.mark.parametrize('actual, expected',
                         [('@', True), ('§', True), (':', False)])
def test_check_special_symbols(actual: str, expected: bool):
    """
    Параметризированный тест для check_special_symbols() функции.
    :param actual: передаваемый тесту параметр, для обработки функции
    :param expected: ожидаемый результат выполнения функции
    """
    assert check_special_symbols(actual) == expected


@pytest.mark.parametrize('arg', ['test', 10])
def test_check_float_type_error(arg: any):
    """
    Тест, для InputTypeError, падает при не возникновении ошибки.
    :param arg: передаваемый тесту параметр для обработки функцией
    check_float_type()
    """
    with pytest.raises(InputTypeError):
        check_float_type(arg)
