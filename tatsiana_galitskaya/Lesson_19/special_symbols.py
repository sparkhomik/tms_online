# 3яя функция – проверяет, является ли переданный аргумент спец символом
special_symbols = '@#$%^&*_+~}|><.,±§'


def check_special_symbols(arg: str) -> bool:
    """
    Функция проверяет, является ли переданный аргумент спец символом,
    содержащимся с строке special_symbols.
    :param arg: переданный аргумент
    :return: булевые результат
    """
    return arg in special_symbols


def main():
    """
    Основная функция, которая принимает от пользователя аргумент и выводит
    на печать результат функции check_special_symbols.
    :return:
    """
    arg = input('Введите аргумент')
    print(check_special_symbols(arg))


if __name__ == '__main__':
    main()
