# 2ая функция – переводит переданное число в тип float, если передана строка
# вызывает пользовательскую ошибку InputTypeError (создать ее самому)


class InputTypeError(Exception):
    """
    Ошибка с заданным сообщением, вызывается при условии описанном в функции.
    """
    def __init__(self, message='Введенный формат не может быть переведен'
                               ' во float'):
        super().__init__(message)


def check_float_type(num: any) -> float:
    """
    Функция, переводит переданное число в тип float, если передана строка
    вызывает пользовательскую ошибку InputTypeError.
    :param num: переданное число
    :return: число во float
    """
    try:
        num = float(num)
    except ValueError:
        raise InputTypeError
    return num


def main():
    """
    Основная функция, принимает от пользователя число и выводит результат.
    """
    num = input('Введите число')
    print(check_float_type(num))


if __name__ == '__main__':
    main()
