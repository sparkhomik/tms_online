from basket_page import BasketPage
from main_page_locators import MainPageLocators
from base_page import BasePage
from books_page import BookPage


class MainPage(BasePage):
    """
    Класс, наследуемый от BasePage, в котором реализованы методы для работы
    с элементами на основной странице.
    """
    URL = "http://selenium1py.pythonanywhere.com/en-gb/"

    def __init__(self, driver):
        """
        Конструктор экземпляра класса с аттрибутом: driver
        :param driver: драйвер
        """
        super().__init__(driver, self.URL)

    def open_main_page(self):
        """
        Открытие основной страницы.
        :return: основная страница - объект класса MainPage
        """
        return MainPage(self.driver)

    def get_title(self):
        """
        Получение заглавия основной страницы на вкладке.
        :return: заглавие страницы
        """
        return self.driver.title

    def get_header(self):
        """
        Получение заголовка основной старницы.
        """
        element = self.find_element(MainPageLocators.HEADER_LOCATORS)
        return element

    def get_search_input_field(self):
        """
        Получение поля ввода для поиска книги.
        """
        element = self.find_element(MainPageLocators.
                                    SEARCH_INPUT_FIELD_LOCATORS)
        return element

    def get_search_button(self):
        """
        Получение кнопки для поска.
        """
        element = self.find_element(MainPageLocators.SEARCH_BUTTON_LOCATORS)
        return element

    def get_search_result(self):
        """
        Получение сообщения на странице с результатом поиска.
        """
        element = self.find_element(MainPageLocators.SEARCH_RESULT_LOCATORS)
        return element

    def get_search_result_message_with_number(self):
        """
        Получение сообщения на странице с результатом поиска о количестве
        найденныйх книг.
        """
        element = self.find_element(MainPageLocators.
                                    SEARCH_RESULT_NUMBER_FOUND)
        return element

    def open_books_page(self) -> BookPage:
        """
        Открытие страницы со всеми книгами.
        :return: страница с книгами - объект класса BookPage
        """
        books_link = self.find_element(MainPageLocators.BOOKS_REF_LOCATORS)
        books_link.click()
        return BookPage(self.driver, self.driver.current_url)

    def open_basket_page(self) -> BasketPage:
        """
        Откртытие страницы с корзиной.
        :return: страница с корзиной - объект класса BasketPage
        """
        basket_page_link = self.find_element(MainPageLocators.
                                             BASKET_REF_LOCATORS)
        basket_page_link.click()
        return BasketPage(self.driver, self.driver.current_url)

    def get_welcome_message(self):
        """
        Получение сообщения приветствия на основной старнице.
        """
        welcome_message = self.find_element(MainPageLocators.WELCOME_MESSAGE)
        return welcome_message
