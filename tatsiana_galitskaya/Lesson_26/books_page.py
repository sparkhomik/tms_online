from selenium.webdriver.common.by import By
from books_page_locators import BooksPageLocators
from base_page import BasePage
from book_overview_page import BookOverviewPage


class BookPage(BasePage):
    """
    Класс, в котором реализованы методы для работы с элементами на странице
    Books.
    """

    def get_title(self) -> any:
        """
        Получение заголовка страницы с книгами на вкладке.
        """
        return self.driver.title

    def get_header(self) -> any:
        """
        Получение заголовка на старнице с книгами.
        """
        element = self.find_element(BooksPageLocators.HEADER_LOCATORS)
        return element

    def get_site_tree(self) -> any:
        """
        Получение дерева переходов на странице
        """
        element = self.find_element(BooksPageLocators.SITE_TREE_LOCATORS)
        return element

    def open_fiction_page(self):
        """
        Открытие раздела fiction.
        :return: страница с fiction книгами
        """
        fiction_page_link = self.find_element(BooksPageLocators.
                                              FICTION_REF_LOCATORS)
        fiction_page_link.click()
        return BookPage(self.driver, self.driver.current_url)

    def find_book_by_name(self, name: str):
        """
        Поиск книги по полному имени в 2х вариантах: либо в одинарных кавычках,
        либо в двойных (для случая если в имени книги есть апостроф)
        :param name: аттрибут с имененм книги
        :return: найденная книга
        """
        book = self.find_element((By.XPATH, f"//a[@title='{name}']"))
        if book is not None:
            return book
        else:
            return self.find_element((By.XPATH, f'//a[@title="{name}"]'))

    def open_book_by_name(self, name: str) -> BookOverviewPage:
        """
        Открытие найденной книги по имени.
        :param name: аттрибут с именем книги
        :return: страница с превью найденной книги - объект BookOverviewPage
        класса
        """
        book = self.find_book_by_name(name)
        book.click()
        return BookOverviewPage(self.driver, self.driver.current_url)
