import pytest


def test_main_page_is_open(open_main_page, driver):
    """
    Тест для проверки открытия основной страницы. Проверяем: заглавие на
    странице, заголовок страницы, сообщение приветствия на основной странице
    :param open_main_page: фикстура open_main_page
    :param driver: драйвер
    """
    assert "Oscar" in open_main_page.get_title(), "Incorrect title"
    assert open_main_page.get_header().text == "Oscar", "Incorrect header name"
    assert open_main_page.get_welcome_message().text == "Welcome!"


@pytest.mark.parametrize('name', ["The shellcoder's handbook",
                                  'Hacking Exposed Wireless',
                                  "The Girl Who Kicked the Hornet's Nest"])
def test_search_on_main_page(open_main_page, driver, name: str):
    """
    Параметризированный тест поиска на основной странице. Проверяем что имя
    искомой книги содержится на странице в сообщении с результом поиска, а
    также что количество найденных книг > 0.
    :param open_main_page: фикстура открытия основной страницы
    :param driver: драйвер
    :param name: аттрибут - имя книги для поиска
    """
    search = open_main_page.get_search_input_field()
    search.send_keys(name)
    search_button = open_main_page.get_search_button()
    search_button.click()
    assert open_main_page.get_search_result().text == f'Products matching' \
                                                      f' "{name}"'
    assert int(open_main_page.get_search_result_message_with_number().text) > 0
