

def test_basket_page_is_open(open_page_with_basket, driver):
    """
    Тест для проверки открытия страницы корзины. Проверяем: заглавие на
    странице, заголовок страницы, дерево переходов на странице.
    :param open_page_with_basket: фикстура открытия страницы корзины
    :param driver: драйвер
    """
    assert open_page_with_basket.get_header().text == "Basket",\
        "Incorrect header name"
    assert open_page_with_basket.get_site_tree(), "Tree exists"
    assert open_page_with_basket.get_site_tree().text == "Basket",\
        "Incorrect tree name"


def test_empty_basket_by_default(open_page_with_basket, driver):
    """
    Тест для проверки что корзина пуста по дефолту по наличию соответствующего
    сообщения.
    :param open_page_with_basket: фикстура открытия страницы корзины
    :param driver: драйвер
    """
    message = "Your basket is empty. Continue shopping"
    assert open_page_with_basket.get_empty_basket_by_default().text == message


def test_continue_shopping_redirects_to_the_main_page(open_page_with_basket,
                                                      driver):
    """
    Проверка что ссылка Continue shopping из пустой корзины перенаправляет на
    остновнуя страницу.
    :param open_page_with_basket: фикстура открытия страницы корзины
    :param driver: драйвер
    """
    continue_shopping = open_page_with_basket.get_continue_shopping_link()
    continue_shopping.click()
    assert open_page_with_basket.get_welcome_message().text == "Welcome!"
