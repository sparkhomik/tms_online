import pytest


def test_books_page_is_open(open_page_with_all_books, driver):
    """
    Тест для проверки открытия страницы с книгами: заголовок,
    заглавие на странице, дерево переходов на странице.
    :param open_page_with_all_books: фикстура октрытия страницы с книгами
    :param driver: драйвер
    """
    assert open_page_with_all_books.get_header().text == "Books", \
        "Incorrect header name"
    assert open_page_with_all_books.get_site_tree(), "Tree not exists"
    assert open_page_with_all_books.get_site_tree().text == "Books", \
        "Incorrect tree name"


@pytest.mark.parametrize('name', ['Hacking Exposed Wireless',
                                  'The Girl Who Played with Non-Fire',
                                  'The Girl with the Dragon Tattoo'])
def test_find_book_by_name(open_page_with_all_books, driver, name: str):
    """
    Параметризированный тест поиска книг по названию. Принимает названия книг
    в качестве аргументов.
    :param open_page_with_all_books: фикстура открытия страницы с книгами
    :param driver: драфвер
    :param name: аттрибут с названием книги
    """
    assert open_page_with_all_books.find_book_by_name(name), "Book not exists"


@pytest.mark.parametrize('name', ["The shellcoder's handbook",
                                  'Hacking Exposed Wireless',
                                  "The Girl Who Kicked the Hornet's Nest"])
def test_open_book_by_name(open_page_with_all_books, driver, name: str):
    """
    Параметризированный тест открытия найденной книги по названию. Принимает
    названия книг которые надо найти и открыть их превью. Проверяет заголовок
    на странице с названием книги, заглавние, наличие изображения.
    :param open_page_with_all_books: фикстура открытия страницы скнигами
    :param driver: драйвер
    :param name: аттрибут с названием книги
    """
    book = open_page_with_all_books.open_book_by_name(name)
    assert name in book.get_title(), "Incorrect title"
    assert book.get_header().text == name, "Incorrect book name"
    assert book.get_book_image(), "Image not exists"
