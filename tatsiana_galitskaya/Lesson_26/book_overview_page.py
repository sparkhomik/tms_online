from book_overview_page_locators import SingleBooksPageLocators

from base_page import BasePage


class BookOverviewPage(BasePage):
    """
    Класс, в котором реализованы методы для работы с элементами на странице
    превью отдельно открытой книги.
    """

    def get_title(self):
        """
        Получение заголовка страницы превью отдельной книги на вкладке.
        """
        return self.driver.title

    def get_header(self):
        """
        Получение заголовка на старнице превью отдельной книги.
        """
        element = self.find_element(SingleBooksPageLocators.BOOK_NAME_LOCATORS)
        return element

    def get_book_image(self):
        """
        Находим изображение книги на превью.
        """
        element = self.find_element(SingleBooksPageLocators.
                                    IMAGE_BOOK_LOCATORS)
        return element
