from selenium.webdriver.common.by import By


class SingleBooksPageLocators:
    """
    Класс для описания локаторов на странице превью отдельно открытой книги.
    """
    BOOK_NAME_LOCATORS = (By.CSS_SELECTOR, "div > h1")
    SITE_TREE_LOCATORS = (By.CSS_SELECTOR, "ul.breadcrumb > li.active")
    IMAGE_BOOK_LOCATORS = (By.CSS_SELECTOR, ".item.active img[src]")
