from selenium.webdriver.common.by import By


class BooksPageLocators:
    """
    Класс для описания локтаоров на странице со всеми книгами.
    """
    HEADER_LOCATORS = (By.CSS_SELECTOR, "div > h1")
    SITE_TREE_LOCATORS = (By.CSS_SELECTOR, "ul.breadcrumb > li.active")
    FICTION_REF_LOCATORS = (By.CSS_SELECTOR, '.page a[href="/en-gb/catalogue/'
                                             'category/books/fiction_3/"]')
