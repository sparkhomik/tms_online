from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Класс для описания локаторов на основной старнице.
    """
    HEADER_LOCATORS = (By.CSS_SELECTOR, 'a[href="/en-gb/"]')
    BOOKS_REF_LOCATORS = (By.CSS_SELECTOR,
                          'a[href="/en-gb/catalogue/category/books_2/"]')
    BASKET_REF_LOCATORS = (By.CSS_SELECTOR, 'span > a.btn.btn-default')
    WELCOME_MESSAGE = (By.CSS_SELECTOR, ".well.well-blank h2")
    SEARCH_BUTTON_LOCATORS = (By.CSS_SELECTOR, 'input[value="Search"]')
    SEARCH_INPUT_FIELD_LOCATORS = (By.CSS_SELECTOR, 'input[type="search"]')
    SEARCH_RESULT_LOCATORS = (By.CSS_SELECTOR, 'div > h1')
    SEARCH_RESULT_NUMBER_FOUND = (By.CSS_SELECTOR, 'form strong')
