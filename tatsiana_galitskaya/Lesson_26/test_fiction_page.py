

def test_fiction_page_is_open(open_fiction_books, driver):
    """
    Тест для проверки открытия страницы с Fiction книгами. Проверяем: заглавие
    на странице, заголовок страницы, дерево переходов на страницу.
    :param open_fiction_books: фикстура открытия раздела fiction
    :param driver: драйвер
    """
    assert open_fiction_books.get_header().text == "Fiction", \
        "Incorrect header name"
    assert open_fiction_books.get_site_tree(), "Tree exists"
    assert open_fiction_books.get_site_tree().text == "Fiction", \
        "Incorrect tree name"
