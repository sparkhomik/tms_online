from selenium.common.exceptions import TimeoutException
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


class BasePage:
    """
    Класс, в котором реализованы методы для работы с webdriver.
    """

    def __init__(self, driver: any, url: any):
        """
        Конструктор экземпляра класса с аттрибутами: driver, url
        :param driver: драйвер
        :param url: ссылка страницы
        """
        self.driver = driver
        self.url = url

    def open(self):
        """
        Функция открытия старницы по ссылке.
        :return:
        """
        self.driver.get(self.url)

    def find_element(self, locator: any, timeout=10):
        """
        Функция поиска элемента на странице с явным ожиданием.
        :param locator: локатор для поиска элемента
        :param timeout: время ожидания
        :return: элемент либо None, после перехвата ошибки
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(
                EC.presence_of_element_located(locator))
            return element
        except TimeoutException:
            return None
