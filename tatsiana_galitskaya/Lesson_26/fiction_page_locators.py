from selenium.webdriver.common.by import By


class FictionPageLocators:
    """
    Класс для описания локаторов на странице с fiction книгами.
    """
    HEADER_LOCATORS = (By.CSS_SELECTOR, "div.page-header.action > h1")
    SITE_TREE_LOCATORS = (By.CSS_SELECTOR, "ul.breadcrumb > li.active")
