import pytest
from selenium.webdriver import Chrome

from main_page import MainPage


@pytest.fixture(scope="session")
def driver():
    """
    Фикстура открытия и закрытия браузера.
    """
    driver = Chrome(executable_path='/Users/tatsianagalitskaya/Downloads/'
                                    'chromedriver')
    yield driver
    driver.quit()


@pytest.fixture()
def open_main_page(driver):
    """
    Фикстура открытия основной страницы.
    """
    page = MainPage(driver)
    page.open()
    page = page.open_main_page()
    return page


@pytest.fixture()
def open_page_with_all_books(open_main_page, driver):
    """
    Фикстура открытия страницы с книгами.
    :param open_main_page: передаем фикстуру открытия основной страницы
    :param driver: драйвер
    :return: страница с книгами
    """
    books = open_main_page.open_books_page()
    return books


@pytest.fixture()
def open_page_with_basket(open_main_page, driver):
    """
    Фикстура открытия страницы с корзиной.
    :param open_main_page: передаем фикстуру открытия основной страницы
    :param driver: драйвер
    :return: старница с корзиной
    """
    basket_page = open_main_page.open_basket_page()
    return basket_page


@pytest.fixture()
def open_fiction_books(open_page_with_all_books, driver):
    """
    Фикстура перехода на страницу с fiction книгами.
    :param open_page_with_all_books: передаем фикстуру открытия страницы
    со всеми книгами
    :param driver: драйвер
    :return: страница с fiction книгами
    """
    fiction_page = open_page_with_all_books.open_fiction_page()
    return fiction_page
