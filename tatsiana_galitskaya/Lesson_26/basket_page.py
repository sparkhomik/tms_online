from base_page import BasePage
from basket_page_locators import BasketPageLocators
from main_page_locators import MainPageLocators


class BasketPage(BasePage):
    """
    Класс, в котором реализованы методы для работы с элементами на странице
    корзины.
    """

    def get_title(self):
        """
        Получение заголовка страницы с корзиной на вкладке.
        """
        return self.driver.title

    def get_header(self):
        """
        Получение заголовка на старнице корзины.
        """
        element = self.find_element(BasketPageLocators.HEADER_LOCATORS)
        return element

    def get_site_tree(self):
        """
        Получение дерева переходов на странице
        """
        element = self.find_element(BasketPageLocators.SITE_TREE_LOCATORS)
        return element

    def get_empty_basket_by_default(self):
        """
        Получение сообщения о том что корзина пуста.
        """
        element = self.find_element(BasketPageLocators.MESSAGE_EMPTY_BASKET)
        return element

    def get_continue_shopping_link(self):
        """
        Получение ссылки 'Continue shopping'
        """
        element = self.find_element(BasketPageLocators.CONTINUE_SHOPPING_REF)
        return element

    def get_welcome_message(self):
        """
        Получение сообщения приветствия на основной старнице.
        """
        welcome_message = self.find_element(MainPageLocators.WELCOME_MESSAGE)
        return welcome_message
