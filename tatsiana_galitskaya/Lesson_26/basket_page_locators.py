from selenium.webdriver.common.by import By


class BasketPageLocators:
    """
    Класс для описания локаторов на странице корзины.
    """
    HEADER_LOCATORS = (By.CSS_SELECTOR, "div.page-header.action > h1")
    SITE_TREE_LOCATORS = (By.CSS_SELECTOR, "ul.breadcrumb > li.active")
    MESSAGE_EMPTY_BASKET = (By.CSS_SELECTOR, "div p")
    CONTINUE_SHOPPING_REF = (By.CSS_SELECTOR, 'p > a[href="/en-gb/"')
