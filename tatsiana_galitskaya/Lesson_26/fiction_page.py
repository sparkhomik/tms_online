from base_page import BasePage
from fiction_page_locators import FictionPageLocators


class FictionPage(BasePage):
    """
    Класс, в котором реализованы методы для работы с элементами на странице
    Fiction.
    """

    def get_title(self):
        """
        Получение заголовка страницы с fiction книгами на вкладке.
        """
        return self.driver.title

    def get_header(self):
        """
        Получение заголовка на старнице с fiction книгами.
        """
        element = self.find_element(FictionPageLocators.HEADER_LOCATORS)
        return element

    def get_site_tree(self):
        """
        Получение дерева переходов на странице
        """
        element = self.find_element(FictionPageLocators.SITE_TREE_LOCATORS)
        return element
