from base_page import BasePage
from shining_panda_page_locators import ShiningPandaPageLocators


class ShiningPandaPage(BasePage):
    """
    Класс, наследуемый от BasePage, в котором реализованы методы для работы
    с элементами на основной странице.
    """
    URL = "https://plugins.jenkins.io/shiningpanda/"

    def __init__(self, driver):
        """
        Конструктор экземпляра класса с аттрибутом: driver
        :param driver: драйвер
        """
        super().__init__(driver, self.URL)

    def open_shining_panda_page(self):
        """
        Открытие основной страницы.
        :return: основная страница - объект класса ShiningPandaPage
        """
        return ShiningPandaPage(self.driver)

    def get_title(self):
        """
        Получение заглавия основной страницы на вкладке.
        :return: заглавие страницы
        """
        return self.driver.title

    def get_visible_shining_panda_title(self):
        """
        Получения отображающегося элемента на странице
        :return: элемент на странице
        """
        element = \
            self.visible_element(ShiningPandaPageLocators.HEADER_LOCATORS)
        return element

    def get_shining_panda(self):
        element = self.find_element(ShiningPandaPageLocators.HEADER_LOCATORS)
        return element
