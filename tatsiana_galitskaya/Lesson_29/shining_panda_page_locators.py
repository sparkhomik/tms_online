from selenium.webdriver.common.by import By


class ShiningPandaPageLocators:
    """
    Класс для описания локаторов на старнице Shining Panda.
    """
    HEADER_LOCATORS = (By.CSS_SELECTOR, '.title-wrapper .title')
