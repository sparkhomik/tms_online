from shining_panda_page import ShiningPandaPage


def test_shining_panda_page_open(driver):
    """
    Тест для проверки открытия страницы Shining Panda. Сравнивает заглавия
    страницы на вкладке с ожидаемым.
    :param driver: драйвер
    """
    page = ShiningPandaPage(driver)
    page.open()
    page = page.open_shining_panda_page()
    assert 'ShiningPanda | Jenkins plugin' in page.get_title(), 'Incorrect'


def test_shining_panda_title(driver):
    """
    Тест для проверки отображения ShiningPanda. Проверяет что элемент
    отображается на странице и сверяет его текст с ожидаемым.
    :param driver:
    :return:
    """
    page = ShiningPandaPage(driver)
    page.open()
    assert page.get_visible_shining_panda_title()
    assert page.get_shining_panda().text, "ShiningPanda"
