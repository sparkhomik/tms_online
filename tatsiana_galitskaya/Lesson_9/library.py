# task 1
# Библиотека
"""
Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
зарезервирована ли книги или нет. Создайте класс пользователь который может
брать книгу, возвращать, бронировать. Если другой пользователь хочет взять
зарезервированную книгу(или которую уже кто-то читает
- надо ему про это сказать).
"""


class Book:
    """
    Класс Book в котором будут создаваться экземпляры книг с аттрибутом класса
    status - флаг, указывающий на статус книги, первоначальное значение free.
    """
    status = 'free'

    def __init__(self, title: str, author: str, pages: int, isbn: str):
        """
        Конструктор экземпляра класса Book с 4 аттрибутами экземпляра.
        :param title: название книги
        :param author: автор книги
        :param pages: количество станиц
        :param isbn: код ISBN
        """
        self.title = title
        self._author = author
        self.pages = pages
        self.isbn = isbn


class User:
    """
    Класс User, в котором будут создаваться пользователи.
    """

    def __init__(self, user_name: str):
        """
        Конструктор экземпляра класса User.
        :param user_name: имя пользователя
        reserved_books: список зарезервированных книг пользователя
        reading_books: список читаемых книг пользователя
        """
        self.user_name = user_name
        self.reserved_books = ['Reserved books:']
        self.reading_books = ['Reading books:']

    def reserve_book(self, book: Book):
        """
        Стандартный метод класса.
        Функция резервирования книги. Если книга свободна - меняем
        ее статус на reserved и передаем название книги в список
        зарезервированных книг этого пользователя.
        :param book: книга, экземпляо класса Book
        """
        if book.status == 'free':
            book.status = 'reserved'
            self.reserved_books.append(book.title)
        else:
            print('This book is not available now')

    def read_book(self, book: Book):
        """
        Стандартный метод класса.
        Функция чтения книги. Если книга свободна или она у данного
        пользователя в резерве - меняем ее статус на in reading и передаем
        название книги в список читаемых книг этого пользователя.
        :param book: книга, экземпляо класса Book
        """
        if book.status == 'free' or book.title in self.reserved_books:
            book.status = 'in reading'
            self.reading_books.append(book.title)
        else:
            print('This book is not available now')
        if book.title in self.reserved_books:
            self.reserved_books.remove(book.title)

    def return_book(self, book):
        """
        Стандартный метод класса.
        Функция возврата книги или отмены брони. Меняет статус книги на free
        и удаляет из списков читаемых или зарезервированных книг данного
        пользователя.
        :param book: книга, экземпляо класса Book
        """
        book.status = 'free'
        if book.title in self.reserved_books:
            self.reserved_books.remove(book.title)
        if book.title in self.reading_books:
            self.reading_books.remove(book.title)


def main():
    """
    Основная функция. Создает экземпляры класса Book и User.
    Вызывает методы 2х классов, выводит на печать списки зарезервированных
    и читаемых книг пользователей.
    """
    book1 = Book("Alice's Adventures in Wonderland", 'Lewis Carroll', 116,
                 '9780194227230')
    book2 = Book("Sapiens: A Brief History of Humankind", 'Yuval Noah Harari',
                 443, '9780062316097')
    book3 = Book("When Nietzsche Wept", 'Irvin D. Yalom', 320, '9780465091720')
    book4 = Book("Hotel", 'Arthur Hailey', 416, '9780553146394')
    book5 = Book('Python Crash Course', 'Eric Matthes', 496, '9781593276034')
    user1 = User('Tatsiana')
    user2 = User('Alex')
    user3 = User('Michael')
    user4 = User('Julia')
    User.reserve_book(user1, book5)   # резервирует книгу 5
    User.read_book(user1, book5)   # берет читать книгу 5
    User.read_book(user2, book5)   # пытается взять читать книгу 5
    User.read_book(user3, book5)   # пытается взять читать книгу 5
    User.read_book(user2, book1)   # берет читать книгу 1
    User.read_book(user2, book2)   # берет читать еще одну книгу 2
    User.reserve_book(user3, book4)   # резервирует книгу 4
    User.read_book(user3, book4)   # берет читать книгу 4
    print('Пользователь 3:', user3.reserved_books, user3.reading_books)
    User.return_book(user3, book4)   # возвращает книгу 4
    print('Пользователь 3:', user3.reserved_books, user3.reading_books)
    User.read_book(user3, book3)   # берет читать книгу 3
    User.read_book(user4, book3)   # пытается взять читать книгу 3
    User.reserve_book(user4, book4)   # резервирует книгу 4
    User.return_book(user4, book4)   # отменяет бронь на книгу 4
    print('Пользователь 1:', user1.reserved_books, user1.reading_books)
    print('Пользователь 2:', user2.reserved_books, user2.reading_books)
    print('Пользователь 3:', user3.reserved_books, user3.reading_books)
    print('Пользователь 4:', user4.reserved_books, user4.reading_books)


if __name__ == '__main__':
    main()
