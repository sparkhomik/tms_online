# task2
# Банковский вклад
"""
Создайте класс инвестиция. Который содержит необходимые поля и методы,
например сумма инвестиция и его срок.
Пользователь делает инвестиция в размере N рублей сроком на R лет под 10%
годовых (инвестиция с возможностью ежемесячной капитализации
- это означает, что проценты прибавляются к сумме инвестиции ежемесячно).
Написать класс Bank, метод deposit принимает аргументы N и R, и возвращает
сумму, которая будет на счету пользователя.
"""


class Investment:
    """
    Класс Инвестиция с аттрибутом класса rate - %-ная ставка с заданным
    значением в 10%.
    """
    rate = 10

    def __init__(self, amount: int, term: int):
        """
        Конструктор класса Investment, создает экземпляр класса Investment
        с передаваемыми параметрами amount и term.
        :param amount: сумма вклада
        :param term: срок вклада
        """
        self.amount = amount
        self.term = term

    @classmethod
    def month_capitalization(cls, amount: int, term: int) -> float:
        """
        Метод класса.
        Функция, вычисляющая сумму ежемесячной капиталицации и общую сумму
        накоплений на счету.
        :param amount: сумма квлада
        :param term: срок вклада
        :return: сумма на счету по истечению срока вклада
        """
        month = 1
        while month <= term:
            month_capitalization = round((cls.rate / 12 * amount) / 100, 2)
            amount = round(month_capitalization + amount, 2)
            month += 1
        return amount


class Bank:
    """
    Класс Bank, с функцией вызыва метода из класса Investment.
    """
    @staticmethod
    def deposit(amount: int, term: int) -> float:
        """
        Статический метод класса Bank.
        Функция вызывает метод класса Investment и передает введенные
        пользователем параметры.
        :param amount: сумма инвестиции
        :param term: срок инвестиции
        result - сумма на счету по истечению срока вклада
        """
        result = Investment.month_capitalization(amount, term)
        return result


def main():
    """
    Основная функция программы. Собираем данные пользователя
    - сумму и срок вклада, вызывает метод класса Bank и выводит результат.
    """
    n = int(input('Введите сумму инвестиции'))
    r = int(input('На какой срок вы хотите сделать инвестиции (в мес.)?'))
    result = Bank.deposit(n, r)
    print(f'Сумма накопленная на счету составит:'
          f' {result}')


if __name__ == '__main__':
    main()
