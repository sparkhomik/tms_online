# Найти все локаторы для функциональных элементов, т.е.
# иконки, кнопки, поля ввода и т.д.
# Найти локаторы для окна Login, залогиниться и найти все элементы в каталоге
# (standard_user), вывести информацию по всем товарам, которая возвращает их
# название и цену. Для всех остальных элементов создать методы, которые
# находят их на странице и возвращают элемент. Предусмотреть логирование
# выполненных действий


from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from logzero import logger


def ger_login_logo(driver):
    """
    Функция нахождения лого имеджа на странице логина.
    :return: найденный элемент
    """
    element = driver.find_element(By.CSS_SELECTOR, '.login_logo')
    return element


def get_login(driver):
    """
    Функция логина. Вводин имя пользователя, пароль, нажимает кнопку логина.
    """
    username = driver.find_element(By.CSS_SELECTOR, '#user-name')
    username.send_keys('standard_user')
    logger.info('Username entered')
    password = driver.find_element(By.CSS_SELECTOR, '#password')
    password.send_keys('secret_sauce')
    logger.debug('Password entered')
    login_button = driver.find_element(By.CSS_SELECTOR, '#login-button')
    login_button.click()
    logger.info("User logged in")


def get_all_elements(driver):
    """
    Функция нахождения всех элементов в каталоге standard_user.
    :return: список всех найденных элементов на странице
    """
    all_elements = driver.find_elements(By.CSS_SELECTOR, '.inventory_item')
    logger.info("All elements in catalog was found")
    return all_elements


def get_all_elements_info(driver):
    """
    Функция нахождения информации по всем найденным элементам на странице:
    возвращает их название и цену.
    :return: список информации по каждому элементу
    """
    num = len(get_all_elements(driver))
    names = driver.find_element(By.CSS_SELECTOR, '.inventory_item_name')
    prices = driver.find_element(By.CSS_SELECTOR, '.inventory_item_price')
    items_info = list()
    for i in range(num):
        items_info.append(f'{names.text} : {prices.text}')
    items_info = (', '.join(items_info))
    logger.info("All elements info was found")
    return items_info


def get_app_logo(driver):
    """
    Функция нахождения лого имеджа на странице каталога.
    :return: найденный элемент
    """
    element = driver.find_element(By.CSS_SELECTOR, '.app_logo')
    logger.info("App logo was found")
    return element


def get_burger_button(driver):
    """
    Функция нахождения бургер-кнопки
    :return: найденный элемент
    """
    element = driver.find_element(By.CSS_SELECTOR, '#react-burger-menu-btn')
    logger.info("Burger button was found")
    return element


def get_shopping_cart_button(driver):
    """
    Функция нахождения кнопки для перехода в корзину.
    :return: найденный элемент
    """
    element = driver.find_element(By.CSS_SELECTOR, '.shopping_cart_link')
    logger.info("Shopping cart button was found")
    return element


def get_sorting_button(driver):
    """
    Функция нахождения книоки сортировки товаров на тсранице.
    :return: найденный элемент
    """
    element = driver.find_element(By.CSS_SELECTOR, '.product_sort_container')
    logger.info("Sorting button was found")
    return element


def get_link_twitter(driver):
    """
    Функция нахождения ссылки для перехода в twitter.
    :return: найденный элемент
    """
    locator = 'a[href="https://twitter.com/saucelabs"]'
    element = driver.find_element(By.CSS_SELECTOR, locator)
    logger.info("Twitter link was found")
    return element


def get_link_facebook(driver):
    """
    Функция нахождения ссылки для перехода в facebook.
    :return: найденный элемент
    """
    locator = 'a[href="https://www.facebook.com/saucelabs"]'
    element = driver.find_element(By.CSS_SELECTOR, locator)
    logger.info("Facebook link was found")
    return element


def get_link_linkedin(driver):
    """
    Функция нахождения ссылки для перехода в linkedin.
    :return: найденный элемент
    """
    locator = 'a[href="https://www.linkedin.com/company/sauce-labs/"]'
    element = driver.find_element(By.CSS_SELECTOR, locator)
    logger.info("Linkedin link was found")
    return element


def get_image_footer_robot(driver):
    """
    Функция находжения имеджа в футере страницы.
    :return: найденный элемент
    """
    element = driver.find_element(By.CSS_SELECTOR, '.footer_robot')
    logger.info("Image footer robot was found")
    return element


def main():
    """
    Основная функция для запуска драйвера, открытия приложения,
    запуска остальных функций, вывода информации по товарам, закрытия браузера.
    """
    driver = Chrome(
        executable_path='/Users/tatsianagalitskaya/Downloads/chromedriver')
    driver.get("https://www.saucedemo.com/")
    logger.info("Web page was opened")
    get_login(driver)
    print(get_all_elements_info(driver))
    get_app_logo(driver)
    get_burger_button(driver)
    get_shopping_cart_button(driver)
    get_sorting_button(driver)
    get_link_twitter(driver)
    get_link_facebook(driver)
    get_link_linkedin(driver)
    get_image_footer_robot(driver)
    driver.close()
    logger.info("Browser was closed")


if __name__ == '__main__':
    main()
