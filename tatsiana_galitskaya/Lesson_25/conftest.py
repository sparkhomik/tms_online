import pytest
from selenium.webdriver import Chrome


@pytest.fixture(scope='session')
def driver():
    """
    Фикстура для запуска, открытия страницы и закрытия драйвера в конце сессии.
    :return:
    """
    driver = Chrome(
        executable_path='/Users/tatsianagalitskaya/Downloads/chromedriver')
    driver.get('http://the-internet.herokuapp.com/frames')
    yield driver
    driver.quit()


@pytest.fixture
def find_iframe_link(driver):
    """
    Фикстура нахождения ссылки iFrame и перехода по ней.
    """
    element = driver.find_element_by_xpath('//a[@href="/iframe"]')
    element.click()
    return driver


@pytest.fixture
def find_the_text_into_paragraph(driver):
    """
    Фикстура переклюдения во frame и нахождения текста внутри параграфа.
    :return: элемент с текстом внутри параграфа
    """
    iframe_locator = 'iframe[id="mce_0_ifr"]'
    text_locator = '//body[@class="mce-content-body "]'
    driver.switch_to.frame(driver.find_element_by_css_selector(iframe_locator))
    element = driver.find_element_by_xpath(text_locator)
    return element
