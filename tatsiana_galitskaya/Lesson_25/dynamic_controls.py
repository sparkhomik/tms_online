# Dynamic Controls
# Найти чекбокс
# Нажать на кнопку
# Дождаться надписи “It’s gone”
# Проверить, что чекбокса нет
# Найти инпут
# Проверить, что он disabled
# Нажать на кнопку
# Дождаться надписи “It's enabled!”
# Проверить, что инпут enabled
import time
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Chrome


def find_checkbox(driver):
    """
    Функция для нахождения чекбокса.
    :return: чекбокс
    """
    element = driver.find_element_by_xpath('//input[@type="checkbox"]')
    return element


def find_remove_button(driver):
    """
    Функция нахождения кнопки удаления чекбокса.
    """
    remove_button_locator = '//button[@onclick="swapCheckbox()"]'
    element = driver.find_element_by_xpath(remove_button_locator)
    return element


def get_the_message(driver):
    """
    Функция нахождения сообщения о том что чекбокс удален.
    """
    element = driver.find_element_by_xpath('//p[@id="message"]')
    return element


def check_checkbox_unavailable(driver) -> bool:
    """
    Функция для проверки доступен ли чекбокс.
    :return: True или False
    """
    try:
        driver.find_element_by_xpath('//input[@type="checkbox"]')
        return False
    except NoSuchElementException:
        return True


def find_the_input(driver):
    """
    Функция нахождения поля для ввода.
    """
    element = driver.find_element_by_xpath('//input[@type="text"]')
    return element


def check_input_unavailable(driver) -> str:
    """
    Функция проверки доступно ли поле для ввода.
    :return: строка с результатом
    """
    if not find_the_input(driver).is_enabled():
        result = 'Disabled'
    else:
        result = 'Enabled'
    return result


def find_enable_button(driver):
    """
    Функция нахождения кнопки для разблокировки поля ввода.
    """
    element = driver.find_element_by_xpath('//button[@onclick="swapInput()"]')
    return element


def dynamic_controls():
    """
    Основная функция, запускает драйвер и остальные необходимые функции.
    """
    driver = Chrome(
        executable_path='/Users/tatsianagalitskaya/Downloads/chromedriver')
    driver.get('http://the-internet.herokuapp.com/dynamic_controls')
    # находит чекбокс
    find_checkbox(driver)
    # находит кнопку удаления чекбокса и нажимает ее
    find_remove_button(driver).click()
    time.sleep(5)
    # находит текст об удалении чекбокса и выводит результат
    print('The message appears:', get_the_message(driver).text)
    # проверяет что чекбокса нет и выводит результат
    print('Checkbox unavailable:', check_checkbox_unavailable(driver))
    # проверяет что поле ввода недоступно и выводит результат
    print('Input in status:', check_input_unavailable(driver))
    # находит кнопку разблокироваки поля ввода и нажимает ее
    find_enable_button(driver).click()
    time.sleep(5)
    # находит текст о разблокировке поля ввода и выводит результат
    print('The message appears:', get_the_message(driver).text)
    # проверяет доступность поля ввода и выводит результат
    print('Input in status:', check_input_unavailable(driver))
    driver.quit()


if __name__ == '__main__':
    dynamic_controls()
