# Frames
# Открыть iFrame
# Проверить, что текст внутри параграфа равен “Your content goes here.”


def test_dynamic_controls(find_iframe_link, find_the_text_into_paragraph):
    """
    Тест для сверки текста внутри параграфа во фрейме с ожидаемым.
    """
    assert find_the_text_into_paragraph.text == 'Your content goes here.'
