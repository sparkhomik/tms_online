# task 1
# Цветочница
"""
Определить иерархию и создать несколько цветов (Розы, Тюльпаны, Фиалки,
Ромашки). У каждого класса цветка следующие атрибуты:
Стоимость – атрибут класса, который определяется заранее
Свежесть (в днях), цвет, длина стебля – атрибуты экземпляра
При попытке вывести информацию о цветке, должен отображаться цвет и тип.
Собрать букет (можно использовать аксессуары – отдельные классы со своей
стоимостью: например, упаковочная бумага) с определением ее стоимости.
У букета должна быть возможность определить время его увядания по среднему
времени жизни всех цветов в букете.
Позволить сортировку цветов в букете на основе различных параметров
(свежесть/цвет/длина стебля/стоимость...)
Реализовать поиск цветов в букете по определенным параметрам.
Узнать, есть ли цветок в букете.
Добавить возможность получения цветка по индексу либо возможность пройтись
по букету и получить каждый цветок по-отдельности
"""


class Flowers:
    """
    Класс Цветы, от которого будут создаваться экземпляры цветов с аттрибутами:
    flower_type, freshness, color, length. flower_type защищенный аттрибут.
    """
    @property
    def flower_type(self) -> str:
        """
        Доступ к защищенному аттрибуту flower_type.
        :return: пустая строка
        """
        return ''

    def __init__(self, freshness: int, color: str, length: int):
        """
        Конструктор экземпляра класса с аттрибутами: freshness, color, length.
        :param freshness: свежесть (в днях)
        :param color: цвет
        :param length: длина стебля
        """
        self.freshness = freshness
        self.color = color
        self.length = length

    def __str__(self) -> str:
        """
        Магический метод, при обращении к экземпляру класса выводит информацию
        о цветке: цвет и тип.
        :return: строка
        """
        return f'{self.color} {self.flower_type}'


class Roses(Flowers):
    """
    Класс наследник от Flowers, в нем будут создаваться экземпляры роз с
    защищенным аттрибутом flower_type и аттрибутом класса cost.
    """
    @property
    def flower_type(self) -> str:
        """
        Доступ к защищенному аттрибуту flower_type.
        :return: строка с именем цветка
        """
        return 'Роза'
    cost = 10


class Tulips(Flowers):
    """
    Класс наследник от Flowers, в нем будут создаваться экземпляры тюльпанов с
    защищенным аттрибутом flower_type и аттрибутом класса cost.
    """
    @property
    def flower_type(self) -> str:
        """
        Доступ к защищенному аттрибуту flower_type.
        :return: строка с именем цветка
        """
        return 'Тюльпан'
    cost = 7


class Violets(Flowers):
    """
    Класс наследник от Flowers, в нем будут создаваться экземпляры фиалок с
    защищенным аттрибутом flower_type и аттрибутом класса cost.
    """
    @property
    def flower_type(self) -> str:
        """
        Доступ к защищенному аттрибуту flower_type.
        :return: строка с именем цветка
        """
        return 'Фиалка'
    cost = 6


class Daisies(Flowers):
    """
    Класс наследник от Flowers, в нем будут создаваться экземпляры ромашек с
    защищенным аттрибутом flower_type и аттрибутом класса cost.
    """
    @property
    def flower_type(self) -> str:
        """
        Доступ к защищенному аттрибуту flower_type.
        :return: строка с именем цветка
        """
        return 'Ромашка'
    cost = 9


class WrappingPaper:
    """
    Класс упаковочной бумаги, в нем будут создаваться экземпляры класса с
    аттрибутом cost.
    """
    def __init__(self, cost: int):
        """
        Конструктор экземпляра класса WrappingPaper с аттрибутом cost.
        :param cost:
        """
        self.cost = cost


class Bouquet:
    """
    Класс Букет. В нем создаются букеты из цветов из класса Flowers и
    совершаются действия с букетом.
    """
    def __init__(self, flowers: list, wrapping_paper=None):
        """
        Конструктор класса Bouquet для создания экземпляров класса с
        аттрибутами: flowers, wrapping_paper
        :param flowers: список цветов
        :param wrapping_paper: упаковочная бумага (может быть или нет)
        """
        self.flowers = flowers
        self.wrapping_paper = wrapping_paper

    def __getitem__(self, index: int) -> Flowers:
        """
        Магический метод индексации для получения цветка по индексу.
        :param index: индекс
        :return: цветок
        """
        return self.flowers[index]

    def __contains__(self, obj: Flowers) -> bool:
        """
        Магический метод проверки на наличие в колекции.
        :param obj: какой цветок ищем в букете
        :return: булевое значение
        """
        return obj in self.flowers

    def __str__(self) -> str:
        """
        Магический метод, при обращении к экземпляру класса Bouquet выводит
        строку содержащихся цветов через запятую.
        :return: строка
        """
        return ', '.join(str(flower) for flower in self.flowers)

    def wilt_time(self) -> int:
        """
        Функция для определения времени увядания букета по среднему времени
        жизни всех цветов в букете.
        :return: среднее время увядания
        """
        all_wilt_time = [flower.freshness for flower in self.flowers]
        average_wilt = round(sum(all_wilt_time) / len(all_wilt_time), 1)
        return average_wilt

    def bouquet_cost(self) -> int:
        """
        Функция для определения стоимости букета. Учитывает наличие упаковочной
        бумаги если она есть.
        :return: стоимость букета
        """
        flowers_cost = sum(flower.cost for flower in self.flowers)
        accessories_cost = self.wrapping_paper.cost \
            if self.wrapping_paper is not None else 0
        return flowers_cost + accessories_cost

    def find_flower_by(self, field_name: str, field_value: any) -> list:
        """
        Функция для поиска цветов в букете по определенным параметрам,
        переданным из функции вызова по этому параметру.
        :param field_name: имя поля по которому ищем
        :param field_value: значение поля, по которому ищем
        :return: список цветов
        """
        return [
            flower for flower in self.flowers
            if getattr(flower, field_name) == field_value
        ]

    def find_flower_by_cost(self, cost: int) -> list:
        """
        Функция вызова функции поиска цветов по параметру cost.
        :param cost: стоимсоть по которой ищем
        :return: вызов функции find_flower_by с параметрами
        """
        return self.find_flower_by('cost', cost)

    def find_flower_by_color(self, color: str) -> list:
        """
        Функция вызова функции поиска цветов по параметру color.
        :param color: цвет по которой ищем
        :return: вызов функции find_flower_by с параметрами
        """
        return self.find_flower_by('color', color)

    def sort_flowers_by(self, param: str) -> list:
        """
        Функция сортировки цветов в букете на основе передаваемых параметров:
        freshness, cost, color, length.
        :param param: параметр для сортировки
        :return: список отсортированных цветов
        """
        return sorted(self.flowers,
                      key=lambda flower: (getattr(flower, param)))


def main():
    """
    Основная функция. В ней создаются экземпляры классов.
    Вызывает методы этих классов, выводит на печать результаты методов.
    """
    rose_1 = Roses(15, 'Красная', 50)
    rose_2 = Roses(15, 'Желтая', 55)
    rose_3 = Roses(15, 'Белая', 60)
    tulip_1 = Tulips(20, 'Желтый', 45)
    violet_1 = Violets(12, 'Фиолетовая', 10)
    violet_2 = Violets(12, 'Синяя', 12)
    daisie_1 = Daisies(25, 'Белая', 20)
    daisie_2 = Daisies(25, 'Белая', 20)
    print(rose_1)   # вывод информации о цветке
    print(tulip_1)   # вывод информации о цветке
    wrapping_paper = WrappingPaper(1)
    flowers_1 = [rose_1, tulip_1, daisie_1, daisie_2, violet_1, violet_2]
    flowers_2 = [rose_1, rose_2, rose_3]
    bouquet1 = Bouquet(flowers_1, wrapping_paper)  # собираем первый букет
    bouquet2 = Bouquet(flowers_2)  # второй букет
    print(Bouquet.wilt_time(bouquet1))  # вывод времени увядания букета1
    print(Bouquet.wilt_time(bouquet2))  # вывод времени увядания букета2
    print(rose_1 in bouquet2)   # есть ли цветок в букете
    print(tulip_1 in bouquet1)   # есть ли цветок в букете
    print(bouquet1[3])   # получение цветка по индексу
    print(bouquet2[2])   # получение цветка по индексу
    print(bouquet1)   # вывод каждого цветка в букете
    print(bouquet1.bouquet_cost())  # стоимость букета
    print(bouquet2.bouquet_cost())  # стоимость букета
    print(Bouquet(bouquet1.find_flower_by_cost(9)))  # поиск по стоимости
    print(Bouquet(bouquet1.find_flower_by_color('Желтый')))  # по цвету
    print(Bouquet(bouquet1.sort_flowers_by('freshness')))   # сорт по увяданию
    print(Bouquet(bouquet1.sort_flowers_by('cost')))   # сорт по цене
    print(Bouquet(bouquet1.sort_flowers_by('color')))   # сорт по цвету
    print(Bouquet(bouquet1.sort_flowers_by('length')))   # сорт по длине стебля


if __name__ == '__main__':
    main()
