import yaml
import json

data = dict(A='a', B=dict(C='c', D='d', E='e', ))

# Создайте свой yaml файл
with open('data.yaml', 'w') as outfile:
    yaml.dump(data, outfile, default_flow_style=False)

# Сконвертируйте yaml файл в json
with open("order.yaml") as f, open("example.json", "w") as j:
    order = yaml.safe_load(f)
    json.dump(order, j, sort_keys=True, default=str)

# Напечатайте номер заказа
print(order["invoice"])

# Напечатайте адрес отправки
bill_to = order["bill-to"]
print(bill_to["address"])

# Напечатайте описание посылки, ее стоимость и кол-во
product = order["product"]
for i in product:
    print(f"Order details: {i['description']}, {i['price']}, {i['quantity']}")
