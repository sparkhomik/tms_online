from requests import get, post, put, delete

link = "https://fakerestapi.azurewebsites.net/api/v1/"


# Получение списка авторов
def get_authors_list():
    r = get(f"{link}Authors")
    return r.status_code


# Получение конкретного автора по его id
def get_authors_by_id(id):
    r = get(f"{link}Authors/{id}")
    return r.status_code, r.text


# Добавить новую книгу
def post_new_book(book):
    r = post(f"{link}Books", json=book)
    return r.status_code


# Добавить нового пользователя
def post_new_user(user):
    r = post(f"{link}Users", json=user)
    return r.status_code


# Обновить данные для книги под номером 10
def update_book_data(id):
    r = put(f"{link}Books/{id}", json=book)
    return r.status_code


# Удалить пользователя под номером 4
def delete_user(id):
    r = delete(f"{link}Users/{id}")
    return r.status_code


book = {
    "id": 1,
    "title": "The Grapes of Wrath",
    "description": "Set during the Great Depression, "
                   "the novel focuses on the Joads, a poor family of "
                   "tenant farmers driven from their Oklahoma home "
                   "by drought, economic hardship, agricultural industry "
                   "changes, and bank foreclosures forcing tenant farmers "
                   "out of work. Due to their nearly hopeless situation, "
                   "and in part because they are trapped in the Dust Bowl, "
                   "the Joads set out for California along with thousands "
                   "of other 'Okies' seeking jobs, land, dignity, "
                   "and a future.",
    "pageCount": 464,
    "excerpt": "How can you frighten a man whose hunger is not only "
               "in his own cramped stomach but in the wretched bellies "
               "of his children? You can't scare him – "
               "he has known a fear beyond every other.",
    "publishDate": "1939-04-14T20:09:24.749Z"
}
user = {
    "id": 1939,
    "userName": "Tom Joad",
    "password": "Dust Bowl"
}
print(get_authors_list())
print(get_authors_by_id(1))
print(post_new_book(book))
print(post_new_user(user))
print(f"update book 10 data: {update_book_data(10)}")
print(f"delete user 4 status: {delete_user(4)}")
