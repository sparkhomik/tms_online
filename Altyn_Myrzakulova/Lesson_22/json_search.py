import json


# Разработайте поиск учащихся
# в одном классе, посещающих одну секцию.
def bukatsu(b):
    d = {}
    for i in s:
        if i["Club"] == b:
            d[i["Name"]] = i["Class"]
    d2 = {}
    for k, v in d.items():
        d2.setdefault(v, set()).add(k)
    res = filter(lambda x: len(x) > 1, d2.values())
    print(list(res))


# Фильтрацию учащихся по их полу
def gender_search(g):
    for i in s:
        if i["Gender"] == g:
            print(i["Name"])


# Поиск ученика по имени(часть имени)
def name_search(n):
    for i in s:
        if n in i["Name"]:
            print(i["Name"])


if __name__ == '__main__':
    with open('students.json') as file:
        f = file.read()
        s = json.loads(f)
    print("Find students by gender:")
    gender_search('W')
    print()
    print("Find students by name:")
    name_search('ta')
    print()
    print("Find classmates in a same club:")
    bukatsu('Chess')
