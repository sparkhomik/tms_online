import xml.etree.ElementTree as elt

root = elt.parse('library.xml').getroot()


# Разработайте поиск книги в библиотеке
# по ее автору(часть имени)/цене/заголовку/описанию.
def get_book_by_tag(tag, param):
    param.lower()
    for i in root:
        for j in i.iter(f'{tag}'):
            if param in j.text:
                print(i.attrib)


if __name__ == '__main__':
    get_book_by_tag('author', 'tt')
    get_book_by_tag('price', '17.89')
    get_book_by_tag('title', 'Code')
    get_book_by_tag('description', 'nanotechnology')
