# LISTS
# 1
listA = ["a", "b", "c"]
listB = [1, 2, 3]

# 2
print(listA[1], '\n')

# 3
listB[2] = 5
print(listB, '\n')

# 4
list3 = listA + listB
print(list3, '\n')

# 5
list4 = list3[::2]
print(list4, '\n')

# 6
list3.append("k")
list3.append(9)
print(list3, '\n')

# 7
a = [1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89]
b = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10, 11, 12, 13]
list5 = list(set(a).intersection(b))
print(list5, '\n')

# 8
test_list = [1, 2, 3, 4, 3, 2, 5, 1, 4, 6, 7, 1, 8, 2, 3]
print(list(set(test_list)), '\n')

# LOG
# 1
x = 12
y = 7

# 2
print(bool(x and True and (0, 2)))
print(bool(1 and 12 and y))
print(bool([] and True and y and {2: 4, 3: '7'}))
print(bool([1, 3] and x and y and 0))

# 3
print(bool(True or x or (1, 2) or False))
print(bool(0 or [] or y))
print(bool(False or [] or 0))
print(bool({} or y - y or 0))

# 4
print('k2' == 'k9' or 'abc' == 'abc')

# DIC
# 1
school = {'1a': 36, '1b': 34, '2a': 32, '2b': 30}

# 2
print(school['2b'], '\n')

# 3
school['2b'] = 33
school['2a'] = 29
school['1b'] = 35
print('number of pupils change: ', school)

school['3a'] = 29
school['4a'] = 27
print('new classes added: ', school)

del school['2a']
print('get rid of one class: ', school)
