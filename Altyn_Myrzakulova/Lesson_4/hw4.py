import re
from collections import Counter

# 1
s1 = "Robin Singh"
print(s1.split())
s2 = "I love arrays they are my favorite"
print(s2.split(), '\n')

# 2
names = ['Ivan', 'Ivanou']
s1 = 'Minsk'
s2 = 'Belarus'
print("Привет, %s %s! Добро пожаловать в %s %s \n" % (names[0], names[1],
                                                      s1, s2))

# 3
list2 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
s = " "
print(s.join(list2), "\n")

# 4
new_list = list(range(10))
new_list[2] = 'k'
new_list.pop(6)
print(new_list, '\n')

# 5
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}
c = {**a, **b}
# 5.1
print(c)
# 5.2
z = a.copy()
z.update(b)
print(z)
# 5.3
res = not any(a.values())
print("Are value lists empty? : " + str(res))
# 5.4
print("Are value lists empty? : " + str(not all(b.values())))
# 5.5
print(sorted(z.items(), reverse=True))
# 5.6
z["b"] = 42
print(z, '\n')

# 6
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]

# 6.1
k = list(set(list_a))
print(k)
# 6.2
k.append(22)
print(k)
# 6.3
print(tuple(list_a))
# 6.4
print(len(list_a), '\n')

# 7
# 7.1
a = 10
b = 25
print('Summ is %s and diff = %s' % (a + b, a - b))
# 7.2
list_of_children = ['Sasha', 'Vasia', 'Nikalai']
print('First child is %s, second is %s, and last one – %s \n' % (
    list_of_children[0], list_of_children[1], list_of_children[2]))

# 8
lst = [1, 10, 2, 9, 2, 9, 1]
sl = Counter(lst)
keys = list(sl.keys())
vals = list(sl.values())
print(keys[vals.index(1)], '\n')

# 9
t = '"a" * 9000 + "b" * 1000'
new_string = t.lower()
# get rid of numbers
pattern = r'[0-9]'
new_string = re.sub(pattern, '', new_string)
# get rid of special characters
new_string = ''.join(filter(str.isalnum, new_string))
n_list = list(new_string)
# sorting in alphabetical order
n_list.sort(reverse=False)
sol = max(set(n_list), key=n_list.count)
print(sol)
