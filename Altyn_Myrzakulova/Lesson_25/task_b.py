import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait


@pytest.fixture(scope="session", autouse=True)
def driver():
    p = r'C:\Users\altmu\OneDrive\Рабочий стол\chromedriver_win32\chromedriver'
    chrome_driver = Chrome(executable_path=p)
    yield chrome_driver
    chrome_driver.quit()


@pytest.fixture(scope="session")
def get_iframe_txt(driver):
    driver.implicitly_wait(5)
    driver.get("http://the-internet.herokuapp.com/frames")
    # Открыть iFrame
    WebDriverWait(driver, 10).until(EC.element_to_be_clickable(
        (By.CSS_SELECTOR, "[href='/iframe']")))
    link = driver.find_element(By.CSS_SELECTOR, "[href='/iframe']")
    link.click()
    driver.switch_to.frame(driver.find_element(By.TAG_NAME, "iframe"))
    WebDriverWait(driver, 10).until(EC.presence_of_element_located(
        (By.CSS_SELECTOR, "#tinymce  p")))
    yield driver.find_element(By.CSS_SELECTOR, "#tinymce  p")


# Проверить, что текст внутри параграфа равен “Your content goes here.”
def test_get_iframe_txt(get_iframe_txt):
    assert get_iframe_txt.text == 'Your content goes here.'
