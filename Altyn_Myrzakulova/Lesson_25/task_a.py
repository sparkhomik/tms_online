from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.support.ui import WebDriverWait

p = r'C:\Users\altmu\OneDrive\Рабочий стол\chromedriver_win32\chromedriver'
driver = Chrome(executable_path=p)
driver.implicitly_wait(5)
driver.get("http://the-internet.herokuapp.com/dynamic_controls")

try:
    # Найти чекбокс
    checkbox = driver.find_element(By.ID, "checkbox")
    remove = driver.find_element(By.CSS_SELECTOR,
                                 "form#checkbox-example > button")
    # Нажать на кнопку
    remove.click()
    # Дождаться надписи “It’s gone”
    msg = WebDriverWait(driver, 10).until(EC.text_to_be_present_in_element(
        (By.ID, "message"), "It's gone!"))
    print(driver.find_element(By.ID, "message").text)
    # Проверить, что чекбокса нет
    checkbox_absent = WebDriverWait(driver, 10).until(
        EC.invisibility_of_element_located(
            (By.ID, "checkbox")))
    if checkbox_absent:
        print("absent")
    # Найти инпут
    text_field = driver.find_element(By.CSS_SELECTOR, "form > input")
    # Проверить, что он disabled
    if not text_field.is_enabled():
        print("text field disabled")
    # Нажать на кнопку
    enable_button = driver.find_element(By.CSS_SELECTOR,
                                        "form > button:last-child")
    enable_button.click()
    # Дождаться надписи “It's enabled!”
    enabled_msg = WebDriverWait(driver, 10).until(
        EC.text_to_be_present_in_element(
            (By.CSS_SELECTOR, "form #message"), "It's enabled!"))
    print(driver.find_element(By.CSS_SELECTOR, "form #message").text)
    # Проверить, что инпут enabled
    text_field_enabled = driver.find_element(By.CSS_SELECTOR,
                                             "#input-example > input")
    text_field_enabled.send_keys("I'm enabled!")
    print(text_field_enabled.get_attribute('value'))
finally:
    driver.quit()
