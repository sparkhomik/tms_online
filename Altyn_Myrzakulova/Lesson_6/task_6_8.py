def func(some_list):
    """
    checks if item in the list callable
    """
    for i in some_list:
        if callable(i):
            print(f"{i} is Callable")
        else:
            print(f"{i} is not Callable")


lst = [15, float, 29, int, str]
print(callable(func(lst)))
