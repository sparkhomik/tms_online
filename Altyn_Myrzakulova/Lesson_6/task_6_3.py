def calc(ans):
    '''
    function performing simple calculation
    '''
    x = int(input("input first number: "))
    y = int(input("input secound number: "))

    if ans == '+':
        return int(x + y)
    if ans == '-':
        return int(x - y)
    if ans == '*':
        return int(x * y)
    if ans == '/':
        if y == 0:
            return "Zero division is not allowed!"
        else:
            if x % y == 0:
                return f"Result: {int(x / y)}"
            else:
                return f"Result: {int(x / y)} Reminder: {x % y}"


def request():
    '''
    function asking usr to choose calc method
    '''
    print("Which operation do you want to perform?")
    print("Choose: +, -, *, or /")
    a = input("Your input here: ")
    print(calc(a))


request()
