import re
from functools import reduce


# Ваша задача написать программу, принимающее число -
# номер кредитной карты(число может быть четным или не четным).
# И проверяющей может ли такая карта существовать.
# Предусмотреть защиту от ввода букв, пустой строки и т.д.

def luhn(code):
    """
    luhn code check
    """
    lookup = (0, 2, 4, 6, 8, 1, 3, 5, 7, 9)
    code = reduce(str.__add__, filter(str.isdigit, code))
    evens = sum(int(i) for i in code[-1::-2])
    odds = sum(lookup[int(i)] for i in code[-2::-2])
    return (evens + odds) % 10 == 0


def validate(number):
    """
    check if number is valid
    """
    if not re.match('^[0-9]{16,16}$', number):
        print("Error! You should enter at least 16 digits!")
        print("Please, re-inter your number.")
        main()
    else:
        print(luhn(number))


def main():
    number = input("Input your number: ").replace(" ", "")
    validate(number)


main()
