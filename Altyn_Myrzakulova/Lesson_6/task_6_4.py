def func(a, *args, name=None, **kwargs):
    '''
    test function with dynamic amount of argument
    '''
    d = {}
    arr = []
    d["mandatory_position_argument"] = a
    for i in args:
        arr.append(i)
    cd = {'name': name}
    d["additional_position_arguments"] = tuple(arr)
    d["mandatory_named_argument"] = cd
    kwd = {}
    for k, v in kwargs.items():
        kwd[k] = v
    d["additional_named_arguments"] = kwd

    return d


print(func(1, 3, 2, 1, 0, name='TestName', a1="A", a2='B', a3='C'))
