# same hash, different id

a = 12
b = 12.0

print(id(a), id(b))
print(hash(a), hash(b))
