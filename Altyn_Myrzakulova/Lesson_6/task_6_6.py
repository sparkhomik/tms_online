def func(some_list):
    '''
    data type checker function
    '''
    i_count = 0
    s_count = 0
    t_count = 0
    for i in some_list:
        if type(i) == int:
            i_count += 1
        if type(i) == str:
            s_count += 1
        if type(i) == tuple:
            t_count += 1
    return f"int: {i_count} str: {s_count} tuple: {t_count}"


print(func([1, 2, 'k', 'o', (1, 2), (1, 2), (1, 2)]))
