x = [1, 2, 3]


def myfunc(x):
    '''
    python scope test
    '''
    y = list(x)
    y.append(42)
    return y


print(myfunc(x))
print(x)
