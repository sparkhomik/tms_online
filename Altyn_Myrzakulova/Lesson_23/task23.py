from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
# Предусмотреть логирование выполненных действий
# Как вообще делать логирование? Оно должно идти отдельным файлом
# или отображаться по ходу исполнения программы?
from logzero import logger

p = r'C:\Users\altmu\OneDrive\Рабочий стол\chromedriver_win32\chromedriver'
driver = Chrome(executable_path=p)
driver.implicitly_wait(15)


# Нужно найти локаторы для окна Login
def login():
    driver.get("https://www.saucedemo.com/")
    username = driver.find_element(By.XPATH, '//*[@id="user-name"]')
    username.send_keys("standard_user")
    logger.debug("insert username")
    password = driver.find_element(By.XPATH, '//*[@id="password"]')
    password.send_keys("secret_sauce")
    logger.debug("insert password")
    login = driver.find_element(By.XPATH, '//*[@id="login-button"]')
    login.click()
    logger.info("login success!")


# Вывести информацию по всем товарам, которая возвращает их название и цену
def get_item():
    a = []
    # почему-то второй элемент не выходил через цикл, пришлось извратиться
    item_name = driver.find_element(By.XPATH, "//div[contains(text(),"
                                              "'Sauce Labs Backpack')]")
    item_price = driver.find_element(By.XPATH, "//div[(text()='29.99')]")
    a.append(f'{item_name.text}, price is {item_price.text}')
    item_name = driver.find_element(By.XPATH, '//div[contains(text(),'
                                              '"Sauce Labs Bike Light")]')
    item_price = driver.find_element(By.XPATH, "//div[text()='9.99']")
    a.append(f'{item_name.text}, price is {item_price.text}')
    for i in range(3, 7):
        n = driver.find_element(By.CSS_SELECTOR, f".inventory_list "
                                                 f":nth-of-type({i}) "
                                                 f"div.inventory_item_name")
        p = driver.find_element(By.CSS_SELECTOR, f".inventory_list "
                                                 f":nth-of-type({i}) "
                                                 f"div.inventory_item_price")
        a.append(f'{n.text}, price is {p.text}')
    print(a)


# Для всех остальных элементов создать методы
# которые находят их на странице и возвращают элемент
def get_product_sort():
    sort = driver.find_element(By.CLASS_NAME, "product_sort_container")
    logger.debug("returning sort element")
    return sort


def get_burger_menu():
    menu = driver.find_element(By.ID, "react-burger-menu-btn")
    logger.debug("returning burger menu element")
    return menu


def get_cart_button():
    cart = driver.find_element(By.CLASS_NAME, "shopping_cart_link")
    logger.debug("returning cart element")
    return cart


def get_social_networks():
    socials = []
    for i in range(1, 4):
        n = driver.find_element(By.CSS_SELECTOR, f".social :nth-of-type({i})")
        socials.append(n.text)
    logger.debug("social network button labels returned")
    print(socials)
    return socials


def main():
    login()
    get_item()
    get_product_sort()
    get_burger_menu()
    get_cart_button()
    get_social_networks()
    driver.close()


if __name__ == '__main__':
    main()
