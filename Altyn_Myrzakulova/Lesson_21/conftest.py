import pytest
from selenium.webdriver import Chrome
from selenium.common.exceptions import NoSuchElementException


@pytest.fixture(scope="session", autouse=True)
def driver():
    p = r'C:\Users\altmu\OneDrive\Рабочий стол\chromedriver_win32\chromedriver'
    chrome_driver = Chrome(executable_path=p)
    yield chrome_driver
    chrome_driver.quit()


@pytest.fixture()
def login_page(driver):
    driver.implicitly_wait(15)
    driver.get("https://the-internet.herokuapp.com/")
    login = driver.find_element_by_xpath('//a[contains(@href,"/login")]')
    login.click()
    search_name = driver.find_element_by_id("username")
    search_name.send_keys("tomsmith")
    search_password = driver.find_element_by_id("password")
    search_password.send_keys("SuperSecretPassword!")
    sbmt = driver.find_element_by_class_name("radius")
    sbmt.click()
    alert = driver.find_element_by_css_selector('h2')
    yield alert


@pytest.fixture()
def not_login_page(driver):
    driver.implicitly_wait(15)
    driver.get("https://the-internet.herokuapp.com/")
    login = driver.find_element_by_xpath('//a[contains(@href,"/login")]')
    login.click()
    search_name = driver.find_element_by_id("username")
    search_name.send_keys("NOTtomsmith")
    search_password = driver.find_element_by_id("password")
    search_password.send_keys("NOTSuperSecretPassword!")
    sbmt = driver.find_element_by_class_name("radius")
    sbmt.click()
    alert = driver.find_element_by_css_selector('h2')
    yield alert


@pytest.fixture()
def checkbox1(driver):
    driver.implicitly_wait(15)
    driver.get("https://the-internet.herokuapp.com/")
    chkbx = driver.find_element_by_xpath('//a[contains(@href,"/checkboxes")]')
    chkbx.click()
    ch1 = driver.find_element_by_css_selector("form input:first-child")
    ch1.click()
    yield ch1.get_attribute('checked')


@pytest.fixture()
def checkbox2(driver):
    driver.implicitly_wait(15)
    driver.get("https://the-internet.herokuapp.com/")
    chkbx = driver.find_element_by_xpath('//a[contains(@href,"/checkboxes")]')
    chkbx.click()
    ch2 = driver.find_element_by_css_selector("form input:last-child")
    ch2.click()
    yield ch2.get_attribute('checked')


@pytest.fixture()
def windows(driver):
    driver.implicitly_wait(15)
    driver.get("https://the-internet.herokuapp.com/")
    mw = driver.find_element_by_xpath('//a[contains(@href,"/windows")]')
    mw.click()
    click_here = driver.find_element_by_xpath(
        '//a[contains(@href,"/windows/new")]')
    click_here.click()
    window_after = driver.window_handles[1]
    driver.switch_to.window(window_after)
    title = driver.find_element_by_css_selector("h3")
    yield title


@pytest.fixture()
def add_remove(driver):
    driver.implicitly_wait(15)
    driver.get("https://the-internet.herokuapp.com/")
    mw = driver.find_element_by_xpath(
        '//a[contains(@href,"/add_remove_elements/")]')
    mw.click()
    driver.find_element_by_xpath(
        "//*[contains(text(), 'Add Element')]").click()
    d = driver.find_element_by_xpath(
        "//*[contains(text(), 'Delete')]")
    yield d


@pytest.fixture()
def is_deleted(driver):
    driver.implicitly_wait(15)
    driver.get("https://the-internet.herokuapp.com/")
    mw = driver.find_element_by_xpath(
        '//a[contains(@href,"/add_remove_elements/")]')
    mw.click()
    driver.find_element_by_xpath(
        "//*[contains(text(), 'Add Element')]").click()
    driver.find_element_by_xpath(
        "//*[contains(text(), 'Delete')]").click()
    try:
        driver.find_element_by_xpath("//*[contains(text(), 'Delete')]")
    except NoSuchElementException:
        yield True
