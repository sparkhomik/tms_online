def test_login(login_page):
    assert login_page.text == "Secure Area"


def test_not_login(not_login_page):
    assert not_login_page.text == "Login Page"


def test_checkbox1(checkbox1):
    assert checkbox1 == 'true'


def test_checkbox2(checkbox2):
    assert checkbox2 is None


def test_tab_title(windows):
    assert windows.text == 'New Window'


def test_add_element(add_remove):
    assert add_remove.text == 'Delete'


def test_delete_element(is_deleted):
    assert is_deleted is True
