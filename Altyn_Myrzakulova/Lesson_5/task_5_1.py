import random


# bulls and cows
# taking random 4 digit number with unique numbers
def get_number():
    num = [str(i) for i in list(range(10))]
    while num[0] == '0':
        random.shuffle(num)
    return int("".join(num[:4]))


def is_unique():
    x = str(input("Input your number: "))
    if len(set(x)) != len(x):
        print("The number has repeat digits!")
        return is_unique()
    elif len(x) != 4:
        print("The number should contain 4 digits only!")
        return is_unique()
    else:
        return x


# counting number of bools in given number
def bulls(n, x):
    count = 0
    for i in range(4):
        if n[i] == x[i]:
            count += 1
    return count


def cows(n, x):
    count = 0
    for i in range(4):
        if ((n[i] != x[i]) and (x[i] in n)):
            count += 1
    return count


def main():
    n = str(get_number())
    print(n)
    x = is_unique()
    if x:
        while bulls(n, x) != 4:
            print("Bulls : ", bulls(n, x), "Cows : ", cows(n, x))
            x = is_unique()
        print("Well done!")


main()
