n = list(map(str, input("Add names separated by comma: ").split(', ')))

if n[0] == '':
    print("No one likes this")
elif len(n) == 1:
    print("%s likes this" % n[0])
elif len(n) == 2:
    print("%s and %s like this" % (n[0], n[1]))
elif len(n) == 3:
    print("%s, %s and %s like this" % (n[0], n[1], n[2]))
elif len(n) >= 4:
    print("%s, %s and %s others like this" % (n[0], n[1], len(n) - 2))
