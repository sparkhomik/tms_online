from abc import ABC, abstractmethod


class NotExistException(Exception):
    """
    custom exception
    """
    message = "This method does not exist"

    def __init__(self, message):
        super().__init__(self.message)


class Creature(ABC):
    """
    creating abstract class Creature
    """

    def __init__(self, name, age):
        self.name = name
        self._age = age

    def aging(self):
        self._age += 1

    # this method would be overridden in child classes
    @abstractmethod
    def voice(self):
        raise NotImplementedError

    def __getattr__(self, atr):
        raise NotExistException(atr)


class Animal(Creature, ABC):
    """
    class Animal inherited from Creature
    """

    @staticmethod
    def walk(self):
        print("walking")


class Bird(Creature, ABC):
    """
    class Bird inherited from Creature
    """

    @staticmethod
    def fly(self):
        print("flying")


class Pig(Animal):
    def voice(self):
        print("oink-oink")

    def __str__(self):
        return f'Pig {self.name} at the age of {self._age}'


class Goose(Bird):
    def voice(self):
        print("honk-honk")

    def __str__(self):
        return f'Goose "{self.name}" at the age of {self._age}'


class Cow(Animal):
    def voice(self):
        print("moo-moo")

    def __str__(self):
        return f'Cow {self.name} at the age of {self._age}'


class Hen(Bird):
    def voice(self):
        print("cluck-cluck")

    def __str__(self):
        return f'Hen {self.name} at the age of {self._age}'


class Farm:
    """
    class Farm takes animal list as an input
    """

    def __init__(self, animal_list):
        self.animal_list = animal_list

    def __getitem__(self, item):
        return self.animal_list[item]

    def print_animals(self):
        for i in self.animal_list:
            print(i)

    def aging(self):
        for i in self.animal_list:
            i.aging()


def main():
    # creating objects from classes
    p = Pig('Pepa', 6)
    g = Goose('The Goose', 4)
    c = Cow('Maggie', 6)
    h = Hen('Ginger', 2)

    # adding objects to the list
    animals = [p, g, c, h]
    farm = Farm(animals)

    farm.print_animals()

    # iterating through list elements
    for i in range(0, len(animals)):
        # using aging method
        farm.aging()
        print(f'animal with index {i} is a {type(farm[i]).__name__}')

    farm.print_animals()

    # calling nonexistent method
    # g.cook()


# executing main func
main()
