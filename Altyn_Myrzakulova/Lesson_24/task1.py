from selenium.webdriver import Chrome
from selenium.webdriver.common.action_chains import ActionChains
from selenium.webdriver.common.by import By

p = r'C:\Users\altmu\OneDrive\Рабочий стол\chromedriver_win32\chromedriver'
driver = Chrome(executable_path=p)
driver.implicitly_wait(15)


# наведение на элемент для нагляности
def hover_element(element):
    a = ActionChains(driver)
    a.move_to_element(element).perform()
    driver.implicitly_wait(2)
    element.click()


# Откройте страницу:
driver.get("https://ultimateqa.com/complicated-page/")
driver.maximize_window()

# Нажмите на 2-ю сверху кнопку во втором столбце используя:
# XPATH
button = driver.find_element(By.XPATH, '//*[contains(@class, '
                                       '"et_pb_button et_pb_button_4 '
                                       'et_pb_bg_layout_light")]')
hover_element(button)

# CSS selector
button = driver.find_element(By.CSS_SELECTOR, 'a.et_pb_button_4')
hover_element(button)

# class name
button = driver.find_element(By.CLASS_NAME, 'et_pb_button_4')
hover_element(button)

driver.quit()
