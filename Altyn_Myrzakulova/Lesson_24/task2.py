import time
import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By


@pytest.fixture(scope="session", autouse=True)
def driver():
    p = r'C:\Users\altmu\OneDrive\Рабочий стол\chromedriver_win32\chromedriver'
    chrome_driver = Chrome(executable_path=p)
    yield chrome_driver
    chrome_driver.quit()


# test 1
@pytest.fixture(scope="session")
def test1(driver):
    """
    Откройте страницу
    Заполните форму обратной связи
    Нажмите на кнопку "Submit"
    Проверьте наличие сообщения "Form filled out successfully"
    :param driver: хром драйвер
    :return: содержимое элемента
    """
    driver.get("https://ultimateqa.com/filling-out-forms/")
    driver.maximize_window()
    driver.implicitly_wait(15)
    name = driver.find_element(By.ID, "et_pb_contact_name_0")
    name.send_keys("ltn")
    time.sleep(2)
    message = driver.find_element(By.ID, "et_pb_contact_message_0")
    message.send_keys("ololo")
    time.sleep(2)
    submit = driver.find_element(By.CSS_SELECTOR,
                                 ".et_pb_contact_submit:first-child")
    submit.click()
    time.sleep(3)
    yield driver.find_element(By.CSS_SELECTOR, "div.et-pb-contact-message > p")


def test_t1_success(test1):
    assert test1.text == 'Thanks for contacting us'


# test 2
@pytest.fixture(scope="session")
def test2(driver):
    """
    Заполните поле "Name"
    Нажмите на кнопку "Submit"
    Проверьте наличие сообщения об ошибке
    :param driver: хром драйвер
    :return:
    """
    driver.get("https://ultimateqa.com/filling-out-forms/")
    driver.maximize_window()
    driver.implicitly_wait(15)
    name = driver.find_element(By.ID, "et_pb_contact_name_0")
    name.send_keys("ltn")
    time.sleep(2)
    submit = driver.find_element(By.CSS_SELECTOR,
                                 ".et_pb_contact_submit:first-child")
    submit.click()
    time.sleep(3)
    yield driver.find_element(By.CSS_SELECTOR,
                              "div .et-pb-contact-message > ul li")


def test_t2_error(test2):
    assert test2.text == 'Message'


# test 3
@pytest.fixture()
def test3(driver):
    """
    Заполните поле "Message"
    Нажмите на кнопку "Submit"
    Проверьте наличие сообщения об ошибке
    :param driver:
    :return:
    """
    driver.get("https://ultimateqa.com/filling-out-forms/")
    driver.maximize_window()
    driver.implicitly_wait(15)
    message = driver.find_element(By.ID, "et_pb_contact_message_0")
    message.send_keys("ololo")
    time.sleep(2)
    submit = driver.find_element(By.CSS_SELECTOR,
                                 ".et_pb_contact_submit:first-child")
    submit.click()
    time.sleep(3)
    yield driver.find_element(By.CSS_SELECTOR,
                              "div .et-pb-contact-message > ul li")


def test_t3_error(test3):
    assert test3.text == 'Name'
