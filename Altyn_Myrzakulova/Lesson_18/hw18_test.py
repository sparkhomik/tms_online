import unittest

from hw18 import OsDetails

c = OsDetails()


class TestMethods(unittest.TestCase):
    """
    Checks if function meets expected results
    """

    def test_get_sys_name(self):
        self.assertEqual(c.get_sys_name(), "Windows")

    def test_release_details(self):
        self.assertEqual(c.release_details(), "10")

    def test_os_version(self):
        self.assertEqual(c.os_version(), "10.0.19041")


class TestFailedMethods(unittest.TestCase):
    """
    Mark the test as an expected failure or error.
    """

    @unittest.expectedFailure
    def test_failed_get_sys_name(self):
        self.assertNotEqual(c.get_sys_name(), "notWindows")

    @unittest.expectedFailure
    def test_failed_release_details(self):
        self.assertNotEqual(c.release_details(), "8")

    @unittest.expectedFailure
    def test_failed_os_version(self):
        self.assertNotEqual(c.os_version(), "10.1.19041")


if __name__ == '__main__':
    unittest.main()
