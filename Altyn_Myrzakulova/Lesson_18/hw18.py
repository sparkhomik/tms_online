import platform


# print the information

class OsDetails:
    '''
    Getting current OS details
    '''

    def get_sys_name(self):
        return platform.system()

    def release_details(self):
        return platform.release()

    def os_version(self):
        return platform.version()
