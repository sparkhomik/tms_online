def is_lower(word):
    """
    Checks if the string is lowercase
    """
    return word.islower()
