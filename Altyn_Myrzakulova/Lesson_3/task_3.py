import random
import math

# 1
a = 10
b = 23
a, b = b, a
print(a, b, "\n")

# 2
a = 10
b = 23
print(a * 3)
print(b - 3, "\n")

# 3
print(type(float(a)))
print(type(str(b)), "\n")

# 4
print("%.3f" % (a / 11), "\n")

# 5
c = float(b)
print(c ** 3, "\n")

# 6
while True:
    rand = random.randrange(1, 100000)
    if rand % 3 == 0:
        print(rand, "\n")
        break

# 7
print(int(math.sqrt(100) ** 4), "\n")

# 8
s = 'Hi guys'
k = s * 3 + "Today"
print(k, "\n")

# 9
print(len(k), "\n")

# 10
print(k)
print(k[:21] + k[25:20:-1], "\n")

# 11
print(k[1::2])
print(k[24::-2], "\n")

# 12
t = "Task 10: %s, %s Task 11: %s, %s" % (
    k, k[:21] + k[25:20:-1], k[1::2], k[24::-2])
print(t, "\n")

# 13 a
print(t.title())
# 13 b
print(t.upper())
# 13 c
print(t.lower(), "\n")

# 14
print(t.count("Task"))
