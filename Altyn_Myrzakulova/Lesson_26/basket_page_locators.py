from selenium.webdriver.common.by import By


class BasketPageLocators:
    BASKET_BREAD = (By.CSS_SELECTOR, 'li.active:nth-child(2)')
    BASKET_NOT_EXIST = (By.CSS_SELECTOR,
                        '[data-navigation="dropdown-menu"]')
    BASKET_EMPTY = (By.CSS_SELECTOR, '#content_inner')
    BASKET_LINK = (By.XPATH, '(//a[@href="/en-gb/"])[3]')
