from selenium.common.exceptions import TimeoutException
from books_page_locators import BooksPageLocators
from base_page import BasePage


class BooksPage(BasePage):
    URL = 'http://selenium1py.pythonanywhere.com/en-gb/catalogue/category/' \
          'books_2/'

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_books_page_title(self):
        return self.driver.title

    def get_books_breadcrumb(self):
        bc = self.find_element(BooksPageLocators.BOOKS_BREAD)
        return bc

    def get_showing(self):
        text = self.find_element(BooksPageLocators.BOOKS_SHOWING)
        return text

    def get_next_button(self):
        button = self.find_element(BooksPageLocators.BOOKS_NEXT)
        return button

    def get_non_fiction(self):
        try:
            return self.find_element(BooksPageLocators.BOOKS_LINK)
        except TimeoutException:
            return None
