from main_page import MainPage


def test_main_page_title(driver):
    page = MainPage(driver)
    page.open()
    assert page.get_main_page_title() == 'Oscar - Sandbox'


def test_get_welcome(driver):
    page = MainPage(driver)
    page.open()
    assert page.get_welcome().text == 'Welcome!', "Was opened not Main page"


def test_get_recommended(driver):
    page = MainPage(driver)
    page.open()
    assert page.get_recommended().text == 'Recommended reading'


def test_get_breadcrumb_error(driver):
    page = MainPage(driver)
    page.open()
    assert page.get_breadcrumb_error() is None, "Was opened not Main page"


def test_get_link_text(driver):
    page = MainPage(driver)
    page.open()
    assert page.get_link_text().text == 'using this form'


# при переходе с главной страницы на страницу
# корзины и проверка на то, что корзина пуста.
def test_open_basket_page(driver):
    page = MainPage(driver)
    page.open()
    assert page.open_basket_page().text == 'Your basket is empty. ' \
                                           'Continue shopping'


# при переходе с главной страницы на страницу Fiction
# и проверка на то, что мы на странице Fiction.
def test_open_fiction_page(driver):
    page = MainPage(driver)
    page.open()
    assert 'Fiction' in page.open_fiction_page().get_fiction_page_title()
