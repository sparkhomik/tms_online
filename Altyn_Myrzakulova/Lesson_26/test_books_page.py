from books_page import BooksPage


def test_books_page_title(driver):
    page = BooksPage(driver)
    page.open()
    assert page.get_books_page_title() in 'Books | Oscar - Sandbox'


def test_books_breadcrumb(driver):
    page = BooksPage(driver)
    page.open()
    assert page.get_books_breadcrumb().text == 'Books', \
        "Was opened not Basket page"


def test_showing(driver):
    page = BooksPage(driver)
    page.open()
    assert 'showing' in page.get_showing().text


def test_next_button(driver):
    page = BooksPage(driver)
    page.open()
    assert page.get_next_button().text == 'next'


def test_non_fiction(driver):
    page = BooksPage(driver)
    page.open()
    assert page.get_non_fiction().text == 'Non-Fiction'
