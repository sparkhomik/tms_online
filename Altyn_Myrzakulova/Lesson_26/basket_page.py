from selenium.common.exceptions import NoSuchElementException
from main_page_locators import MainPageLocators
from basket_page_locators import BasketPageLocators
from base_page import BasePage


class BasketPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb/basket/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_basket_page_title(self):
        return self.driver.title

    def get_basket_breadcrumb(self):
        bc = self.find_element(BasketPageLocators.BASKET_BREAD)
        return bc

    def get_drop_down_error(self):
        try:
            self.find_element(BasketPageLocators.BASKET_NOT_EXIST)
        except NoSuchElementException:
            return None

    def get_basket_error(self):
        try:
            self.find_element(MainPageLocators.MAIN_PAGE_BASKET)
        except NoSuchElementException:
            return None

    def get_basket_link(self):
        link = self.find_element(BasketPageLocators.BASKET_LINK)
        return link
