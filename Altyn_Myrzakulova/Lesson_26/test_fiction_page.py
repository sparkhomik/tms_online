from fiction_page import FictionPage


def test_fiction_page_title(driver):
    page = FictionPage(driver)
    page.open()
    assert 'Fiction' in page.get_fiction_page_title()


def test_fiction_breadcrumb(driver):
    page = FictionPage(driver)
    page.open()
    assert 'Fiction' in page.get_fiction_page_title()


def test_fiction_header(driver):
    page = FictionPage(driver)
    page.open()
    assert page.get_header().text == 'Fiction'
