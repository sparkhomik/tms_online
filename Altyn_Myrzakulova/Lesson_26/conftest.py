import pytest
from selenium.webdriver import Chrome


@pytest.fixture(scope="session")
def driver():
    p = r'C:\Users\altmu\OneDrive\Рабочий стол\chromedriver_win32\chromedriver'
    driver = Chrome(executable_path=p)
    yield driver
    driver.quit()
