from selenium.webdriver.common.by import By


class FictionPageLocators:
    FICTION_BREAD = (By.CSS_SELECTOR, 'ul.breadcrumb > li:nth-child(3)')
    FICTION_HEADER = (By.CSS_SELECTOR, 'div.page-header.action > h1')
