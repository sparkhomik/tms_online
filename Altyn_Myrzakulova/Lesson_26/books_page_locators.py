from selenium.webdriver.common.by import By


class BooksPageLocators:
    BOOKS_BREAD = (By.CSS_SELECTOR, 'li.active:nth-child(2)')
    BOOKS_SHOWING = (By.CSS_SELECTOR, 'form.form-horizontal')
    BOOKS_NEXT = (By.CSS_SELECTOR, 'li.next')
    BOOKS_LINK = (By.XPATH, '(//a[@href="/en-gb/catalogue/category/books'
                            '/non-fiction_5/"])[2]')
    BOOKS_SEARCH = (By.ID, 'id_q')
    BOOKS_BUTTON = (By.XPATH, "//input[@value='Search']")
    BOOK_EMPTY = (By.CSS_SELECTOR, 'p > strong')
    BOOK_FOUND_NAME = (By.CSS_SELECTOR, 'h3 > a')
    BOOK_IMAGE = (By.CSS_SELECTOR, 'a > img')
    BOOKS_FICTION = (By.XPATH, '(//a[@href="/en-gb/catalogue/category/'
                               'books/fiction_3/"])[2]')
