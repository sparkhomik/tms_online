from basket_page import BasketPage


def test_basket_page_title(driver):
    page = BasketPage(driver)
    page.open()
    assert page.get_basket_page_title() in 'Basket | Oscar - Sandbox', \
        "Was opened not Basket page"


def test_basket_breadcrumb(driver):
    page = BasketPage(driver)
    page.open()
    assert page.get_basket_breadcrumb().text == 'Basket', \
        "Was opened not Basket page"


def test_drop_down_error(driver):
    page = BasketPage(driver)
    page.open()
    assert page.get_drop_down_error() is None, "Was opened not Main page"


def test_basket_link(driver):
    page = BasketPage(driver)
    page.open()
    assert page.get_basket_link().text == 'Continue shopping'


def test_basket_error(driver):
    page = BasketPage(driver)
    page.open()
    assert page.get_basket_error() is None
