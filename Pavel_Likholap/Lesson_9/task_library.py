class Book:
    def __init__(self, name, author, page_amount, isbn):
        self.name = name
        self.author = author
        self.page_amount = page_amount
        self.isbn = isbn
        self.book_status = 'Not reserved'

    def get_status(self):
        if self.book_status == 'Not reserved':
            return True
        else:
            return False


class Users:
    def __init__(self, user_name):
        self.reserved_books = []
        self.reading_books = []
        self.user_name = user_name

    def reserve_book(self, b):
        if Book.get_status(b):
            self.reserved_books.append(b)
        else:
            print('This book is reserved')
            b.book_status = 'Reserved'

    def read_reserved_book(self, b):
        self.reading_books.append(self.reserved_books.pop(b))

    def read_not_reserved_book(self, b):
        if Book.get_status(b):
            self.reading_books.append(b)
        else:
            print('This book is reserved or already reading')

    def stop_reading(self, b):
        self.reading_books.remove(b)
        b.book_status = 'Not reserved'
