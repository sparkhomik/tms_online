class Bank:
    def __init__(self, percent, amount, period):
        self.percent = percent
        self.amount = amount
        self.period = period

    def get_month(self):
        return self.period * 12

    def get_sum(self):
        for i in range(self.get_month()):
            temp_sum = self.amount * (self.percent / 100) / self.get_month()
            self.amount += temp_sum
        return format(self.amount, '.2f')
