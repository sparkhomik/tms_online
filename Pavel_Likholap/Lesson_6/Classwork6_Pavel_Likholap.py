import datetime

# 1
"""
Напишите функцию, которая возвращает строку: “Hello world!”
"""


def task_1():
    return 'Hello world'


print(task_1())

# 2
"""
Напишите функцию, которая вычисляет сумму трех чисел
и возвращает результат в основную ветку программы.
"""


def task_2(*values):
    res = 0
    for i in values:
        res += i
    return res


print(task_2(1, 2, 3))

# 3
"""
Придумайте программу, в которой из одной
функции вызывается вторая. При этом ни
одна из них ничего не возвращает в основную
ветку программы, обе должны выводить результаты
своей работы с помощью функции print().
"""


def main_3():
    print('main_3 was run')
    task_3()


def task_3():
    print('task_3 was run')


print(main_3())

# 4
"""
Напишите функцию, которая не
принимает отрицательные числа
и возвращает число наоборот.
"""


def task_4(n):
    if n < 0:
        return 'Введите число, больше или равное нулю'
    else:
        return str(n)[::-1]


print(task_4(int(input())))
# 5
"""
Напишите функцию fib(n), которая по
данному целому неотрицательному n
возвращает n-e число Фибоначчи.
"""


def fib(n):
    if n < 2:
        return n
    else:
        fab_list = [0, 1]
        temp = 0
        while n != 2:
            fab_list.append(fab_list[temp] + fab_list[temp + 1])
            n -= 1
            temp += 1
        return fab_list[-1]


print(fib(int(input())))

# 6
"""
Напишите функцию, которая проверяет
на то, является ли строка палиндромом или нет.
Палиндром — это слово или фраза, которые
одинаково читаются слева направо и справа налево.
"""


def pal(n):
    if n == n[::-1]:
        return 'Строка является палиндромом'
    else:
        return 'Строка НЕ является палиндромом'


print(pal(input()))

# 7
"""
У вас интернет магазин, надо написать
функцию которая проверяет что введен
правильный купон и он еще действителен
"""


def cc(ent_c, cor_c, cur_d, exp_d):
    if ent_c == cor_c and cur_d <= exp_d:
        return True
    else:
        return False


print(cc('123', '124', datetime.date(2021, 8, 20), datetime.date(2021, 9, 17)))

# 8
"""
Фильтр. Функция принимает на вход список,
проверяет есть ли эти элементы в списке
exclude, если есть удаляет их и возвращает
список с оставшимися элементами
"""

exc = ['Pavel', 'is', 5, 6, 'bad', 7, 8, 9, 'pupil']


def fil(n):
    if n in exc:
        return False
    else:
        return True


print(list(filter(fil, [i for i in range(11)])))
