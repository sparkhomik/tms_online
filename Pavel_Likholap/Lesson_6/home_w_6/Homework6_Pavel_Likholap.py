# 1
"""
Ваша задача написать программу, принимающее число - номер
кредитной карты(число может быть четным или не четным).
И проверяющей может ли такая карта существовать.
Предусмотреть защиту от ввода букв, пустой строки и т.д.
"""


def validation_symb():
    """
    Функция проверяет валидность номера карты,
    который ввел пользователь: количество и характер символов
    введенных пользователем
    """
    cart_n = input()
    # Запускаем валидацию на характер символов
    # В этом же случае проверяем количество символов
    if cart_n.isdigit() is not True:
        while cart_n.isdigit() is not True:
            print('ОШИБКА: номер карты дожен состоять только из цифр')
            cart_n = (input('Введите номер карты состоящий только из цифр: '))
            if len(cart_n) < 13 or len(cart_n) > 18:
                while len(cart_n) < 13 or len(cart_n) > 18:
                    print("""ОШИБКА: мин кол-во символов в номере: 13
макс кол-во символов в номере: 18 """)
                    cart_n = (input('Введите допустимое кол-во символов: '))
    # Запускаем валидацию на количество символов
    # В этом же случае проверяем на характер символов
    elif len(cart_n) < 13 or len(cart_n) > 18:
        while 13 > len(cart_n) or len(cart_n) > 18:
            print("""ОШИБКА: мин кол-во символов в номере: 13
макс кол-во символов в номере: 18 """)
            cart_n = (input('Введите допустимое количество символов: '))
            if cart_n.isdigit() is not True:
                while cart_n.isdigit() is not True:
                    print('ОШИБКА: номер карты дожен состоять только из цифр')
                    cart_n = (input('Введите номер только из цифр: '))
    return cart_n


def luhn():
    """
    Функция проверяет возможность существования
    карты, основываясь на алгоритме Луна
    """
    # Вызываем функцию валидации
    # И возвращаем уже валидное значение
    cart_n = validation_symb()
    # Запускаем основной алгоритм проверки номера карты
    cart_n = [int(i) for i in cart_n]
    # Проверяем количество цифр в последовательности(чет/нечет)
    # Исключая последний элемент(контрольная цифра)
    if (len(cart_n) - 1) % 2 == 1:
        for i in range(0, len(cart_n), 2):
            cart_n[i] = cart_n[i] * 2
            if cart_n[i] > 9:
                cart_n[i] = cart_n[i] - 9
            else:
                continue
    else:
        for i in range(1, len(cart_n), 2):
            cart_n[i] = cart_n[i] * 2
            if cart_n[i] > 9:
                cart_n[i] = cart_n[i] - 9
            else:
                continue
    # На основе расчитанного номера, проверяем
    # Может ли существовать такая карта
    if sum(cart_n) % 10 == 0:
        return True
    else:
        return False


print('Карта не существует' if luhn() is False else 'Карта существует')


# 2
"""
Подсчет количества букв
На вход подается строка, например,
"cccbba" результат работы программы - строка "c3b2a"
"""


def amount_symbols(n: str):
    """
    Добавляем пробел к концу строки
    Чтобы сравнивать последний символ
    Введенной строки с пробелом
    """
    n += ' '
    res = ''
    count = 1
    # Исключем добавленный пробел сокращением цикла
    # на одну итерацию
    for i in range(len(n) - 1):
        if n[i] == n[i + 1]:
            count += 1
        else:
            res += (n[i] + str(count))
            count = 1
    return res.replace('1', '')


print(amount_symbols(input()))

# 3
"""
Реализуйте программу, которая спрашивала у пользователя,
какую операцию он хочет произвести над числами, а затем
запрашивает два числа и выводит результат
Проверка деления на 0.
"""


def elements():
    a = int(input('Введите первое число:'))
    b = int(input('Введите второе число:'))

    return a, b


def calc(z: int):
    """
    Простейший калькулятор
    """
    if z == 1:
        return sum(elements())
    elif z == 2:
        a, b = elements()
        return a - b
    elif z == 3:
        a, b = elements()
        return a * b
    else:
        a, b = elements()
        if b == 0:
            return 'ОШИБКА: на ноль делить нельзя'
        else:
            return f'Частное: {a // b}, Остаток: {a % b}'


welcome = ['1: Сложение', '2: Вычитание', '3: Умножение', '4: Деление']
print('Выберите операцию:', *welcome, sep='\n')
actions = int(input('Введите номер пункта меню:'))
# т.к на вход принимаем сразу целочисленное значение
# валидируем только по значению введенного числа
if actions not in [1, 2, 3, 4]:
    while actions not in [1, 2, 3, 4]:
        print('ОШИБКА: Введите число от 1 до 4')
        actions = int(input('Введите номер пункта меню:'))
# Вызов основной функции
print(calc(actions))

# 4
"""
Написать функцию с изменяемым числом входных параметров
"""


def ar(one, *args, name=None, **kwargs):
    return{'mandatory_position_argument': one,
           'additional_position_arguments': args,
           'mandatory_named_argument': dict(name=name),
           'additional_named_arguments': kwargs
           }


print(ar(1, 2, 3, name='test', surname='test2', some='something'))

# 5
"""
На уровне модуля создать список из 3-х элементов
Написать функцию, которая принимает на вход этот
список и добавляет в него элементы. Функция должна
вернуть измененный список.
При этом исходный список не должен измениться.
Пример c функцией которая добавляет в список символ “a”

"""

task_5 = [1, 2, 3, '']


def add(list1):
    list1 = task_5.copy()
    list1.append("a")
    return list1


print(task_5)
print(add(task_5))
print(task_5)

# 6
"""
Функция проверяющая тип данных
"""


def types(task_6):
    result = {"<class 'int'>": 0, "<class 'str'>": 0, "<class 'tuple'>": 0}
    for i in task_6:
        # Можно добавлять не содержащиеся типы в наш
        # result словарь
        if str(type(i)) not in result:
            result[str(type(i))] = 1
        # Или просто игнорировать их:
        # if str(type(i)) not in result:
            # continue
        else:
            result[str(type(i))] += 1
    return result


print(types([1, 2, 'a', (1, 2), 'b', {1: 26}, {2: 27}, False, True, '']))

# 7
"""
Написать пример чтобы hash от объекта
1 и 2 были одинаковые, а id разные
"""

a = 1
b = 1.0
print(id(a), id(b), sep='\n')
print(hash(a), hash(b), sep='\n')

# 8
"""
написать функцию, которая проверяет
есть ли в списке объект, которые можно вызвать
"""


def call(task_8):
    callable_list = []
    for i in task_8:
        if callable(i):
            callable_list.append(i)
    return callable_list if callable_list else False


print(call([1, 2, tuple, 'a', (1, 2), str]))
