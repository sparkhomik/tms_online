import platform


class System:
    """
    Class to get information
    about current operation system
    """

    @staticmethod
    def get_os_name() -> str:
        """
        :return: OS name
        """
        return platform.system()

    @staticmethod
    def get_os_release() -> str:
        """
        :return: OS release
        """
        return platform.release()

    @staticmethod
    def get_os_version() -> str:
        """
        :return: OS version
        """
        return platform.version()


if __name__ == "__main__":
    print(System.get_os_name())
    print(System.get_os_release())
    print(System.get_os_version())
