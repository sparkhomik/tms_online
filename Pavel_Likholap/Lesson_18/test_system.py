import unittest
from system_info import System


class TestSystem(unittest.TestCase):

    def test_get_os_name(self):
        self.assertEqual(System.get_os_name(), 'Windows')

    @unittest.expectedFailure
    def test_get_os_name_negative(self):
        self.assertNotEqual(System.get_os_name(), 'Windows')

    def test_get_os_release(self):
        self.assertEqual(System.get_os_release(), '10')

    @unittest.expectedFailure
    def test_get_os_release_negative(self):
        self.assertNotEqual(System.get_os_release(), '10')

    def test_get_os_version(self):
        self.assertEqual(System.get_os_version(), '10.0.19042')

    @unittest.expectedFailure
    def test_get_os_version_negative(self):
        self.assertNotEqual(System.get_os_version(), '10.0.19042')
