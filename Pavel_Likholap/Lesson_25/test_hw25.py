import pytest
from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


@pytest.fixture(scope='session', autouse=True)
def drv():
    """
    :return: Chrome browser
    """
    drv = Chrome(executable_path='D:/chromedriver.exe')
    yield drv
    drv.quit()


@pytest.fixture()
def get_text(drv):
    """
    :param drv: Chrome browser
    :return: Web-element which is appeared
    after clicking to Remove button
    """
    drv.get('http://the-internet.herokuapp.com/dynamic_controls')
    drv.find_element(By.XPATH, '//input[@type="checkbox"]').click()
    drv.find_element(By.XPATH, '(//button[@type="button"])[1]').click()
    target_text = WebDriverWait(drv, 10).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, "p#message"))
    )
    return target_text


@pytest.fixture()
def absent_checkbox(drv):
    """
    :param drv: Chrome browser
    :return: Bool value True if
    checkbox was disappeared from web page
    """
    drv.get('http://the-internet.herokuapp.com/dynamic_controls')
    drv.find_element(By.XPATH, '//input[@type="checkbox"]').click()
    drv.find_element(By.XPATH, '(//button[@type="button"])[1]').click()
    WebDriverWait(drv, 10).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, "p#message"))
    )
    try:
        drv.find_element(By.XPATH, '//input[@type="checkbox"]')
    except NoSuchElementException:
        return True


@pytest.fixture()
def get_disabled_input_value(drv):
    """
    :param drv: Chrome browser
    :return: Value of 'Disabled' attribute
    """
    drv.get('http://the-internet.herokuapp.com/dynamic_controls')
    field = drv.find_element(By.XPATH, '//input[@type="text"]')
    return field.get_attribute('Disabled')


@pytest.fixture()
def get_enabling_text(drv):
    """
    :param drv: Chrome browser
    :return: Web-element with test
    text
    """
    drv.get('http://the-internet.herokuapp.com/dynamic_controls')
    drv.find_element(By.XPATH, '(//button[@type="button"])[2]').click()
    target_text = WebDriverWait(drv, 10).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, "p#message"))
    )
    return target_text


@pytest.fixture()
def get_enabled_input_value(drv):
    """
    :param drv: Chrome browser
    :return: Value of 'Disabled' attribute
    """
    drv.get('http://the-internet.herokuapp.com/dynamic_controls')
    drv.find_element(By.XPATH, '(//button[@type="button"])[2]').click()
    WebDriverWait(drv, 10).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, "p#message"))
    )
    field = drv.find_element(By.XPATH, '//input[@type="text"]')
    return field.get_attribute('Disabled')


@pytest.fixture()
def get_iframe(drv):
    """
    :param drv: Chrome browser
    :return: Web-element from frame
    of web page
    """
    drv.get('http://the-internet.herokuapp.com/frames')
    drv.find_element(By.XPATH, '//a[@href="/iframe"]').click()
    drv.switch_to.frame(drv.find_element_by_tag_name('iframe'))
    WebDriverWait(drv, 10).until(
        ec.presence_of_element_located((By.XPATH, "//p"))
    )
    frame = drv.find_element_by_xpath('//p')
    return frame


def test_get_text(get_text):
    """
    Test checks that text 'It's gone!'
    is appeared after clicking to Remove button
    :param get_text: Web-element which is appeared
    after clicking to Remove button
    :return: None
    """
    assert get_text.text == "It's gone!"


def test_absent_checkbox(absent_checkbox):
    """
    Test checks that checkbox
    was disappeared from web page
    :param absent_checkbox: Bool value
    :return: None
    """
    assert absent_checkbox is True


def test_disabled_input_value(get_disabled_input_value):
    """
    Test checks that
    input field is disabled
    :param get_disabled_input_value:
    Value of 'Disabled' attribute
    :return: None
    """
    assert get_disabled_input_value == 'true'


def test_enabling_text(get_enabling_text):
    """
    Test checks that text 'It's enabled!'
    is shown on web page
    :param get_enabling_text: Web element
    which appeared after clicking
    to Enable button
    :return: None
    """
    assert get_enabling_text.text == "It's enabled!"


def test_enabled_input_value(get_enabled_input_value):
    """
    Test checks that
    input field is enabled
    :param get_enabled_input_value: Value of 'Disabled' attribute
    :return: None
    """
    assert get_enabled_input_value is None


def test_iframe(get_iframe):
    """
    Test checks that
    frame contains particular text
    :param get_iframe: Web-element from frame
    :return: None
    """
    assert get_iframe.text == 'Your content goes here.'
