import yaml
import json
from pprint import pprint


def write_yaml(some_text):
    """
    Function to create new yaml file
    :param some_text: Config data
    :return: None
    """
    with open('new_yaml.yaml', 'w') as file:
        yaml.dump(some_text, file)


if __name__ == '__main__':
    with open('order.yaml') as f, open("order.json", "w") as f_json:
        templates = yaml.safe_load(f)
        # get json from yaml
        json.dump(templates, f_json)

    # invoice number
    pprint(templates['invoice'])
    # shipping address
    address = templates['bill-to']
    print(*address["address"].values())

    # get description, price, quantity
    for i in (templates['product']):
        print(f'Description: {i["description"]}, Price: {i["price"]} \
        Quantity: {i["quantity"]}')
    about = {'name': 'Pavel', 'last_name': 'Likholap'}
    write_yaml(about)
