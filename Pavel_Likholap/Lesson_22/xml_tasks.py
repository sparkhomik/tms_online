from xml.etree import ElementTree as Et


def search(by_that: str):
    """
    Decorator for get_book function
    :param by_that: search parameters:
    author, title, description
    :return: Matched result or string message
    """
    def decorator(func):
        def wrapper(arg):
            for i in root:
                temp_dict = dict()
                for j in i:
                    temp_dict[j.tag] = j.text
                if arg in temp_dict[by_that]:
                    return temp_dict['title']
        return wrapper
    return decorator


@search(by_that='author')
def get_book(arg):
    """
    :param arg: Search parameter
    :return: Found book
    """
    return arg


def get_book_by_price(file_1, price: float) -> str:
    """
    :param file_1: Root catalog
    :param price: Book price
    :return: Book name
    """
    for i in file_1:
        temp_dict = dict()
        for j in i:
            temp_dict[j.tag] = j.text
        if str(price) == temp_dict['price']:
            return temp_dict['title']


if __name__ == '__main__':
    with open('library.xml') as file:
        content = file.read()
        root = Et.fromstring(content)
        # get book by price
        print(get_book_by_price(root, 36.75))
        # get book by part of author
        print(get_book('va'))
