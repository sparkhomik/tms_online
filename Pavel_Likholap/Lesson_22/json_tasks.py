import json


def get_common_club(club_name: str) -> str:
    """
    :param club_name: Name of sport club
    :return: Students with common
    clubs
    """
    c_c_list = []
    for i in content_json:
        if i['Club'] == club_name:
            c_c_list.append(i['Name'])
        else:
            continue
    return f'Those guys: {", ".join(c_c_list)} play at {club_name} club' \
        if c_c_list else 'There are no items which matched to request'


def get_same_gender(gender: str) -> list:
    """
    :param gender: M - men or W - women
    :return: Filtered students list
    """
    return [i for i in content_json if i['Gender'] == gender]


def get_student_by_name(name: str) -> str:
    """
    :param name: Part or full student name
    :return: Full students name and class from
    which that student
    """
    for i in content_json:
        if name in i['Name']:
            return f'That is {i["Name"]} from {i["Class"]}'


if __name__ == '__main__':
    with open("students.json") as file:
        content = file.read()
        content_json = json.loads(content)
        print(content_json)
    # get common section
    print(get_common_club('Chess'))
    # students filtering by gender
    print(get_same_gender('W'))
    # search students by name
    print(get_student_by_name('io'))
