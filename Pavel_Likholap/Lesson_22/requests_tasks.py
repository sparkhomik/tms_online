from requests import get, post, put, delete

link = "https://fakerestapi.azurewebsites.net/api/v1/"


def get_authors():
    r = get(f"{link}Authors")
    return r.status_code


def get_authors_by_id(id_a: int):
    """
    :param id_a: Author ID
    :return: Request status and
    data of author
    """
    r = get(f"{link}/Authors/{id_a}")
    return r.status_code, r.text


def add_new_book(book: dict):
    """
    :param book: Json data with book attribute
    :return: Request status
    """
    r = post(f"{link}Books", json=book)
    return r.status_code


def add_new_user(user: dict):
    """
    :param user: Json data with user attribute
    :return: Request status
    """
    r = post(f"{link}Users", json=user)
    return r.status_code


def change_data_particular_book(d: dict, b: int):
    """
    :param d: New book data
    :param b: Book ID
    :return: Request status
    """
    r = put(f"{link}Books/{b}", json=d)
    return r.status_code


def remove_user(u: int):
    """
    :param u: User ID
    :return: Request status
    """
    r = delete(f"{link}Users/{u}")
    return r.status_code


if __name__ == '__main__':
    # get all authors
    print(get_authors())
    # get authors by id
    print(get_authors_by_id(3))
    # post: add new book
    new_book = {
        "id": 206,
        "title": "Test_title",
        "description": "Useful book",
        "pageCount": 568,
        "excerpt": "None",
        "publishDate": "2021-10-14T11:45:25.221Z"
    }
    print(add_new_book(new_book))
    # post: add new user
    new_user = {
        "id": 0,
        "userName": "string",
        "password": "string"
    }
    print(add_new_user(new_user))
    # put: changing book data
    book_updating = {
        "id": 0,
        "title": "string",
        "description": "string",
        "pageCount": 0,
        "excerpt": "string",
        "publishDate": "2021-10-14T11:57:34.571Z"
    }
    print(change_data_particular_book(book_updating, 3))
    # delete: remove user by id
    print(remove_user(4))
