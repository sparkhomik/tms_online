from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
import time
import pytest


@pytest.fixture(scope='session')
def get_driver():
    """
    :return: Chrome browser
    """
    drv = Chrome(executable_path='D:/chromedriver.exe')
    yield drv
    drv.quit()


@pytest.fixture()
def get_success(get_driver):
    """
    :param get_driver: Chrome browser
    :return: Found web element
    """
    n = 'Test_name'
    m = 'Test message test test'
    get_driver.get('https://ultimateqa.com/filling-out-forms/')
    input_1 = get_driver.find_element_by_id('et_pb_contact_name_0')
    input_1.send_keys(n)
    input_2 = get_driver.find_element_by_id('et_pb_contact_message_0')
    input_2.send_keys(m)
    css_b = 'button.et_pb_contact_submit.et_pb_button:nth-child(1)'
    time.sleep(5)
    get_driver.find_element_by_css_selector(css_b).click()
    wtr = 'div.et-pb-contact-message :nth-child(1)'
    WebDriverWait(get_driver, 5).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, wtr))
    )
    final_element = 'div.et-pb-contact-message :nth-child(1)'
    yield get_driver.find_element_by_css_selector(final_element)


def test_success(get_success):
    """
    Test checks that submit is
    completed successfully
    :param get_success: Web element
    :return: True or False
    """
    assert get_success.text == 'Thanks for contacting us'


@pytest.fixture()
def get_error_message(get_driver):
    """
    :param get_driver: Chrome browser
    :return: Found web element
    """
    n = 'Test_name'
    get_driver.get('https://ultimateqa.com/filling-out-forms/')
    input_1 = get_driver.find_element_by_id('et_pb_contact_name_0')
    input_1.send_keys(n)
    css_b = 'button.et_pb_contact_submit.et_pb_button:nth-child(1)'
    time.sleep(5)
    get_driver.find_element_by_css_selector(css_b).click()
    wtr = 'div.et-pb-contact-message :nth-child(1)'
    WebDriverWait(get_driver, 5).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, wtr))
    )
    final_element = "//div[@class='et-pb-contact-message']/ul/li"
    yield get_driver.find_element_by_xpath(final_element)


def test_error_message(get_error_message):
    """
    Test checks that submit fails
    and name of not filled field (Message)
    is Shown on browser
    :param get_error_message: Web element
    :return: True or False
    """
    assert get_error_message.text == 'Message'


@pytest.fixture()
def get_error_name(get_driver):
    """
    :param get_driver: Chrome browser
    :return: Found web element
    """
    m = 'Test message test test'
    get_driver.get('https://ultimateqa.com/filling-out-forms/')
    input_1 = get_driver.find_element_by_id('et_pb_contact_message_0')
    input_1.send_keys(m)
    css_b = 'button.et_pb_contact_submit.et_pb_button:nth-child(1)'
    time.sleep(5)
    get_driver.find_element_by_css_selector(css_b).click()
    wtr = 'div.et-pb-contact-message :nth-child(1)'
    WebDriverWait(get_driver, 5).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, wtr))
    )
    final_element = "//div[@class='et-pb-contact-message']/ul/li"
    yield get_driver.find_element_by_xpath(final_element)


def test_error_name(get_error_name):
    """
    Test checks that submit fails
    and name of not filled field (Name)
    is Shown on browser
    :param get_error_name: Web element
    :return: True or False
    """
    assert get_error_name.text == 'Name'
