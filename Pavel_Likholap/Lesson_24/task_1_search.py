from selenium.webdriver import Chrome


def get_browser():
    browser = Chrome(executable_path='D:/chromedriver.exe')
    return browser


def get_element_by_css():
    """
    :return: None
    """
    driver = get_browser()
    driver.get('https://ultimateqa.com/complicated-page/')
    driver.find_element_by_css_selector('a.et_pb_button_4').click()


def get_element_by_xpath():
    """
    :return: None
    """
    driver = get_browser()
    driver.get('https://ultimateqa.com/complicated-page/')
    e = f"{'//a[@class=et_pb_button et_pb_button_4 et_pb_bg_layout_light]'}"
    driver.find_element_by_xpath(f'{e}').click()


def get_element_by_class():
    """
    :return: None
    """
    driver = get_browser()
    driver.get('https://ultimateqa.com/complicated-page/')
    driver.find_element_by_class_name('et_pb_button_4').click()


if __name__ == '__main__':
    get_element_by_css()
    get_element_by_xpath()
    get_element_by_class()
