"""
This is caesar cipher
program
"""


def brutus():
    """
Current function encodes or decodes provided string
encoding/decoding is performed by shifting exist
letter to specified digit value in alphabet.
Eg. 'abc' is target string and 1 is digit value
after encoding result will be: 'bcd'
    """
    print('Welcome to caesar cipher')
    target_action = ''
    while not (target_action == 1 or target_action == 2):
        target_action = int(input("""Select target execution:
            1)Encode
            2)Decode
            : """))
    target_strng = input("""Provide string
            to Encode/Decode: """)
    target_nmbr = int(input("""Provide digit
            to shift: """))
    res = ''
    if target_action == 1:
        for i in target_strng:
            if i.lower() not in 'abcdefghijklmnopqrstuvwxyz':
                res += i
            else:
                if ord(i) + target_nmbr > 122:
                    res += chr((ord(i) + target_nmbr) - 26)
                else:
                    res += chr(ord(i) + target_nmbr)
    else:
        for i in target_strng:
            if i.lower() not in 'abcdefghijklmnopqrstuvwxyz':
                res += i
            else:
                if ord(i) - target_nmbr < 97:
                    res += chr((ord(i) - target_nmbr) + 26)
                else:
                    res += chr(ord(i) - target_nmbr)
    return res


if __name__ == '__main__':
    print(brutus())
