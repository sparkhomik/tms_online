"""
This is tic-tac-toe game
numbers are accepted
which correspond to cells:

 7 | 8 | 9
---|---|---
 4 | 5 | 6
---|---|---
 1 | 2 | 3

"""

import random

main_space = [' ' for i in range(10)]


def get_space(board: list):
    """
    Function draws game space
    :param board:
    :return:
    """
    sl = '|'
    big_sl = '---|---|---'
    print(f' {board[7]} {sl} {board[8]} {sl} {board[9]}')
    print(big_sl)
    print(f' {board[4]} {sl} {board[5]} {sl} {board[6]}')
    print(big_sl)
    print(f' {board[1]} {sl} {board[2]} {sl} {board[3]}')


def get_players_letter():
    """
    Function returns option X or 0
    which was chose by human
    :return:
    """
    letter = ''
    while not (letter == 'X' or letter == '0'):
        print('Do you choose X or 0?')
        letter = input('Please enter chosen option(X or 0): ').upper()
    return ['X', '0'] if letter == 'X' else ['0', 'X']


def get_who_first():
    """
    Function returns player, who will make
    first step
    :return:
    """
    return 'Human' if random.randint(0, 1) else 'Computer'


def get_move(board: list, letter: str, move: int):
    """
    Function changes main space list
    :return:
    """
    board[move] = letter


def winner(b, let):
    """
    Function is returned winner
    combinations
    :param b: main space
    :param let: player letters
    :return: True or False
    """
    return (b[7] == let and b[8] == let and b[9] == let) or \
           (b[4] == let and b[5] == let and b[6] == let) or \
           (b[1] == let and b[2] == let and b[3] == let) or \
           (b[7] == let and b[4] == let and b[1] == let) or \
           (b[8] == let and b[5] == let and b[2] == let) or \
           (b[9] == let and b[6] == let and b[3] == let) or \
           (b[7] == let and b[5] == let and b[3] == let) or \
           (b[9] == let and b[5] == let and b[1] == let)


def free_space(board, move):
    """
    Function checks parts of main space
    and returns True if space is free
    :param board:
    :param move:
    :return: True or False
    """
    return board[move] == ' '


def allow_player_move():
    """
    Function returns digit for make run
    """
    move = ' '
    while move not in '123456789':
        print('Choose coordinates 1-9')
        move = input('Enter coordinates from 1 to 9: ')
    return int(move)


def cmp_random_choose(space, mv_list):
    """
    Function returns digit for make run by computer
    :param space: main space
    :param mv_list: numbers of side and corner cells
    :return:
    """
    available_mov = []
    for i in mv_list:
        if free_space(space, i):
            available_mov.append(i)
    if available_mov:
        return random.choice(available_mov)
    else:
        return None


def get_winners(space_copy: list, letter: str):
    """
    Function returns winner case move
    :param space_copy: main space
    :param letter: player letter
    :return: winner move
    """
    for i in range(1, 10):
        space = space_copy.copy()
        if free_space(space, i):
            get_move(space, letter, i)
            if winner(space, letter):
                return i


def get_cmp_run(space, cmp_player):
    """
    Function for checking optimal
    computer move
    :param space:
    :param cmp_player:
    :return:
    """
    if cmp_player == 'X':
        hmn_player = '0'
    else:
        hmn_player = 'X'
    get_winners(main_space, cmp_player)
    get_winners(main_space, hmn_player)
    move = cmp_random_choose(main_space, [1, 3, 7, 9])
    if move:
        return move
    if free_space(main_space, 5):
        return 5
    return cmp_random_choose(main_space, [2, 4, 6, 8])


def space_full():
    """
    Checked main space: Full or Not
    :return: True or False
    """
    for i in range(1, 10):
        if free_space(main_space, i):
            return False
    return True


def resume():
    """
    Function to continue or to end the Game
    :return:
    """
    global main_space
    res_1 = ''
    while res_1 not in ('y', 'n'):
        res_1 = input("Let's play one more time (y/n): ")
    if res_1 == 'y':
        main_space = [' ' for i in range(10)]
        zero()
    else:
        print('See you later!!!')


def zero():
    """
    Main function to play the Game
    :return: nothing
    """
    print('Welcome tic-tac-toe game!!')
    hmn_player, cmp_player = get_players_letter()
    print(f'Human plays with {hmn_player}')
    print(f'Computer plays with {cmp_player}')
    run = get_who_first()
    print(f'{run}, starts first')
    playing = True
    while playing:
        if run == 'Computer':
            move_cmp = get_cmp_run(main_space, cmp_player)
            get_move(main_space, cmp_player, move_cmp)
            if winner(main_space, cmp_player):
                print("Computer WIN!!!")
                get_space(main_space)
                playing = False
            else:
                if space_full():
                    print('Draw!')
                    get_space(main_space)
                    break
                else:
                    run = 'Human'
        else:
            get_space(main_space)
            move_hmn = allow_player_move()
            get_move(main_space, hmn_player, move_hmn)
            if winner(main_space, hmn_player):
                get_space(main_space)
                print("You WIN!!!")
                playing = False
            else:
                if space_full():
                    get_space(main_space)
                    print('Draw!')
                    break
                else:
                    run = 'Computer'
    resume()


if __name__ == "__main__":
    zero()
