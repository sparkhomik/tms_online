"""
From this module we can run
the following programs:
caesar cipher
tic-tac-toe game
"""

import caesar.caeserpr
import tictactoe.tictactoepr
import random


def programs():
    """
    By this function we can run our
    programs: caesar cipher
    tic-tac-toe game
    :return:
    """
    playing = True
    while playing:
        select = ''
        print("""Choose program which you want to use
        1 is caesar cipher
        2 is tic-tac-toe
        """)
        error_counter = 0
        while select not in ('1', '2'):
            select = input('Select digit: ')
            if select not in ('1', '2'):
                print('Error, please choose 1 or 2')
                error_counter += 1
            if error_counter == 3:
                break
        if error_counter == 3:
            select = random.randint(0, 1)
        if select == '1':
            caesar.caeserpr.brutus()
        else:
            tictactoe.tictactoepr.zero()
        res_1 = ''
        while res_1 not in ('y', 'n'):
            res_1 = input("Do you want to continue or not (y/n): ")
        if res_1 == 'y':
            select = ''
            programs()
        else:
            print('See you later!!!')
            break


if __name__ == "__main__":
    programs()
