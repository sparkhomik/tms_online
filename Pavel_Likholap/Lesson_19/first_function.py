def check_register(string: str) -> bool:
    """
    :param: String
    :return: Bool expression
    """
    res = list(filter(lambda x: x == x.lower(), string))
    return True if len(res) == len(string) else False


if __name__ == '__main__':
    print(check_register('check'))
