import pytest
from first_function import check_register
from second_function import change_number_type, InputTypeError
from third_function import check_spec_symbol


@pytest.mark.parametrize('a, e', [('pa', True), ('AD', False), ('sD', False)])
def test_check_register(a, e):
    """
    Test checks correct work of
    check_register function
    :param: a is actual result,
    e is expected result
    """
    assert check_register(a) is e


@pytest.mark.parametrize('act, exp', [(13, 13.0), (0, 0.0), (-1, -1.0)])
def test_change_number_type(act, exp):
    """
    Test checks correct work of
    change_number_type function
    """
    assert change_number_type(act) == exp


def test_correct_error(test_errors):
    """
    Test for checking Errors appearing
    for change_number_type function
    """
    with pytest.raises(InputTypeError):
        assert change_number_type(test_errors)


@pytest.mark.parametrize('a, e', [('#', True), ('*', True), ('text', False)])
def test_check_spec_symbol(a, e):
    """
    Test checks correct work of
    check_spec_symbol function
    :param: a is actual result,
    e is expected result
    """
    assert check_spec_symbol(a) is e
