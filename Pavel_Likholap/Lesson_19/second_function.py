class InputTypeError(Exception):
    """
    Class to change default error to custom
    with additional message
    """
    message = 'Provided data is not integer digit'

    def __init__(self, value):
        """
        :param value: Argument for
        'change_number_type' function
        """
        self.final_message = f'{self.message}, data: {value}'
        super().__init__(self.final_message)


def change_number_type(value: int) -> float:
    """
    :param: Integer
    :return: Float
    """
    if type(value) != int:
        raise InputTypeError(value)
    else:
        return float(value)


if __name__ == '__main__':
    print(change_number_type(78))
