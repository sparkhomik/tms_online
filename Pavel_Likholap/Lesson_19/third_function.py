def check_spec_symbol(value: str) -> bool:
    """
    :param: String
    :return: Bool
    """
    return True if value in '!@#$%^&*()_+<>|' else False


if __name__ == '__main__':
    print(check_spec_symbol('test'))
    print(check_spec_symbol('&'))
