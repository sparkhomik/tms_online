import pytest
from datetime import datetime


@pytest.fixture(scope='session', autouse=True)
def get_current_time():
    """
    Fixture provides start and finish
    entire session
    """
    start = datetime.now().time()
    print(f'tests are started at: {start}')
    yield start
    print(f'tests are finished at: {datetime.now().time()}')


@pytest.fixture(scope='module', autouse=True)
def get_module_name(request):
    """
    Fixture provides module name
    """
    print(f'module name is {str(request.module).split()[1]}')


@pytest.fixture(autouse=True)
def get_fixtures_names(request):
    """
    Fixture provides fixtures names
    which used by current test
    """
    print(*request.fixturenames)


@pytest.fixture(autouse=True)
def get_time_diff(get_current_time):
    """
    Fixture provides start time of entire
    session and start time of particular test
    """
    test_time = datetime.now().time()
    print(f'session started at {get_current_time}, \
    test started at {test_time}')


@pytest.fixture(params=['1', 2, False])
def test_errors(request):
    """
    Fixture provides input values
    to test of Errors appearing
    """
    return request.param
