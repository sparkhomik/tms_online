from selenium.webdriver import Chrome
import logging

# logging is performed to module_logs.log on DEBUG level
logging.basicConfig(level='DEBUG', filename='module_logs.log')
logging.getLogger()


# browser initialization
drv = Chrome(executable_path='D:/chromedriver.exe')
drv.implicitly_wait(15)


def perform_login():
    """
    Method opens browser
    where performs login
    to
    :return: None
    """
    drv.get('https://www.saucedemo.com/')
    un = drv.find_element_by_css_selector('#user-name')
    un.send_keys('standard_user')
    psw = drv.find_element_by_css_selector('#password')
    psw.send_keys('secret_sauce')
    drv.find_element_by_xpath('//input[@type="submit"]').click()


def get_products():
    """
    :return: Products with prices
    value is price
    """
    perform_login()
    products = []
    for i in range(1, 7):
        e = f'.inventory_list :nth-child({i})'
        n = drv.find_element_by_css_selector(f'{e} div.inventory_item_name')
        p = drv.find_element_by_css_selector(f'{e} div.inventory_item_price')
        products.append(f'{n.text}, price is {p.text}')
    return '\n'.join(products)


def get_page_menu():
    """
    :return: 'burger' menu button
    from site page
    """
    perform_login()
    menu = drv.find_element_by_css_selector('div.bm-burger-button')
    return menu


def get_sorting_drop_down():
    """
    :return: Sorting drop down list
    from site page
    """
    perform_login()
    s = drv.find_element_by_xpath('//select')
    return s


def get_cart():
    """
    :return: User cart
    from site page
    """
    perform_login()
    c = drv.find_element_by_class_name('shopping_cart_link')
    return c


if __name__ == '__main__':
    try:
        print(get_products())
        get_page_menu()
        get_sorting_drop_down()
        get_cart()
        with open('module_logs.log') as file_log:
            logs = file_log.read()
            print(logs)
    finally:
        drv.quit()
