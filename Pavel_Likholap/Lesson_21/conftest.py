from selenium.common.exceptions import NoSuchElementException
from selenium.webdriver import Chrome
import pytest


@pytest.fixture(scope='session', autouse=True)
def drv():
    """
    :return: Chrome browser
    """
    browser = Chrome(executable_path='D:/chromedriver.exe')
    yield browser
    browser.quit()


@pytest.fixture()
def send_right_credentials(drv):
    """
    :param drv: Chrome browser
    :return: Web-element after
    successfully registration
    """
    lgn = 'tomsmith'
    psw = 'SuperSecretPassword!'
    drv.implicitly_wait(15)
    drv.get('http://the-internet.herokuapp.com/login')
    input_name = drv.find_element_by_xpath('//input[@id="username"]')
    input_name.send_keys(lgn)
    input_psw = drv.find_element_by_xpath('//input[@id="password"]')
    input_psw.send_keys(psw)
    button = drv.find_element_by_css_selector('button.radius')
    button.click()
    yield drv.find_element_by_css_selector('h2')


@pytest.fixture()
def send_wrong_credentials(drv):
    """
    :param drv: Chrome browser
    :return: Excepted error
    after providing wrong credentials
    """
    lgn = 'pavel'
    psw = 'qwe123QWE'
    drv.implicitly_wait(15)
    drv.get('http://the-internet.herokuapp.com/login')
    input_name = drv.find_element_by_xpath('//input[@id="username"]')
    input_name.send_keys(lgn)
    input_psw = drv.find_element_by_xpath('//input[@id="password"]')
    input_psw.send_keys(psw)
    button = drv.find_element_by_css_selector('button.radius')
    button.click()
    yield drv.find_element_by_css_selector('h2')


@pytest.fixture()
def checkbox_1(drv):
    """
    :param drv: Chrome browser
    :return: 'true' for marked
    checkbox
    """
    drv.get('http://the-internet.herokuapp.com')
    check_boxes = drv.find_element_by_xpath('//a[@href="/checkboxes"]')
    check_boxes.click()
    box_1 = drv.find_element_by_css_selector('#checkboxes > :nth-child(1)')
    box_1.click()
    yield box_1.get_attribute('checked')


@pytest.fixture()
def checkbox_2(drv):
    """
    :param drv: Chrome browser
    :return: None for unmarked
    checkbox
    """
    drv.get('http://the-internet.herokuapp.com')
    check_boxes = drv.find_element_by_xpath('//a[@href="/checkboxes"]')
    check_boxes.click()
    box_2 = drv.find_element_by_css_selector('#checkboxes > :nth-child(3)')
    box_2.click()
    yield box_2.get_attribute('checked')


@pytest.fixture()
def get_page_amount(drv):
    """
    :param drv: Chrome browser
    :return: List with opened browser
    pages
    """
    drv.get('http://the-internet.herokuapp.com')
    drv.find_element_by_xpath('//a[@href="/windows"]').click()
    drv.find_element_by_xpath('//a[@href="/windows/new"]').click()
    yield drv.window_handles


@pytest.fixture()
def get_title_new_page(drv):
    """
    :param drv: Chrome browser
    :return: Web-element of page title
    """
    drv.get('http://the-internet.herokuapp.com')
    drv.find_element_by_xpath('//a[@href="/windows"]').click()
    drv.find_element_by_xpath('//a[@href="/windows/new"]').click()
    names = drv.window_handles
    drv.switch_to.window(names[1])
    yield drv.find_element_by_tag_name('h3')


@pytest.fixture()
def get_added_element(drv):
    """
    :param drv: Chrome browser
    :return: e - Element which was added
    to web page
    """
    drv.get('http://the-internet.herokuapp.com')
    drv.find_element_by_xpath('//a[@href="/add_remove_elements/"]').click()
    drv.find_element_by_xpath('//button[@onclick="addElement()"]').click()
    e = drv.find_element_by_css_selector('#elements > button.added-manually')
    yield e


@pytest.fixture()
def get_deleted_element(drv):
    """
    :param drv: Chrome browser
    :return: True after error exception
    """
    drv.get('http://the-internet.herokuapp.com')
    drv.get('http://the-internet.herokuapp.com')
    drv.find_element_by_xpath('//a[@href="/add_remove_elements/"]').click()
    drv.find_element_by_xpath('//button[@onclick="addElement()"]').click()
    d = drv.find_element_by_css_selector('#elements > button.added-manually')
    d.click()
    try:
        drv.find_element_by_css_selector('#elements > button.added-manually')
    except NoSuchElementException:
        yield True
