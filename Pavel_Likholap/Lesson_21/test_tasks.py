def test_credentials(send_right_credentials):
    """
    Test checks log in using
    right credentials
    :param send_right_credentials: Text of web element
    """
    assert send_right_credentials.text == 'Secure Area'


def test_credentials_negative(send_wrong_credentials):
    """
    Test checks appearing error
    using wrong credentials
    :param send_wrong_credentials: Text of web element
    """
    assert send_wrong_credentials.text == 'Login Page'


def test_checkbox_1(checkbox_1):
    """
    Test checks marked checkbox
    :param checkbox_1: String
    """
    assert checkbox_1 == 'true'


def test_checkbox_2(checkbox_2):
    """
    Test checks unmarked checkbox
    :param checkbox_2: None
    """
    assert checkbox_2 is None


def test_new_page_opening(get_page_amount):
    """
    Test checks that new page was
    opened on browser
    :param get_page_amount: List with web pages
    """
    assert len(get_page_amount) == 2


def test_new_page_title(get_title_new_page):
    """
    Test checks title of opened page
    :param get_title_new_page: Text of web element
    """
    assert get_title_new_page.text == 'New Window'


def test_add_new_element(get_added_element):
    """
    Test checks adding new element
    on web page
    :param get_added_element: Text of web element
    """
    assert get_added_element.text == 'Delete'


def test_deleted_element(get_deleted_element):
    """
    Test checks that element was
    removed from web page
    :param get_deleted_element: Bool expression
    """
    assert get_deleted_element is True
