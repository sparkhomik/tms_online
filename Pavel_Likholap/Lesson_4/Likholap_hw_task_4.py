# 1
"""
Перевести строку в массив
"Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" =>
["I", "love", "arrays", "they", "are", "my", "favorite"]
"""
# получаем на вход значение строки и сразу
# переводим ее элементы в список
test_string = input().split()
print(test_string)

# 2
"""
Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
"""
test_list = ' '.join(['Ivan', 'Ivanou'])
string_1, string_2 = 'Minsk', 'Belarus'
print(f'Привет, {test_list}! Добро пожаловать в {string_1} {string_2}')

# 3
"""
Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => "I love arrays they are my favorite"
"""
test_list_2 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
test_list_2 = ' '.join(test_list_2)
print(test_list_2)

# 4
"""
Создайте список из 10 элементов, вставьте на 3-ю позицию
новое значение,удалите элемент из списка под индексом 6
"""

test_list_3 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
test_list_3[2] = 'new_value'
del test_list_3[6]
print(test_list_3)

# 5
"""
Есть 2 словаря
a = { 'a': 1, 'b': 2, 'c': 3}
b = { 'c': 3, 'd': 4,'e': “”}
"""
#  Создайте словарь, который будет содержать
# в себе все элементы обоих словарей
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ""}
c = {**a, **b}
# Обновите словарь “a” элементами из словаря “b”
a.update(b)
print(a)
# Проверить что все значения в словаре “a” не пустые либо не равны нулю
print(True if all(a.values()) else False)
# Проверить что есть хотя бы одно пустое значение
# (результат выполнения должен быть True)
print(False if all(a.values()) else True)
# Отсортировать словарь по алфавиту в обратном порядке
print(sorted(c.items(), reverse=True))
# Изменить значение под одним из ключей и вывести все значения
c['e'] = 'changed'
print(c)

# 6
# Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
# Вывести только уникальные значения и сохранить
# их в отдельную переменную
new_item = set(list_a)
print(new_item)
# Добавить в полученный объект значение 22
new_item.add(22)
print(new_item)
# Сделать list_a неизменяемым
# Перезаписываем значения из листа в tuple
list_a = tuple(list_a)
print(list_a)
# Измерить его длинну
print(len(list_a))

"""
Задачи на закрепление форматирования:
Есть переменные a=10, b=25
Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
При решении задачи использовать оба способа форматирования
"""
a = 10
b = 25
# format
print('Summ is {c} and diff = {d}.'.format(c=a + b, d=a - b))

# f-str
sss = a + b
diff = a - b
print(f'Summ is {sss} and diff = {diff}.')

"""
Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>,
second is “<второе>”, and last one – “<третье>””
"""
lc = ['Sasha', 'Vasia', 'Nikalai']
print(f'First child is {lc[0]}, second is {lc[1]}, and last one – {lc[2]}')

"""
*1) Вам передан массив чисел. Известно, что каждое число
в этом массиве имеет пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
Напишите программу, которая будет выводить уникальное число
"""
list_1 = input().split()
for i in list_1:
    if list_1.count(i) != 1:
        continue
    else:
        print(i)
        break

"""
*2) Дан текст, который содержит различные английские
буквы и знаки препинания.Вам необходимо найти самую
частую букву в тексте. Результатом должна быть
буква в нижнем регистре.При поиске самой частой
буквы, регистр не имеет значения,так что при подсчете
считайте, что "A" == "a". Убедитесь, что вы не считайте
знаки препинания, цифры и пробелы, а только буквы.
Если в тексте две и больше буквы с одинаковой
частотой, тогда результатом будет буква, которая
идет первой в алфавите. Для примера, "one"
содержит "o", "n", "e" по одному разу, так
что мы выбираем "e".
"""

text = input().lower()
count = 0
actual_letter = ''
for i in text:
    if i not in 'abcdefghijklmnopqrstuvwxyz':
        continue
    else:
        temp_count = text.count(i)
        if temp_count > count:
            count = temp_count
            actual_letter = i
        elif temp_count == count:
            if ord(i) < ord(actual_letter):
                actual_letter = i
            else:
                continue
print(actual_letter)
