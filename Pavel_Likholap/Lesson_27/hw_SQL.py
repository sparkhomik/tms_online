import mysql.connector as mysql

db = mysql.connect(
    host="localhost",
    user="root",
    passwd="qwe123QWE",
    database="hw27_1"
)
cursor = db.cursor()
# TASK_1
query1 = "SELECT ord_no, ord_date, purch_amt " \
         "FROM orders3 where salesman_id = 5002"
query2 = "SELECT distinct salesman_id FROM orders3"
query3 = "SELECT ord_date, salesman_id, ord_no " \
         "FROM orders3 ORDER by ord_date"
query4 = "SELECT ord_no from orders3 " \
         "WHERE ord_no between 70001 and 70007"
# TASK_2
query21 = "SELECT SUM(PRO_PRICE) as SUM_PRICE FROM PC"
query22 = "SELECT AVG(PRO_PRICE) as AVG_PRICE FROM PC"
query23 = "SELECT PRO_NAME FROM PC " \
          "WHERE PRO_PRICE between 200 and 600"
query24 = "SELECT PRO_NAME FROM PC " \
          "WHERE PRO_PRICE=(SELECT MAX(PRO_PRICE) from PC)"
query25 = "SELECT PRO_NAME FROM PC " \
          "WHERE PRO_PRICE=(SELECT MIN(PRO_PRICE) from PC)"

# cursor.execute(queryN)
# print(cursor.fetchall())
