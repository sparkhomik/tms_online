from functools import reduce

"""
На вход подаётся некоторое количество
(не больше сотни) разделённых пробелом
целых чисел (каждое не меньше 0 и не больше 19).
Выведите их через пробел в порядке
лексикографического возрастания названий этих
чисел в английском языке.
"""

nn = {0: 'zero', 1: 'one', 2: 'two',
      3: 'three', 4: 'four', 5: 'five',
      6: 'six', 7: 'seven', 8: 'eight',
      9: 'nine', 10: 'ten', 11: 'eleven',
      12: 'twelve', 13: 'thirteen', 14: 'fourteen',
      15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
      18: 'eighteen', 19: 'nineteen'}


def decorator(func):
    def wrapper(*args):
        """
        :param args: List
        :return: Changed list of numbers
        """
        dic = {}
        list_1 = []
        for i in args:
            dic[nn[i]] = i
        for i in sorted(dic.items()):
            list_1.append(i[1])
        return func(*list_1)
    return wrapper


@decorator
def main(*args):
    """
    :param args: List
    :return: Changed list
    """
    return args


print(*main(1, 3, 0, 5, 19, 11))

"""
Напишите декоратор,
который проверял бы тип
параметров функции, конвертировал их
если надо и складывал:
"""


def dec(type_1):
    def decorator3(func):
        def wrapper(*args):
            """
            :param args: tuple
            :return: Changed tuple
            """
            new = list(map(type_1, args))
            return func(*new)
        return wrapper
    return decorator3


def plus(a, b):
    """
    :param a: str or int
    :param b: str or int
    :return: Sum of a and b
    """
    return a + b


@dec(type_1=str)
def func1(*args):
    """
    :param args: tuple
    :return: Common sum of elements
    """
    res = reduce(plus, args)
    return res


print(func1('Pavel', 15, '6', 1.0555))
