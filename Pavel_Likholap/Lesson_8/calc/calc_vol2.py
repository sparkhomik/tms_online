"""
Current module calculates input expression,
which was entered by user
"""


def plus(a, b):
    """
    :param a: float
    :param b: float
    :return: Sum of a and b
    """
    return a + b


def minus(a, b):
    """
    :param a: float
    :param b: float
    :return: Difference of a and b
    """
    return a - b


def multiply(a, b):
    """
    :param a: float
    :param b: float
    :return: Product of a and b
    """
    return a * b


def divide(a, b):
    """
    :param a: float
    :param b: float
    :return: Quotient of a and b
    """
    return a / b


def extent(a, b):
    """
    :param a: float
    :param b: float
    :return: Raised number
    """
    return a ** b


def get_digits(main_stack):
    """
    :param main_stack: list
    :return: Removed 2 last digit from stack
    """
    f = main_stack.pop()
    s = main_stack.pop()
    return f, s


prior = {'+': 1, '-': 1, '*': 2, '/': 2, '**': 3}
expression = input().split()
m_list = []
expr_l = []


def change_order(exp):
    """
    :param exp: list
    :return: Changed list with digits and operands
    """
    for i in range(len(exp)):
        if exp[i].isdigit():
            m_list.append(float(exp[i]))
        else:
            if not expr_l:
                expr_l.append(exp[i])
            else:
                if prior[exp[i]] <= prior[expr_l[-1]]:
                    m_list.append(expr_l.pop())
                    expr_l.append(exp[i])
                else:
                    expr_l.append(exp[i])
    m_list.extend(expr_l)
    return m_list


def get_result(changed_list):
    """
    :param changed_list: Changed list with digits and operands
    :return: Calculated result
    """
    m_stack = []
    for i in changed_list:
        if type(i) == float:
            m_stack.append(i)
        else:
            if i == '+':
                y, x = get_digits(m_stack)
                m_stack.append(plus(x, y))
            elif i == '-':
                y, x = get_digits(m_stack)
                m_stack.append(minus(x, y))
            elif i == '*':
                y, x = get_digits(m_stack)
                m_stack.append(multiply(x, y))
            elif i == '/':
                y, x = get_digits(m_stack)
                m_stack.append(divide(x, y))
            else:
                y, x = get_digits(m_stack)
                m_stack.append(extent(x, y))
    return m_stack.pop()


if __name__ == "__main__":
    print(get_result(change_order(expression)))
