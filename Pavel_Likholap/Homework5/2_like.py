lang_1 = 'abcdefghijklmnopqrstuvwxyz ,'
names = input('Введите имена через запятую: ')
if len(names) == 0:
    print('No one likes this/Никто не лайкнул это')
else:
    flag = True
    for i in names:
        if i.lower() in lang_1:
            continue
        else:
            flag = False
            break
    n_list = names.split(', ')
    if flag:
        if len(n_list) == 1:
            print(*n_list, 'likes this')
        elif len(n_list) == 2:
            print(n_list[0], 'and', n_list[1], 'likes this')
        else:
            am = len(n_list) - 2
            print(f'{n_list[0]}, {n_list[1]} and {am} others likes this')
    else:
        if len(n_list) == 1:
            print(*n_list, 'лайкнул(а) это')
        elif len(n_list) == 2:
            print(n_list[0], 'и', n_list[1], 'лайкнули это')
        else:
            am = len(n_list) - 2
            print(f'{n_list[0]}, {n_list[1]} и {am} других лайкнули это')
