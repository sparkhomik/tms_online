# 4
"""
Напишите код, который возьмет список
строк и пронумерует их
"""
t_s = input('Введите несколько строк через пробел: ').split()
final = {t_s[i]: i + 1 for i in range(len(t_s))}
print(final)

# 5
"""
Проверить, все ли элементы одинаковые
"""
n = [1, 3, 4, 5]
if len(set(n)) >= 1:
    print(True)
else:
    print(False)

# 6
"""
Проверка строки. В данной подстроке проверить
все ли буквы в строчном регистре или нет
и вернуть список не подходящих.
"""
string_10 = input()
l_10 = []
flag = bool()
for i in string_10:
    if i == i.upper():
        l_10.append(i)
    else:
        continue
if len(l_10) > 0:
    flag = False
else:
    flag = True
final_list = [[flag], l_10]
print(final_list)

# 7
"""
Сложите все числа в списке, они могут
быть отрицательными, если список пустой вернуть 0
"""
# для целых чисел
list_1 = list(map(int, input().split()))
print(sum(list_1) if len(list_1) > 0 else 0)
# для вещественных чисел
list_1 = list(map(float, input().split()))
print(sum(list_1) if len(list_1) > 0 else 0)
