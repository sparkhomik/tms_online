import random
# Генерируем случайное число
# Цикл закончится когда все цифры будут разные
l_1 = [0, 0, 0, 0]
while len(set(l_1)) != 4:
    l_1[0] = random.randint(0, 9)
    l_1[1] = random.randint(0, 9)
    l_1[2] = random.randint(0, 9)
    l_1[3] = random.randint(0, 9)
# Выводим рандомное число для тестирования
print(l_1)
bulls_counter = 0
while bulls_counter != 4:
    cows_counter = 0
    bulls_counter = 0
# Генерируем список из введенного числа
    num_customer = [int(i) for i in input("Введите число для проверки: ")]
    for i in range(4):
        if num_customer[i] == l_1[i]:
            bulls_counter += 1
        else:
            if num_customer[i] in l_1:
                cows_counter += 1
            else:
                continue
    d_cows = {0: "нет коров", 1: "одна корова",
              2: "две коровы", 3: "три коровы",
              4: "четыре коровы"}
    d_bulls = {0: "нет быков", 1: "один бык", 2: "два быка", 3: "три быка"}
    if bulls_counter == 4:
        print("Вы выиграли!")
    else:
        print(d_cows[cows_counter].title(), d_bulls[bulls_counter], sep=', ')
        print('Попробуйте еще раз)')
