# 1
"""
Из заданной строки получить нужную строку:
"String" => "SSttrriinngg"
"Hello World" => "HHeelllloo  WWoorrlldd"
"1234!_ " => "11223344!!__  "
"""
n = input()
res_str = ''
for i in n:
    res_str += i * 2
print(res_str)

# 2
"""
Если х в квадрате больше 1000 - пишем
"Hot" иначе - "Nope"
'50' == "Hot"
4 == "Nope"
"12" == "Nope"
60 == "Hot"
"""
x = int(input('Введите значение "х": '))
print("Hot" if x**2 > 1000 else "Nope")

# 3
"""
Сложите все числа в списке, они могут быть отрицательными,
если список пустой вернуть 0
[] == 0
[1, 2, 3] == 6
[1.1, 2.2, 3.3] == 6.6
[4, 5, 6] == 15
range(101) == 5050
"""
# для целых чисел
list_1 = list(map(int, input().split()))
print(sum(list_1) if len(list_1) > 0 else 0)
# для вещественных чисел
list_1 = list(map(float, input().split()))
print(sum(list_1) if len(list_1) > 0 else 0)

# 4
"""
Подсчет букв
Дано строка и буква => "fizbbbuz","b",
нужно подсчитать сколько раз буква b
встречается в заданной строке 3
"""
string_1 = input("Введите строку: ")
letter = input("Введите искомую букву: ")
print(string_1.count(letter))

# 5
"""
Напишите код, который возьмет список строк и пронумерует их.
Нумерация начинается с 1, имеет : и пробел
"""
l22 = input().split()
c = {l22[j]: j + 1 for j in range(len(l22))}
print(c)

# 6
"""
Напишите программу, которая по данному числу
n от 1 до n выводит на экран n флагов.
Изображение одного флага имеет
размер 4×4 символов. Внутри каждого
флага должен быть записан его номер —
число от 1 до n.
"""
n = int(input())
print('+___ ' * n)
for i in range(n):
    print('|', i + 1, ' /', sep='', end=' ')
print()
print('|__\\ ' * n)
print('|    ' * n)

# 7
"""
Напишите программу. Есть 2 переменные
salary и bonus. Salary - integer,
bonus - boolean. Если bonus - true,
salary должна быть умножена на 10. Если false - нет
"""
salary = int(input())
bonus = bool(input())
if bonus:
    salary *= 1.1
print(salary)

# 8
"""
Проверить, все ли элементы одинаковые
"""
n = [1, 3, 4, 5]
if len(set(n)) == 1 or len(set(n)) == 0:
    print(True)
else:
    print(False)

# 9
"""
Суммирование. Необходимо подсчитать сумму всех
чисел до заданного:
дано число 2, результат будет -> 3(1+2)
дано число 8, результат будет ->
36(1 + 2 + 3 + 4 + 5 + 6 + 7 + 8)
"""
print(sum([i for i in range(1, int(input()) + 1)]))

# 10
"""
Проверка строки. В данной подстроке проверить
все ли буквы в строчном регистре или нет и
вернуть список не подходящих.
"""

string_10 = input()
l_10 = []
flag = bool()
for i in string_10:
    if i == i.upper():
        l_10.append(i)
    else:
        continue
if len(l_10) > 0:
    flag = False
else:
    flag = True
final_list = [[flag], l_10]
print(final_list)
