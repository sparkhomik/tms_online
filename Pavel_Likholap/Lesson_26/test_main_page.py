from main_page import MainPage


def test_main_page_title(drv):
    """
    Test checks books page title
    """
    page = MainPage(drv)
    page.open()
    assert page.get_main_page_title() == 'Oscar - Sandbox'


def test_get_welcome(drv):
    """
    Test checks particular web element
    """
    page = MainPage(drv)
    page.open()
    assert page.get_welcome().text == 'Welcome!', "Was opened not Main page"


def test_get_recommended(drv):
    """
    Test checks particular web element
    """
    page = MainPage(drv)
    page.open()
    assert page.get_recommended().text == 'Recommended reading'


def test_get_breadcrumb_error(drv):
    """
    Test checks that
    there ar are correct
    breadcrumbs
    """
    page = MainPage(drv)
    page.open()
    assert page.get_breadcrumb_error() is None, "Was opened not Main page"


def test_get_link_text(drv):
    """
    Test checks particular web element
    """
    page = MainPage(drv)
    page.open()
    assert page.get_link_text().text == 'using this form'


def test_open_basket_page(drv):
    """
    Test checks opening basket page
    and that it is blank
    """
    page = MainPage(drv)
    page.open()
    assert page.open_basket_page().text == 'Your basket is empty'


def test_open_fiction_page(drv):
    """
    Test checks opening fiction page from
    main page
    """
    page = MainPage(drv)
    page.open()
    assert 'Fiction' in page.open_fiction_page().get_fiction_page_title()
