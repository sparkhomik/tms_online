from fiction_page import FictionPage


def test_fiction_page_title(drv):
    """
    Test checks fiction page title
    """
    page = FictionPage(drv)
    page.open()
    assert 'Fiction' in page.get_fiction_page_title()


def test_fiction_breadcrumb(drv):
    """
    Test checks fiction page breadcrumbs
    """
    page = FictionPage(drv)
    page.open()
    assert 'Fiction' in page.get_fiction_page_title()


def test_fiction_header(drv):
    """
    Test checks particular fiction page header
    """
    page = FictionPage(drv)
    page.open()
    assert page.get_header().text == 'Fiction'
