from books_page_locators import BooksPageLocators
from books_page import BooksPage
import pytest


def test_books_page_title(drv):
    """
    Test checks books page title
    """
    page = BooksPage(drv)
    page.open()
    assert page.get_books_page_title() in 'Books | Oscar - Sandbox'


def test_books_breadcrumb(drv):
    """
    Test checks books page breadcrumbs
    """
    page = BooksPage(drv)
    page.open()
    assert page.get_books_breadcrumb().text == 'Books',\
        "Was opened not Basket page"


def test_showing(drv):
    """
    Test checks that page contains particular web element
    """
    page = BooksPage(drv)
    page.open()
    assert 'showing' in page.get_showing().text


def test_next_button(drv):
    """
    Test checks that page contains particular web element
    """
    page = BooksPage(drv)
    page.open()
    assert page.get_next_button().text == 'next'


def test_non_fiction(drv):
    """
    Test checks that page contains particular web element
    """
    page = BooksPage(drv)
    page.open()
    assert page.get_non_fiction().text == 'Non-Fiction'


def test_choose_book(drv):
    """
    Test checks search books by
    book name
    """
    page = BooksPage(drv)
    page.open()
    book_page = page.choose_book("The shellcoder's handbook")
    assert "The shellcoder's handbook" in book_page.get_book_page_title()


def test_choose_book_negative(drv):
    """
    Test checks that not existed books
    won't find
    """
    page = BooksPage(drv)
    page.open()
    book_page = page.choose_book("Blank name")
    assert book_page.find_element(BooksPageLocators.BOOK_EMPTY).text == '0'


def test_found_book_name(drv):
    """
    Test checks
    name of found book
    """
    page = BooksPage(drv)
    page.open()
    book_page = page.choose_book("Hacking Exposed Wireless")
    assert book_page.get_book_name().text == 'Hacking Exposed Wireless'


def test_book_image(drv):
    """
    Test checks existence of book image
    """
    page = BooksPage(drv)
    page.open()
    book_page = page.choose_book("Hacking Exposed Wireless")
    assert book_page.get_book_image() == "Books has main image"


@pytest.mark.xfail
def test_book_image_negative(drv):
    """
    That case is failed
    all books has images on target site
    """
    page = BooksPage(drv)
    page.open()
    book_page = page.choose_book("Visual Guide to Lock")
    assert book_page.get_book_image() == "Books hasn't main image"
