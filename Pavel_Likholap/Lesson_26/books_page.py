from book_page import BookPage
from books_page_locators import BooksPageLocators
from base_page import BasePage


class BooksPage(BasePage):
    """
    Class for Books page
    """
    URL = """http://selenium1py.pythonanywhere.com/en-gb
    /catalogue/category/books_2/"""

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_books_page_title(self):
        """
        :return: Books page title
        """
        return self.driver.title

    def get_books_breadcrumb(self):
        """
        :return: Books page breadcrumbs
        """
        bc = self.find_element(BooksPageLocators.BOOKS_BREAD)
        return bc

    def get_showing(self):
        """
        :return: Web element
        """
        text = self.find_element(BooksPageLocators.BOOKS_SHOWING)
        return text

    def get_next_button(self):
        """
        :return: Web element
        buttpm to open next page
        """
        button = self.find_element(BooksPageLocators.BOOKS_NEXT)
        return button

    def get_non_fiction(self):
        """
        :return: Web element
        """
        non = self.find_element(BooksPageLocators.BOOKS_LINK)
        return non

    def choose_book(self, book_name):
        """
        :param book_name: Book name to search
        :return: Object of BookPage class
        """
        input_book = self.find_element(BooksPageLocators.BOOKS_SEARCH)
        input_book.send_keys(book_name)
        search_button = self.find_element(BooksPageLocators.BOOKS_BUTTON)
        search_button.click()
        return BookPage(self.driver, self.driver.current_url)
