from basket_page import BasketPage


def test_basket_page_title(drv):
    """
    Test checks basket page title
    """
    page = BasketPage(drv)
    page.open()
    assert page.get_basket_page_title() in 'Basket | Oscar - Sandbox',\
        "Was opened not Basket page"


def test_basket_breadcrumb(drv):
    """
    Test checks basket page breadcrumbs
    """
    page = BasketPage(drv)
    page.open()
    assert page.get_basket_breadcrumb().text == 'Basket',\
        "Was opened not Basket page"


def test_drop_down_error(drv):
    """
    Test checks that dropdown is
    not shown on basket page
    """
    page = BasketPage(drv)
    page.open()
    assert page.get_drop_down_error() is None, "Was opened not Main page"


def test_basket_link(drv):
    """
    Test checks that page
    contains particular link
    """
    page = BasketPage(drv)
    page.open()
    assert page.get_basket_link().text == 'Continue shopping'


def test_basket_error(drv):
    """
    Test checks that basket page
    not contains particular web element
    """
    page = BasketPage(drv)
    page.open()
    assert page.get_basket_error() is None
