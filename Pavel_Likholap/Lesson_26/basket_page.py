from selenium.common.exceptions import NoSuchElementException
from main_page_locators import MainPageLocators
from basket_page_locators import BasketPageLocators
from base_page import BasePage


class BasketPage(BasePage):
    """
    Class for Basket page objects
    """
    URL = "http://selenium1py.pythonanywhere.com/en-gb/basket/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_basket_page_title(self):
        """
        :return: Page title
        """
        return self.driver.title

    def get_basket_breadcrumb(self):
        """
        :return: Page breadcrumb
        """
        bc = self.find_element(BasketPageLocators.BASKET_BREAD)
        return bc

    def get_drop_down_error(self):
        """
        :return: None
        """
        try:
            self.find_element(BasketPageLocators.BASKET_NOT_EXIST)
        except NoSuchElementException:
            return None

    def get_basket_error(self):
        """
        :return: None
        """
        try:
            self.find_element(MainPageLocators.MAIN_PAGE_BASKET)
        except NoSuchElementException:
            return None

    def get_basket_link(self):
        """
        :return: Web element
        """
        link = self.find_element(BasketPageLocators.BASKET_LINK)
        return link
