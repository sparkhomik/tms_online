from books_page_locators import BooksPageLocators
from base_page import BasePage
from selenium.common.exceptions import NoSuchElementException


class BookPage(BasePage):
    """
    Class for particular books
    """
    def get_book_page_title(self):
        """
        :return: Page title
        """
        return self.driver.title

    def get_book_name(self):
        """
        :return: Web element
        """
        return self.find_element(BooksPageLocators.BOOK_FOUND_NAME)

    def get_book_image(self):
        """
        :return: String with text
        which depends on main book image
        """
        try:
            self.find_element(BooksPageLocators.BOOK_FOUND_NAME)
            return f'{"Books has main image}"}'
        except NoSuchElementException:
            return f"""{"Books hasn't main image"}"""
