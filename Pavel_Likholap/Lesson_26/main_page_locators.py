from selenium.webdriver.common.by import By


class MainPageLocators:
    MAIN_PAGE_WELCOME = (By.CSS_SELECTOR, "section:nth-child(1) > div")
    MAIN_PAGE_RECOMMEND = (By.CSS_SELECTOR, "article>div.sub-header")
    MAIN_PAGE_NOT_EXIST = (By.CSS_SELECTOR, ".breadcrumb")
    MAIN_PAGE_LINK = (By.XPATH,
                      "//a[@href='http://latest.oscarcommerce.com/gateway/']")
    MAIN_PAGE_BASKET = (By.CSS_SELECTOR, 'span.btn-group > a.btn.btn-default')
    MAIN_PAGE_BOOKS = (By.XPATH,
                       "//a[@href='/en-gb/catalogue/category/books_2/']")
