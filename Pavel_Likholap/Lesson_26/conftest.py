import pytest
from selenium.webdriver import Chrome


@pytest.fixture(scope="session")
def drv():
    """
    :return: Chrome web-driver
    """
    drv = Chrome(executable_path="D:/chromedriver.exe")
    yield drv
    drv.quit()
