from fiction_page_locators import FictionPageLocators
from base_page import BasePage


class FictionPage(BasePage):
    URL = """http://selenium1py.pythonanywhere.com
    /en-gb/catalogue/category/books/fiction_3/"""

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_fiction_page_title(self):
        """
        :return: Fiction page title
        """
        return self.driver.title

    def get_fiction_breadcrumb(self):
        """
        :return: Web element - fiction page
        breadcrumbs
        """
        return self.find_element(FictionPageLocators.FICTION_BREAD)

    def get_header(self):
        """
        :return: Web element - one of
        fiction page headers
        """
        return self.find_element(FictionPageLocators.FICTION_HEADER)
