from selenium.common.exceptions import NoSuchElementException
from books_page import BooksPage
from books_page_locators import BooksPageLocators
from fiction_page import FictionPage
from main_page_locators import MainPageLocators
from base_page import BasePage
from basket_page import BasketPage
from basket_page_locators import BasketPageLocators


class MainPage(BasePage):
    URL = "http://selenium1py.pythonanywhere.com/en-gb/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_main_page_title(self):
        """
        :return: Main page title
        """
        return self.driver.title

    def get_welcome(self):
        """
        :return: Web element
        """
        welcome = self.find_element(MainPageLocators.MAIN_PAGE_WELCOME)
        return welcome

    def get_recommended(self):
        """
        :return: Web element - recommended
        """
        rec = self.find_element(MainPageLocators.MAIN_PAGE_RECOMMEND)
        return rec

    def get_breadcrumb_error(self):
        """
        :return: Main page breadcrumbs
        """
        try:
            self.find_element(MainPageLocators.MAIN_PAGE_WELCOME)
        except NoSuchElementException:
            return True

    def get_link_text(self):
        """
        :return: Web element
        """
        text = self.find_element(MainPageLocators.MAIN_PAGE_LINK)
        return text

    def open_basket_page(self):
        """
        :return: Web-element from BasketPage object
        """
        basket = self.find_element(MainPageLocators.MAIN_PAGE_BASKET)
        basket.click()
        bask_e = BasketPage(self.driver)
        return bask_e.find_element(BasketPageLocators.BASKET_EMPTY)

    def open_fiction_page(self):
        """
        :return: Fiction page object
        """
        books = self.find_element(MainPageLocators.MAIN_PAGE_BOOKS)
        books.click()
        books_page = BooksPage(self.driver)
        fiction_page = books_page.find_element(BooksPageLocators.BOOKS_FICTION)
        fiction_page.click()
        return FictionPage(self.driver)
