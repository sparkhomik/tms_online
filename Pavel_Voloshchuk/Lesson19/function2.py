"""
переводит переданное число в тип float, если передана строка вызывает
пользовательскую ошибку InputTypeError
"""


def float_number(number):
    """
    :param number: число
    :return: число, преобразованное в float
    """
    if type(number) != int:
        raise InputTypeError()
    return float(number)


class InputTypeError(Exception):
    """
    Класс для определения ошибки
    """

    def __init__(self):
        self.message = 'Type of number isn\'t an int'
        super().__init__(self.message)


if __name__ == "__main__":
    print(float_number(1234))
