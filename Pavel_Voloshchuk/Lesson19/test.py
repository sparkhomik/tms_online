import pytest
from function1 import lowercase_word
from function2 import float_number, InputTypeError
from function3 import check_special_characters


@pytest.mark.parametrize('word, result', [('test', True), ('Test', False),
                                          ('TEst', False)])
def test_lowercase_word(word, result):
    """
    Проверка функции lowercase_word.
    :param word: слово для проверки
    :param result: в нижнем регистре все буквы в слове или нет
    """
    assert lowercase_word(word) == result


@pytest.mark.parametrize('number, result', [(11, 11.0), (22, 22.0),
                                            (-33, -33.0)])
def test_float_number(number, result):
    """
    Проверка функции float_number.
    :param number: переданное число
    :param result: число, преобразованное в тип float
    """
    assert float_number(number) == result


@pytest.mark.parametrize('number', [('one', '1', '1.0')])
def test_fail_float_number(number):
    """
    Проверка ошибки при передаче строки
    :param number: переданное число
    """
    with pytest.raises(InputTypeError):
        float_number(number)


@pytest.mark.parametrize("character, result", [('#', True), ('Null', False),
                                               ('@', True)])
def test_check_special_characters(character, result):
    """
    Проверка функции check_special_characters.
    :param character: переданный аргумент
    :param result: спецсимвол или нет
    """
    assert check_special_characters(character) == result
