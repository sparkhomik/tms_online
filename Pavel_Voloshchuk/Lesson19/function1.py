"""
Проверяет что все буквы в переданном слове в нижнем регистре
"""


def lowercase_word(word):
    """
    :param word:слово для проверки
    :return: в нижнем регистре все буквы в слове или нет
    """
    return word.islower()


if __name__ == "__main__":
    print(lowercase_word("heloworld"))
