# Функция проверяющая тип данных
"""
Написать функцию которая принимает на фход список из чисел, строк и таплов

Функция должна вернуть сколько в списке элементов приведенных данных

print(my_function([1, 2, “a”, (1, 2), “b”])

🡪 {“int”: 2, “str”: 2, “tuple”: 1}

"""

# список для вывода
test_list = [1, 2, 'a', (1, 2), 'b']


def func(test_list: list) -> dict:
    """
    Функция принимает список элементов и возвращает кол-во элементов
    :param test_list: входной список
    :return: словарь с кол-вом элементов
    """
    test_list_dict = {}
    test_list_type = []
    for a in test_list:
        test_list_type.append(type(a))
    for b in test_list_type:
        test_list_dict[b] = test_list_dict.get(b, 0) + 1
    return test_list_dict


print(func(test_list))
