# Validate
"""
Ваша задача написать программу, принимающее число - номер кредитной
карты(число может быть четным или не четным). И проверяющей может ли такая
карта существовать. Предусмотреть защиту от ввода букв, пустой строки и т.д.
"""


def luhn(card_num: str) -> int:
    """
    Алгоритм Луна
    :param card_num: номер карты
    :return: прохождение Луна
    """
    sum = 0
    num_card = list(map(int, card_num))
    for count, i in enumerate(num_card):
        if count % 2 == 0:
            x = i * 2
            if x > 9:
                x -= 9
            sum += x
        else:
            sum += i
    return sum % 10 == 0


user_num = input('введите номер карты: ')
# проверка валидного ввода:
if user_num.isdigit() and len(user_num) == 16:

    print(luhn(user_num))
else:
    print('некорректный номер карты')
