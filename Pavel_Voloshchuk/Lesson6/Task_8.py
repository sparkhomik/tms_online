# написать функцию, которая проверяет есть ли в списке объект, которые можно
# вызвать


def func(my_list: list) -> list:
    """
    функция проверяет есть ли в списке объект, которые можно вызвать
    :param my_list: проверяемый список
    :return: есть ли в списке объект, который можно вызвать
    """
    result = []
    for a in my_list:
        if callable(a):
            result.append(a)
    return result


my_list = [1, [1, 2], str, 'asdf']
print(func(my_list))
