# Работа с областями видимости
"""
На уровне модуля создать список из 3-х элементов Написать функцию,
которая принимает на вход этот список и добавляет в него элементы. Функция
должна вернуть измененный список. При этом исходный список не должен
измениться. Пример c функцией которая добавляет в список символ “a”:

My_list = [1, 2, 3]
Changed_list = change_list(My_list)
Print(My_list)
🡪 [1, 2, 3]
Print(Changed_list)
🡪 [1, 2, 3, “a”]
"""

# Список на уровне модуля
my_list = [1, 2, 3]


def change_list(my_list: list) -> list:
    """
    функция изменяющая список
    :param my_list: список на уровне модуля
    :return: измененный список
    """
    my_list = my_list.copy()
    my_list.append('a')
    return my_list


print(my_list)
print(change_list(my_list))
