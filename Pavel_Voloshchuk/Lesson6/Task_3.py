# Простейший калькулятор v0.1
"""
Реализуйте программу, которая спрашивала у пользователя, какую операцию
он хочет произвести над числами, а затем запрашивает два числа и выводит
результат Проверка деления на 0.
Пример
Выберите операцию:
    1. Сложение
    2. Вычитание
    3. Умножение
    4. Деление
Введите номер пункта меню:
>>> 4
Введите первое число:
>>> 10
Введите второе число:
>>> 3
Частное: 3, Остаток: 3

"""


def calculator(operation, num1, num2) -> str:
    """

    :param operation: проводимая операция
    :param num1: первое число
    :param num2: второе число
    :return: результат
    """
    num1 = float(num1)
    num2 = float(num2)
    result = 0
    if operation == '1':
        result = str(f'Сумма: {num1 + num2}')
    elif operation == '2':
        result = str(f'Разность: {num1 - num2}')
    elif operation == '3':
        result = str(f'Произведение: {num1 * num2}')
    elif operation == '4':
        if num2 == 0:
            result = 'Деление на 0'
        else:
            result = str(f'Частное: {num1 // num2}, Остаток: {num1 % num2}')
    return result


# Получение данных
operation = input(
    "Введите номер операции: 1. Сложение 2. Вычитание 3. Умножение 4. "
    "Деление")
num1 = (input('Введите 1-ое число'))
num2 = (input('Введите 2-ое число'))
# Условия вывода
if operation.isdigit() and 1 <= int(operation) <= 4:
    print(calculator(operation, num1, num2))
else:
    print('Неверная операция')
