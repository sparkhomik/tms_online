# Подсчет количества букв
"""
На вход подается строка, например, "cccbba" результат работы программы -
строка “c3b2a"
Примеры для проверки работоспособности:
"cccbba" == "c3b2a"
"abeehhhhhccced" == "abe2h5c3ed"
"aaabbceedd" == "a3b2ce2d2"
"abcde" == "abcde"
"aaabbdefffff" == "a3b2def5"
"""

user_string = input('введите строку: ')


def count(letters: str) -> str:
    """
    подсчет кол-ва букв
    :param letters: набор букв
    :return: буквы и их кол-во
    """
    letters = list(letters)
    letters_num = []
    for a in letters:
        if a not in letters_num:
            letters_num.append(a)
            if letters.count(a) > 1:
                letters_num.append(str(letters.count(a)))
    return ''.join(letters_num)


print(count(user_string))
