# Написать функцию с изменяемым числом входных параметров
"""При объявлении функции предусмотреть один позиционный и один именованный
аргумент, который по умолчанию равен None (в примере это аргумент с именем
name). Также предусмотреть возможность передачи нескольких именованных и
позиционных аргументов

Функция должна возвращать следующее
result = function(1, 2, 3, name=’test’, surname=’test2’, some=’something’)
Print(result)
🡪 {“mandatory_position_argument”: 1, “additional_position_arguments”: (2,
3), “mandatory_named_argument”: {“name”: “test2”},
“additional_named_arguments”:       {“surname”: “test2”, “some”: “something”}}
"""


def function(a, *b, name, **c) -> dict:
    """
    функция с изменяемым числом параметров
    :param a: позиционный аргумент
    :param b: переменное кол-во позиционных аргументов
    :param name: обязательный именованный
    :param c: переменное кол-во именованных
    :return: словарь, ключ - описание, значение - переменная
    """
    name = {'name': name}
    result = {'mandatory_position_argument': a,
              'additional_position_arguments': b,
              'mandatory_named_argument': name,
              'additional_named_arguments': c}
    return result


result = function(1, 2, 3, name='test2', surname='test2', some='something')
print(result)
