from abc import ABC, abstractmethod

"""
Ферма
На ферме могут жить животные и птицы И у тех и у других есть два
атрибута: имя и возраст При этом нужно сделать так, чтобы возраст нельзя
было изменить извне напрямую, но при этом можно было бы получить его
значение У каждого животного должен быть обязательный метод – ходить А для
птиц – летать Эти методы должны выводить сообщения (контекст придумайте)
Также должны быть общие для всех классов обязательные методы – постареть -
который увеличивает возраст на 1 и метод голос При обращении к
несуществующему методу или атрибуту животного или птицы должен срабатывать
пользовательский exception NotExistException и выводить сообщение о том,
что для текущего класса (имя класса), такой метод или атрибут не существует
– (имя метода или атрибута) На основании этих классов создать отдельные
классы для Свиньи, Гуся, Курицы и Коровы – для каждого класса метод голос
должен выводить разное сообщение (хрю, гага, кудах, му) После этого создать
класс Ферма, в который можно передавать список животных Должна быть
возможность получить каждое животное, живущее на ферме как по индексу,
так и через цикл for. Также у фермы должен быть метод, который увеличивает
возраст всех животных, живущих на ней.
"""


class Basic(ABC):
    """
    Абстрактный базовый класс с общими атрибутами и методами животных
    """

    def __init__(self, name: str, age: int):
        """
        Экземпляры бфзового класса
        :param name: имя
        :param age: возраст
        """
        self.name = name
        self._age = age

    def add_age(self) -> None:
        """
        Метод увеличения возраста
        :return: возраст увеличивается на 1
        """
        self._age += 1

    def __getattr__(self, atr: None):
        """
        проверка наличия атрибута класса
        :param atr: входящий атрибут
        :return: ошибка с атрибутом
        """
        raise NotExistException(atr)

    @abstractmethod
    def voice(self):
        """
        Абстрактный метод голоса для переопределения
        :return: ошибка
        """
        raise NotImplementedError


class Birds(Basic, ABC):
    """
    Класс для птиц с методом полет
    """

    @staticmethod
    def fly() -> str:
        """
        статик метод полета
        :return: возвращает строку для птиц
        """
        return 'я умею летать'


class Animals(Basic, ABC):
    """
    Класс для животных с методом ходьбы
    """

    @staticmethod
    def walk() -> str:
        """
        статик метод ходьбы
        :return: возвращает строку для животных
        """
        return 'я умею ходить'


class Pig(Animals):
    """
    Класс свинья
    """

    def voice(self) -> str:
        """
        переопределяем голос для свиньи
        :return: возвращает голос свиньи
        """
        return 'хрю-хрю'

    def __str__(self):
        """
        Вывод информации о свинье
        :return: информация о свинье
        """
        return f"Свинья {self.name} говорит {self.voice()} и {self.walk()}," \
               f" возраст {self._age} ."


class Goose(Birds):
    """
    Класс гусь
    """

    def voice(self) -> str:
        """
        переопределяем голос для гуся
        :return: возвращает голос гуся
        """
        return 'га-га-га'

    def __str__(self):
        """
        Вывод информации о гусе
        :return: информация о гусе
        """
        return f"Гусь {self.name} говорит {self.voice()} и {self.fly()}," \
               f" возраст {self._age} ."


class Chicken(Birds):
    """
    Класс курица
    """

    def voice(self) -> str:
        """
        переопределяем голос для курицы
        :return: возвращает голос курицы
        """
        return 'ко-ко-ко'

    def __str__(self):
        """
        Вывод информации о курице
        :return: информация о курице
        """
        return f"Курица {self.name} говорит {self.voice()} и {self.fly()}," \
               f" возраст {self._age} ."


class Cow(Animals):
    """
    Класс корова
    """

    def voice(self) -> str:
        """
        переопределяем голос для коровы
        :return: возвращает голос коровы
        """
        return 'му-му'

    def __str__(self):
        """
        Вывод информации о корове
        :return: информация о корове
        """
        return f"Корова {self.name} говорит {self.voice()} и {self.walk()}," \
               f" возраст {self._age} ."


class Farm:
    """
    Класс ферма для списка животных
    """

    def __init__(self, list_animals):
        """
        Атрибут класса со списком животных
        :param list_animals: список животных
        """
        self.list_animals = list_animals

    def __getitem__(self, item):
        """
        Метод для получения индексов животных
        :param item: индекс
        :return: индекс животного из списка
        """
        return self.list_animals[item]

    def animals(self) -> None:
        """
        Метод для получения списка животных
        :return: список животных
        """
        for animal in self.list_animals:
            print(animal)

    def add_age(self) -> list:
        """
        Увеличиваем возраст животных
        :return: список животных с увеличенным возрастом
        """
        return [animal.add_age() for animal in self.list_animals]


class NotExistException(Exception):
    """
    Обработка ошибки
    """

    def __init__(self, func):
        """
        Вывод сообщения об ошибке при отсутствии атрибута
        """
        self.message = f"Function {func} doesn't exist"
        super(NotExistException, self).__init__(self.message)


def main():
    """
    Запуск
    """
    # Создаем экземпляры классов
    pig = Pig('Пепа', 22)
    goose = Goose('Мартин', 48)
    chicken = Chicken('Ряба', 11)
    cow = Cow('Элоиза', 44)
    animals = [pig, goose, chicken, cow]
    farm = Farm(animals)

    # Выводим всех
    farm.animals()

    # Выводим по индексу
    print(f'Животное под индексом [1]: {farm[1]}')
    print(f'Животное под индексом [0]: {farm[0]}')
    print(f'Животное под индексом [3]: {farm[3]}')
    print(f'Животное под индексом [2]: {farm[2]}')

    # Добавляем год
    farm.add_age()

    # Вывод постаревших животных
    farm.animals()

    # Тест ошибки
    pig.swim()


if __name__ == "__main__":
    main()
