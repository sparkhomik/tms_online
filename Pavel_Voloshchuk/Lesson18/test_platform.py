"""
Написать тесты для каждого метода
Должны быть написаны не только позитивные тесты, но и негативные,
тем самым показывая корректность работы программы.

"""

import unittest

from Pavel_Voloshchuk.Lesson18.platform import MySystem


class TestPlatform(unittest.TestCase):
    """
    Тестовый класс.
    """

    def test_get_name(self):
        """
        Тест проверки имени ос.
        """
        self.assertEqual(MySystem.get_name(), 'Windows')

    def test_get_release(self):
        """
        Тест проверки релиза ос.
        """
        self.assertEqual(MySystem.get_release(), '10')

    def test_get_version(self):
        """
        Тест проверки версии ос.
        """
        self.assertEqual(MySystem.get_version(), '10.0.19042')

    @unittest.expectedFailure
    def test_get_name_fail(self):
        """
        Негативный тест проверки имени ос.
        """
        self.assertNotEqual(MySystem.get_name(), 'Darwin')

    @unittest.expectedFailure
    def test_get_release_fail(self):
        """
        Негативный тест проверки релиза ос.
        """
        self.assertEqual(MySystem.get_release(), '666')

    @unittest.expectedFailure
    def test_get_version_fail(self):
        """
        Негативный тест проверки версии ос.
        """
        self.assertNotEqual(MySystem.get_version(), '777')
