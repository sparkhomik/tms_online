"""
Написать класс для работы с текущей операционной системой:
Должно быть три метода:
- получить имя операционной системы
- релиз операционной системы
- версию OS
"""

import platform


class MySystem:
    """
    Класс для работы с ОС.
    """

    @staticmethod
    def get_name():
        """
        Получение имени ос.
        :return: Returns the system/OS name, e.g. 'Linux', 'Windows' or 'Java'.
        """
        return platform.system()

    @staticmethod
    def get_release():
        """
        Получение релиза ос.
        :return: Returns the system's release, e.g. '2.2.0' or 'NT'
        """
        return platform.release()

    @staticmethod
    def get_version():
        """
        Получение версии ос.
        :return: Returns the system's release version, e.g. '#3 on degas'
        """
        return platform.version()


if __name__ == '__main__':
    system = MySystem()
    print(system.get_name())
    print(system.get_release())
    print(system.get_version())
