import random

# Случайное 4х-значное число, загадываемое компом:

comp_number = (random.sample(range(0, 10), 4))

# Ввод числа игроком, ожидаем что игрок введет только 4 цифры, без проверок:

player_number = [int(a) for a in input('ввидите 4 цифры:')]

# Проверяем:

bulls = 0
cows = 0

for a in range(4):
    if player_number[a] == comp_number[a]:
        bulls += 1
    else:
        if player_number[a] in comp_number:
            cows += 1
            continue
print('быков: ' + str(bulls) + ' коров: ' + str(cows))
if bulls == 4:
    print('победа')
