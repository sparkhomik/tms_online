# Ввод имён:

name = input('Введите имена через запятую: ')
# делим строку:
list_names = name.split()
# перечисляем условия:
if len(list_names) == 1:
    print(f'{list_names} likes this')
elif len(list_names) == 2:
    print(f'{list_names[0]} and {list_names[1]} like this')
elif len(list_names) == 3:
    print(f'{list_names[0]},{list_names[1]}and{list_names[2]} like this')
elif len(list_names) >= 4:
    print(f'{list_names[0]} {list_names[1]} and {len(list_names) - 2} others '
          f'like this')
else:
    print('No one likes this')
