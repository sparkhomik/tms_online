# начиная с а = 1 применяем проверку на оба условия и после на каждое отдельно.
# после прибавляем 1
a = 1
while a < 101:
    if a % 3 == 0 and a % 5 == 0:
        print('FuzzBuzz')
    elif a % 3 == 0:
        print('Fuzz')
    elif a % 5 == 0:
        print('Buzz')
    else:
        print(a)
    a += 1
