# сравниваем с первым элементом все элементы списка
# all возвращает True, если все элементы истинные или объект пустой:
a = [1, 1, 1]
b = [1, 2, 1]
c = ['a', 'a', 'a']
d = []

print(all(i == a[0] for i in a))
print(all(i == b[0] for i in b))
print(all(i == c[0] for i in c))
print(all(i == c[0] for i in d))
