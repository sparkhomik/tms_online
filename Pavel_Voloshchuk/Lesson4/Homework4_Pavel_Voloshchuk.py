# Task 1
"""
Перевести строку в массив
"Robin Singh" => ["Robin”, “Singh"]
"I love arrays they are my favorite" =>
["I", "love", "arrays", "they", "are", "my", "favorite"]

"""

string1 = 'Robin Singh'
string2 = 'I love arrays they are my favorite'

string1 = string1.split()
string2 = string2.split()

print(string1)
print(string2)

# Task 2
"""
Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
"""

fio = ['Ivan', 'Ivanou']
city = 'Minsk'
country = 'Belarus'
fio = ' '.join(fio)

print(f'Привет, {fio}! Добро пожаловать в {city} {country}')

# Task 3
"""
Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
сделайте из него строку => "I love arrays they are my favorite"
"""

text1 = ['I', 'love', 'arrays', 'they', 'are', 'my', 'favorite']
text1 = ' '.join(text1)

print(text1)

# Task 4
"""
Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
удалите элемент из списка под индексом 6
"""

list1 = [1, 2, 3, 4, 5, 6, 7, 8, 9, 10]
list1[2] = 'change'
del list1[6]

print(list1)

# Test 5
"""
Есть 2 словаря
                 a = { 'a': 1, 'b': 2, 'c': 3}
                 b = { 'c': 3, 'd': 4,'e': “”}

1. Создайте словарь, который будет содержать в себе все элементы обоих словарей
2. Обновите словарь “a” элементами из словаря “b”
3. Проверить что все значения в словаре “a” не пустые либо не равны нулю
4. Проверить что есть хотя бы одно пустое значение (результат выполнения должен
быть True)
5. Отсортировать словарь по алфавиту в обратном порядке
6. Изменить значение под одним из ключей и вывести все значения

"""

# Task 5_1

a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ""}

c = {**a, **b}
print(c)

# Task 5_2

a.update(b)
print(a)

# Task 5_3
a1 = a.values()
print(a1)

# Task 5_4

print(not all(a1))

# Task 5_5

a = sorted(a.items(), reverse=True)
print(a)

# Task 5_6

c['e'] = 'new'
print(c)

# Task 6

"""
Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
a. Вывести только уникальные значения и сохранить их в отдельную переменную
b. Добавить в полученный объект значение 22
c. Сделать list_a неизменяемым
d. Измерить его длинну

"""

list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]

# Task 6_a

list_a1 = set(list_a)
list_a1 = list(list_a1)
print(list_a1)

# Task 6_b

list_a1.append(22)
print(list_a1)

# Task 6_c

list_a = tuple(list_a)
print(list_a)

# Task_d

print(len(list_a))

# Задачи на закрепление форматирования:
# Task 1
"""
Есть переменные a=10, b=25
Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
При решении задачи использовать оба способа форматирования
"""

a = 10
b = 25

print('Summ is {c} and diff = {d}.'.format(c=a + b, d=a - b))
print(f'Summ is {a + b} and diff = {a - b}.')

# Task 2
"""
Есть список list_of_children = [“Sasha”, “Vasia”, “Nikalai”]
Вывести “First child is <первое имя из списка>, second is “<второе>”,
and last one – “<третье>””
"""

list_of_children = ["Sasha", "Vasia", "Nikalai"]
print(f'First child is {list_of_children[0]}, second is {list_of_children[1]},'
      f' and last one – {list_of_children[2]}')

# Task *1

"""
Вам передан массив чисел. Известно, что каждое число в этом массиве имеет пару,
кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
Напишите программу, которая будет выводить уникальное число
"""

mass = [1, 5, 2, 9, 2, 9, 1]
