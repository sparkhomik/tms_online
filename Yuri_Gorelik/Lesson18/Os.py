"""
Написать класс для работы с текущей операционной системой:
Должно быть три метода:
- получить имя операционной системы
- релиз операционной системы
- версию OS
"""
import platform


class Os:
    """
    Класс Os
    """

    @staticmethod
    def name_os():
        """
        Получение имени операционной системы
        """
        return platform.system()

    @staticmethod
    def name_release():
        """
        Получение релиза операционной системы
        """
        return platform.release()

    @staticmethod
    def name_version():
        """
        Получение версии операционной системы
        """
        return platform.version()


if __name__ == '__main__':
    os = Os()
    print(os.name_os())
    print(os.name_release())
    print(os.name_version())
