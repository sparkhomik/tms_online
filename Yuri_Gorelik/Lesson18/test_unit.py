import unittest
from Os import Os


class TestOs(unittest.TestCase):
    """
    Класс для позитивных тестов
    """

    def test_name_os(self):
        self.assertEqual(Os.name_os(), 'Darwin')

    def test_name_release(self):
        self.assertEqual(Os.name_release(), '20.6.0')

    def test_name_version(self):
        self.assertEqual(Os.name_version(),
                         'Darwin Kernel Version 20.6.0: Mon Aug 30 06:12:21 '
                         'PDT 2021; root:xnu-7195.141.6~3/RELEASE_X86_64')


class TestFailedOs(unittest.TestCase):
    """
    Класс для негативных тестов
    """

    @unittest.expectedFailure
    def test_failed_name_os(self):
        self.assertNotEqual(Os.name_os(), 'Monterey')

    @unittest.expectedFailure
    def test_failed_name_release(self):
        self.assertNotEqual(Os.name_release(), '21.0.0')

    @unittest.expectedFailure
    def test_failed_name_version(self):
        self.assertNotEqual(Os.name_version(), '12.0')
