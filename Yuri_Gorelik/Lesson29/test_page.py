from page import Page


def test_page_title(driver):
    """
    Тест на проверку title
    """
    page = Page(driver)
    page.open()
    assert page.get_title() == "ShiningPanda | Jenkins plugin", \
        "incorrect title"


def test_page_name(driver):
    """
    Тест на проверку имени на странице
    """
    page = Page(driver)
    page.open()
    assert page.get_name().text == "ShiningPanda", \
        "incorrect page name"
