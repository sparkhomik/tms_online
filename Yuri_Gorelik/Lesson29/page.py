from base_page import BasePage
from locators import Locators


class Page(BasePage):
    """
    Класс страницы сайта
    """
    URL = "https://plugins.jenkins.io/shiningpanda"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title(self):
        """
        Получений тайтла окна
        :return: тайтл
        """
        return self.driver.title

    def get_name(self):
        """
        Получение имени га странице
        :return: имя
        """
        return self.find_element(Locators.LOCATOR_PAGE_TITLE)
