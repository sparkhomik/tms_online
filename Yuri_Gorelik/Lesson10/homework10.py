from operator import itemgetter

"""
Цветочница.
Определить иерархию и создать несколько цветов (Розы, Тюльпаны, Фиалки,
Ромашки). У каждого класса цветка следующие атрибуты:
Стоимость – атрибут класса, который определяется заранее
Свежесть (в днях), цвет, длинна стебля – атрибуты экземпляра
Собрать букет (можно использовать аксессуары – отдельные классы со своей
стоимостью: например, упаковочная бумага) с определением ее стоимости.
У букета должна быть возможность определить время его увядания по среднему
времени жизни всех цветов в букете
Позволить сортировку цветов в букете на основе различных параметров
(свежесть/цвет/длина стебля/стоимость...)
Реализовать поиск цветов в букете по определенным параметрам.
Узнать, есть ли цветок в букете.
Добавить возможность получения цветка по индексу либо возможность пройтись
по букету и получить каждый цветок по-отдельности
"""


class Flowers:

    def __init__(self, freshness: str, color: str, stem_length: str) -> None:
        """
        Создание цветка.
        :param freshness: свежесть
        :param color: цвет
        :param stem_length: длина стебля
        """
        self.freshness = freshness
        self.color = color
        self.stem_length = stem_length

    def __str__(self) -> str:
        """
        Вывод цвета, наименования, стоимости и длины стебля цветка.
        """
        return f'{self.color} {self.name} - price: {self.price}BYN ' \
               f'- stem length: {self.stem_length}'

    def __int__(self) -> int:
        """
        Преобразование типа свежести цветка в integer.
        """
        return int(self.freshness)

    def __float__(self) -> float:
        """
        Преобразование типа длины стебля во float.
        """
        return float(self.stem_length)

    def __getitem__(self, atr) -> str:
        """
        Доступ атрибутов цветка для поиска.
        :param atr: атрибут цветка
        """
        return getattr(self, atr)


class Bouquet:

    def __init__(self, *flowers: object) -> None:
        """
        Создание букета.
        :param flowers: цветы для букета и упаковочная бумага
        """
        self.index = 0
        self.bouquet = list(flowers)
        self.flowers = list(flowers)[1::]
        self.accessories = list(flowers)[0]

    def __contains__(self, obj: object) -> object:
        """
        Проверка наличия цветка в букете.
        :param obj: цветок, который ищем
        :return: есть цветок в букете или нет
        """
        return obj in self.flowers

    def sorted_flowers(self, atr: str) -> list:
        """
        Сортировка цветов в букете.
        :param atr: параметр, по которому осуществляется сортировка
        :return: отсортированный список цветов в букете
        """
        if atr == 'stem_length':
            bouquet = {flower: float(flower) for flower in self.flowers}
            sorted_values = sorted(bouquet.values())
            sorted_bouquet = {}
            for i in sorted_values:
                for k in bouquet.keys():
                    if bouquet[k] == i:
                        sorted_bouquet[k] = bouquet[k]
                        break
            sorted_flowers = list(sorted_bouquet.keys())
        else:
            sorted_flowers = sorted(self.flowers, key=itemgetter(atr))
        return sorted_flowers

    def bouquet_price(self) -> int:
        """
        Расчет стоимости букета.
        :return: стоимость букета
        """
        bouquet_price = 0
        for i in self.bouquet:
            bouquet_price += i.price
        return bouquet_price

    @staticmethod
    def freshness_time(bouquet: list) -> int:
        """
        Расчет времени увядания букета.
        :param bouquet: букет цветов
        :return: время увядания букета
        """
        fresh, kol = 0, 0
        for flower in bouquet:
            fresh += int(flower)
            kol += 1
        fresh /= kol
        return fresh

    @staticmethod
    def flower_search(bouquet: list, color: str) -> bool:
        """
        Поиск цветка в букете по цвету.
        :param bouquet: букет цветов
        :param color: цвет, по которому осуществляется поиск
        :return: есть цветок такого цвета в букете или нет
        """
        search = 0
        for flower in bouquet:
            if flower.color == color:
                search += 1
        return True if search > 0 else False


class Roses(Flowers):
    price = 15
    name = 'Rose'


class Tulips(Flowers):
    price = 7
    name = 'Tulip'


class Violets(Flowers):
    price = 8
    name = 'Violet'


class Chamomile(Flowers):
    price = 4
    name = 'Chamomile'


class Lily(Flowers):
    price = 6
    name = "Lily"


class CraftPaper:
    name = 'Craft paper'
    price = 2


def main() -> None:
    """
    Функция запуска программы
    Создание букета и получение дополнительной информации о нем.
    """
    flower1 = Roses('1', 'red', '19.5')
    flower2 = Roses('2', 'yellow', '19.1')
    flower3 = Tulips('3', 'white', '12')
    flower4 = Chamomile('1', 'white', '10')
    flower5 = Violets('2', 'purple', '8')
    craft = CraftPaper()
    my_bouquet = Bouquet(craft, flower1, flower2, flower3, flower4, flower5)
    my_bouquet_flowers = my_bouquet.flowers
    print(*my_bouquet_flowers, sep=',\n')
    withering_time = Bouquet.freshness_time(my_bouquet_flowers)
    print(f'Withering time: {withering_time} '
          f'days \n')
    print(f'Bouquet total price: {Bouquet.bouquet_price(my_bouquet)}BYN')
    sorted_bouquet_name = my_bouquet.sorted_flowers(atr='name')
    print('\nSorted flowers from bouquet by name: ')
    print(*sorted_bouquet_name, sep=',\n')
    sorted_bouquet_stem_length = my_bouquet.sorted_flowers(atr='stem_length')
    print('\nSorted flowers from bouquet by stem length: ')
    print(*sorted_bouquet_stem_length, sep=',\n')
    sorted_bouquet_color = my_bouquet.sorted_flowers(atr='color')
    print('\nSorted flowers from bouquet by color: ')
    print(*sorted_bouquet_color, sep=',\n')
    print(f'\nIs white flowers in my bouquet? - '
          f'{Bouquet.flower_search(my_bouquet_flowers, "white")}')
    print(f'\nIs flower2 in my bouquet? - {flower2 in my_bouquet}')
    print(f'\nFlower with index 2: {my_bouquet_flowers[2]}:')


if __name__ == "__main__":
    main()
