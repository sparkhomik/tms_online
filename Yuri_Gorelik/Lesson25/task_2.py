def test_text_frame(get_iframe_text):
    """
    Тестовая функция проверки текста
    Сравнивает актуальный текст с ожидаемым
    """
    assert get_iframe_text == "Your content goes here."
