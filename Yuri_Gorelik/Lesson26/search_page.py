from base_page import BasePage
from search_page_locators import SearchPageLocators


class SearchPage(BasePage):
    """
    Класс страницы с результатом пойска
    """

    def get_title(self):
        """
        Получение заголовка окна
        """
        return self.driver.title

    def get_header_name(self):
        """
        Получение header страницы корзины
        """
        return self.find_element(SearchPageLocators.SEARCH_HEADER_LOCATORS)

    def get_path_name(self):
        """
        Получение пути страницы корзины
        """
        return self.find_element(SearchPageLocators.SEARCH_PATH_LOCATORS)

    def get_image(self):
        """
        Получение обложки найденной книги
        """
        return self.find_element(SearchPageLocators.SEARCH_IMAGE_LOCATORS)
