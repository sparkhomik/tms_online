from base_page import BasePage
from fiction_page import FictionPage
from books_page_locators import BooksPageLocators


class BooksPage(BasePage):
    """
    Класс страницы с книгами
    """

    def open_fiction_page(self):
        """
        Открытие страницы fiction
        """
        fiction_link = self.find_element(
            BooksPageLocators.BOOKS_FICTION_LOCATORS)
        fiction_link.click()
        return FictionPage(self.driver, self.driver.current_url)

    def get_title(self):
        """
        Получение заголовка окна
        """
        return self.driver.title

    def get_header_name(self):
        """
        Получение header страницы с книгами
        """
        return self.find_element(BooksPageLocators.BOOKS_HEADER_LOCATORS)

    def get_path_name(self):
        """
        Получение пути страницы с книгами
        """
        return self.find_element(BooksPageLocators.BOOKS_PATH_LOCATORS)
