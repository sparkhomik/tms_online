from main_page import MainPage


def test_basket_page_title(driver):
    """
    Проверка, что заголовок страницы "Basket | Oscar - Sandbox"
    """
    main_page = MainPage(driver)
    main_page.open()
    basket_page = main_page.open_basket_page()
    assert basket_page.get_title() == "Basket | Oscar - Sandbox", \
        "Incorrect basket page title!"


def test_basket_page_header_name(driver):
    """
    Проверка, что header равен "Basket"
    """
    main_page = MainPage(driver)
    main_page.open()
    basket_page = main_page.open_basket_page()
    assert basket_page.get_header_name().text == "Basket", \
        "Incorrect basket page header!"


def test_basket_page_path_name(driver):
    """
    Проверка наличия "Basket" в path
    """
    main_page = MainPage(driver)
    main_page.open()
    basket_page = main_page.open_basket_page()
    assert basket_page.get_path_name().text == "Basket", \
        "Incorrect basket page path!"


def test_basket_is_empty(driver):
    """
    Проверка, что корзина пуста
    """
    main_page = MainPage(driver)
    main_page.open()
    basket_page = main_page.open_basket_page()
    text = "\n            Your basket is empty.\n " \
           "           Continue shopping\n        "
    assert basket_page.get_empty_basket().text == text,\
        "Your basket isn't empty!"
