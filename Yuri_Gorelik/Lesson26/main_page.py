from base_page import BasePage
from basket_page import BasketPage
from books_page import BooksPage
from search_page import SearchPage
from main_page_locators import MainPageLocators


class MainPage(BasePage):
    """
    Класс главной страницы сайта
    """
    URL = "http://selenium1py.pythonanywhere.com/en-gb/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title(self):
        """
        Получение заголовка окна
        """
        return self.driver.title

    def get_oscar_reference(self):
        reference = self.find_element(MainPageLocators.OSCAR_REF_LOCATORS)
        return reference

    def get_text(self):
        text = self.find_element(MainPageLocators.TEXT_LOCATORS)
        return text

    def open_books_page(self):
        """
        Открытие страницы книг
        """
        books_link = self.find_element(MainPageLocators.BOOKS_PAGE_LOCATORS)
        books_link.click()
        return BooksPage(self.driver, self.driver.current_url)

    def open_basket_page(self):
        """
        Открытие страницы корзины
        """
        basket_link = self.find_element(MainPageLocators.BASKET_PAGE_LOCATORS)
        basket_link.click()
        return BasketPage(self.driver, self.driver.current_url)

    def open_search_page(self, book_name):
        """
        Открытие страницы с результатами пойска по названию книги
        """
        search_field = self.find_element(
            MainPageLocators.SEARCH_INPUT_LOCATORS)
        search_field.send_keys(book_name)
        search_button = self.find_element(
            MainPageLocators.SEARCH_BUTTON_LOCATORS)
        search_button.click()
        return SearchPage(self.driver, self.driver.current_url)
