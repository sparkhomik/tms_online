from selenium.webdriver.common.by import By


class MainPageLocators:
    OSCAR_REF_LOCATORS = (By.CSS_SELECTOR, 'a[href="/en-gb/"]')
    TEXT_LOCATORS = (By.XPATH, '//div[@class="sub-header"]')
    BOOKS_PAGE_LOCATORS = (By.XPATH,
                           '//a[@href="/en-gb/catalogue/category/books_2/"]')
    BASKET_PAGE_LOCATORS = (By.XPATH, '//a[text()="View basket"]')
    SEARCH_INPUT_LOCATORS = (By.XPATH, '//input[@type="search"]')
    SEARCH_BUTTON_LOCATORS = (By.XPATH, '//input[@type="submit"]')
