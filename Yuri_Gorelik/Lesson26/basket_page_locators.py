from selenium.webdriver.common.by import By


class BasketPageLocators:
    """
    Класс локаторов страницы с художественной литературой
    """
    BASKET_HEADER_LOCATORS = (By.CSS_SELECTOR, '.page-header>h1')
    BASKET_PATH_LOCATORS = (By.CSS_SELECTOR, '.breadcrumb>.active')
    EMPTY_BASKET_LOCATORS = (By.CSS_SELECTOR, '#content_inner>p')
