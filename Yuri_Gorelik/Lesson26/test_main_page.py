from main_page import MainPage


def test_basket_page_title(driver):
    """
    Проверка, что заголовок главной страницы "Basket | Oscar - Sandbox"
    """
    main_page = MainPage(driver)
    main_page.open()
    assert main_page.get_title() == "Oscar - Sandbox", \
        "Incorrect main page title!"


def test_oscar_ref_exists(driver):
    """
    Проверка, что ссылка на главной странице называется "Oscar"
    """
    main_page = MainPage(driver)
    main_page.open()
    assert main_page.get_oscar_reference().text == "Oscar", \
        "No reference to Oscar!"


def test_text_exists(driver):
    """
    Проверка наличия текста "Welcome!" на главной странице
    """
    main_page = MainPage(driver)
    main_page.open()
    assert main_page.get_text().text == "\n        Welcome!\n    ",\
        "No text on page!"
