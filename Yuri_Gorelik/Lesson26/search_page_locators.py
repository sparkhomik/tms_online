from selenium.webdriver.common.by import By


class SearchPageLocators:
    """
    Класс локаторов страницы с результатом пойска
    """
    SEARCH_HEADER_LOCATORS = (By.CSS_SELECTOR, '.page-header>h1')
    SEARCH_PATH_LOCATORS = (By.CSS_SELECTOR, '.breadcrumb>.active')
    SEARCH_IMAGE_LOCATORS = (By.CSS_SELECTOR, 'img')
