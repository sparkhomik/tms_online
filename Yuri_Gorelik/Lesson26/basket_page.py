from base_page import BasePage
from basket_page_locators import BasketPageLocators


class BasketPage(BasePage):
    """
    Класс страницы корзины
    """

    def get_title(self):
        """
        Получение заголовка окна
        """
        return self.driver.title

    def get_header_name(self):
        """
        Получение header страницы корзины
        """
        return self.find_element(BasketPageLocators.BASKET_HEADER_LOCATORS)

    def get_path_name(self):
        """
        Получение пути страницы корзины
        """
        return self.find_element(BasketPageLocators.BASKET_PATH_LOCATORS)

    def get_empty_basket(self):
        """
        Получение надписи о том, что страница пустая
        """
        return self.find_element(BasketPageLocators.EMPTY_BASKET_LOCATORS)
