from main_page import MainPage


def test_search_page_title(driver, book_name='The Robot Novels'):
    """
    Тест на прроверку, что заголовок страницы содержит названия искомой книги
    """
    main_page = MainPage(driver)
    main_page.open()
    search_page = main_page.open_search_page(book_name)
    assert book_name in search_page.get_title(), "Incorrect search page title!"


def test_search_header_name(driver, book_name='The Robot Novels'):
    """
    Проверка, что header содержит название искомой книги
    """
    main_page = MainPage(driver)
    main_page.open()
    search_page = main_page.open_search_page(book_name)
    assert book_name in search_page.get_header_name().text, \
        "Incorrect search page header!"


def test_search_path_name(driver, book_name='The Robot Novels'):
    """
    Проверка наличия названия искомой книги в path
    """
    main_page = MainPage(driver)
    main_page.open()
    search_page = main_page.open_search_page(book_name)
    assert book_name in search_page.get_path_name().text, \
        "Incorrect search page path!"


def test_search_image(driver, book_name='The Robot Novels'):
    """
    Проверка наличия изображения искомой книги
    """
    main_page = MainPage(driver)
    main_page.open()
    search_page = main_page.open_search_page(book_name)
    assert search_page.get_image(), "Image not found!"
