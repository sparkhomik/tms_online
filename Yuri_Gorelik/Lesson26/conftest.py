import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def driver():
    """
    Фикстура старта и закрытия драйвера
    :return: драйвер
    """
    driver = webdriver.Safari()
    yield driver
    driver.quit()
