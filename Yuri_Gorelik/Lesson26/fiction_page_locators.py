from selenium.webdriver.common.by import By


class FictionPageLocators:
    """
    Класс локаторов страницы с художественной литературой
    """
    FICTION_HEADER_LOCATORS = (By.CSS_SELECTOR, '.page-header>h1')
    FICTION_PATH_LOCATORS = (By.CSS_SELECTOR, '.breadcrumb>.active')
