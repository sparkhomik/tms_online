from base_page import BasePage
from fiction_page_locators import FictionPageLocators


class FictionPage(BasePage):
    """
    Класс страницы с книгами по художественной литературе
    """

    def get_title(self):
        """
        Получение заголовка окна
        """
        return self.driver.title

    def get_header_name(self):
        """
        Получение header страницы с книгами по художественной литературе
        """
        return self.find_element(FictionPageLocators.FICTION_HEADER_LOCATORS)

    def get_path_name(self):
        """
        Получение пути страницы с книгами по художественной литературе
        """
        return self.find_element(FictionPageLocators.FICTION_PATH_LOCATORS)
