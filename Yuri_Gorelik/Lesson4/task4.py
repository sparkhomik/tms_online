from collections import Counter

# 1. Перевести строку в массив
str1 = 'Robin Singh'
str2 = 'I love arrays they are my favorite'
str1 = str1.split()
str2 = list(str2.split())
print(str1)
print(str2)
# 2. Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”
list1 = ['Ivan', 'Ivanou']
str3 = 'Minsk'
str4 = 'Belarus'
print(f"Привет, {list1[0]} {list1[1]}! Добро пожаловать в {str3} {str4}")
# 3 Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
# Сделайте из него строку => "I love arrays they are my favorite"
list2 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
str5 = str(' '.join(list2))
print(str5)
# 4 Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
# удалите элемент из списка под индексом 6
list3 = [1, 2, 3, 'cat', 'dog', 'mouse', 7, 8, 9, 10]
list3[2] = 4
del list3[6]
print(list3)
# 5 Есть 2 словаря a = {'a': 1, 'b': 2, 'c': 3} и b = {'c': 3, 'd': 4,'e':''}
# 5.1 Создайте словарь, который будет содержать в себе все элементы
# обоих словарей
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}
new_a = {**a, **b}
print(new_a)
# 5.2 Обновите словарь “a” элементами из словаря “b”
a.update(b)
print(a)
# 5.3 Проверить что все значения в словаре “a” не пустые либо не равны нулю
a_1 = a.values()
print('' in a_1 or 0 in a_1)
# 5.4 Проверить что есть хотя бы одно пустое значение
# (результат выполнения должен быть True)
print('' in a_1)
# 5.5 Отсортировать словарь по алфавиту в обратном порядке
a_2 = a.items()  # отсортировать по алфавиту
a_2 = dict(a_2)
a_2 = tuple(a_2)
a_2 = a_2[::-1]  # изменить порядок на обратный
a_2 = dict(a_2)
print(a_2)
# 5.6 Изменить значение под одним из ключей и вывести все значения
a['b'] = 44
print(a)
# 6 Создать список из элементов list_a = [1,2,3,4,4,5,2,1,8]
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
# a. вывести только уникальные значения и сохранить их в отдельную переменную
b = set(list_a)
print(b)
# b. добавить в полученный объект значение 22
b.add(22)
print(b)
# c. сделать list_a неизменяемым
list_a = tuple(list_a)
# d. измерить его длинну
print(len(list_a))

# ФОРМАТИРОВАНИЕ
# 1. есть переменные
a = 10
b = 25
# Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
c = a + b
d = a - b
print(f"Summ is {c} and diff = {d}.")
print("Summ is {summ} and diff = {diff}.".format(summ=c, diff=d))
# 2. есть список
list_of_children = ['Sasha', 'Vasia', 'Nikalai']
# Вывести “First child is <первое имя из списка>, second is “<второе>”,
# and last one – “<третье>””
print(f"First child is {list_of_children[0]}, second is {list_of_children[1]},"
      f" and last one – {list_of_children[2]}")

# * Вам передан массив чисел. Известно, что каждое число в этом массиве имеет
# пару, кроме одного: [1, 5, 2, 9, 2, 9, 1] => 5
# Напишите программу, которая будет выводить уникальное число
x = [1, 5, 2, 9, 2, 9, 1]
y = Counter(x)  # определяем число без пары (т.е. со згачением "1")
print(min(y, key=y.get))

# Дан текст, который содержит различные английские буквы и знаки препинания.
# Вам необходимо найти самую частую букву в тексте.
# Результатом должна быть буква в нижнем регистре
text = 'Happiness is not a destination. It is a method of life'
text = text.split(' ')  # разбивает строку на части, используя как разделитель
# пробел
text = (''.join(text))  # собрать список строк в одну строку
text = text.split('.')  # разбивает строку на части, используя как разделитель
# точку
text = (''.join(text))  # собрать список строк в одну строку
letter = sorted(text.lower())  # отсортировать буквы в нижнем реестре
# по алфавиту
letter = Counter(letter)  # подсчёт элементов
print('This letter is ', max(letter, key=letter.get))
