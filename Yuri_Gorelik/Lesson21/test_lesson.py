def test_login(login):
    """
    Тестовая функция проверки окна логина.
    Сравнивает полученный текст с ожидаемым.
    """
    assert login[0] == login[1]


def test_checkboxes(checkboxes):
    """
    Тестовая функция проверки чекбоксов.
    Сравнивает список полученных значений с ожидаемым.
    """
    assert checkboxes == [True, False]


def test_windows(windows):
    """
    Тестовая функция проверки открытия нового окна.
    Сравнивает полученый заголовок окна с ожидаемым.
    """
    assert windows == "New Window"


def test_add_elements(add_elements):
    """
    Тестовая функция проверки добавления элементов.
    Проверяет существует ли созданный элемент.
    """
    assert bool(add_elements) is True


def test_delete_elements(delete_elements):
    """
    Тестовая функция проверки удаления созданного элемента.
    Проверяет, что созданые ранее элементы, а затем удаленные, не существуют.
    """
    assert bool(delete_elements) is False
