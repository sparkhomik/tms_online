import pytest
from selenium.webdriver import Safari
from selenium.webdriver.common.by import By


@pytest.fixture(scope='session', autouse=True)
def safari_driver():
    """
    Фикструра открытия и закрытия браузера
    """
    safari_driver = Safari()
    yield safari_driver
    safari_driver.quit()


@pytest.fixture()
def driver_login(safari_driver):
    """
    Фикстура открытия окна с логином
    """
    safari_driver.get("http://the-internet.herokuapp.com/login")
    return safari_driver


@pytest.fixture()
def driver_checkboxes(safari_driver):
    """
    Фикстура открытия окна с чекбоксами
    """
    safari_driver.get("http://the-internet.herokuapp.com/checkboxes")
    return safari_driver


@pytest.fixture()
def driver_windows(safari_driver):
    """
    Фикстурв для работы с новым окном браузера
    """
    safari_driver.get("http://the-internet.herokuapp.com/windows")
    return safari_driver


@pytest.fixture()
def driver_elements(safari_driver):
    """
    Фикстура открытия окна с возможностью добавления/удаления элементов
    """
    safari_driver.get("http://the-internet.herokuapp.com/add_remove_elements/")
    return safari_driver


@pytest.fixture(params=[("Username", "Password", "Your username is invalid!"),
                        ("tomsmith", "SuperSecretPassword!",
                         "You logged into a secure area!")],
                ids=["invalid username", "successful"])
def login(driver_login, request) -> tuple:
    """
    Фикстура логина.
    Ищет поле логин и пароль и вводит соответсвующие данные.
    Ищет кнопку login и нажимает на нее.
    Ищет сообщение о состоянии логина.
    Фикстура параметризированна и вводит 2-x групп логин/пароль, для получения
    соответсвующих сообщений которых 2:
    1. неправильное имя и неправильный пароль.
    2. правильное имя и правильный пароль.
    :return: тапл (полученный результат, ожидаемый результат)
    """
    params = request.param
    driver_login.implicitly_wait(15)
    login_field = driver_login.find_element(By.XPATH,
                                            "//input[@id='username']")
    login_field.send_keys(params[0])
    password_field = driver_login.find_element(By.CSS_SELECTOR,
                                               "input[name='password']")
    password_field.send_keys(params[1])
    login_button = driver_login.find_element(By.XPATH, "//i[text()=' Login']")
    login_button.click()
    actual_result = driver_login.find_element(By.XPATH, "//div[@id='flash']")
    actual_result = actual_result.text
    expected_result = params[2]
    return actual_result, expected_result


@pytest.fixture()
def checkboxes(driver_checkboxes) -> list:
    """
    Фикстура выбора чекбоксов.
    Ищет чекбоксы и нажимает на каждый.
    :return: список состояния чекбоксов.
    """
    checkboxes = driver_checkboxes.find_elements(By.XPATH,
                                                 "//input[@type='checkbox']")
    result = []
    for checkbox in checkboxes:
        checkbox.click()
        result.append(checkbox.is_selected())
    return result


@pytest.fixture()
def windows(driver_windows) -> str:
    """
    Фикстура открытия нового окна.
    Ищет ссылку на новое окно и нажимает на нее.
    Переключает драйвер на новое открытое окно.
    :return: строка с заголовком окна.
    """
    driver_windows.find_element(By.XPATH, "//a[@href='/windows/new']").click()
    driver_windows.switch_to.window(driver_windows.window_handles[1])
    text_new_window = driver_windows.title
    return text_new_window


@pytest.fixture()
def add_elements(driver_elements) -> list:
    """
    Фикстура добавления элементов.
    Находит кнопку добавления элемента и нажимает на нее.
    Находит созданные элементы.
    :return: список созданных элементов.
    """
    add_button = driver_elements. \
        find_element(By.XPATH, "//div[@id='content']/div/button")
    add_button.click()
    delete_button = driver_elements. \
        find_elements(By.XPATH, "//div[@id='elements']/button")
    return delete_button


@pytest.fixture()
def delete_elements(driver_elements, add_elements) -> list:
    """
    Фикстура удаления элементов.
    Нажимает на каждый созданный фикстурой add_elements элемент.
    Ищет созданные элементы.
    :param add_elements: список созданных фикстурой add_elements элементов.
    :return: список созданных элементов.
    """
    for button in add_elements:
        button.click()
    delete_button = driver_elements. \
        find_elements(By.XPATH, "//div[@id='elements']/button")
    return delete_button
