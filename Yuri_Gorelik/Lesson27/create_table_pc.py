import mysql.connector as mysql

db = mysql.connect(host="localhost",
                   user="root",
                   passwd="root",
                   database="homework_db")

cursor = db.cursor()
# создание таблицы PC и создание в ней столбцов заданного типа
cursor.execute(
    "CREATE TABLE PC ("
    "PRO_ID INT(3),"
    "PRO_NAME CHAR(25),"
    "PRO_PRICE INT(4),"
    "PRO_COM_id INT(2)"
)
# заполнение таблицы orders данными
for PRO_ID, PRO_NAME, PRO_PRICE, PRO_COM in [
    (101, "Mother Board", 3200, 15),
    (102, "Key Board", 450, 16),
    (103, "ZIP drive", 250, 14),
    (104, "Speaker", 550, 16),
    (105, "Monitor", 5000, 11),
    (106, "DVD drive", 900, 12),
    (107, "CD drive", 800, 2),
    (108, "Printer", 2600, 13),
    (109, "Refill cartridge", 350, 13),
    (110, "Mouse", 250, 12)
]:
    cursor.execute(f"INSERT INTO PC "
                   f"(PRO_ID, PRO_NAME, PRO_PRICE, PRO_COM) "
                   f"values ({PRO_ID}, '{PRO_NAME}', {PRO_PRICE}, {PRO_COM})"
                   )
db.commit()
db.close()
