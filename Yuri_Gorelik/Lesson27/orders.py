import mysql.connector as mysql

db = mysql.connect(host="localhost",
                   user="root",
                   passwd="root",
                   database="homework_db")


def get_id_5002(cursor):
    """
    Функция выводит номер заказа, дату заказа и количество для каждого заказа,
    который продал продавец под номером: 5002
    :return: список заказов от продавца 5002 и сортирует его по номеру заказа
    """
    query_1 = """
    SELECT ord_no, purch_amt, ord_date
    FROM orders
    WHERE salesman_id = 5002
    GROUP BY ord_no
    """
    cursor.execute(query_1)
    result = cursor.fetchall()
    return result


def get_distinct(cursor):
    """
    Функция выводит уникальные id продавца
    :return: список уникальных id продавцов и сортирует их в порядке
    возростания id
    """
    query_2 = """
    SELECT DISTINCT salesman_id
    FROM orders
    GROUP BY salesman_id
    """
    cursor.execute(query_2)
    distinct_id = cursor.fetchall()
    return distinct_id


def get_data_by_order(cursor):
    """
    Выводит все данные и сортирует их в порядке от самого ряннего до
    самого позднего
    :return: список со всеми данными и отсортированный по дате от самого
    ряннего до самого позднего
    """
    query_3_1 = """
    SELECT *
    FROM orders
    GROUP BY ord_date
    """
    cursor.execute(query_3_1)
    data_order = cursor.fetchall()
    return data_order


def get_id_by_order(cursor):
    """
    Выводит все данные и сортирует их в порядке возрастания id
    :return: список со всеми данными и отсортированный по возрастанию id
    продавца
    """
    query_3_2 = """
    SELECT *
    FROM orders
    GROUP BY customer_id
    """
    cursor.execute(query_3_2)
    id_order = cursor.fetchall()
    return id_order


def get_number_by_order(cursor):
    """
    Выводит все данные и сортирует их в порядке возрастания номера заказа
    :return: список со всеми данными и отсортированный по возрастанию номера
    заказа
    """
    query_3_3 = """
    ELECT *
    FROM orders
    GROUP BY ord_no
    """
    cursor.execute(query_3_3)
    number_order = cursor.fetchall()
    return number_order


def get_amount_by_order(cursor):
    """
    Выводит все данные и сортирует их в порядке возрастания количества
    :return: список со всеми данными и отсортированный по возрастанию
    количества
    """
    query_3_4 = """
    SELECT *
    FROM orders
    GROUP BY purch_amt
    """
    cursor.execute(query_3_4)
    amount_order = cursor.fetchall()
    return amount_order


def get_data_between(cursor):
    """
    Выводит все данные и сортирует их в порядке возрастания количества
    :return: список со всеми данными и отсортированный по возрастанию
    количества
    """
    query_4 = """
    SELECT *
    FROM orders
    WHERE ord_no BETWEEN 70001 AND 70007
    GROUP BY ord_no
    """
    cursor.execute(query_4)
    between = cursor.fetchall()
    return between


if __name__ == '__main__':
    # создаем объект, который делает запросы и получает их результаты
    cursor = db.cursor()
    # список заказов от продавца 5002
    print(get_id_5002(cursor))
    # уникальный id продавца
    print(get_distinct(cursor))
    # список заказов по дате
    print(get_data_by_order(cursor))
    # список заказов по id продавца
    print(get_id_by_order(cursor))
    # список заказов по номеру заказа
    print(get_number_by_order(cursor))
    # список заказов по количеству
    print(get_amount_by_order(cursor))
    # список заказов между 70001 и 70007
    print(get_data_between(cursor))
