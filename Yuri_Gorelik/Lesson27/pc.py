import mysql.connector as mysql

db = mysql.connect(host="localhost",
                   user="root",
                   passwd="root",
                   database="homework_db")


def get_price_sum(cursor):
    """
    Функция выводит общую стоимость компонентов
    :return: общая стоимость компонентов
    """
    query_1 = """
    SELECT SUM(PRO_PRICE)
    FROM PC
    """
    cursor.execute(query_1)
    sum = cursor.fetchall()
    return sum


def get_avg_price(cursor):
    """
    Функция выводит среднюю стоимость компонентов
    :return: средняя стоимость компонентов
    """
    query_2 = """
    SELECT AVG(PRO_PRICE)
    FROM PC
    """
    cursor.execute(query_2)
    avg = cursor.fetchall()
    return avg


def get_price_between(cursor):
    """
    Функция выводит компоненты с ценой от 200 до 600
    :return: список компонентов, у которых цена от 200 до 600 и отсортированный
    по id продукта
    """
    query_3 = """
    SELECT *
    FROM PC
    WHERE PRO_PRICE BETWEEN 200 AND 600
    BY GROUP PRO_ID
    """
    cursor.execute(query_3)
    between = cursor.fetchall()
    return between


def get_max_price(cursor):
    """
    Функция выводит компонент с самой большой ценой
    :return: компонента, у которого самая высокая цена
    """
    query_4 = """
    SELECT PRO_NAME
    FROM PC
    WHERE PRO_PRICE = (SELECT MAX(PRO_PRICE)
                       FROM PC)
    """
    cursor.execute(query_4)
    max = cursor.fetchall()
    return max


def get_min_price(cursor):
    """
    Функция выводит компонент с самой низкой ценой
    :return: компонент, у которого самая низкая цена
    """
    query_5 = """
        SELECT PRO_NAME
        FROM PC
        WHERE PRO_PRICE = (SELECT MIN(PRO_PRICE)
                           FROM PC)
        """
    cursor.execute(query_5)
    min = cursor.fetchall()
    return min


if __name__ == '__main__':
    # создаем объект, который делает запросы и получает их результаты
    cursor = db.cursor()
    # общая стоимость компонентов
    print(get_price_sum(cursor))
    # средняя стоимость компонентов
    print(get_avg_price(cursor))
    # компоненты с ценой между 200 и 600
    print(get_price_between)
    # название компонента, у которого самая высокая цена
    print(get_max_price(cursor))
    # название компонент, у которого самая низкая цена
    print(get_min_price(cursor))
