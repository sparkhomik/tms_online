import mysql.connector as mysql

db = mysql.connect(host="localhost",
                   user="root",
                   passwd="root",
                   database="homework_db")

cursor = db.cursor()
# создание таблицы orders и создание в ней столбцов заданного типа
cursor.execute(
    "CREATE TABLE orders ("
    "ord_no INT(5),"
    "purch_amt DEC(6,2),"
    "ord_date DATE,"
    "customer_id INT(4),"
    "salesman_id INT(4))"
)
# заполнение таблицы orders данными
for ord_no, purch_amt, ord_date, customer_id, salesman_id in [
    (70001, 150.5, "2012-10-05", 3005, 5002),
    (70009, 270.65, "2012-09-10", 3001, 5005),
    (70002, 270.65, "2012-10-05", 3002, 5001),
    (70004, 110.5, "2012-08-17", 3009, 5003),
    (70007, 948.5, "2012-09-10", 3005, 5002),
    (70005, 2400.6, "2012-07-27", 3007, 5001),
    (70008, 5760, "2012-09-10", 3002, 5001),
    (70010, 1983.43, "2012-10-10", 3004, 5006),
    (70003, 2480.4, "2012-10-10", 3009, 5003),
    (70012, 250.45, "2012-06-27", 3008, 5002),
    (70011, 75.29, "2012-08-17", 3003, 5007),
    (70013, 3045.6, "2012-04-25", 3002, 5001)
]:
    cursor.execute(f"INSERT INTO orders "
                   f"(ord_no, purch_amt, ord_date, customer_id, salesman_id) "
                   f"values ({ord_no}, {purch_amt}, '{ord_date}', "
                   f"{customer_id}, {salesman_id})"
                   )
db.commit()
db.close()
