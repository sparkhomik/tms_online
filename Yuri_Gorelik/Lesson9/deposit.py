class Investment:
    total = 0

    def __init__(self, deposit, term, percent):
        self.deposit = deposit
        self.term = term
        self.percent = percent
        self.deposit_func()

    def deposit_func(self):
        """
        Функция для подсчета суммы вклада на конец срока.
        Расчет производится по формуле ежемесячной капитализации:

        P * (1 + r / n) ** t

        Р – начальная сумма вклада.
        r – годовая ставка в процентах.
        n – сколько раз в год учитывается процентная ставка.
        t – срок вклада в месяцах.
        """

        total = self.deposit * (1 + ((self.percent / 100) / 12)) ** self.term
        self.total = total
        return self.total


def main():
    """
    Функция запуска программы
    :return: вывод суммы с округлением до целого числа
    """
    print('Расчет итоговой суммы вашего вклада')
    deposit = input('Введите сумму вклада в рублях: ')
    deposit = int(deposit)
    term = input('Введите срок вклада в месяцах: ')
    term = int(term)
    percent = input('Введите годовую ставку в процентах: ')
    percent = int(percent)
    invest = Investment(deposit, term, percent)
    return f'Итоговая сумма, вашего вклада - {round(invest.total)}'


if __name__ == '__main__':
    print(main())
