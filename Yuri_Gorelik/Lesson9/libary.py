# 1. Библиотека
# Создайте класс book с именем книги, автором, кол-м страниц, ISBN, флагом,
# зарезервирована ли книги или нет. Создайте класс пользователь который может
# брать книгу, возвращать, бронировать. Если другой пользователь хочет взять
# зарезервированную книгу(или которую уже кто-то читает -
# надо ему про это сказать).
class Book:
    def __init__(self, name: str, author: str, pages: int, isbn: str):
        self.name = name
        self.author = author
        self.pages = pages
        self.isbn = isbn
        self.reserve = False  # зарезервирована
        self.free = True  # свободна
        self.take = False  # на руках

    def book_free(self):
        """
        Функция сободной книги
        :return: True или False
        """
        self.free = True
        self.reserve = False
        self.take = False

    def book_reserve(self):
        """
        Функция брони книги
        :return: True или False
        """
        self.reserve = True
        self.free = False

    def book_take(self):
        """
        Функция проверки занятости книги
        :return: True или False
        """
        self.take = True
        self.free = False


# Создаём книги
book_1 = Book('\"Три мушкетёра\"', 'Александр Дюма', 555,
              '987-3-16-148410-0')
book_2 = Book('\"Остров сокровищ\"', 'Роберт Льюис Бэлфур Стивенсон', 777,
              '987-4-18-146938-0')
book_3 = Book('\"Белый Клык\"', 'Jack London', 333, '987-7-18-913938-0')
book_4 = Book('\"Старик и море\"', 'Эрнест Хемингуэй', 666,
              '987-7-08-190538-0')


class User:
    def __init__(self, user_name: str):
        self.user_name = user_name
        self.get_book = None
        self.reserve_book = None

    def user_take_book(self, user_book):
        """
        Функция взятия книги читателем
        """
        if user_book.free:
            self.get_book = user_book
            user_book.book_take()
            return f'Книга {user_book.name} взята ' \
                   f'читателем {self.user_name}'
        else:
            return f'Книга {user_book.name} взята или зарезервирована ' \
                   f'другим читателем'

    def user_return_book(self, user_book):
        """
        Функция возврата книги читателем
        """
        user_book.book_free()
        return f'Книга {user_book.name} возвращена ' \
               f'читателем {self.user_name}'

    def user_book_book(self, user_book):
        """
        Функция резервирования книги читателем
        """
        if user_book.free:
            self.reserve_book = user_book
            user_book.book_reserve()
            return f'Книга {user_book.name} зарезервирована ' \
                   f'пользователем {self.user_name}'
        else:
            return f'Книга {user_book.name} взята или зарезервирована ' \
                   f'другим пользователем'


# Создаём читателей
user_1 = User('Гомер Симпсон')
user_2 = User('Марджори Жаклиин «Мардж» Симпсон')
user_3 = User('Лиза Мари Симпсон')
user_4 = User('Бартоломью Джо-Джо «Барт» Симпсон')
user_5 = User('Ма́ргарет Эвелин «Мэ́гги» Си́мпсон')


def main():
    """
    Функция запуска программы
    """
    print(user_1.user_take_book(book_1))
    print(user_2.user_return_book(book_2))
    print(user_3.user_book_book(book_3))
    print(user_1.user_return_book(book_1))
    print(user_4.user_take_book(book_1))
    print(user_5.user_take_book(book_2))
    print(user_3.user_take_book(book_3))


if __name__ == '__main__':
    main()
