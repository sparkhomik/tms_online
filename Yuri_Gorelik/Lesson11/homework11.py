from abc import ABC, abstractmethod

"""
На ферме могут жить животные и птицы
И у тех и у других есть два атрибута: имя и возраст
При этом нужно сделать так, чтобы возраст нельзя было изменить извне напрямую,
но при этом можно было бы получить его значение
У каждого животного должен быть обязательный метод – ходить
А для птиц – летать
Эти методы должны выводить сообщения (контекст придумайте)
Также должны быть общие для всех классов обязательные методы – постареть -
который увеличивает возраст на 1 и метод голос

При обращении к несуществующему методу или атрибуту животного или птицы должен
срабатывать пользовательский exception NotExistException
и выводить сообщение о том, что для текущего класса (имя класса), такой метод
или атрибут не существует – (имя метода или атрибута)

На основании этих классов создать отдельные классы для Свиньи, Гуся, Курицы и
Коровы – для каждого класса метод голос должен выводить разное сообщение
(хрю, гага, кудах, му)

После этого создать класс Ферма, в который можно передавать список животных
Должна быть возможность получить каждое животное, живущее на ферме как по
индексу, так и через цикл for.
Также у фермы должен быть метод, который увеличивает возраст всех животных,
 живущих на ней.
"""


class NotExistException(Exception):
    """
    В случае атрибут экземпляра класса не существует, вызывается
    ошибка с выводом сообщения
    """

    def __init__(self, func):
        message = f"Function {func} doesn't exist"
        super().__init__(message)


class Nature(ABC):
    """
    Абстрактный класс Nature, от которого будут создаваться все последующие
    классы
    Атрибуты - имя и возраст
    Метод старения
    """

    def __init__(self, name, age):
        self.name = name
        self.age = age

    def increase_age(self):
        """
        Метод старения.
        Увеличивает возраст на 1.
        """
        self.age += 1

    @abstractmethod
    def voice(self):
        """
        Абстрактный метод голос
        """
        raise NotImplementedError

    def __getattr__(self, atr):
        """
        Метод проверки наличия атрибута класса.
        Если нет, вызывается пользовательский exception.
        """
        raise NotExistException(atr)


class Animals(Nature, ABC):
    """
    Класс животные, которые умеют ходить
    """

    @staticmethod
    def go():
        return "Я могу ходить"


class Birds(Nature, ABC):
    """
    Класс птицы, котрые умеют летать
    """

    @staticmethod
    def fly():
        return "Я могу лететь"


class Pig(Animals):
    def voice(self):
        """
        Переопределенный метод голос
        """
        return "хрю-хрю"

    def __str__(self):
        return f'Свинья {self.name} говорит {self.voice()} и {self.go()},' \
               f' её возраст {self.age} года'


class Goose(Birds):
    def voice(self):
        """
        Переопределенный метод голос
        """
        return "га-га"

    def __str__(self):
        return f'Гусь {self.name} говорит {self.voice()} и {self.fly()},' \
               f' его возраст {self.age} года'


class Chicken(Birds):
    def voice(self):
        """
        Переопределенный метод голос
        """
        return "кудах"

    def __str__(self):
        return f'Курочка {self.name} говорит {self.voice()} и {self.fly()},' \
               f' её возраст {self.age} года'


class Cow(Animals):
    def voice(self):
        """
        Переопределенный метод голос
        """
        return "му-му"

    def __str__(self):
        return f'Коровка {self.name} говорит {self.voice()} и {self.go()},' \
               f' её возраст {self.age} года'


class Farm:
    """
    Класс Ферма, принимает на вход список животных
    """

    def __init__(self, list_of_animals):
        self.list_of_animals = list_of_animals

    def __getitem__(self, item):
        """
        Функция получения животного по индексу
        """
        return self.list_of_animals[item]

    def all_animals(self):
        """
        Функция получения всех животных.
        Выводит каждый экземпляр класса из списка.
        """

        for animal in self.list_of_animals:
            print(animal)

    def increase_age(self) -> None:
        """
        Функция увеличение возраста каждого животного на 1
        """
        for animal in self.list_of_animals:
            animal.increase_age()


def main():
    """
    Запуск функционала
    """
    # Создание экземпляров класса и составления списках из них
    pig = Pig('Марфа', 3)
    goose = Goose('Валера', 2)
    chicken = Chicken('Ряба', 1)
    cow = Cow('Пеструшка', 5)
    animals = [pig, goose, chicken, cow]
    # Все животных фермы c их сообщениями
    farm = Farm(animals)
    farm.all_animals()
    # Вывод животтного под индексу
    print(f'Животное под индексом [0]: {farm[0]}')
    # Вызов функции старения
    farm.increase_age()
    # Все животные уже с измененным возрастом
    farm.all_animals()
    # Проверка коровы на действие "летать"
    pig.drink()


if __name__ == '__main__':
    main()
