def test_success(success_login):
    """
    Тест для проверки успешного логина
    :param success_login: 2 элемента, полученный и ожидаемый
    :return: True or False
    """
    assert success_login[0] == success_login[1]


def test_error(failed_login):
    """
    Тест для проверки неуспешного логина
    :param failed_login: 2 элемента, полученный и ожидаемый
    :return: True or False
    """
    assert failed_login[0] in failed_login[1]
