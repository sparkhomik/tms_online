import xml.etree.ElementTree as ET


def get_xml():
    """
    Функция получения содержимого xml файла
    :return: содержимое файла переведенное из xml формата
    """
    file = open("library.xml", "r")
    content = ET.fromstring(file.read())
    file.close()
    return content


def search(propertykey: str, propertyvalue: str, xml_list) -> list:
    """
    Функция фильтрации по заданному параметру
    :param propertyvalue: ключ
    :param propertykey: значение
    :param xml_list: список книг
    :return: список отфильтрованных объектов
    """
    books = []
    for book in xml_list:
        tag_name = book.find(propertykey)
        if propertyvalue in tag_name.text:
            books.append(book.attrib["id"])
    return books


if __name__ == '__main__':
    # содержимое файла сохраняется в объект
    root = get_xml()
    # использование фильтра по параметрам: автору(часть имени)
    book_by_author = search('author', 'Eva', root)
    print(book_by_author)
    # использование фильтра по параметрам: цене
    book_by_price = search('price', '36.75', root)
    print(book_by_price)
    # использование фильтра по параметрам: заголовку
    book_by_title = search('title', 'Learn Python 3 the Hard Way', root)
    print(book_by_title)
    # использование фильтра по параметрам: описанию(часть текста)
    book_by_description = search('description', 'her own childhood', root)
    print(book_by_description)
