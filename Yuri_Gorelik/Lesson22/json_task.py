import json


def get_json():
    """
    Функция получения содержимого json файла
    :return: словарь содержимого файла
    """
    file_obj = open("students.json", "r")
    content = file_obj.read()
    content_json = json.loads(content)
    file_obj.close()
    return content_json


def search(propertykey: str, propertyvalue: str, json_list: list) -> list:
    """
    Функция фильтрации по заданному параметру
    :param propertyvalue: ключ
    :param propertykey: значение
    :param json_list: список студентов
    :return: список отфильтрованных объектов
    """
    students = []
    filtered = filter(lambda x: propertyvalue in x[propertykey], json_list)
    for item in filtered:
        students.append(item)
    return students


if __name__ == '__main__':
    # содержимое файла сохраняется в объект
    obj_student = get_json()
    # использование фильтра по параметраму "класс"
    students_by_class = search('Class', '5a', obj_student)
    print(students_by_class)
    # использование фильтра по параметраму "имя"
    students_by_name = search('Name', 'Saki Miyu', obj_student)
    print(students_by_name)
    # использование фильтра по параметраму "часть имени"
    students_by_part_of_name = search('Name', 'Kokona', obj_student)
    print(students_by_part_of_name)
    # использование фильтра по параметраму "секция"
    students_by_club = search('Club', 'Football', obj_student)
    print(students_by_club)
    # использование фильтра по параметраму "пол"
    students_by_gender = search('Gender', 'W', obj_student)
    print(students_by_gender)
