from requests import get, post, put, delete
import json

link = "https://fakerestapi.azurewebsites.net/api/v1/"


def get_authors() -> list:
    """
    Функция get запроса для получения всех авторов.
    """
    response = get(f"{link}Authors")
    return json.loads(response.content)


def get_authors_by_id(author_id: int) -> str:
    """
    Функция get запроса для получения автора по ID.
    :return: данные автора
    """
    response = get(f"{link}/Authors/{author_id}")
    return response.text


def add_book(book: dict):
    """
    Функция post запроса для добавления книги
    :return: код ответа
    """
    response = post(f"{link}Books", json=book)
    return response


def add_user(user_data: dict):
    """
    Функция post запроса для юзера
    :return: код ответа
    """
    response = post(f"{link}Users", json=user_data)
    return response


def update_book_data(book_data: dict, book_id: int):
    """
    Функция put запроса для обновления книги
    :return: код ответа
    """
    response = put(f"{link}Books/{book_id}", json=book_data)
    return response


def remove_user(user_id: int):
    """
    Функция delete запроса для удаления юзера
    :return: код ответа
    """
    response = delete(f"{link}Users/{user_id}")
    return response


if __name__ == '__main__':
    # получение всех авторов
    all_authors = get_authors()
    print(all_authors)
    # получение автора по ID
    author_by_id = get_authors_by_id(1)
    print(author_by_id)
    # добавление книги
    book = {
        "id": 123,
        "title": "tестирование dot com",
        "description": "пособие по жёсткому обращению с багами",
        "pageCount": 1,
        "excerpt": "test",
        "publishDate": "2021-11-02T23:05:33.0002"
    }
    add_book = add_book(book)
    print(add_book)
    # добавление юзера
    user = {
        "id": 12,
        "userName": "Mister_X",
        "password": "xXx"
    }
    add_user = add_user(user)
    print(add_user)
    # обновление данных книги
    book_data = {
        "id": 0,
        "title": "title",
        "description": "description",
        "pageCount": 0,
        "excerpt": "excerpt",
        "publishDate": "2021-11-03T12:00:01.571Z"
    }
    updated_book = update_book_data(book_data, 3)
    print(updated_book)
    # удаление юзера
    user_to_remove = remove_user(2)
    print(user_to_remove)
