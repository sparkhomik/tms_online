import string


def check_special_characters(character):
    """
    Проверка, является ли переданный аргумент спецсимволом.
    :param character: переданный аргумент
    :return: спецсимвол или нет
    """
    return character in string.punctuation


if __name__ == "__main__":
    check_special_characters(',')
