import pytest
from function_1 import lowercase_word
from function_2 import float_number, InputTypeError
from function_3 import check_special_characters


@pytest.mark.parametrize('word, result', [('lesson', True), ('leSSon', False),
                                          ('LeSsOn', False)])
def test_lowercase_word(word, result):
    """
    Проверка работы функции lowercase_word.
    :param word: слово для проверки
    :param result: в нижнем регистре все буквы в слове или нет
    """
    assert lowercase_word(word) == result


@pytest.mark.parametrize('number, result', [(777, 777.0), (69, 69.0),
                                            (-18, -18.0)])
def test_float_number(number, result):
    """
    Проверка работы функции float_number.
    :param number: переданное число
    :param result: число, преобразованное в тип float
    """
    assert float_number(number) == result


@pytest.mark.parametrize('number', [('twenty', '20', '20.0')])
def test_fail_float_number(number):
    """
    Проверка выдачи сообщения об ошибке при передаче строки в функцию
    float_number.
    :param number: переданное число
    """
    with pytest.raises(InputTypeError):
        float_number(number)


@pytest.mark.parametrize("character, result", [('*', True), ('Null', False),
                                               ('|', True)])
def test_check_special_characters(character, result):
    """
    Проверка работы функции check_special_characters.
    :param character: переданный аргумент
    :param result: спецсимвол или нет
    """
    assert check_special_characters(character) == result
