import pytest
import sys
from datetime import datetime


@pytest.fixture(scope='session', autouse=True)
def time_test() -> None:
    """
    Вывод времени запуска и окончания теста.
    :return: дата и время запуска и окончания теста
    """
    session_datetime = datetime.now()
    print(f'\nTest started at {session_datetime.strftime("%d-%m-%Y %H:%M")}')
    yield session_datetime
    print(f'\nTest ended at {session_datetime.strftime("%d-%m-%Y %H:%M")}')


@pytest.fixture(scope='module', autouse=True)
def file_name() -> None:
    """
    Вывод имени файла, из которого запущен тест.
    :return: имя файла, из которого запущен тест
    """
    print(f'Test started from file: {sys.argv[0]}')


@pytest.fixture(autouse=True)
def fixture_info(request) -> None:
    """
    Вывод имен фикстур, примененных к тесту.
    :param request: параметр для определения имени фикстуры
    """
    print('\nFixture information:')
    print(*request.fixturenames)


@pytest.fixture(autouse=True)
def time_between_starts(time_test):
    """
    Вывод времени между стартом сессии и теста.
    """
    test_datetime = datetime.now()
    between_datetime = test_datetime - time_test
    print(f'Time between session and test starts: {between_datetime}\n')
