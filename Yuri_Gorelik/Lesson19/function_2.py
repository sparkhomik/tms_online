def float_number(number):
    """
    Преобразование числа в тип float. Если в функцию будет передана строка, то
    произойдет вызов пользовательской ошибки InputTypeError.
    :param number: переданное число
    :return: число, преобразованное в тип float
    """
    if type(number) != int:
        raise InputTypeError()
    return float(number)


class InputTypeError(Exception):
    """
    Класс для определения ошибки, которая будет выведена пользователю, в случае
    если была передана строка.
    """

    def __init__(self):
        """
        Определение сообщения об ошибке.
        """
        self.message = 'Type of number isn\'t an int'
        super().__init__(self.message)


if __name__ == "__main__":
    print(float_number(123))
