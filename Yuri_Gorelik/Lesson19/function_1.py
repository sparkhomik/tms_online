def lowercase_word(word):
    """
    Проверка того, что все буквы в слове в нижнем регистре.
    :param word:слово для проверки
    :return: в нижнем регистре все буквы в слове или нет
    """
    return word.islower()


if __name__ == "__main__":
    print(lowercase_word("lesson"))
