# 1. Validate
# написать программу, принимающее число - номер кредитной карты(число может
# быть четным или не четным). И проверяющей может ли такая карта существовать.
# Предусмотреть защиту от ввода букв, пустой строки и т.д.
def card_checker(card: str) -> bool:
    '''
    :param card: строка с введённым номером карты;
    :return: значение True/False
    '''
    sum = 0
    number = list(map(int, card))
    for i, elem in enumerate(number):
        if i % 2 == 0:
            elem = elem * 2
            if elem > 9:
                elem -= 9
            sum += elem
        else:
            sum += elem
    return sum % 10 == 0


x = input('Input card number - ')
print(card_checker(x))

# 2. Подсчет количества букв
# На вход подается строка,
# например, "cccbba" результат работы программы - строка “c3b2a"
str_first = input('Введите строку - ')


def count_list(str_first: str) -> str:
    '''
    :param str_first: строка;
    :return: строка в виде буквы и её количества
    '''
    str_dict = dict()
    second_str = ''
    for i in str_first:
        str_dict[i] = str_dict.get(i, 0) + 1
    for key, value in str_dict.items():
        if value == 1:
            value = ''
        second_str = second_str + key + str(value)
    return second_str


print(count_list(str_first))


# 3. Калькулятор
# Реализуйте программу, которая спрашивала у пользователя, какую операцию
# он хочет произвести над числами, а затем запрашивает два числа и выводит
# результат
# Проверка деления на 0.
def calculate(action: int, num_1: int, num_2: int) -> None:
    '''
    :param action: операция;
    :param num_1: первое число;
    :param num_2: второе число;
    :return: результат.
    '''
    if action == '1':
        result = num_1 + num_2
        print(result)
    if action == '2':
        result = num_1 - num_2
        print(result)
    if action == '3':
        result = num_1 * num_2
        print(result)
    if action == '4':
        if num_2 == 0:
            print('На ноль делить нельзя')
        else:
            result = num_1 / num_2
            print(result)
    else:
        result = (print('Выбран недопустимый параметр'))
    return result


separator = '\n'
print("Выберите операцию: {0} 1.Сложение {0} 2.Вычитание {0} 3.Умножение {0} "
      "4.Деление".format(separator))
a = input('Введите номер пункта меню:')
b = int(input('Введите первое число:'))
c = int(input('Введите второе число:'))
calculate(a, b, c)


# 4.Написать функцию с изменяемым числом входных параметров
def function(x: int, *y: int, z=None, **w: str) -> dict:
    '''
    :param x: обязательный позиционный аргумент;
    :param y: переменное количество позиционных аргументов;
    :param z: обязательный именованный аргумент;
    :param w: переменное количество именованных аргументов
    :return: словарь, в котором ключи это описание переменных.
    '''

    result = {'mandatory_position_argument': x,
              'additional_position_arguments': y,
              'mandatory_named_argument': {'name': z},
              'additional_named_arguments': w}

    return result


print(function(1, 2, 3, name='test', surname='test2', some='something'))


# 5. Работа с областями видимости
# На уровне модуля создать список из 3-х элементов
# Написать функцию, которая принимает на вход этот список и добавляет в него
# элементы. Функция должна вернуть измененный список.
# При этом исходный список не должен измениться.
def function_5(my_list: list) -> list:
    '''
    :param my_list: список из 3-х элементов заданный на уровне модуля
    :return: измененный список
    '''
    list_new = my_list.copy()
    list_new.append('a')
    return list_new


my_list = [1, 2, 3]
print(my_list)
print(function_5(my_list))


# 6.Функция проверяющая тип данных
# Написать функцию которая принимает на вход список из чисел, строк и таплов
# Функция должна вернуть сколько в списке элементов приведенных данных
def function_6(list_data: list) -> dict:
    '''
    :param list_data: входной список
    :return: слорь, где ключ - тип, а значение - количество
    '''
    types = [type(element) for element in list_data]
    return {element_type: types.count(element_type) for element_type in types}


print(function_6([1, 2, 'a', (1, 2), 'b']))
# 7.Написать пример чтобы hash от объекта 1 и 2 были одинаковые, а id разные
g = 123
h = 123.0
print('id g:', id(g), 'id h:', id(h))
print('hash g:', hash(g), 'hash h:', hash(h))


# 8. Написать функцию, которая проверяет есть ли в списке объект,
# которые можно вызвать
def fuction_8(list: list) -> list:
    '''
    :param list: список
    :return: если в этом списке объект, который можно вызвать
    '''
    return bool(filter(callable, list))


list = ['one', 2, 3, {}, [1, 6], str]
print(fuction_8(list))
