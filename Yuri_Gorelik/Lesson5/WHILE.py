# 1.Напишите скрипт на языке программирования Python, выводящий ряд чисел
# Фибоначчи (числовая последоватьность в которой числа начинаются
# с 1 и 1 или же и 0 и 1, пример: 0, 1, 1, 2, 3, 5, 8, 13, 21, 34, 55, 89,
# 144, 233, ...). Запустите его на выполнение. Затем измените код так, чтобы
# выводился ряд чисел Фибоначчи, начиная с пятого члена ряда и заканчивая
# двадцатым.
i = 0
j = 1
fib = []

# 2.Напишите цикл, выводящий ряд четных чисел от 0 до 20.
# Затем, каждое третье число в ряде от -1 до -21
i = 0
while i <= 20:
    if i % 2 == 0:
        print(i)
    i += 1

j = -21
while j <= -1:
    print(j)
    j += 3
