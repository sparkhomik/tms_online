import random
import string

# 1. Быки и коровы.В классическом варианте игра рассчитана на двух игроков.
# Каждый из игроков задумывает и записывает тайное 4-значное число с
# неповторяющимися цифрами. Игрок, который начинает игру по жребию,
# делает первую попытку отгадать число. Попытка — это 4-значное число
# с неповторяющимися цифрами, сообщаемое противнику. Противник сообщает
# в ответ, сколько цифр угадано без совпадения с их позициями в тайном числе
# (то есть количество коров) и сколько угадано вплоть до позиции в тайном
# числе (то есть количество быков).

bulls_cows_user = [int(a) for a in input('input 4 digits - ')]
bulls_cows_comp_change = (random.sample(range(0, 10), 4))

bulls = 0
cows = 0

for x in list(bulls_cows_user):
    if x in bulls_cows_comp_change:
        # is here
        # not here
        if list(bulls_cows_user).index(x) == list(
                bulls_cows_comp_change).index(x):
            bulls += 1
        else:
            cows += 1
    else:
        continue

if bulls == 4:
    print('You win!')
elif bulls == 1:
    bulls_message = f"You have {bulls} bull "
elif 2 <= bulls <= 4:
    bulls_message = f"You have {bulls} bulls "
else:
    bulls_message = f"You don't have {bulls} bulls "

if cows == 1:
    cows_message = f"and you have {cows} cow"
elif 2 <= cows <= 4:
    cows_message = f"and you have {cows} cows"
else:
    cows_message = f"and you don't have {cows} cows"

print(bulls_message + cows_message)
# 2.Создайте программу, которая, принимая массив имён, возвращает строку
# описывающая количество лайков (как в Facebook).
names = input('Введите имена через запятую:')
names_list = names.split(', ')
if names_list[0] == '':
    print('No one likes this')
elif len(names_list) == 1:
    print(f'{names} likes this')
elif len(names_list) <= 3:
    print(f'{", ".join(names_list[:-1])} and {names_list[-1]} like this')
elif len(names_list) > 3:
    print(f'{", ".join(names_list[:2])} and {len(names_list[2:])} '
          f'others like this')
# 3. Напишите программу, которая перебирает последовательность от 1 до 100.
# Для чисел кратных 3 она должна написать: "Fuzz" вместо печати числа,
# а для чисел кратных 5  печатать "Buzz". Для чисел которые кратны 3 и 5
# надо печатать "FuzzBuzz". Иначе печатать число.
i = 1
while i <= 100:
    if i % 3 == 0 and i % 5 == 0:
        print('FuzzBuzz')
    elif i % 3 == 0:
        print('Fuzz')
    elif i % 5 == 0:
        print('Buzz')
    else:
        print(i)
    i += 1
# 4.Напишите код, который возьмет список строк и пронумерует их
list_1 = ['one', 'two', 'three', 'nine', 'ten']
new_list = []
for i, item in enumerate(list_1):
    j = (str(i + 1) + ': ' + list_1[i])
    new_list.append(j)
print(new_list)
# 5. Проверить, все ли элементы одинаковые
a = [1, 1, 1]
print(all(i == a[0] for i in a))
b = [1, 2, 1]
print(all(i == b[0] for i in a))
c = ['a', 'a', 'a']
print(all(i == c[0] for i in c))
d = []
print(all(i == d[0] for i in d))
# 6. Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
while True:
    x = input('Введите текс латиницей: ')
    x1 = list(x)
    y = []
    boolean = True
    if not all(i in string.ascii_letters for i in x1):
        print('Ввод только латинтцей')
    elif len(x1) == 0:
        print('Пустой ввод. Введите текс латиницей')
    else:
        for j in range(len(x1)):
            if x1[j].isupper():
                y.append((x1[j]))
            if len(y) > 0:
                boolean = False
                print([boolean, y])
            break
# 7. Сложите все числа в списке, они могут быть отрицательными,
# если список пустой вернуть 0
a = []
b = [1, 2, 3]
c = [1.1, 2.2, 3.3]
d = [4, 5, 6]
e = range(101)
print(sum(a))
print(sum(b))
print(sum(c))
print(sum(d))
print(sum(e))
