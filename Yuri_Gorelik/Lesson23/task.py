from selenium.webdriver import Safari
from selenium.webdriver.common.by import By
import logging.config

"""
Для следующего приложения: https://www.saucedemo.com/ необходимо найти
- все локаторы для функциональных элементов
- локаторы для окна Login
- залогиниться и найти все элементы в каталоге (standard_user)
- вывести информацию по всем товарам, которая возвращает их название и цену
- для всех остальных элементов создать методы, которые находят их на странице
и возвращают элемент
- предусмотреть логирование выполненных действий
"""

# параметры логирования
logging.basicConfig(format='%(asctime)s-%(message)s',
                    level=logging.DEBUG, filename='logs.log')
logger = logging.getLogger()


def start_safari_driver():
    """
    Функция запуска браузера
    :return: Chrome browser
    """
    browser = Safari()
    return browser


def log_in(driver) -> None:
    """
    Авторизация пользователя на сайте.
    """
    username = driver.find_element(By.CSS_SELECTOR, "#user-name")
    username.send_keys("standard_user")
    password = driver.find_element(By.CSS_SELECTOR, "#password")
    password.send_keys("secret_sauce")
    login_button = driver.find_element(By.CSS_SELECTOR, "#login-button")
    login_button.click()


def products_info(driver) -> str:
    """
    Вывод информации по товарам, включая их название и цену.
    :return: список товаров (название и цена)
    """
    title = driver.find_element(By.XPATH,
                                "//div[@class='inventory_item_name']")
    prices = driver.find_element(By.XPATH,
                                 "//div[@class='inventory_item_price']")
    products = []
    for number in range(len(title)):
        products.append(f"{title[number].text} - {prices[number].text}")
    return '\n'.join(products)


def header_elements(driver):
    """
    Поиск всех элементов в хидере
    :return: корзина, актив меню, логотип
    """
    shopping_cart = driver.find_element(By.CSS_SELECTOR,
                                        ".shopping_cart_link")
    active_menu = driver.find_element(By.CSS_SELECTOR,
                                      "#react-burger-menu-btn")
    app_logo = driver.find_element(By.CSS_SELECTOR, ".app_logo")
    return shopping_cart, active_menu, app_logo


def product_catalog_elements(driver):
    """
    Поиск элементов в каталоге товаров
    :return: кнопка осртировки, кнопка 'добавить в корзину'
    """
    add_btn = driver.find_element(By.CSS_SELECTOR, ".btn_inventory")
    sort_btn = driver.find_element(By.CSS_SELECTOR,
                                   ".product_sort_container")
    return add_btn, sort_btn


def footer_elements(driver):
    """
    Поиск всех элементов в футере
    :return: иконки twitter, facebook, linkedin
    """
    twitter_xpath = "//a[@href='https://twitter.com/saucelabs']"
    twitter_element = driver.find_element(By.XPATH, twitter_xpath)
    facebook_xpath = "//a[@href='https://www.facebook.com/saucelabs']"
    facebook_element = driver.find_element(By.XPATH, facebook_xpath)
    linked_xpath = "//a[@href='https://www.linkedin.com/company/sauce-labs/']"
    linkedin_element = driver.find_element(By.XPATH, linked_xpath)
    return twitter_element, facebook_element, linkedin_element


def get_log():
    """
    Функция получения содержимого log файла.
    :return: логи из файла
    """
    file_obj = open("logs.log", "r")
    logs = file_obj.read()
    file_obj.close()
    return logs


if __name__ == "__main__":
    # создание экземпляра класса драйвера Chrome
    safari_driver = start_safari_driver()
    # переход на нужный урл сайта
    safari_driver.get("https://www.saucedemo.com/")
    safari_driver.implicitly_wait(10)
    # логин юзера
    log_in(safari_driver)
    # вывод информации по товарам
    print(products_info(safari_driver))
    # поиск всех элементов в хидере
    header_elements(safari_driver)
    # поиск всех элементов в каталоге продуктов
    product_catalog_elements(safari_driver)
    # поиск всех элементов в футере
    footer_elements(safari_driver)
    # закрытие браузера
    safari_driver.close()
    # вывод логов из файла logs.log
    print(get_log())
