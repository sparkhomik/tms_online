import random

'''# Игра "Крестики-Нолики"
В этой игре человек играет против компьютера
Изначально компьютер рисует поле на котором будете играть, после чего просит
выбрать, за какую команду играет человек: 0 или Х
И дальше сделать ход
После чего компьютер делает ход и рисует обновленное поле
Игра продолжается до последнего хода, либо пока кто то не победит'''
board = [' ' for x in range(9)]


def printboard(board):
    '''
    Отрисовка доски для игры
    '''
    print(
        ' ' + board[0] + ' ' + '|' + ' ' + board[1] + ' ' + '|' + ' ' + board[
            2] + ' ')
    print('___|___|___')
    print(
        ' ' + board[3] + ' ' + '|' + ' ' + board[4] + ' ' + '|' + ' ' + board[
            5] + '  ')
    print('___|___|___')
    print(
        ' ' + board[6] + ' ' + '|' + ' ' + board[7] + ' ' + '|' + ' ' + board[
            8] + ' ')
    print('   |   |   ')


def character_input(character: str, place: int):
    """
    Функция вставки буквы
    :character: символ
    :place: место
    """
    board[place] = character


def checkfree(place) -> bool:
    """
    Функция проверки ячейки на доступность
    Возвращает True если ход возможен
    :pos: место
    """
    return board[place] == ' '


def checkwinner(a, b) -> bool:
    """
    Функция проверки победителя.
    Функция учитывает позицию на доске и текщий ход игрока. Возвращает
    True, если игрок выиграл
    :a: ячейка доски
    :b: значение
    """
    return (a[0] == b and a[1] == b and a[2] == b) or \
           (a[3] == b and a[4] == b and a[5] == b) or \
           (a[6] == b and a[7] == b and a[8] == b) or \
           (a[0] == b and a[3] == b and a[6] == b) or \
           (a[1] == b and a[4] == b and a[7] == b) or \
           (a[2] == b and a[5] == b and a[8] == b) or \
           (a[0] == b and a[4] == b and a[8] == b) or \
           (a[2] == b and a[4] == b and a[6] == b)


def usermove() -> None:
    """
    Функция хода игрока.
    Пользователь выбирает позицию от 0-8.
    В случае невозможности ввода отображается соответствующее
    сообщение.
    """
    run = True
    while run:
        move = input('Выберите позицию для \'X\' от 0 до 8: ')
        try:
            move = int(move)
            if 0 <= move <= 8:
                if checkfree(move):
                    run = False
                    character_input('X', move)
                else:
                    print('Эта ячейка занята!')
            else:
                print('Пожалуйста введите числа от 0 до 8!')
        except ValueError:
            print('Вы можете ввести только цифры!')


def compmove() -> int:
    """
    Функция хода компьютера
    Получает копию содержимого доски и букву, которой ходит компьютер.
    Исходя из этого определяет куда двигаться и возвращает ход
    """

    # Определяем возможность победы на следукющем ходу
    possible_moves = [x for x, letter in enumerate(board) if
                      letter == ' ' and x != 0]
    move = 0

    for let in ['O', 'X']:
        for i in possible_moves:
            board_copy = board[:]
            board_copy[i] = let
            if checkwinner(board_copy, let):
                move = i
                return move

    # Попытаемся занять один из углов, если они свободны
    corners_open = []
    for i in possible_moves:
        if i in [0, 2, 6, 8]:
            corners_open.append(i)

    if len(corners_open) >= 0:
        move = selectrandom(corners_open)
        return move

    # Занимаем центр, если он свободен
    if 4 in possible_moves:
        move = 4
        return move

    # Занимаем остальные ячейки, если они свободны
    edges_open = []
    for i in possible_moves:
        if i in [1, 3, 5, 7]:
            edges_open.append(i)

    if len(edges_open) >= 0:
        move = selectrandom(edges_open)

    return move


def selectrandom(list_sel: list) -> int:
    """
    Функция рандомного выбора позиции
    :li: список позиций
    """
    ln = len(list_sel)
    r = random.randrange(0, ln)
    return list_sel[r]


def isboardfull(board) -> bool:
    """
    Функция проверки заполненности доски
    Возвращаем True, если все клетки на доске были заняты.
    """
    if board.count(' ') > 1:
        return False
    else:
        return True


def start() -> None:
    """
    Функция запуска игры и вывод итогов.
    """
    separator = '\n'
    print('Добро пожаловать в игру tic tac toe! {0}Расположение доски: {0}'
          ' 0 | 1 | 2 {0}---|---|--- {0} 3 | 4 | 5 {0}---|---|--- {0}'
          ' 6 | 7 | 8'.format(separator))

    while not (isboardfull(board)):
        if not (checkwinner(board, 'O')):
            usermove()
            printboard(board)
        else:
            print('Очень жаль, вы проиграли!')
            break

        if not (checkwinner(board, 'X')):
            move = compmove()
            if move == 0:
                print('Ничья!')
            else:
                move = compmove()
                character_input('O', move)
                print('Соперник поставил \'O\' на позицию', move, ':')
                printboard(board)
        else:
            print('Вы выиграли! Поздравляю!')
            break

    if isboardfull(board):
        print('Ничья!')


if __name__ == "__main__":
    start()
