# 1. Напишите декоратор, который проверял бы тип параметров функции,
# конвертировал их если надо и складывал

def typed(element):
    def decorator_wrapper(func):
        def wrapper(*args):
            args = map(element, args)
            return func(*args)

        return wrapper

    return decorator_wrapper


@typed(str)
def add_two_symbols(a, b) -> str:
    return a + b


print(add_two_symbols('3', 5))
print(add_two_symbols(5, 5))
print(add_two_symbols('a', 'b'))


@typed(int)
def add_three_symbols(a, b, c) -> int:
    return a + b + c


print(add_three_symbols(5, 6, 7))
print(add_three_symbols("3", 5, 0))


@typed(float)
def add_three_symbols(a, b, c) -> float:
    return a + b + c


print(add_three_symbols(0.1, 0.2, 0.4))


# 2.Лексикографическое возрастание
# На вход подаётся некоторое количество (не больше сотни) разделённых пробелом
# целых чисел (каждое не меньше 0 и не больше 19).
# Выведите их через пробел в порядке лексикографического возрастания названий
# этих чисел в английском языке.
def decorator(func):
    def wrapper(*args):
        given_variable = []
        sought_variable = []
        number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three',
                        4: 'four', 5: 'five', 6: 'six', 7: 'seven',
                        8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven',
                        12: 'twelve', 13: 'thirteen', 14: 'fourteen',
                        15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
                        18: 'eighteen', 19: 'nineteen'}
        for entered_variable in args:
            for k, v in number_names.items():
                if entered_variable == k:
                    given_variable.append(v)

        given_variable.sort()
        for alphabetic_var in given_variable:
            for k, v in number_names.items():
                if alphabetic_var == v:
                    sought_variable.append(k)
        #                print("w", sought_variable)
        func(sought_variable)

    return wrapper


@decorator
def numbers(input_str):
    print(input_str)


numbers(1, 2, 7, 14, 5)


# 3 программа ожидает от пользователя ввода
# математического выражения и правильно его трактует
# p.s. каждый элемент вводиться через пробел в матиматическом выражении
def decorator(func):
    def wrapper(operation):
        list_with_numbers = []
        list_with_symbols = []
        operation_list = operation.split()
        for i in operation_list:
            if i != "+" and i != "-" and i != "*" and i != "/":
                list_with_numbers.append(i)  # ["1", "3"]
            elif i == "+" or i != "-" or i != "*" or i != "/":
                list_with_symbols.append(i)  # ["+"]
        for y in list_with_symbols:
            if y == "+":
                answer = int(list_with_numbers[0]) + int(list_with_numbers[1])
                print(answer)
            elif y == "-":
                answer = int(list_with_numbers[0]) - int(list_with_numbers[1])
                print(answer)
            elif y == "*":
                answer = int(list_with_numbers[0]) * int(list_with_numbers[1])
                print(answer)
            elif y == "/":
                answer = int(list_with_numbers[0]) / int(list_with_numbers[1])
                print(answer)
        func(operation)

    return wrapper


@decorator
def my_operation(operation_input):
    return operation_input


str1 = input("Введите математическое выражения согласно условию:")
my_operation(str1)
