from random import randrange

"""
Модуль игры крестики-нолики находится в пакете tic tac toe.
Пользователь выбирает позицию на доске от 1-9, затем ход компьютера.
Игра длится до победы 1 из игроков
"""

board = [' ' for x in range(10)]


def insertletter(letter: str, pos: int):
    """
    Функция вставки буквы
    :letter: буква
    :pos: позиция
    """
    board[pos] = letter


def checkfree(pos) -> bool:
    """
    Функция проверки свободной ячейки.
    Возвращает True если ход возможен
    :pos: позиция
    """
    return board[pos] == ' '


def printboard(board):
    """
    Функция отрисовки доски
    """
    print('   |   |')
    print(' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   |')
    print('-----------')
    print('   |   |')
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   |')


def checkwinner(a, b) -> bool:
    """
    Функция проверки победителя.
    Функция учитывает позицию на доске и текщий ход игрока. Возвращает
    True, если игрок выиграл
    :a: ячейка доски
    :b: значение
    """
    return (a[7] == b and a[8] == b and a[9] == b) or\
           (a[4] == b and a[5] == b and a[6] == b) or\
           (a[1] == b and a[2] == b and a[3] == b) or\
           (a[1] == b and a[4] == b and a[7] == b) or\
           (a[2] == b and a[5] == b and a[8] == b) or\
           (a[3] == b and a[6] == b and a[9] == b) or\
           (a[1] == b and a[5] == b and a[9] == b) or\
           (a[3] == b and a[5] == b and a[7] == b)


def playermove() -> None:
    """
    Функция хода игрока.
    Пользователь выбирает позицию от 1-9.
    В случае невозможности ввода отображается соответствующее
    сообщение.
    """
    run = True
    while run:
        move = input('Выберите позицию для \'X\' (1-9): ')
        try:
            move = int(move)
            if 0 < move < 10:
                if checkfree(move):
                    run = False
                    insertletter('X', move)
                else:
                    print('Эта ячейка занята!')
            else:
                print('Пожалуйста введите числа от 1-9!')
        except ValueError:
            print('Вы можете ввести только цифры!')


def compmove() -> int:
    """
    Функция хода компьютера
    Получает копию содержимого доски и букву, которой ходит компьютер.
    Исходя из этого определяет куда двигаться и возвращает ход
    """

    # Определяем возможность победы на следукющем ходу
    possible_moves = [x for x, letter in enumerate(board) if
                      letter == ' ' and x != 0]
    move = 0

    for let in ['O', 'X']:
        for i in possible_moves:
            board_copy = board[:]
            board_copy[i] = let
            if checkwinner(board_copy, let):
                move = i
                return move

    # Попытаемся занять один из углов, если они свободны
    corners_open = []
    for i in possible_moves:
        if i in [1, 3, 7, 9]:
            corners_open.append(i)

    if len(corners_open) > 0:
        move = selectrandom(corners_open)
        return move

    # Занимаем центр, если он свободен
    if 5 in possible_moves:
        move = 5
        return move

    # Занимаем остальные ячейки, если они свободны
    edges_open = []
    for i in possible_moves:
        if i in [2, 4, 6, 8]:
            edges_open.append(i)

    if len(edges_open) > 0:
        move = selectrandom(edges_open)

    return move


def selectrandom(li: list) -> int:
    """
    Функция рандомного выбора позиции
    :li: список позиций
    """
    ln = len(li)
    r = randrange(0, ln)
    return li[r]


def isboardfull(board) -> bool:
    """
    Функция проверки заполненности доски
    Возвращаем True, если все клетки на доске были заняты.
    """
    if board.count(' ') > 1:
        return False
    else:
        return True


def start() -> None:
    """
    Функция запуска игры и вывод итогов.
    """
    separator = '\n'
    print('Добро пожаловать в игру tic tac toe! {0}Расположение доски: {0}'
          ' 1 | 2 | 3 {0}---|---|--- {0} 4 | 5 | 6 {0}---|---|--- {0}'
          ' 7 | 8 | 9'.format(separator))

    while not (isboardfull(board)):
        if not (checkwinner(board, 'O')):
            playermove()
            printboard(board)
        else:
            print('Очень жаль, вы проиграли!')
            break

        if not (checkwinner(board, 'X')):
            move = compmove()
            if move == 0:
                print('Ничья!')
            else:
                move = compmove()
                insertletter('O', move)
                print('Соперник поставил \'O\' на позицию', move, ':')
                printboard(board)
        else:
            print('Вы выиграли! Поздравляю!')
            break

    if isboardfull(board):
        print('Ничья!')


if __name__ == "__main__":
    start()
