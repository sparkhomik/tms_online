import string

"""
Модуль шифра Цезаря находится в пакете cipher..
Принимает текст для зашифровки.
Каждый символ текста сдвигает на опреденную позицию.
"""

# словарь латинницы
latin_low = string.ascii_lowercase
latin_up = string.ascii_uppercase

# словарь кириллицы
kirill_low = [chr(i) for i in range(ord('а'), ord('я') + 1)]
kirill_up = [chr(i) for i in range(ord('А'), ord('Я') + 1)]


def caesar(operation: str, phrase: str, shift: int) -> str:
    """
    Функция для зашифровки и дешифровки.
    В случае дешифровки shift *= -1
    :operation: выбранная операция
    :phrase: фраза
    :shift: сдвиг
    """
    """
    преобразуем словарь кириллицы в строку
    Определяем язык фразы и присвоим к переменным list_low и list_low
    соответсвующие словари
    """
    if [x for x in kirill_low if x in phrase.lower()]:
        list_low = ''.join(kirill_low)
        list_up = ''.join(kirill_up)
    else:
        list_low = latin_low
        list_up = latin_up
    operation = int(operation)
    ret = ''
    # для дешифровки используем сдвиг сознаком "-"
    if operation == 2:
        shift *= -1
    for letter in phrase:
        if letter in list_low:
            position = list_low.find(letter)
            new_key = (position + shift) % len(list_low)
            ret += list_low[new_key]
        elif letter in list_up:
            position = list_up.find(letter)
            new_key = (position + shift) % len(list_up)
            ret += list_up[new_key]
        else:
            ret += letter
    return ret


def selectoperation():
    """
    Функция запуска программы
    Позволяет юзеру выбрать операцию и запускает шифратор либо дешифратор
    """
    separator = '\n'
    print('Добро пожаловать в Шифр Цезаря! {0} Меню операций: {0}'
          ' 1. Encode {0} 2. Decode'.format(separator))
    operation = input('Введите номер пункта меню: ')
    # создадим валидацию для пропуска данных
    if operation.isdigit() and 1 <= int(operation) <= 2:
        phrase = input('Введите фразу: ')
        if not phrase:
            print('Пустой ввод! Повторите попытку')
        elif phrase.isalpha():
            try:
                shift = int(input('Введите ключ: '))
                print(caesar(operation, phrase, shift))
            except ValueError:
                print('Вы можете ввести только числа!')
        else:
            print('Введите фразу латинницей или кириллицей')
    else:
        print('Выбран неверный пункт меню операции')


if __name__ == "__main__":
    selectoperation()
