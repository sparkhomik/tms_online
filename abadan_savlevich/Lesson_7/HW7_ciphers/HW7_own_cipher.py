"""
Модуль персонального шифра находится в пакете cipher.
Принимает текст для зашифровки.
Каждый символ текста заменяется другим, на той же позиции из алфавита шифра.
"""

alphabet_en = 'ABCDEFGHIJKLMNOPQRSTUVWXYZabcdefghijklmnopqrstuvwxyz'
alphabet_cipher = 'fghijvwxyzabcdekVWXYZAmnopqrstuBCDEKLMNOPQRSTUlFGHIJ'


def encoding(phrase: str) -> str:
    """
    Функция зашифровки строки.
    Для каждого символа в строке определяется его позиция в
    оригинальном алфавите.
    Символ в соответсвующей позиции из алфавита-шифра добавляется в
    строку ret.
    :phrase: строка для зашифровки.
    """
    ret = ''
    for letter in phrase:
        if letter in alphabet_en:
            new = alphabet_en.find(letter)
            ret += alphabet_cipher[new]
        else:
            ret += letter
    return ret


def decoding(phrase: str) -> str:
    """
    Функция расшифровки строки.
    Для каждого символа в строке определяется его позиция в
    алфавите-шифре.
    Символ в соответсвующей позиции из оргинального алфавита добавляется в
    строку ret.
    :phrase: строка для расшифровки.
    """
    ret = ''
    for letter in phrase:
        if letter in alphabet_cipher:
            new = alphabet_cipher.find(letter)
            ret += alphabet_en[new]
        else:
            ret += letter
    return ret


def selectoperation_own():
    """
    Функция запуска программы
    Позволяет юзеру выбрать операцию и запускает шифратор либо дешифратор
    """
    separator = '\n'
    print('Добро пожаловать в Шифр от А.К.! {0} Меню операций: {0}'
          ' 1. Encode {0} 2. Decode'.format(separator))
    operation = input('Введите номер пункта меню: ')
    if operation.isdigit() and 1 <= int(operation) <= 2:
        # создадим валидацию для пропуска
        phrase = input('Введите фразу латинницей: ')
        if not phrase:
            print('Пустой ввод! Повторите попытку')
        elif not phrase.isalpha():
            print('Введите фразу латинницей')
        else:
            if operation == '1':
                print(encoding(phrase))
            else:
                print(decoding(phrase))
    else:
        print('Выбран неверный пункт меню операции')


if __name__ == "__main__":
    selectoperation_own()
