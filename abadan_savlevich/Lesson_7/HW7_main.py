from HW7_ciphers.HW7_ceasar_cipher import selectoperation
from HW7_ciphers.HW7_own_cipher import selectoperation_own
from HW7_tic_tac_toe.HW7_tic_tac_toe import start
from random import randint
import time

"""
Главный модуль.
Позволяют юзеру выбрать 1 из 3 предложенных программ.
"""


def module() -> str:
    """
    Функция выбора пункта операции.
    Цикл внутри функции отработает 3 раза при неверном вводе пункта меню,
    при вводе неверного пункта меню 4й раз вернется случайный пункт меню
    1, 2 или 3.
    :return: возвращает строку с выбранным пунктом меню.
    """
    separator = '\n'
    print('Добро пожаловать! {0} Меню операций: {0}'
          ' 1. Крестики нолики {0} 2. Шифр Цезаря {0}'
          ' 3. Шифр от А.К. {0} 4. Выход'.format(separator))
    operation = input('Введите номер пункта меню: ')
    count = 0
    while operation not in ('1', '2', '3', '4'):
        print('Повторите попытку!')
        operation = input('Введите номер пункта меню: ')
        count += 1
        # Если 3 раза юзер ошибся, система рандомом запускает программу 1-3
        if count == 3:
            operation = str(randint(1, 3))
            print(f'Вы ошиблись {count} раза! \n Запускаю игру под номером'
                  f' {operation}')
            time.sleep(1)
    return operation


def main():
    """
    Функция запуска программы состоит из цикла, запускает в зависмости
    от номера операции
    """
    operation = module()
    while True:
        if operation == '1':
            print('Запуск...')
            time.sleep(1)
            print(start())
            break
        elif operation == '2':
            print('Запуск...')
            time.sleep(1)
            print(selectoperation())
            break
        elif operation == '3':
            print('Запуск...')
            time.sleep(1)
            print(selectoperation_own())
            break
        elif operation == '4':
            print('До скорой встречи!')
            break


if __name__ == "__main__":
    main()
