from collections import OrderedDict
from collections import Counter

# task_1
# перевести строку в массив
a = "Robin Singh"
b = "I love arrays they are my favorite"
a = a.split()
b = b.split()
print(list(a))
print(list(b))

# task_2
# объединить список и 2 строки
name = ['Ivan', 'Ivanou']
city = "Minsk"
country = "Belarus"

# переводим список в строку
name = ' '.join(name)

# объединяем строки
print(f'Привет, {name}! Добро пожаловать в {city} {country}')

# task_3
# сделать из списка строку
task3_list = ["I", "love", "arrays", "they", "are", "my", "favorite"]
task3_str = ' '.join(task3_list)
print(task3_str)

# task_4
'''
создать список из 10 элементов, вставьте на 3-ю позицию новое значение,
удалите элемент из списка под индексом 6
'''
task4_list = [1, 'v', 3, 4, 55, 'hi', (1, 54), 7, 10, [7, 0]]

# вставим на 3-ю позицию значение "new"
task4_list.insert(2, "new")

# удалим элемент под индексом 6
del task4_list[6]
print(task4_list)

# task_5
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ''}

# создать словарь, который будет содержать в себе все элементы обоих словарей
c = {**a, **b}
print(c)

# обновить словарь 'a' элементами из словаря 'b'
a.update(b)
print(a)

# проверить, что все значения в словаре “a” не пустые либо не равны нулю
d = all(a.values())
print(d)
"""
False, так как мы обновили словарь 'a' элементами из словаря 'b', где
последний элемент ''
"""

# проверить, что есть хотя бы одно пустое значение
print(not all(a.values()))

# отсортировать словарь по алфавиту в обратном порядке
a = a.items()
a = tuple(a)
a = OrderedDict(a[::-1])
a = dict(a)
print(a)

# изменить значение под одним из ключей и вывести все значения
a['a'] = 200
for v in a.values():
    print(v)

# task_6
# создать список из элементов
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]

# вывести только уникальные значения и сохранить их в отдельную переменную
b = set(list_a)
print(b)

# добавить в полученный объект значение 22
b.add(22)
print(b)

# сделать list_a неизменяемым
b = tuple(b)
print(b)

# измерить длину объекта
print(len(b))

# Задачи на закрепление форматирования
# task_1
a = 10
b = 25

# метод format()
print("Summ is {sum}, and diff = {diff}".format(sum=a + b, diff=a - b))

# метод f-strings
print(f"Summ is {a + b}, and diff = {a - b}")

# task_2
list_of_children = ['Sasha', 'Vasia', 'Nikalai']

# метод format()
print("First child is {Sasha}, second is {Vasia},"
      " and last one – {Nikalai}".format(Sasha=list_of_children[0],
                                         Vasia=list_of_children[1],
                                         Nikalai=list_of_children[2]))

# метод f-strings
print(f"First child is {list_of_children[0]}, second is {list_of_children[1]},"
      f" and last one – {list_of_children[2]}")

# tasks with *
# task_1

a = [1, 5, 2, 9, 2, 9, 1]

# подсчет количества элементов с помощью Counter
a = Counter(a)

# вывести ключ с минимальным значением
print(min(a, key=a.get))

# task_2
# найти самую частую букву в тексте
text = 'To be, or not to be, that is the question - W. Shakespeare'

# Первый способ решения
# перевести строку в список с элементами, разделенными ","
text = text.split(',')

# перевести список в строку, где исключены ","
text = (''.join(text))

# перевести строку в список с элементами, разделенными "-"
text = text.split('-')

# перевести список в строку, где исключены "-"
text = (''.join(text))

# перевести строку в список с элементами, разделенными "."
text = text.split('.')

# перевести список в строку, где исключены "."
text = (''.join(text))

# перевести строку в список с элементами, разделенными пробелом
text = text.split(' ')

# перевести список в строку, где исключены пробелы
text = (''.join(text))

# отсортировать по алфавиту в нижнем регистре
a = sorted(text.lower())

# подсчет количества элементов с помощью Counter
a = Counter(a)

print('Самая частая буква в тексте:', max(a, key=a.get))

# Второй способ решения
text2 = 'To be, or not to be, that is the question - W. Shakespeare'

# убираем из строки все знаки препинания и пробелы
text_string = text2.replace(' ', '').replace('-', '').replace('.', '').\
    replace(',', '')

# отсортировать по алфавиту в нижнем регистре
b = sorted(text_string.lower())

# подсчет количества элементов с помощью Counter
b = Counter(b)

print('Самая частая буква в тексте:', max(b, key=b.get))
