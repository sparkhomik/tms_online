from selenium.webdriver import Chrome
import logging.config

"""
1.	Для следующего приложения: https://www.saucedemo.com/
2.	необходимо найти все локаторы для функциональных элементов, т.е. иконки,
кнопки, поля ввода и т.д.
a.	Нужно найти локаторы для окна Login
Залогиниться и найти все элементы в каталоге (standard_user)
b.	Вывести информацию по всем товарам, которая возвращает их название и цену
c.	Для всех остальных элементов создать методы, которые находят их на странице
и возвращают элемент
d.	* Предусмотреть логирование выполненных действий
"""

# параметры логирования
logging.basicConfig(format='%(asctime)s-%(message)s',
                    level=logging.DEBUG, filename='logs.log')
logger = logging.getLogger()


def start_chrome_driver():
    """
    Функция запуска браузера
    :return: Chrome browser
    """
    browser = Chrome(executable_path='/Users/abadankarlieva/Documents'
                                     '/chrome_driver/chromedriver')
    return browser


def log_in(driver) -> None:
    """
    Авторизация пользователя на сайте.
    :param driver: экземпляр класса драйвера Chrome
    """
    username = driver.find_element_by_css_selector("#user-name")
    username.send_keys("standard_user")
    password = driver.find_element_by_css_selector("#password")
    password.send_keys("secret_sauce")
    login_button = driver.find_element_by_css_selector("#login-button")
    login_button.click()


def products_info(driver) -> str:
    """
    Вывод информации по товарам, включая их название и цену.
    :param driver: экземпляр класса драйвера Chrome
    :return: список товаров (название и цена)
    """
    title = driver.find_elements_by_xpath(
        "//div[@class='inventory_item_name']")
    prices = driver.find_elements_by_xpath(
        "//div[@class='inventory_item_price']")
    products = []
    for number in range(len(title)):
        products.append(f"{title[number].text} - {prices[number].text}")
    return '\n'.join(products)


def header_elements(driver):
    """
    Поиск всех элементов в хидере
    :param driver: экземпляр класса драйвера Chrome
    :return: корзина, бургер меню, логотип
    """
    shopping_cart = driver.find_element_by_css_selector(".shopping_cart_link")
    burger_menu = driver.find_element_by_css_selector("#react-burger-menu-btn")
    app_logo = driver.find_element_by_css_selector(".app_logo")
    return shopping_cart, burger_menu, app_logo


def product_catalog_elements(driver):
    """
    Поиск элементов в каталоге товаров
    :param driver: экземпляр класса драйвера Chrome
    :return: кнопка осртировки, кнопка 'добавить в корзину'
    """
    add_btn = driver.find_element_by_css_selector(".btn_inventory")
    sort_btn = driver.find_element_by_css_selector(".product_sort_container")
    return add_btn, sort_btn


def footer_elements(driver):
    """
    Поиск всех элементов в футере
    :param driver: экземпляр класса драйвера Chrome
    :return: иконки twitter, facebook, linkedin
    """
    twitter_xpath = "//a[@href='https://twitter.com/saucelabs']"
    twitter_element = driver.find_element_by_xpath(twitter_xpath)
    facebook_xpath = "//a[@href='https://www.facebook.com/saucelabs']"
    facebook_element = driver.find_element_by_xpath(facebook_xpath)
    linked_xpath = "//a[@href='https://www.linkedin.com/company/sauce-labs/']"
    linkedin_element = driver.find_element_by_xpath(linked_xpath)
    return twitter_element, facebook_element, linkedin_element


def get_log():
    """
    Функция получения содержимого log файла.
    :return: логи из файла
    """
    file_obj = open("logs.log", "r")
    logs = file_obj.read()
    file_obj.close()
    return logs


if __name__ == "__main__":
    # создание экземпляра класса драйвера Chrome
    chrome_driver = start_chrome_driver()

    # переход на нужный урл сайта
    chrome_driver.get("https://www.saucedemo.com/")
    chrome_driver.implicitly_wait(10)

    # логин юзера
    log_in(chrome_driver)

    # вывод информации по товарам
    print(products_info(chrome_driver))

    # поиск всех элементов в хидере
    header_elements(chrome_driver)

    # поиск всех элементов в каталоге продуктов
    product_catalog_elements(chrome_driver)

    # поиск всех элементов в футере
    footer_elements(chrome_driver)

    # закрытие браузера
    chrome_driver.close()

    # вывод логов из файла logs.log
    print(get_log())
