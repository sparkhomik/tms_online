# task_2
"""
Лексикографическое возрастание
На вход подаётся некоторое количество (не больше сотни) разделённых пробелом
целых чисел (каждое не меньше 0 и не больше 19).
Выведите их через пробел в порядке лексикографического возрастания
названий этих чисел в английском языке.
"""

number_names = {0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four',
                5: 'five', 6: 'six', 7: 'seven', 8: 'eight', 9: 'nine',
                10: 'ten', 11: 'eleven', 12: 'twelve', 13: 'thirteen',
                14: 'fourteen', 15: 'fifteen', 16: 'sixteen', 17: 'seventeen',
                18: 'eighteen', 19: 'nineteen'}


def validation(nums: list):
    """
    Функция валидации, для проверки каждого элемента списка
    :param nums: список
    :return: возвращает True или False
    """
    for n in nums:
        try:
            n = int(n)
            if len(nums) <= 100:
                if 19 >= n >= 0:
                    continue
                else:
                    print("Числа должны быть не меньше 0 и не больше 19")
                    return False
            else:
                print("Вы можете ввести не больше 100 чисел")
                return False
        except ValueError:
            print("Вы можете ввести только числа через 1 пробел!")
            return False

    return True


def sorting(e: str):
    """
    Функция сортировки
    :param e: итерируемый элемент списка
    """
    return number_names[int(e)]


def start():
    """
    Функция запуска программы
    :return: вывод отсортированной строки
    """
    nums = input('Введите числа через 1 пробел (от 0 до 19): ')
    nums_list = nums.split(' ')
    if validation(nums_list):
        nums_list.sort(key=sorting)
        return ', '.join(nums_list)
    else:
        start()


if __name__ == '__main__':
    print(start())
