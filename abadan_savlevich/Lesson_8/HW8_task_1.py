from functools import reduce

# task_1
"""
Декоратор типов.
Проверяет передоваемый тип функии, конвертирует в соответсвующий тип
данные и производит сложение.
"""


def typed(data_type):
    """
    Декоратор, принимающий тип аргумента.
    :param data_type: тип данных
    """

    def decorator(func):
        """
        Декоратор, принимающий функцию
        :param func: функция под wrapper
        """

        def wrapper(*args):
            """
            Функция преобразует аргументы в соответствии с data_type
            :param *arg: n-е количество аргументов.
            """
            args = map(data_type, args)
            return func(*args)
        return wrapper
    return decorator


def sum_function(a, b):
    """
    Функция суммирования
    :param a:
    :param b:
    :return:
    """
    return a + b


@typed(str)
def getsum(a, b, c):
    """
    Функция принимает 3 аргумента и суммирует через функцию sum_function.
    :param a: 1-ый аргумент.
    :param b: 2-ой аргумент.
    :param c: 3-ий аргумент.
    :return: сумма аргументов.
    """
    return reduce(sum_function, [a, b, c])


print(getsum(1, 2, '9'))
