import yaml
import json
from pprint import pprint


def get_yaml() -> dict:
    """
    Функция получения содержимого yaml файла.
    :return: словарь содержимого файла.
    """
    file_obj = open("order.yaml", "r")
    content = yaml.safe_load(file_obj)
    file_obj.close()
    return content


def convert_to_json(yaml_file):
    """
    Функция конвертации yaml файла в json файл
    :type yaml_file: yaml файл
    :return: None
    """
    file_obj = open("order.json", "w")
    json.dump(yaml_file, file_obj)


def create_yaml(some_text):
    """
    Функция получения создания yaml файла.
    :param some_text: данные
    :return: None
    """
    file_obj = open('created_yaml.yaml', 'w')
    yaml.dump(some_text, file_obj)


if __name__ == '__main__':
    # содержимое файла сохраняется в объект
    yaml_file = get_yaml()

    # вывод номера заказа
    pprint(yaml_file['invoice'])

    # вывод адреса
    address = yaml_file['bill-to']
    print(*address["address"].values())

    # вывод описание посылки, ее стоимость и кол-во
    for i in (yaml_file['product']):
        print(["description"], i["price"], i["quantity"])

    # конвертация yaml файла в json файл
    convert_to_json(yaml_file)

    # создание yaml файла
    info = ({'name': 'test'}, ['list', 9])
    create_yaml(info)
