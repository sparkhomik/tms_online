import json


def get_json():
    """
    Функция получения содержимого json файла.
    :return: словарь содержимого файла.
    """
    file_obj = open("students.json", "r")
    content = file_obj.read()
    content_json = json.loads(content)
    file_obj.close()
    return content_json


def search_by_property(propertykey: str, propertyvalue: str,
                       json_list: list) -> list:
    """
    Функция фильтрации по параметру
    :param propertyvalue: ключ
    :param propertykey: значение
    :param json_list: список студентов
    :return: список отфильтрованных объектов
    """
    students = []
    filtered = filter(lambda x: propertyvalue in x[propertykey], json_list)
    for item in filtered:
        students.append(item)
    return students


if __name__ == '__main__':
    # содержимое файла сохраняется в объект
    obj_student = get_json()
    # использование фильтра по параметрам: класс
    students_by_class = search_by_property('Class', '3a', obj_student)
    print(students_by_class)
    # использование фильтра по параметрам: имя(часть имени)
    students_by_name = search_by_property('Name', 'Senp', obj_student)
    print(students_by_name)
    # использование фильтра по параметрам: секции
    students_by_club = search_by_property('Club', 'box', obj_student)
    print(students_by_club)
    # использование фильтра по параметрам: полу
    students_by_gender = search_by_property('Gender', 'W', obj_student)
    print(students_by_gender)
