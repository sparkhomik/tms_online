from xml.etree import ElementTree


def get_xml():
    """
    Функция get запроса для получения содержимого файла.
    :return: содержимое файла переведенное из xml формата.
    """
    file = open("library.xml", "r")
    content = ElementTree.fromstring(file.read())
    file.close()
    return content


def search_by_property(propertykey: str, propertyvalue: str, xml_list) -> list:
    """
    Функция фильтрации по параметру
    :param propertyvalue: ключ
    :param propertykey: значение
    :param xml_list: список книг
    :return: список отфильтрованных объектов
    """
    books = []
    for book in xml_list:
        tag_name = book.find(propertykey)
        if propertyvalue in tag_name.text:
            books.append(book.attrib["id"])
    return books


if __name__ == '__main__':
    # содержимое файла сохраняется в объект
    root = get_xml()
    # использование фильтра по параметрам: автору(часть имени)
    book_by_author = search_by_property('author', 'Robert', root)
    print(book_by_author)
    # использование фильтра по параметрам: цене
    book_by_price = search_by_property('price', '5.95', root)
    print(book_by_price)
    # использование фильтра по параметрам: заголовку
    book_by_title = search_by_property('title', 'Midnight Rain', root)
    print(book_by_title)
    # использование фильтра по параметрам: описанию(часть текста)
    book_by_description = search_by_property('description',
                                             'A former architect', root)
    print(book_by_description)
