from requests import get, post, put, delete
import json

link = "https://fakerestapi.azurewebsites.net/api/v1/"


def get_authors() -> list:
    """
    Функция get запроса для получения всех авторов.
    :return:
    """
    response = get(f"{link}Authors")
    return json.loads(response.content)


def get_authors_by_id(author_id: int) -> str:
    """
    Функция get запроса для получения автора по ID.
    :param author_id: ID автора
    :return: данные автора
    """
    response = get(f"{link}/Authors/{author_id}")
    return response.text


def add_book(book: dict):
    """
    Функция post запроса для добавления книги
    :param book: Json данные книги
    :return: статус
    """
    response = post(f"{link}Books", json=book)
    return response


def add_user(user_data: dict):
    """
    Функция post запроса для юзера
    :param user_data: Json данные юзера
    :return: статус
    """
    response = post(f"{link}Users", json=user_data)
    return response


def update_book_data(book_data: dict, book_id: int):
    """
    Функция put запроса для обновления книги
    :param book_data: Json данные книги
    :param book_id: ID книги
    :return: статус
    """
    response = put(f"{link}Books/{book_id}", json=book_data)
    return response


def remove_user(user_id: int):
    """
    Функция delete запроса для удаления юзера
    :param user_id: ID юзера
    :return: статус
    """
    response = delete(f"{link}Users/{user_id}")
    return response


if __name__ == '__main__':
    # получение всех авторов
    all_authors = get_authors()
    print(all_authors)

    # получение автора по ID
    author_by_id = get_authors_by_id(1)
    print(author_by_id)

    # добавление книги
    book = {
        "id": 206,
        "title": "Test",
        "description": "test",
        "pageCount": 1,
        "excerpt": "test",
        "publishDate": "2021-10-24T19:33:34.799Z"
    }
    add_book = add_book(book)
    print(add_book)

    # добавление юзера
    user = {
        "id": 7,
        "userName": "A",
        "password": "S"
    }
    add_user = add_user(user)
    print(add_user)

    # обновление данных книги
    book_data = {
        "id": 0,
        "title": "title",
        "description": "description",
        "pageCount": 1,
        "excerpt": "excerpt",
        "publishDate": "2021-10-24T11:57:34.571Z"
    }
    updated_book = update_book_data(book_data, 3)
    print(updated_book)

    # удаление юзера
    user_to_remove = remove_user(1)
    print(user_to_remove)
