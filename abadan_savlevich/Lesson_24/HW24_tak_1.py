from selenium.webdriver import Chrome


def chrome_driver():
    browser = Chrome(executable_path='/Users/abadankarlieva/Documents'
                                     '/chrome_driver/chromedriver')
    return browser


def get_element_by_css(driver):
    """
    Функция поиска элемента по css селектору и клик по нему
    :return: None
    """
    driver.find_element_by_css_selector('a.et_pb_button_4').click()


def get_element_by_xpath(driver):
    """
    Функция поиска элемента по xpath селектору и клик по нему
    :return: None
    """
    path = f"{'//a[@class=et_pb_button et_pb_button_4 et_pb_bg_layout_light]'}"
    driver.find_element_by_xpath(f'{path}').click()


def get_element_by_class(driver):
    """
    Функция поиска элемента по class селектору и клик по нему
    :return: None
    """
    driver.find_element_by_class_name('et_pb_button_4').click()


if __name__ == '__main__':
    # создаем объект браузера
    driver = chrome_driver()

    # переход на нужный урл сайта
    driver.get('https://ultimateqa.com/complicated-page/')

    # поиск элемента и клик по css
    get_element_by_css(driver)

    # поиск элемента и клик по xpath
    get_element_by_xpath(driver)

    # поиск элемента и клик по class
    get_element_by_class(driver)

    # закрытие браузера
    driver.close()
