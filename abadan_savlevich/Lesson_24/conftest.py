import time
import pytest
from selenium.webdriver import Chrome


@pytest.fixture(scope='session', autouse=True)
def get_driver():
    """
    Фикстура запуска браузера
    :return: Chrome browser
    """
    driver = Chrome(executable_path='/Users/abadankarlieva/Documents'
                                    '/chrome_driver/chromedriver')
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def get_page_driver(get_driver):
    """
    Открытие страницы https://ultimateqa.com/filling-out-forms/ в браузере.
    :param driver: экземпляр класса драйвера Chrome
    :return: экземпляр класса драйвера Chrome
    """
    get_driver.get('https://ultimateqa.com/filling-out-forms/')
    return get_driver


@pytest.fixture(params=[("Test_name", "Test message test test",
                         "Thanks for contacting us")])
def success_login(get_page_driver, request):
    """
    Фикстура логина, принимающая параметры для успешного логина
    :param get_page_driver: Chrome browser
    :return: Итоговое сообщение и сообщение из параметров
    """
    params = request.param
    get_page_driver.implicitly_wait(15)
    input_1 = get_page_driver.find_element_by_xpath(
        "//input[@id='et_pb_contact_name_0']")
    input_1.send_keys(params[0])
    input_2 = get_page_driver.find_element_by_xpath(
        "//textarea[@id='et_pb_contact_message_0']")
    input_2.send_keys(params[1])
    time.sleep(5)
    css_b = "//button[@name='et_builder_submit_button']"
    get_page_driver.find_element_by_xpath(css_b).click()
    success_message = get_page_driver.find_element_by_xpath(
        "//div[@class='et-pb-contact-message']/p").text
    expected_result = params[2]
    return expected_result, success_message


@pytest.fixture(params=[("Test_name", "", "Message"),
                        ("", "Test message test test", "Name")])
def failed_login(get_page_driver, request):
    """
    Фикстура логина, принимающая параметры для неуспешного логина
    :param get_page_driver: Chrome browser
    :return: Итоговое сообщение и сообщение из параметров
    """
    params = request.param
    get_page_driver.implicitly_wait(15)
    get_page_driver.refresh()
    input_1 = get_page_driver.find_element_by_xpath(
        "//input[@id='et_pb_contact_name_0']")
    input_1.send_keys(params[0])
    input_2 = get_page_driver.find_element_by_xpath(
        "//textarea[@id='et_pb_contact_message_0']")
    input_2.send_keys(params[1])
    time.sleep(5)
    css_b = "//button[@name='et_builder_submit_button']"
    get_page_driver.find_element_by_xpath(css_b).click()
    error_message = get_page_driver.find_elements_by_xpath(
        "//div[@class='et-pb-contact-message']/ul/li")
    result = error_message[0].text
    expected_result = params[2]
    return expected_result, result
