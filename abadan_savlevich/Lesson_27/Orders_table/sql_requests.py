import mysql.connector as mysql

db = mysql.connect(host="localhost", user="root",
                   database="test_db")

'''
Напечатайте номер заказа, дату заказа и количество для каждого заказа,
который продал продавец под номером: 5002
'''


def get_5002(cursor) -> list:
    """
    Функция, которая выводит номер заказа, дату заказа и количество
    для каждого заказа, который продал продавец под номером: 5002
    :param cursor: объект, который делает запросы и получает их результаты
    :return: список заказов от продавца 5002
    """
    cursor.execute("select ord_no, purch_amt, ord_date from orders where"
                   " salesman_id = 5002")
    myresult = cursor.fetchall()
    return myresult


'''
Напечатайте уникальные id продавца(salesman_id). Используйте distinct
'''


def get_unique_id(cursor) -> list:
    """
    Функция вывода уникального id продавца(salesman_id)
    :param cursor: объект, который делает запросы и получает их результаты
    :return: уникальный id продавца
    """
    cursor.execute("select distinct salesman_id from orders")
    unique_id = cursor.fetchall()
    return unique_id


'''
Напечатайте по порядку данные о дате заказа, id продавца, номер заказа,
количество
'''


def get_data_by_order(cursor) -> list:
    """
    Функция вывода данных по порядку: дата заказа, id продавца, номер заказа,
    количество
    :param cursor: объект, который делает запросы и получает их результаты
    :return: список заказов по порядку
    """
    cursor.execute(
        "select ord_date, salesman_id, ord_no, purch_amt from orders")
    order = cursor.fetchall()
    return order


'''
Напечатайте заказы между 70001 и 70007(используйте between, and)
'''


def get_data_between(cursor) -> list:
    """
    Функция вывода заказов между 70001 и 70007
    :type cursor: объект, который делает запросы и получает их результаты
    :return: список заказов между 70001 и 70007
    """
    cursor.execute("select * from orders where ord_no between 70001 and 70007")
    between = cursor.fetchall()
    return between


if __name__ == '__main__':
    # создаем объект, который делает запросы и получает их результаты
    cursor = db.cursor()

    # список заказов от продавца 5002
    print(get_5002(cursor))

    # уникальный id продавца
    print(get_unique_id(cursor))

    # список заказов по порядку
    print(get_data_by_order(cursor))

    # список заказов между 70001 и 70007
    print(get_data_between(cursor))
