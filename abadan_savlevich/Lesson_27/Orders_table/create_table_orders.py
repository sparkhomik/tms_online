import mysql.connector as mysql

db = mysql.connect(host="localhost", user="root",
                   database="test_db")

cursor = db.cursor()
cursor.execute("CREATE TABLE orders"
               "(ord_no INT(255), purch_amt FLOAT, ord_date VARCHAR(255),"
               " customer_id INT(255), salesman_id INT(255))")
