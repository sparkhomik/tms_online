import mysql.connector as mysql

db = mysql.connect(host="localhost", user="root",
                   database="test_db")

cursor = db.cursor()
cursor.execute("CREATE TABLE PC"
               "(PRO_ID INT(255), PRO_NAME VARCHAR(255), PRO_PRICE INT(255),"
               " PRO_COM INT(255))")
