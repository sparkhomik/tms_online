import mysql.connector as mysql

db = mysql.connect(host="localhost", user="root",
                   database="test_db")

'''
Напечатайте общую стоимость компонентов
'''


def get_price_sum(cursor) -> int:
    """
    Функция, которая выводит общую стоимость компонентов
    :param cursor: объект, который делает запросы и получает их результаты
    :return: общая стоимость компонентов
    """
    cursor.execute("select sum(PRO_PRICE) from PC")
    price_sum = cursor.fetchall()
    return price_sum


'''
Напечатайте среднюю стоимость компонентов
'''


def get_avg_price(cursor) -> int:
    """
    Функция, которая выводит среднюю стоимость компонентов
    :param cursor: объект, который делает запросы и получает их результаты
    :return: средная стоимость компонентов
    """
    cursor.execute("select avg(PRO_PRICE) from PC")
    avg_price = cursor.fetchall()
    return avg_price


'''
Напечатайте компоненты с ценой между 200 и 600
'''


def get_price_between(cursor) -> list:
    """
    Функция, которая выводит компоненты с ценой между 200 и 600
    :param cursor: объект, который делает запросы и получает их результаты
    :return: список компонентов с ценой между 200 и 600
    """
    cursor.execute("select * from PC where PRO_PRICE between 200 and 600")
    between = cursor.fetchall()
    return between


'''
Напечатайте название компонента у которого самая высокая цена
'''


def get_max_price(cursor) -> list:
    """
    Функция, которая выводит название компонента, у которого самая высокая цена
    :param cursor: объект, который делает запросы и получает их результаты
    :return: компонент(ы) с самой высокой ценой
    """
    cursor.execute("select PRO_NAME from PC where"
                   " PRO_PRICE = (select max(PRO_PRICE) from PC)")
    order = cursor.fetchall()
    return order


'''
Напечатайте название компонента у которого самая низкая цена
'''


def get_min_price(cursor) -> list:
    """
    Функция, которая выводит название компонента, у которого самая низкая цена
    :param cursor: объект, который делает запросы и получает их результаты
    :return: компонент(ы) с самой низкой ценой
    """

    cursor.execute("select PRO_NAME from PC where"
                   " PRO_PRICE = (select min(PRO_PRICE) from PC)")
    order = cursor.fetchall()
    return order


if __name__ == '__main__':
    # создаем объект, который делает запросы и получает их результаты
    cursor = db.cursor()

    # общая стоимость компонентов
    print(get_price_sum(cursor))

    # средная стоимость компонентов
    print(get_avg_price(cursor))

    # список компонентов с ценой между 200 и 600
    print(get_price_between(cursor))

    # компонент(ы) с самой высокой ценой
    print(get_max_price(cursor))

    # компонент(ы) с самой низкой ценой
    print(get_min_price(cursor))
