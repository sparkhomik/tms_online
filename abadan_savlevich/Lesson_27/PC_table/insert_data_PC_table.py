import mysql.connector as mysql

db = mysql.connect(host="localhost", user="root",
                   database="test_db")

cursor = db.cursor()
cursor.execute("insert into PC (PRO_ID, PRO_NAME, PRO_PRICE, PRO_COM)"
               " values"
               "(101, 'Mother Board', 3200, 15),"
               "(102, 'Key Board', 450, 16),"
               "(103, 'ZIP drive', 250, 14),"
               "(104, 'Speaker', 550, 16),"
               "(105, 'Monitor', 5000, 11),"
               "(106, 'DVD drive', 900, 12),"
               "(107, 'CD drive', 800, 2),"
               "(108, 'Printer', 2600, 13),"
               "(109, 'Refill cartridge', 350, 13),"
               "(110, 'Mouse', 250, 12)")

db.commit()
db.close()
