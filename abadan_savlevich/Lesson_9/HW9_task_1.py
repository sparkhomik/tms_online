# task_1
# Библиотека

class Book:
    def __init__(self, name, author, pages_count, book_isbn):
        self.book_name = name
        self.book_author = author
        self.pages_count = pages_count
        self.book_isbn = book_isbn
        self.is_reserved = False
        self.is_taken = False
        self.is_free = True

    def make_book_reserved(self):
        """
        Функция брони книги
        Меняет значение для is_reserved и is_free
        :return: возвращает True или False
        """
        self.is_reserved = True
        self.is_free = False

    def make_book_taken(self):
        """
        Функция проверки занятости книги
        Меняет значение для is_taken и is_free
        :return: возвращает True или False
        """
        self.is_taken = True
        self.is_free = False

    def make_book_free(self):
        """
        Функция освобождения книги
        Меняет значение для is_taken, is_reserved и is_free
        :return: возвращает True или False
        """
        self.is_taken = False
        self.is_reserved = False
        self.is_free = True


class User:
    def __init__(self, name):
        self.name = name
        self.taken_book = None
        self.reserved_book = None

    def get_book(self, some_book):
        """
        Функция взятия книги
        """
        if some_book.is_free:
            # Забираем себе книгу, записывая в поле юзера "taken_book"
            self.taken_book = some_book
            some_book.make_book_taken()
            return f'Книга {some_book.book_name} взята юзером {self.name}'
        else:
            return 'Книга взята или зарезервирована другим пользователем'

    def reserve_book(self, some_book):
        """
        Функция брони книги
        """
        if some_book.is_free:
            # Забираем себе книгу, записывая в поле юзера "reserved_book"
            self.reserved_book = some_book
            some_book.make_book_reserved()
            return f'Книга {some_book.book_name} зарезервирована' \
                   f' юзером {self.name}'
        else:
            return f'Книга {some_book.book_name} взята или зарезервирована' \
                   ' другим пользователем'

    def free_book(self, some_book):
        """
        Функция освобождения книги
        """
        some_book.make_book_free()
        return f'Книга {some_book.book_name} возвращена {self.name}'


# Создаем 5 книг (они все свободные по дефолту (см. строку 7 и 8))
book_1 = Book(name='Book1', author='Author1', pages_count=100, book_isbn=123)
book_2 = Book(name='Book2', author='Author2', pages_count=200, book_isbn=111)
book_3 = Book(name='Book3', author='Author3', pages_count=300, book_isbn=3234)
book_4 = Book(name='Book4', author='Author4', pages_count=400, book_isbn=333)
book_5 = Book(name='Book5', author='Author5', pages_count=500, book_isbn=12355)

# Создаем 3 юзеров (у каждого пока нет книги на руках никакой (см. строку 13))
user_1 = User(name='Alex')
user_2 = User(name='Abadan')
user_3 = User(name='Mike')


def start():
    print(user_2.reserve_book(book_2))
    print(user_1.get_book(book_2))
    print(user_2.free_book(book_2))
    print(user_3.reserve_book(book_2))
    print(user_1.get_book(book_3))
    print(user_2.reserve_book(book_5))
    print(user_1.get_book(book_5))
    print(user_2.free_book(book_2))


if __name__ == '__main__':
    start()
