# task_1
# Банковский вклад

def validation(num):
    """
    Функция валидации, для проверки каждого введенного элемента
    :param num: строка
    :return: возвращает True или False
    """

    try:
        int(num)
        return True
    except ValueError:
        print("Вы можете ввести только числа!")
        return False


class Investment:
    total = 0

    def __init__(self, deposit, term, percent=10):
        self.deposit = deposit
        self.term = term
        self.percent = percent
        self.deposit_f()

    def deposit_f(self):
        """
        Метод экземпляра класса.
        Функция для подсчета суммы вклада на конец срока.

        Расчет производится по формуле ежемесячной капитализации:

        P * (1 + r / n) ** t

        Р – начальная сумма вклада.
        r – годовая ставка в процентах.
        n – сколько раз в год учитывается процентная ставка.
        t – срок вклада в месяцах.
        """

        total = self.deposit * (1 + ((self.percent / 100) / 12)) ** self.term
        self.total = total
        return self.total


def start():
    """
    Функция запуска программы
    Проверяет на валидацию все введеные числа с помощью функции validation()
    :return: вывод экземпляра класса Investment, округленный до сотых.
    """
    print('Расчет итоговой суммы с ежемесячной капитализацией под 10% годовых')
    deposit = input('Введите сумму вклада в рублях: ')
    if validation(deposit):
        deposit = int(deposit)
        term = input('Введите срок вклада в месяцах: ')
        if validation(term):
            term = int(term)
            invest = Investment(deposit, term)
            return f'Сумма, которая будет на вашем счету:' \
                   f' {round(invest.total, 2)}'
        else:
            print(start())
    else:
        print(start())


if __name__ == '__main__':
    print(start())
