from panda_page import PandaPage


def test_panda_page_title(driver):
    """
    Тест на проверку title.
    :param driver: драйвер.
    """
    panda_page = PandaPage(driver)
    panda_page.open()
    assert "ShiningPanda" in panda_page.get_title(), \
        "incorrect title"


def test_panda_page_name(driver):
    """
    Тест на проверку имени на странице
    :param driver: драйвер.
    """
    panda_page = PandaPage(driver)
    panda_page.open()
    assert panda_page.get_name().text == "ShiningPanda", \
        "incorrect page name"
