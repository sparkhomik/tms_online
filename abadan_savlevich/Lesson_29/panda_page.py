from base_page import BasePage
from panda_page_locators import PandaPageLocators


class PandaPage(BasePage):
    """
    Класс panda.
    """
    URL = "https://plugins.jenkins.io/shiningpanda"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title(self):
        """
        Получение тайтла окна.
        :return: тайтла окна
        """
        return self.driver.title

    def get_name(self):
        """
        Получение имени на странице.
        :return: name
        """
        return self.find_element(PandaPageLocators.LOCATOR_PAGE_TITLE)
