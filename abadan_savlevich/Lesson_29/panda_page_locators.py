from selenium.webdriver.common.by import By


class PandaPageLocators:
    """
    Класс с локаторами элементов страницы ShiningPanda.
    """
    LOCATOR_PAGE_TITLE = (By.XPATH, "//h1[@class='title']")
