import pytest
from check_lower_func import check_lower
from check_symbol_func import check_symbol
from float_func import get_float, InputTypeError

"""
Для каждой функции создать параметризированный тест. В качестве
параметров тест принимает значение, которое должна обработать функция и
ожидаемый результат ее выполнения.

- Каждый тест должен быть вызван по 3 раза

- Для 2-ой функции написать отдельный тест, который будет ожидать ошибку
InputTypeError, и если эта ошибка не произошла, падать!
"""


@pytest.mark.parametrize('word, result', [('hello', True), ('Hello', False),
                                          ('HELLO', False)])
def test_lowercase(word: str, result: bool):
    """
    Проверка работы функции check_lower.
    :param word: слово для проверки
    :param result: в нижнем регистре все буквы в слове или нет
    """
    assert check_lower(word) == result


@pytest.mark.parametrize('number, result', [(45, 45.0), (2, 2.0), (84, 84.0)])
def test_float(number: any, result: float):
    """
    Проверка работы функции get_float.
    :param number: переданное число
    :param result: число, преобразованное в тип float
    """
    assert get_float(number) == result


@pytest.mark.parametrize('number', ['k', '8'])
def test_fail_float_number(number: str):
    """
    Проверка выдачи сообщения об ошибке при передаче строки в функцию
    get_float.
    :param number: переданное число
    """
    with pytest.raises(InputTypeError):
        get_float(number)


@pytest.mark.parametrize('symbol, result', [('&', True), ('0', False),
                                            ('%', True)])
def test_check_symbol(symbol: str, result: bool):
    """
    Проверка работы функции check_symbol.
    :param symbol: переданный аргумент
    :param result: спецсимвол или нет
    """
    assert check_symbol(symbol) == result
