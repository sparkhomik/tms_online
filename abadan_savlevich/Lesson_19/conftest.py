from datetime import datetime
import pytest
import sys

"""
Фикстура, которая будет выводить время начала запуска всех тестов и
когда все тесты отработали. По итогу она должна отработать один раз!
"""


@pytest.fixture(scope='session', autouse=True)
def get_time() -> None:
    """
    Вывод времени начала и окончания теста.
    :return: дата и время
    """
    start_time = datetime.now()
    print(f'\nTest started {start_time.strftime("%d-%m-%Y %H:%M")}')
    yield start_time
    stop_time = datetime.now()
    print(f'\nTest finished {stop_time.strftime("%d-%m-%Y %H:%M")}')


"""
Фикстура, которая будут выводить имя файла, из которого
запущен тест. Если тесты находятся в одном файле, для них фикстура
должна отработать один раз!
"""


@pytest.fixture(scope='module', autouse=True)
def file_name() -> None:
    """
    Вывод имени файла, из которого запущен тест.
    :return: имя файла
    """
    print(f'Test started from file: {sys.argv[0]}')


"""
Фикстура, которая для каждого теста выводит информацию о фикстурах,
которые используются в этом тесте
"""


@pytest.fixture(autouse=True)
def fixture_info() -> None:
    """
    Вывод фикстур, примененных к тесту.
    """
    print(f'\nFixture information: {sys.argv[-1]}')


"""
Фикстура, которая для каждого теста выводит информацию о том, сколько
времени прошло между запуском сессии и запуском текущего теста
"""


@pytest.fixture(scope='function', autouse=True)
def fixture_name(get_time):
    """
    Вывод времени между стартом сессии и теста.
    """
    time = datetime.now()
    delta = time - get_time
    print(f'Time between session and test starts: {delta}\n')
