"""
Функция проверки, является ли переданный аргумент спец символом
"""


def check_symbol(arg):
    """
    Функция проверяет, является ли переданный аргумент спец символом
    :param arg: аргумент
    :return: True/False, bool
    """
    return any([str(arg) in "!@#$%^&*(){}<!№%:,.;[]"])


if __name__ == "__main__":
    print(check_symbol(4))
    print(check_symbol("&"))
