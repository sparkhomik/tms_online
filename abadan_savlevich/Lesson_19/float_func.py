"""
Функция переводит переданное число в тип float, если передана строка вызывает
 пользовательскую ошибку InputTypeError (создать ее самому)
"""


class InputTypeError(Exception):
    """
    Пользовательская ошибка InputTypeError.
    """
    def __init__(self, arg):
        self.message = f"Number '{arg}' is not an int!"
        super().__init__(self.message)


def get_float(arg) -> float:
    """
    Функция перевода аргумента в тип float.
    Если переданный аргумент не типа int или float, появляется
    пользовательская ошибка InputTypeError.
    :param arg: аргумент
    :return: число типа float.

    """
    if type(arg) != int:
        raise InputTypeError(arg)
    else:
        return float(arg)


if __name__ == "__main__":
    print(get_float(5))
    print(get_float('arg'))
    print(get_float(2.5))
