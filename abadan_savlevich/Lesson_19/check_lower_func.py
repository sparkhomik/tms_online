"""
Функция проверки, что все буквы в переданном слове в нижнем регистре.
"""


def check_lower(word: str):
    """
    Функция проверяет, что все буквы в слове в нижнем регистре
    :param word: слова, str
    :return: True/False, bool
    """
    return word.islower()


if __name__ == "__main__":
    print(check_lower("Hi"))
    print(check_lower("hello"))
