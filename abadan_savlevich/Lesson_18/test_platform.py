import unittest
from platform import System


class TestPlatformOs(unittest.TestCase):
    """
    Тестовый класс.
    """

    def test_get_name(self):
        """
        Тест проверки на равенство имени ос.
        """
        self.assertEqual(System.get_name(), 'Darwin')

    def test_get_release(self):
        """
        Тест проверки на равенство релиза ос.
        """
        self.assertEqual(System.get_release(), '20.6.0')

    def test_get_version(self):
        """
        Тест проверки на равенство версии ос.
        """
        self.assertEqual(System.get_version(), '11.6')

    @unittest.expectedFailure
    def test_get_name_fail(self):
        """
        Негативный тест проверки на равенство имени ос.
        """
        self.assertNotEqual(System.get_name(), 'Darwin')

    @unittest.expectedFailure
    def test_get_release_fail(self):
        """
        Негативный тест проверки на равенство релиза ос.
        """
        self.assertEqual(System.get_release(), '11.1')

    @unittest.expectedFailure
    def test_get_version_fail(self):
        """
        Негативный тест проверки на равенство версии ос.
        """
        self.assertNotEqual(System.get_version(), '11.6')
