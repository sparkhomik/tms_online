
"""
Написать класс для работы с текущей операционной системой:
Должно быть три метода:
- получить имя операционной системы
- релиз операционной системы
- версию OS
"""

import platform


class System:
    """
    Класс System.
    """

    @staticmethod
    def get_name():
        """
        Получение имени ос.
        :return: строка, имя ос.
        """
        return platform.system()

    @staticmethod
    def get_release():
        """
        Получение релиза ос.
        :return: строка, релиз ос.
        """
        return platform.release()

    @staticmethod
    def get_version():
        """
        Статический метод класса, получение версии ос.
        :return: строка, версия ос.
        """
        return platform.version()


if __name__ == '__main__':

    system = System()
    print(system.get_name())
    print(system.get_release())
    print(system.get_version())
