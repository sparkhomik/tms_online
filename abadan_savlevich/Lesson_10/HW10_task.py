from operator import itemgetter

"""
Цветочница
"""


class Flower:
    name = None
    """"
    Класс Flower, от которого будут создаваться все последующие цветы
    """

    def __init__(self, stem_length: int, colour: str, fresh: float):
        """
        Атрибуты экземпляра класса:
        :param stem_length: длина стебля
        :param colour: цвет
        :param fresh: свежесть цветка
        """
        self.stem_length = stem_length
        self.colour = colour
        self.fresh = fresh

    def __str__(self):
        """"
        Магический метод, котрый при вызове выводит имя и цвет
        """
        return f'{self.colour} {self.name}'

    def __getitem__(self, item):
        """
        Магический метод для доступа к элементам для быстрого
        поиска атрибутов экземпляра.
        """
        return getattr(self, item)


class Rose(Flower):
    name = 'rose'
    price = 20


class Tulip(Flower):
    name = 'tulip'
    price = 10


class Chamomile(Flower):
    name = 'chamomile'
    price = 60


class Violets(Flower):
    name = 'violet'
    price = 30


class BouquetStuff:
    name = None
    """"
    Класс BouquetStuff, от которого будут создаваться все последующие предметы
    декора букета
    """

    def __init__(self, colour):
        """
        Атрибут экземпляра
        :param colour: цвет
        """
        self.colour = colour

    def __str__(self):
        """"
        Магический метод, котрый при вызове выводит имя и цвет
        """
        return f'{self.colour} {self.name}'


class Paper(BouquetStuff):
    name = 'paper'
    price = 2


class Ribbon(BouquetStuff):
    name = 'ribbon'
    price = 5


class Bouquet:
    def __init__(self, flowers: list, stuff: list):
        """
         Атрибуты экземпляра класса:
        :param flowers: список цветов
        :param stuff: список декора
        """
        self.flowers = flowers
        self.stuff = stuff

    def __str__(self) -> str:
        """
        Магический метод проходится по всему букету и выводит каждый цветок
        :return: str, все цветы в букете
        """
        flowers_1 = [str(flower).split(',') for flower in self.flowers]
        flowers_str = ''
        for flower in zip(*flowers_1):
            flowers_str = ', '.join(flower)
        return flowers_str

    def get_flower_wilting_time(self) -> float:
        """
        Функция определения среднего времени увядания букета
        :return: float, среднее значение увядания, округленное до десятых
        """
        wilting_time = [i.fresh for i in self.flowers]
        average_wilting = round(sum(wilting_time) / len(wilting_time), 1)
        return average_wilting

    def calculate_bouquet_price(self) -> int:
        """
        Функция определения среднего времени увядания букета
        :return: int, общая сумма букета
        """
        flowers_price = [i.price for i in self.flowers]
        accessories_price = [i.price for i in self.stuff]
        price = sum(flowers_price) + sum(accessories_price)
        return price

    def sort_by_property(self, prop: str) -> str:
        """
        Функция сортировки цветов в букете по параметру
        :param prop: str, параметр сортировки
        :return: str, отсортированная строка
        """
        sorted_flowers = sorted(self.flowers, key=itemgetter(prop))
        sort_str = ', '.join(str(flower) for flower in sorted_flowers)
        return sort_str

    def check_if_flower_exist(self, name: str) -> bool:
        """
        Функция проверки наличия цветка в букете
        :param name: str, имя цветка
        :return: bool, True or False
        """
        flower_exist = any(flower.name == name for flower in self.flowers)
        return bool(flower_exist)

    def get_flower_by_index(self, index: int) -> str:
        """
        Функция получения цветка по индексу из букета
        :param index: индекс
        :return: str, цветок по индексу
        """
        return self.flowers[index]

    def get_search_by_colour_and_price(self, colour=None, price=None) -> str:
        """
        Функция поиска цветка по цвету и/или стоимости
        :param colour: цвет
        :param price: стоимость
        :return: str, цветок, соответствующий параметрам
        """
        flower_list = []
        sorting_args_str = ''
        for flower in self.flowers:
            if (price is None or flower.price == price) and (
                    colour is None or flower.colour == colour):
                flower_list.append(flower)
        flowers_list = [str(flower).split(',') for flower in flower_list]
        if len(flower_list):
            for flower in zip(*flowers_list):
                sorting_args_str += ', '.join(flower)
            return sorting_args_str


def start() -> None:
    """
    Функция запуска
    """
    # создаются экземпляры классов.
    rose = Rose(colour='red', fresh=5, stem_length=3)
    rose_1 = Rose(colour='white', fresh=2, stem_length=3)
    tulip = Tulip(colour='white', fresh=2, stem_length=3)
    ribbon_1 = Ribbon(colour='green')
    paper_1 = Paper(colour='yellow')
    flowers = [rose, tulip, rose_1]
    stuff = [ribbon_1, paper_1]
    bouquet = Bouquet(flowers, stuff)

    # перебор цветов по букету
    print(f'Список цветов: {bouquet}')

    # сортировка по параметрам
    print(f'Сортировка по "name": {bouquet.sort_by_property(prop="name")}')
    print(f'Сортировка по "price": {bouquet.sort_by_property(prop="price")}')

    # проверка наличия цветка в букете
    print(f'В букете есть цветок tulip: '
          f'{bouquet.check_if_flower_exist(name="tulip")}')
    print(f'В букете есть цветок rose: '
          f'{bouquet.check_if_flower_exist(name="rose")}')
    print(f'В букете есть цветок violet: '
          f'{bouquet.check_if_flower_exist(name="violet")}')

    # стоимость букета
    print(f'Стоимость букета: {bouquet.calculate_bouquet_price()}')

    # среднее время свежести букета
    print(f'Время увядания: {bouquet.get_flower_wilting_time()}')

    # вывод цветка по индексу
    print(f'Цветок под индексом [1]: {bouquet.get_flower_by_index(1)}')
    print(f'Цветок под индексом [0]: {bouquet.get_flower_by_index(0)}')

    # поиск цветка по параметрам: цена и/или цвет
    print(f'Результаты поиска по цвет: красный, цена = 20: '
          f'{bouquet.get_search_by_colour_and_price(colour="red",price=20)}')
    print(f'Результаты поиска по цена = 20: '
          f'{bouquet.get_search_by_colour_and_price(price=20)}')
    print(f'Результаты поиска по цвет: белый: '
          f'{bouquet.get_search_by_colour_and_price(colour="white")}')


if __name__ == '__main__':
    start()
