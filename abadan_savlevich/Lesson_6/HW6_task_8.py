# task_8
# написать функцию, которая проверяет есть ли в списке вызываемый объект

def call(a: list) -> list:
    """
    Функция, которая проверяет есть ли в списке вызываемый объект
    :arg a: список
    """
    new_list = []
    # проходимся по списку для проверки каждого элемента на вызываемость
    for i in a:
        if callable(i):
            new_list.append(i)
    return new_list


my_list = [1, 2, 5, str, list]
print(call(my_list))
