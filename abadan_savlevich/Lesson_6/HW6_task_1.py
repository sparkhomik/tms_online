from random import randrange

# task_1
"""
Validate
Ваша задача написать программу, принимающее число - номер кредитной
карты(число может быть четным или не четным). И проверяющей может ли
такая карта существовать. Предусмотреть защиту от ввода букв,
пустой строки и т.д.
"""


def card_checker(card: str) -> int:
    """
    Функция проверки номера карты по алгоритму Луна.
    :arg card: номер карты
    """
    sum = 0
    number = list(map(int, card))
    for i, e in enumerate(number):
        if not i % 2:
            elem = e * 2
            elem = elem - 9 if elem > 9 else elem
            sum += elem
        else:
            sum += elem
    return sum % 10


# генератор валидной карты
def generatecardnumber() -> str:
    """
    Функция генерации валидной карты
    """
    result = '4'
    for i in range(2, 16):
        randint = str(randrange(0, 9))
        result += randint
        if i % 4 == 0:
            result += ' '
    for i in range(0, 9):
        if card_checker((result + str(i)).replace(" ", '')) == 0:
            result += str(i)
            return result


# раскомментировать для получения валидного номера карты
# print(generatecardnumber())

# валидация для переменных
cardNumber = input("Введите номер банковской карты: ")
cardNumber = cardNumber.replace(" ", '')
if len(cardNumber) == 16 and cardNumber.isdigit():
    if card_checker(cardNumber):
        print("Не равильный номер карты!")
    else:
        print("Правильный номер карты!")
else:
    print("Не правильно набран номер!")
