# task_2
# подсчет количества букв

my_string = input('Введите строку с разными буквами')


def count(a: str) -> str:
    """
    Функция подсчета количества букв в строке.
    :arg a: строка
    """
    d = {}
    for i in set(a):
        d[i] = a.count(i)
    return ''.join('{}{}'.format(key, val) for key, val in d.items())


# валидация для проверки пустого ввода
if not my_string:
    print('Пустой ввод! Повторите попытку')
else:
    print(count(my_string))
