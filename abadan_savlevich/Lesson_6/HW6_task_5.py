# task_5
# области видимости:

def func(old_list: list) -> list:
    """
    Функция добавления элемента в список.
    :param old_list: список на уровне модуля.
    :return: измененный список.
    """
    new_list = old_list[:]
    new_list.append('a')
    return new_list


my_list = ['d', 'c', 'b']

print(func(my_list))
print(my_list)
