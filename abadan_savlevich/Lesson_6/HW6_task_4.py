# task_4
# функция с изменяемым числом входных параметров:

def func(a: int, *b: int, c=None, **d: str) -> dict:
    """
    Функия с изменяемым числом входных параметров.
    :param a: обязательный позиционный аргумент.
    :param b: переменное количество позиционных аргументов.
    :param c: обязательный именованный аргумент.
    :param d: переменное количество именованных аргументов.
    :return: словарь, в котором ключи это описание переменных.
  """

    result = {'mandatory_position_argument': a,
              'additional_position_arguments': b,
              'mandatory_named_argument': {'name': c},
              'additional_named_arguments': d}

    return result


print(func(1, 2, 3, name='Abadan', surname='Karlieva', some='qa'), '\n')
