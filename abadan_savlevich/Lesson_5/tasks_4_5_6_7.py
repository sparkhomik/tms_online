import string

# task_4
# Напишите код, который возьмет список строк и пронумерует их.
items = ['a', 'b', 'c', 'd', 'e']
new = []
for i, item in enumerate(items):
    j = (str(i + 1) + ': ' + items[i])
    new.append(j)
print(new)

# task_5
# Проверить, все ли элементы одинаковые
a = [1, 1, 1]
print(all(x == a[0] for x in a))

b = [1, 2, 1]
print(all(x == b[0] for x in b))

c = ['a', 'a', 'a']
print(all(x == c[0] for x in c))

d = []
print(all(x == d[0] for x in d))

# task_6
"""
Проверка строки. В данной подстроке проверить все ли буквы в
строчном регистре или нет и вернуть список не подходящих.
"""

while True:
    a = input('Введите текст в любом регистре латинницей: ')
    a1 = list(a)
    a2 = []
    boolean = True
    # проверим, что текст содержит только буквы латинницей
    if not all(j in string.ascii_letters for j in a):
        print('Вы можете ввести только буквы латинницей!')
    elif len(a1) == 0:
        print('Пустой ввод! Повторите попытку')
    else:
        # создадим цикл, где переберем все цифры списка.
        for i in range(len(a1)):
            if a1[i].isupper():
                # Все буквы в верхнем регистре занесем в список а2
                a2.append(a1[i])
        # Если список пустой, значение будет False
        if len(a2) > 0:
            boolean = False
            print([boolean, a2])
        break

# task_7
"""
Сложите все числа в списке, они могут быть отрицательными,
если список пустой вернуть 0
"""

a = []
b = [1, 2, 3]
c = [1.1, 2.2, 3.3]
d = [4, 5, 6]
e = range(101)
# используем sum для суммирования всех элементов
print(sum(a))
print(sum(b))
print(sum(c))
print(sum(d))
print(sum(e))
