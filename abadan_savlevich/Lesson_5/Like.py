import string

# task_2
"""
Создайте программу, которая, принимая массив имён, возвращает строку
описывающая количество лайков (как в Facebook).
"""

while True:
    names = input('Введите имена через запятую: ')
    # преобразуем в список
    ls_names = names.split()
    # проверим, что число содержит только цифры
    if all(j in string.digits for j in names):
        print('Вы можете ввести только буквы латинницей!')
    # проверим ввод не пустой
    elif len(ls_names) == 0:
        print('Пустой ввод! Повторите попытку')
    else:
        # пропишем условия, в зависимости от длины списка
        if len(ls_names) == 1:
            ls_names = ''.join(ls_names)
            print(f'{ls_names} like this')
        elif len(ls_names) == 2:
            print(f'{ls_names[0]} and{ls_names[1]} like this')
        elif len(ls_names) == 3:
            print(f'{ls_names[0]},{ls_names[1]} and{ls_names[2]} like this')
        elif len(ls_names) > 3:
            other = int(len(ls_names)) - 2
            print(f'{ls_names[0]},{ls_names[1]} and {other} others like this')
        else:
            print('No one likes this')
        break
