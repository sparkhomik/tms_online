# task_1
i = 0
while i <= 20:
    if i % 2 == 0:
        print(i)
    i += 1
# task_2
# первый способ
j = - 1
while j > -22:
    print(j)
    j -= 3

# второй способ
y = [-x for x in range(1, 21)]
for i in y[::3]:
    print(i)
