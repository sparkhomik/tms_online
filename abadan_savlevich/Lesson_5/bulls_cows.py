from random import randrange
import string

# task_1
"""
Каждый из игроков задумывает и записывает тайное 4-значное число
с неповторяющимися цифрами. Игрок, который начинает игру по жребию,
делает первую попытку отгадать число. Попытка — это 4-значное число
с неповторяющимися цифрами, сообщаемое противнику. Противник сообщает в
ответ, сколько цифр угадано без совпадения с их позициями в тайном числе
(то есть количество коров) и сколько угадано вплоть до позиции в тайном числе
(то есть количество быков).
"""
# сгенерируем число четырехзначное
secret_num = []
randInt = randrange(0, 9)

# чтобы сделать число с неповторяющимися элементами, используем цикл
while len(secret_num) < 4:
    # если число уже в списке, мы генерируем новое число и добавляем в список
    while randInt in secret_num:
        randInt = randrange(0, 9)
    secret_num.append(randInt)
print(secret_num)
print('Загадано секретное число...')

while True:
    num = input("Угадайте четырехзначное число с неповторяющимися цифрами: ")
    # проверим, что число содержит только цифры
    if not all(j in string.digits for j in num):
        print("Вы можете ввести только числа!")
    # проверим, что длина числа не более и не менее 4 символов
    elif 1 <= len(str(num)) < 4 or 4 < len(str(num)):
        print(f'Число {len(num)} значное! Введите четырехзначное')
    # проверим, что в числе символы не повторяются
    elif len(num) > len(set(num)):
        print("Неверное число! Введите число с неповторяющимися цифрами")
    # проверим ввод не пустой
    elif not num:
        print('Пустой ввод! Повторите попытку')
    else:
        # преобразуем список со строками в список с числами
        ls_num = list(map(int, num))
        # сравним 2 списка в цикле по индексу и по элементу
        bulls = 0
        cows = 0
        for i, num in enumerate(secret_num):
            if num in secret_num and ls_num[i] == secret_num[i]:
                bulls += 1
            elif num in secret_num and ls_num[i] != secret_num[i]:
                cows += 1
            else:
                bulls = bulls
                cows = cows
        # сообщения на основании полученного сравнения
        if bulls == 4:
            print('Вы выиграли!')
            break
        elif bulls == 1:
            bull_message = f"У вас {bulls} бык"
        elif 2 <= bulls <= 4:
            bull_message = f"У вас {bulls} быка"
        else:
            bull_message = f"У вас {bulls} быков"
        # сообщения на основании полученного сравнения
        if cows == 1:
            cows_message = f" и {cows} корова"
        elif 2 <= cows <= 4:
            cows_message = f" и {cows} коровы"
        else:
            cows_message = f" и {cows} коров"
        print(bull_message + cows_message)
        break
