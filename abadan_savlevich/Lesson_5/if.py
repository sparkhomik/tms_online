# task_1
x = -7
if x > 0:
    print('Hello!')

# task_2
x = -7
if x > 0:
    print(1)
else:
    print(-1)

# task_3
age = int(input())
if age <= 13:
    print('детство')
elif 14 <= age <= 24:
    print('молодость')
elif 25 <= age <= 59:
    print('зрелость')
else:
    print('старость')
