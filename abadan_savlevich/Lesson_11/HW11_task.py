from abc import ABC


# Ферма

class Basic(ABC):
    """
    Абстрактный класс Basic, от которого будут создаваться все последующие
    классы
    Атрибуты имя и возраст.
    """
    def __init__(self, name, age):
        self.name = name
        self._age = age

    def increase_age(self) -> None:
        """
        Метод старения.
        Увеличивает возраст на 1.
        """
        self._age += 1

    def voice(self):
        """
        Абстрактный метод голос.
        Для переопредления в классе наследника
        """
        raise NotImplementedError

    def __getattr__(self, atr):
        """
        Метод проверки наличия атрибута класса.
        Если нет, вызывается пользовательский exception.
        """
        raise NotExistException(atr)


class Animals(Basic):
    """
    Класс Животные.
    Наследуется от класса Basic
    """

    @staticmethod
    def run() -> str:
        """
        Метод Ходит.
        """
        return 'может бегать'


class Birds(Basic):
    """
    Класс Птицы.
    Наследуется от класса Basic
    """
    @staticmethod
    def fly() -> str:
        """
        Метод Летает.
        """
        return 'может летать'


class Pig(Animals):
    """
    Класс Свинья.
    Наследует атрибуты класса Animals и Basic.
    """

    def voice(self) -> str:
        """
        Переопределенный метод голос.
        """
        return 'хрю-хрю'

    def __str__(self):
        return f'Свин {self.name} говорит {self.voice()} и {self.run()},' \
               f' возрастом {self._age} года'


class Goose(Birds):
    """
    Класс Гусь.
    Наследует атрибуты класса Birds и Basic.
    """

    def voice(self) -> str:
        """
        Переопределенный метод голос.
        """
        return 'га-га'

    def __str__(self):
        return f'Гусь {self.name} говорит {self.voice()} и {self.fly()},' \
               f' возрастом {self._age} года'


class Chicken(Birds):
    """
    Класс Курица.
    Наследует атрибуты класса Birds и Basic.
    """

    def voice(self) -> str:
        """
        Переопределенный метод голос.
        """
        return 'кудах'

    def __str__(self):
        return f'Курочка {self.name} говорит {self.voice()} и {self.fly()},' \
               f' возрастом {self._age} года'


class Cow(Animals):
    """
    Класс Корова.
    Наследует атрибуты класса Animals и Basic
    """

    def voice(self) -> str:
        """
        Переопределенный метод голос.
        """
        return 'му-муу'

    def __str__(self):
        return f'Коровка {self.name} говорит {self.voice()} и {self.run()},' \
               f' возрастом {self._age} года'


class Farm:
    """
    Класс Ферма.
    Принимает animals_list
    """

    def __init__(self, list_of_animals):
        self.list_of_animals = list_of_animals

    def __getitem__(self, item):
        """
        Функция получения животного по индексу
        """
        return self.list_of_animals[item]

    def all_animals(self) -> None:
        """
        Функция получения всех животных.
        Выводит каждый экземпляр класса из списка.
        """

        for animal in self.list_of_animals:
            print(animal)

    def increase_age(self) -> None:
        """
        Функция увеличение возраста каждого животного на 1
        """
        for animal in self.list_of_animals:
            animal.increase_age()


class NotExistException(Exception):
    """
    Exception.
    В случае атрибут экземпляра класса не существует, вызывается
    ошибка с выводом сообщения
    """

    def __init__(self, func):
        self.message = f'Func "{func}" does not exist'
        super().__init__(self.message)


def start():
    """
    Функция запуска
    """
    # создаются экземпляры классов

    goose = Goose('Гусак', 2)
    pig = Pig('Степан', 3)
    cow = Cow('Зорька', 2)
    chicken = Chicken('Ряба', 2)
    animals = [pig, goose, chicken, cow]
    farm = Farm(animals)

    # вывод всех животных
    farm.all_animals()

    # вывод животного по индексу
    print(f'Животное под индексом [0]: {farm[0]}')
    print(f'Животное под индексом [3]: {farm[3]}')

    # вызов функции старения
    farm.increase_age()

    # вывод всех животных для проверки возраста
    farm.all_animals()

    # вывод несуществующей функции
    pig.eat()


if __name__ == '__main__':
    start()
