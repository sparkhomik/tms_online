import random
import math

a = 10
b = 23

# task_3_1
# поменять значения
a, b = b, -a
print(a, b)

# task_3_2
# увеличить в 3 раза
a *= 3

# уменьшить в 3 раза
b /= 3
print(a, b)

# task_3_3
# преобразовать в число с плавающей точкой
a = float(a)

# преобразовать в строку
b = str(b)
print(a, b)

# task_3_4
# разделить значение в переменной “a” на 11 и округлить до 3-х знаков
a = round(a / 11, 3)
print(a)

# task_3_5
# преобразовать в число с плавающей точкой и записать в переменную “c”
c = float(b)

# возвести число в 3-ю степень
c = pow(c, 3)
print(c)

# task_3_6
# случайное число, кратное 3-м
d = random.randrange(3, 99, 3)
print(d)

# task_3_7
# корень числа 100
root = math.sqrt(100)

# возвести в 4 степень
print(pow(root, 4))

# task_3_8
# строку “Hi guys” вывести 3 раза и в конце добавить “Today”
new_str = 'Hi guys' * 3 + 'Today'
print(new_str)

# task_3_9
# получить длину строки new_str
print(len(new_str))

# task_3_10
# из строки new_str вывести слово “Today” в прямом порядке
print(new_str[-5::1])

# из строки new_str вывести слово “Today” в обратном порядке
print(new_str[-1:-6:-1])

# task_3_11
# в строке new_str вывести каждую 2 букву в прямом порядке
print(new_str[1::2])

# в строке new_str вывести каждую 2 букву в обратном порядке
print(new_str[::-2])

# task_3_12
'''
Используя форматирование вывести следующую строку:
“Task 10: <в прямом>, <в обратном> Task 11: <в прямом>, <в обратном>”
'''

print('Task 10: {a}, {b}, Task 11: {c}, {d}'.format(a=new_str[-5::1],
                                                    b=new_str[-1:-6:-1],
                                                    c=new_str[1::2],
                                                    d=new_str[::-2]))
# task_3_13
txt = 'Task 10: Today, yadoT, Task 11: igyH usigyTdy, ydTygisu Hygi'

print("Capitalization\t", txt.capitalize())
print("Lower\t\t", txt.lower())
print("Upper\t\t", txt.upper())

# task_3_14
txt = 'Task 10: Today, yadoT, Task 11: igyH usigyTdy, ydTygisu Hygi'
print(txt.count('Task'))
