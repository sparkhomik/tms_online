def test_checkbox_true(get_checkbox):
    """
    Тестовая функция проверки наличия чекбокса.
    """
    assert bool(get_checkbox) is True


def test_text_gone(get_text):
    """
    Тестовая функция проверки текста.
    Сравнивает полученный текст с ожидаемым.
    """
    assert get_text[0] == "It's gone!"


def test_checkbox_false(get_text):
    """
    Тестовая функция проверки отсутствия чекбокса после клика на кнопку.
    """
    assert bool(get_text[1]) is True


def test_disabled_input(get_disabled_input):
    """
    Тестовая функция проверки неактивности поля
    """
    assert get_disabled_input is False


def test_text_enabled(get_enabled_input):
    """
    Тестовая функция проверки текста.
    Сравнивает полученный текст с ожидаемым
    """
    assert get_enabled_input[0] == "It's enabled!"


def test_enabled_input(get_enabled_input):
    """
    Тестовая функция проверки активности поля
    """
    assert bool(get_enabled_input[1]) is True
