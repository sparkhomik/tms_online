import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as ec


@pytest.fixture(scope='session', autouse=True)
def chrome_driver():
    """
    Фикстура запуска браузера
    :return: Chrome browser
    """
    browser = Chrome(executable_path='/Users/abadankarlieva/Documents'
                                     '/chrome_driver/chromedriver')
    yield browser
    browser.quit()


@pytest.fixture()
def driver_page(chrome_driver):
    """
    Фикстура открытия http://the-internet.herokuapp.com/dynamic_controls
     :param chrome_driver: экземпляр класса драйвера Chrome
    """
    chrome_driver.get("http://the-internet.herokuapp.com/dynamic_controls")
    return chrome_driver


@pytest.fixture()
def get_checkbox(driver_page):
    """
    Фикстура поиска чекбокса.
    :param driver_page: экземпляр класса драйвера Chrome
    :return: web-element.
    """
    checkbox = driver_page.find_element_by_xpath(
        "//input[@type='checkbox']")
    return checkbox


@pytest.fixture()
def get_text(driver_page) -> tuple:
    """
    Фикстура клика на кнопку и проверки наличия текста и отсутствия чекбокса.
    :param driver_page: экземпляр класса драйвера Chrome
    :return: тапл текст и чекбокс.
    """
    driver_page.find_element_by_xpath(
        "//button[@type='button'][1]").click()
    message = WebDriverWait(driver_page, 10).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, "p#message"))
    )
    checkbox = WebDriverWait(driver_page, 10).until_not(
        ec.presence_of_element_located((By.XPATH, "//input[@type='checkbox']"))
    )
    return message.text, checkbox


@pytest.fixture()
def get_disabled_input(driver_page):
    """
    Фикстура проверки чекбокса.
    :param driver_page: экземпляр класса драйвера Chrome
    :return: True or False.
    """
    input = driver_page.find_element_by_css_selector("input[type='text']")
    return input.is_enabled()


@pytest.fixture()
def get_enabled_input(driver_page) -> tuple:
    """
    Фикстура клика на кнопку и проверки наличия текста и проверки
    кликабельности поля
    :param driver_page: экземпляр класса драйвера Chrome
    :return: тапл текст и статус.
    """
    driver_page.find_element(By.XPATH, '(//button[@type="button"])[2]').click()
    message = WebDriverWait(driver_page, 10).until(
        ec.presence_of_element_located((By.CSS_SELECTOR, "p#message"))
    )
    status = WebDriverWait(driver_page, 5).until(ec.element_to_be_clickable(
        (By.XPATH, "//form[@id='input-example']/input")))
    return message.text, status


@pytest.fixture()
def get_iframe_text(chrome_driver):
    """
    Фикстура проверки текста из iframe.
    :param chrome_driver: экземпляр класса драйвера Chrome
    :return: текст.
    """
    chrome_driver.get('http://the-internet.herokuapp.com/frames')
    chrome_driver.find_element_by_xpath("//a[@href='/iframe']").click()
    chrome_driver.switch_to.frame(chrome_driver.find_element_by_tag_name
                                  ("iframe"))
    WebDriverWait(chrome_driver, 30).until(ec.visibility_of_element_located(
        (By.CSS_SELECTOR, ".mce-content-body  p"),
    ))
    message = chrome_driver.find_element_by_css_selector(".mce-content-body p")
    return message.text
