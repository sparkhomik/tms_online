from main_page import MainPage


def test_basket_page_title(driver):
    """
    Проверка, что заголовок страницы "Basket | Oscar - Sandbox".
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    basket_page = main_page.open_basket_page()
    assert basket_page.get_title() == "Basket | Oscar - Sandbox", \
        "Incorrect basket page title!"


def test_basket_header_name(driver):
    """
    Проверка, что header равен "Basket".
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    basket_page = main_page.open_basket_page()
    assert basket_page.get_header_name().text == "Basket", \
        "Incorrect basket page header!"


def test_basket_path_name(driver):
    """
    Проверка наличия "Basket" в path.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    basket_page = main_page.open_basket_page()
    assert basket_page.get_path_name().text == "Basket", \
        "Incorrect basket page path!"


def test_basket_is_empty(driver):
    """
    Проверка, что корзина пустая.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    basket_page = main_page.open_basket_page()
    empty_label = "Your basket is empty. Continue shopping"
    assert basket_page.get_empty_basket().text == empty_label, \
        "Your basket isn't empty!"
