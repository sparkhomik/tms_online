from main_page import MainPage


def test_login_page_title(driver):
    """
    Проверка, что заголовок страницы "Login or register | Oscar - Sandbox".
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.get_title() == "Login or register | Oscar - Sandbox", \
        "Incorrect login page title!"


def test_login_form(driver):
    """
    Проверка наличия формы Log In на странице.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.get_login_form(), "Login form doesn't exist!"


def test_login_register_form(driver):
    """
    Проверка наличия формы Register на странице.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.get_register_form(), "Register form doesn't exist!"


def test_login_header_name(driver):
    """
    Проверка, что header содержит "Login or register".
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.get_header_name().text == "Login or register", \
        "Incorrect login page header!"
