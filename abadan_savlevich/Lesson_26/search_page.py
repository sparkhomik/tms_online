from base_page import BasePage
from search_page_locators import SearchPageLocators


class SearchPage(BasePage):
    """
    Класс страницы с результатами поиска.
    """

    def get_title(self):
        return self.driver.title

    def get_header_name(self):
        return self.find_element(SearchPageLocators.SEARCH_HEADER_LOCATORS)

    def get_path_name(self):
        return self.find_element(SearchPageLocators.SEARCH_PATH_LOCATORS)

    def get_image(self):
        return self.find_element(SearchPageLocators.SEARCH_IMAGE_LOCATORS)
