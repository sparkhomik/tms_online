from base_page import BasePage
from basket_page_locators import BasketPageLocators


class BasketPage(BasePage):
    """
    Класс корзины.
    """

    def get_title(self):
        """
        Получение тайтла окна.
        :return: тайтла окна
        """
        return self.driver.title

    def get_header_name(self):
        """
        Получение header.
        :return: header
        """
        return self.find_element(BasketPageLocators.LOCATOR_BASKET_HEADER)

    def get_path_name(self):
        """
        Получение пути открытой страницы.
        :return:
        """
        return self.find_element(BasketPageLocators.LOCATOR_BASKET_PATH)

    def get_empty_basket(self):
        """
        Получение надписи о том, что страница пустая.
        :return: надпись о том, что страница пустая
        """
        return self.find_element(BasketPageLocators.LOCATOR_EMPTY_BASKET)
