from main_page import MainPage


def test_search_page_title(driver, book_name='Hacking Exposed Wireless'):
    """
    Тест на прроверку, что заголовок страницы содержит названия искомой книги.
    :param driver: экземпляр класса драйвера Chrome
    :param book_name: название искомой книги
    """
    main_page = MainPage(driver)
    main_page.open()
    search_page = main_page.open_search_page(book_name)
    assert book_name in search_page.get_title(), "Incorrect search page title!"


def test_search_header_name(driver, book_name='Hacking Exposed Wireless'):
    """
    Проверка, что header содержит название искомой книги.
    :param driver: экземпляр класса драйвера Chrome
    :param book_name: название искомой книги
    """
    main_page = MainPage(driver)
    main_page.open()
    search_page = main_page.open_search_page(book_name)
    assert book_name in search_page.get_header_name().text, \
        "Incorrect search page header!"


def test_search_path_name(driver, book_name='Hacking Exposed Wireless'):
    """
    Проверка наличия названия искомой книги в path.
    :param driver: экземпляр класса драйвера Chrome
    :param book_name: название искомой книги
    """
    main_page = MainPage(driver)
    main_page.open()
    search_page = main_page.open_search_page(book_name)
    assert book_name in search_page.get_path_name().text, \
        "Incorrect search page path!"


def test_search_image(driver, book_name='Hacking Exposed Wireless'):
    """
    Проверка наличия изображения найденной книги
    :param driver: экземпляр класса драйвера Chrome
    :param book_name: название искомой книги
    """
    main_page = MainPage(driver)
    main_page.open()
    search_page = main_page.open_search_page(book_name)
    assert search_page.get_image(), "Image not found!"
