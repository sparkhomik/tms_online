from base_page import BasePage
from fiction_page_locators import FictionPageLocators


class FictionPage(BasePage):
    """
    Класс страницы Fiction.
    """

    def get_title(self):
        return self.driver.title

    def get_header_name(self):
        return self.find_element(FictionPageLocators.LOCATOR_FICTION_HEADER)

    def get_path_name(self):
        return self.find_element(FictionPageLocators.LOCATOR_FICTION_PATH)
