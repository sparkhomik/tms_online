from selenium.webdriver.common.by import By


class LoginPageLocators:
    """
    Класс локаторов страницы логина.
    """
    LOCATOR_LOGIN_FORM = (By.CSS_SELECTOR, '#login_form')
    LOCATOR_REGISTER_FORM = (By.CSS_SELECTOR, '#register_form')
    LOCATOR_LOGIN_HEADER = (By.CSS_SELECTOR, '.breadcrumb>.active')
