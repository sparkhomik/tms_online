from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Класс локаторов главной страницы.
    """
    LOCATOR_MAIN_PAGE = (By.CSS_SELECTOR, "#login_link")
    LOCATOR_BOOKS_PAGE = (By.XPATH, '//a[@href="/en-gb/catalogue/'
                                    'category/books_2/"]')
    LOCATOR_BASKET_PAGE = (By.XPATH, '//a[text()="View basket"]')
    LOCATOR_SEARCH_INPUT = (By.XPATH, '//input[@type="search"]')
    LOCATOR_SEARCH_BTN = (By.XPATH, '//input[@type="submit"]')
