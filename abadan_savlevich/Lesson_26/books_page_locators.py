from selenium.webdriver.common.by import By


class BooksPageLocators:
    """
    Класс локаторов книг.
    """
    LOCATOR_BOOKS_HEADER = (By.CSS_SELECTOR, '.page-header>h1')
    LOCATOR_BOOKS_PATH = (By.CSS_SELECTOR, '.breadcrumb>.active')
    LOCATOR_BOOKS_FICTION = (By.XPATH, '(//a[@href="/en-gb/catalogue'
                                       '/category/books/fiction_3/"])[2]')
