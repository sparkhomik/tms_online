from selenium.webdriver.common.by import By


class BasketPageLocators:
    """
    Класс локаторов корзины.
    """
    LOCATOR_BASKET_HEADER = (By.CSS_SELECTOR, '.page-header>h1')
    LOCATOR_BASKET_PATH = (By.CSS_SELECTOR, '.breadcrumb>.active')
    LOCATOR_EMPTY_BASKET = (By.CSS_SELECTOR, '#content_inner>p')
