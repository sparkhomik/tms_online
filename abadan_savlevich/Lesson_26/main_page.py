from base_page import BasePage
from login_page import LoginPage
from books_page import BooksPage
from basket_page import BasketPage
from search_page import SearchPage
from main_page_locators import MainPageLocators


class MainPage(BasePage):
    """
    Класс главной страницы сайта.
    """
    URL = "http://selenium1py.pythonanywhere.com/en-gb/"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def open_login_page(self):
        """
        Открытие страницы авторизации.
        :return: страница авторизации
        """
        login_link = self.find_element(MainPageLocators.LOCATOR_MAIN_PAGE)
        login_link.click()
        return LoginPage(self.driver, self.driver.current_url)

    def open_books_page(self):
        """
        Открытие страницы книг.
        :return: страница книг
        """
        books_link = self.find_element(MainPageLocators.LOCATOR_BOOKS_PAGE)
        books_link.click()
        return BooksPage(self.driver, self.driver.current_url)

    def open_basket_page(self):
        """
        Открытие страницы корзины.
        :return: страница корзины
        """
        basket_link = self.find_element(MainPageLocators.LOCATOR_BASKET_PAGE)
        basket_link.click()
        return BasketPage(self.driver, self.driver.current_url)

    def open_search_page(self, book_name):
        """
        Поиск книги по названию.
        :param book_name: название книги
        :return: страница с результатом поиска
        """
        search_field = self.find_element(MainPageLocators.LOCATOR_SEARCH_INPUT)
        search_field.send_keys(book_name)
        search_button = self.find_element(MainPageLocators.LOCATOR_SEARCH_BTN)
        search_button.click()
        return SearchPage(self.driver, self.driver.current_url)
