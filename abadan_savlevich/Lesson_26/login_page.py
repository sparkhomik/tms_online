from base_page import BasePage
from login_page_locators import LoginPageLocators


class LoginPage(BasePage):
    """
    Класс страницы логина.
    """

    def get_title(self):
        return self.driver.title

    def get_login_form(self):
        return self.find_element(LoginPageLocators.LOCATOR_LOGIN_FORM)

    def get_register_form(self):
        return self.find_element(LoginPageLocators.LOCATOR_REGISTER_FORM)

    def get_header_name(self):
        return self.find_element(LoginPageLocators.LOCATOR_LOGIN_HEADER)
