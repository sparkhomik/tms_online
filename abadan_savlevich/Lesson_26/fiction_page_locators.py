from selenium.webdriver.common.by import By


class FictionPageLocators:
    """
    Класс локаторов страницы Fiction.
    """
    LOCATOR_FICTION_HEADER = (By.CSS_SELECTOR, '.page-header>h1')
    LOCATOR_FICTION_PATH = (By.CSS_SELECTOR, '.breadcrumb>.active')
