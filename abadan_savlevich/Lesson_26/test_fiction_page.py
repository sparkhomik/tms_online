from main_page import MainPage


def test_fiction_page_title(driver):
    """
    Проверка, что заголовок страницы "Fiction | Oscar - Sandbox".
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    books_page = main_page.open_books_page()
    fiction_page = books_page.open_fiction_page()
    assert fiction_page.get_title() == "Fiction | Oscar - Sandbox", \
        "Incorrect fiction page title!"


def test_fiction_header_name(driver):
    """
    Проверка, что header равен "Fiction".
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    books_page = main_page.open_books_page()
    fiction_page = books_page.open_fiction_page()
    assert fiction_page.get_header_name().text == "Fiction", \
        "Incorrect fiction page header!"


def test_fiction_path_name(driver):
    """
    Проверка наличия "Fiction" в path.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    books_page = main_page.open_books_page()
    fiction_page = books_page.open_fiction_page()
    assert fiction_page.get_path_name().text == "Fiction", \
        "Incorrect fiction page path!"
