from pages.shining_panda_page import ShiningPandaPage


def test_shining_panda_page_title(driver):
    """
    Тестовая функция проверки заголовка страницы.
    :param driver: драйвер.
    """
    shining_panda_page = ShiningPandaPage(driver)
    shining_panda_page.open()
    assert shining_panda_page.get_title() == "ShiningPanda | Jenkins plugin", \
        "incorrect title"


def test_shining_panda_page_visible_title(driver):
    """
    Тестовая функция проверки существования элемента title.
    :param driver: драйвер.
    """
    shining_panda_page = ShiningPandaPage(driver)
    shining_panda_page.open()
    assert shining_panda_page.get_visible_title(), "title absent"


def test_shining_panda_page_text(driver):
    """
    Тестовая функция проверки текста title.
    :param driver: драйвер.
    """
    shining_panda_page = ShiningPandaPage(driver)
    shining_panda_page.open()
    assert shining_panda_page.get_visible_title().text == "ShiningPanda", \
        "incorrect title text"
