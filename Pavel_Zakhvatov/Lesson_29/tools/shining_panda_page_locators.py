from selenium.webdriver.common.by import By


class ShiningPandaPageLocators:
    """
    Класс с локаторами элементов страницы ShiningPanda.
    """
    LOCATOR_TITLE = (By.XPATH, "//h1[@class='title']")
