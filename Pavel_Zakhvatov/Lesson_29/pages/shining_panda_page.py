from pages.base_page import BasePage
from tools.shining_panda_page_locators import ShiningPandaPageLocators


class ShiningPandaPage(BasePage):
    """
    Класс страницы ShiningPanda.
    """

    URL = "https://plugins.jenkins.io/shiningpanda"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title(self):
        """
        Метод получения заголовка страницы.
        :return: заголовок.
        """
        return self.driver.title

    def get_visible_title(self):
        """
        Метод проверки видимости элемента - title.
        :return: элемент если найден и видимый.
        """
        title_element = self.visible_element(
            ShiningPandaPageLocators.LOCATOR_TITLE)
        return title_element
