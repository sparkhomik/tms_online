"""
Цветочница.
Для каждого вида цветка создан класс.
Для каждого вида аксессуаров создан класс.
Для букета создан класс.
Возможность вывода одного цветка.
Возможность собрать букет из цветов и аксессуаров.
Возможность определить стоимость букета с учетом аксессуаров.
Возможность определить среднее время увядания букета.
Возможность сортировки цветов в букете по времени увядания, цвету,
длине стебля и стоимости цветов.
Возможность поиска цветов в букете по времени увядания, цвету,
длине стебля и стоимости.
Возможность проверить букет на наличие цветка.
Возможность получения каждого цветка из букета.
"""


class Flowers:
    """
    Класс Цветы.
    Атрибуты экземпляра класса:
    - freshness - время увядания в днях.
    - color - цвет.
    - length - длина стебля.
    """

    def __init__(self, freshness, color, length):
        self.freshness = freshness
        self.color = color
        self.length = length

    def __lt__(self, freshness):
        self.freshness = freshness


class Accessories:
    """
    Класс Аксессуары.
    Атрибуты экземпляра класса:
    - color - цвет.
    """

    def __init__(self, color):
        self.color = color


class Roses(Flowers):
    """
    Класс Розы.
    Атрибуты класса:
    - price - стоимость.
    - name - имя.
    Экземпляр класса можно вывести(цвет имя).
    """
    price = 80
    name = 'роза'

    def __str__(self):
        return f'{self.color} {Roses.name}'


class Tulips(Flowers):
    """
    Класс Тюльпаны.
    Атрибуты класса:
    - price - стоимость.
    - name - имя.
    Экземпляр класса можно вывести(цвет имя).
    """
    price = 40
    name = 'тюльпан'

    def __str__(self):
        return f'{self.color} {Tulips.name}'


class Violets(Flowers):
    """
    Класс Фиалки.
    Атрибуты класса:
    - price - стоимость.
    - name - имя.
    Экземпляр класса можно вывести(цвет имя).
    """
    price = 60
    name = 'фиалка'

    def __str__(self):
        return f'{self.color} {Violets.name}'


class Daisies(Flowers):
    """
    Класс Ромашки.
    Атрибуты класса:
    - price - стоимость.
    - name - имя.
    Экземпляр класса можно вывести(цвет имя).
    """
    price = 20
    name = 'ромашка'

    def __str__(self):
        return f'{self.color} {Daisies.name}'


class Paper(Accessories):
    """
    Класс Бумага.
    Атрибуты класса:
    - price - стоимость.
    - name - имя.
    Экземпляры класса можно вывести(цвет имя).
    """
    price = 5
    name = 'бумага'

    def __str__(self):
        return f'{self.color} {Paper.name}'


class Tape(Accessories):
    """
    Класс Лента.
    Атрибуты класса:
    - price - стоимость.
    - name - имя.
    Экземпляры класса можно вывести(цвет имя).
    """
    price = 2
    name = 'лента'

    def __str__(self):
        return f'{self.color} {Tape.name}'


class Bouquet:
    """
       Класс Букет.
       Атрибуты экземпляра класса:
       - flower_list - список цветов.
       - accessories_list - список аксессуаров.
       Экземпляры класса можно:
       - вывести.
       - проверить на наличие в flower_list.
       - пройтись по элементам flower_list.
       Методы класса:
       - стоимость.
       - среднее время увядания.
       - сортировка.
       - поиск.
       """

    def __init__(self, flower_list, accessories_list):
        self.flower_list = flower_list
        self.accessories_list = accessories_list

    def __len__(self):
        return len(self.flower_list)

    def __iter__(self):
        return iter(self.flower_list)

    def __contains__(self, item):
        for i in self.flower_list:
            if bool(i.freshness == item.freshness and i.color == item.
                    color and i.length == item.length):
                return True
            continue
        return False

    def __str__(self):
        flowers = [str(arg).split(',') for arg in self.flower_list]
        accessories = [str(arg).split(',') for arg in self.accessories_list]
        args_str = ''
        flowers_str = ''
        for arg in zip(*flowers):
            args_str += ', '.join(arg)
        for arg in zip(*accessories):
            flowers_str += ', '.join(arg)
        return args_str + ', ' + flowers_str

    def bouquet_price(self) -> int:
        """
        Функция получения общей стоимости букета.
        return: общая стоимость цветов в букете + аксессуары
        """
        flower_price = [i.price for i in self.flower_list]
        accessories_price = [i.price for i in self.accessories_list]
        total_flower = sum(flower_price)
        total_accessories = sum(accessories_price)
        return total_flower + total_accessories

    def wilting_time(self) -> float:
        """
        Функция получения среднего времени увядания букета.
        :return: среднее время увядания букета.
        """
        args_time = [i.freshness for i in self.flower_list]
        total_time = sum(args_time)
        average_time = total_time / len(args_time)
        return round(average_time, 1)

    def sorting(self, key=None) -> str:
        """
        Фукнция сортировки цветов в букете по параметру key.
        Первый шаг - сортировка списка.
        Второй шаг - из списка собирается строка.
        :param key: параметр фильтрации, lambda функция.
        :return: отсортированная строка.
        """
        sorting_args_str = ''
        args = sorted(self.flower_list, key=key)
        args = [str(arg).split(',') for arg in args]
        for arg in zip(*args):
            sorting_args_str += ', '.join(arg)
        return sorting_args_str

    def sorting_freshness(self) -> str:
        """
        Фукнция сортировки цветов в букете по цвету.
        Вызов функции сортировки в которую передается ключ параметр сортировки.
        :return: отсортированная строка.
        """
        return self.sorting(key=lambda flower: flower.freshness)

    def sorting_color(self) -> str:
        """
        Фукнция сортировки цветов в букете по цвету.
        Вызов функции сортировки в которую передается ключ параметр сортировки.
        :return: отсортированная строка.
        """
        return self.sorting(key=lambda flower: flower.color)

    def sorting_length(self) -> str:
        """
        Фукнция сортировки цветов в букете по возрастанию длины стебля.
        Вызов функции сортировки в которую передается ключ параметр сортировки.
        :return: отсортированная строка.
        """
        return self.sorting(key=lambda flower: flower.length)

    def sorting_price(self) -> str:
        """
        Фукнция сортировки цветов в букете по возрастанию цены за цветок.
        Вызов функции сортировки в которую передается ключ параметр сортировки.
        :return: отсортированная строка.
        """
        return self.sorting(key=lambda flower: flower.price)

    def search(self, key=None) -> str:
        """
        Фукнция поиска цветов в букете по параметру key.
        Первый шаг - фильрация списка.
        Второй шаг - из списка собирается строка.
        :param key: параметр фильтрации, lambda функция.
        :return: отфильтрованная строка.
        """
        sorting_args_str = ''
        args = filter(key, self.flower_list)
        args = [str(arg).split(',') for arg in args]
        for arg in zip(*args):
            sorting_args_str += ', '.join(arg)
        return sorting_args_str

    def freshness_search(self, freshness=None) -> str:
        """
        Функция поиска цветов в букете по значению freshness.
        Вызов функции фильтрации и передача в нее параметра.
        :param freshness: значени freshness, время увядания.
        :return: отсортированная строка.
        """
        return self.search(lambda flower: flower.freshness == freshness)

    def color_search(self, color=None) -> str:
        """
        Функция поиска цветов в букете по значению color.
        Вызов функции фильтрации и передача в нее параметра.
        :param color: значени color, цвет.
        :return: отсортированная строка.
        """
        return self.search(lambda flower: flower.color == color)

    def length_search(self, length=None) -> str:
        """
        Функция поиска цветов в букете по значению length.
        Вызов функции фильтрации и передача в нее параметра.
        :param length: значени length, длина стебля.
        :return: отсортированная строка.
        """
        return self.search(lambda flower: flower.length == length)

    def price_search(self, price=None) -> str:
        """
        Функция поиска цветов в букете по значению price.
        Вызов функции фильтрации и передача в нее параметра.
        :param price: значени price, стоимость цветка.
        :return: отсортированная строка.
        """
        return self.search(lambda flower: flower.price == price)


def start() -> None:
    """
    Функция запуска модуля.
    :return: None.
    """

    # создаются экземпляры классов.
    rose1 = Roses(4, 'красная', 15)
    rose2 = Roses(5, 'белая', 14)
    rose3 = Roses(4, 'ораньжевая', 12)
    tulips1 = Tulips(3, 'белый', 10)
    tulips2 = Tulips(6, 'красный', 11)
    daisies1 = Daisies(2, 'белая', 5)
    violets1 = Violets(4, 'фиолетовая', 4)
    tulips3 = Tulips(2, 'розовый', 10)

    paper1 = Paper('крафт')
    tape2 = Tape('нить')

    # создаются списки цветов и аксессуаров.
    flower_list = [rose1, rose2, tulips1, tulips2, daisies1, violets1, tulips3]
    accessories_list = [paper1, tape2]

    # создается экземпляр класса букет используя списки цветов и аксессуаров.
    bouquet = Bouquet(flower_list, accessories_list)

    print('ЦВЕТОК:', violets1)
    print('СОСТАВ БУКЕТА:', bouquet)
    print(f'СТОИМСОТЬ БУКЕТА: {bouquet.bouquet_price()} руб')
    print(f'СРЕДНЕЕ ВРЕМЯ УВЯДАНИЯ БУКЕТА: {bouquet.wilting_time()} дней/дня')
    print('СОРТИРОВКА ПО РОСТУ ВРЕМЕНИ УВЯДАНИЯ:', bouquet.sorting_freshness())
    print('СОРТИРОВКА ПО ЦВЕТУ:', bouquet.sorting_color())
    print('СОРТИРОВКА ПО УВЕЛИЧЕНИЮ ДЛИНЫ СТЕБЛЯ:', bouquet.sorting_length())
    print('СОРТИРОВКА ПО УВЕЛИЧЕНИЮ ЦЕНЫ:', bouquet.sorting_price())
    print('ПОИСК ПО УВЯДАНИЮ(2):', bouquet.freshness_search(freshness=2))
    print('ПОИСК ПО ЦВЕТУ(белая):', bouquet.color_search(color='белая'))
    print('ПОИСК ПО ДЛИНЕ СТЕБЛЯ(10):', bouquet.length_search(length=10))
    print('ПОИСК ПО ЦЕНЕ(80):', bouquet.price_search(price=80))
    print('ПРОВЕРКА НАЛИЧИЯ(rose1):', rose1 in bouquet)
    print('ПРОВЕРКА НАЛИЧИЯ(rose3):', rose3 in bouquet)

    print('КАЖДЫЙ ЦВЕТОК В БУКЕТЕ:')
    for i in bouquet:
        print(i)


if __name__ == '__main__':
    start()
