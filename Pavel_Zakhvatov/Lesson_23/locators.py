from selenium.webdriver import Safari
import time


def safari_driver_start():
    """
    Функия старта драйвера.
    :return: драйвер.
    """
    safari_driver = Safari()
    return safari_driver


def login_page(driver_arg, link_arg: str) -> str:
    """
    Функция открытия страницы логина.
    :param driver_arg: Драйвер.
    :param link_arg: ссылка на страницу логина.
    :return: строка с заголовком страницы.
    """
    driver_arg.get(link_arg)
    return driver_arg.title


def username_find(driver_arg):
    """
    Функция поиска поля username.
    :param driver_arg: драйвер.
    :return: найденный элемент.
    """
    return driver_arg.find_element_by_xpath("//input[@name='user-name']")


def password_find(driver_arg):
    """
    Функция поиска поля password.
    :param driver_arg: драйвер.
    :return: найденный элемент.
    """
    return driver_arg.find_element_by_xpath("//input[@name='password']")


def login_find(driver_arg):
    """
    Функция поиска кнопки login.
    :param driver_arg: драйвер.
    :return: найденный элемент.
    """
    return driver_arg.find_element_by_xpath("//input[@name='login-button']")


def login_start(username_arg, password_arg, login_arg, username_str_arg: str,
                password_str_arg: str) -> None:
    """
    Функция ввода username, пароля и нажатия на кнопку login.
    :param username_arg: элемент поле username.
    :param password_arg: элемент поле password.
    :param login_arg: элемент кнопка login.
    :param username_str_arg: строка с логином.
    :param password_str_arg: строка с паролем.
    :return: None.
    """
    username_arg.send_keys(username_str_arg)
    password_arg.send_keys(password_str_arg)
    login_arg.click()


def products_find(driver_arg) -> None:
    """
    Функция поиска и вывода всех товаров и их цену.
    :param driver_arg: драйвер.
    :return: None.
    """
    products = driver_arg.find_elements_by_xpath(
        "//div[@class='inventory_item_name']")
    prices = driver_arg.find_elements_by_xpath(
        "//div[@class='inventory_item_price']")
    print("products and prices:")
    for number in range(len(products)):
        print(f"{products[number].text} - {prices[number].text}")


def not_clickable_elements_find(driver_arg) -> list:
    """
    Функция поиска всех некликабельных элементов на странице.
    Находит элемент и добавляет его в список.
    Если поиск идет по elements, то добавляет список элементов в общий список.
    :param driver_arg: драйвер.
    :return: список найденных элементов.
    """
    not_clickable_elements = []
    main_logo = driver_arg.find_element_by_xpath("//div[@class='app_logo']")
    title = driver_arg.find_element_by_xpath("//span[@class='title']")
    title_picture = driver_arg.find_element_by_xpath("//div[@class='peek']")
    copy = driver_arg.find_element_by_xpath("//div[@class='footer_copy']")
    footer_logo = driver_arg.find_element_by_xpath(
        "//img[@class='footer_robot']")
    products_description = driver_arg.find_elements_by_xpath(
        "//div[@class='inventory_item_desc']")
    not_clickable_elements.extend(
        [main_logo, title, title_picture, copy, footer_logo,
         products_description])
    return not_clickable_elements


def clickable_elements_find(driver_arg) -> list:
    """
    Функция поиска всех кликабельных элементов на странице.
    Находит элемент и добавляет его в список.
    Если поиск идет по elements, то добавляет список элементов в общий список.
    :param driver_arg: драйвер.
    :return: список найденных элементов.
    """
    clickable_elements = []
    burger = driver_arg.find_element_by_xpath(
        "//div[@class='bm-burger-button']")
    cart = driver_arg.find_element_by_xpath("//a[@class='shopping_cart_link']")
    filter_el = driver_arg.find_element_by_xpath(
        "//span[@class='active_option']")
    twitter = driver_arg.find_element_by_xpath(
        "//a[@href='https://twitter.com/saucelabs']")
    facebook = driver_arg.find_element_by_xpath(
        "//a[@href='https://www.facebook.com/saucelabs']")
    linkedin = driver_arg.find_element_by_xpath(
        "//a[@href='https://www.linkedin.com/company/sauce-labs/']")
    add_button = driver_arg.find_elements_by_xpath(
        "//button[@class='btn btn_primary btn_small btn_inventory']")
    products_picture = driver_arg.find_elements_by_xpath(
        "//img[@class='inventory_item_img']")
    clickable_elements.extend(
        [burger, cart, filter_el, twitter, facebook, linkedin, add_button,
         products_picture])
    return clickable_elements


def safari_driver_close(driver_arg) -> None:
    """
    Функция закрытия драйвера.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.close()


def main_logging(link_arg, username_str_arg, password_str_arg) -> None:
    """
    Функция вызова всех функций и их логирования.
    После каждого вызова фукции выводит соответсвующее сообщение.
    :param link_arg: ссылка на страницу логина.
    :param username_str_arg: строка с логином.
    :param password_str_arg: строка с паролем.
    :return: None.
    """
    # старт драйвера.
    driver = safari_driver_start()
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"driver start: {driver}")

    # открытие страницы логина.
    page_title = login_page(driver, link_arg)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"login page open: {page_title}")

    # поиск поля username.
    username = username_find(driver)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"username field found: {username}")

    # поиск поля password.
    password = password_find(driver)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"password field found: {password}")

    # поиск кнопки login.
    login = login_find(driver)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"login button found: {login}")

    # ввод логина, пароля и нажатие на кнопку login.
    login_start(username, password, login, username_str_arg, password_str_arg)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"login and password are accepted and login successful")

    # вывод всех товаров и их цену.
    products_find(driver)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"products and prices found and printed")

    # поиск кликабельных элементов на странице.
    clickable_elements = clickable_elements_find(driver)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"clickable elements found: {clickable_elements}")

    # поиск некликабельных элементов на странице.
    not_clickable_elements = not_clickable_elements_find(driver)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"not clickable elements found: {not_clickable_elements}")

    # закрытие драйвера.
    safari_driver_close(driver)
    print(f"----log---- {time.strftime('%H:%M:%S')} ---- "
          f"driver close: {driver}")


if __name__ == '__main__':
    link = "https://www.saucedemo.com/"
    username_str = "standard_user"
    password_str = "secret_sauce"

    # вызов функции логирования и передача в нее ссылки, логина и пароля.
    main_logging(link, username_str, password_str)
