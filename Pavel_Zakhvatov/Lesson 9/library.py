"""
Библиотека.
Пользователь может брать книгу, возвращать и резервировать.

Вывод в консоле по логике:
Если требуется ввод - просто строка.
Если одно сообщение - строка начинается с |.
Если сообщение содержит пункты - строка начинается с |, пункты с ||.
"""


class Book:
    """
    Класс Book.
    Создает экземпляр по соответсвующим атрибутам.
    status - по умолчанию каждый экземпляр класса имеет статус 'free',
    так как каждая новая книга по умолчанию считается свободной.
    """
    status = 'free'

    def __init__(self, title: str, author: str, pages: int, isbn: str) -> None:
        """
        Атрибуты экземпляра класса.
        :param title: название книги.
        :param author: автор.
        :param pages: количество страниц.
        :param isbn: ISBN.
        """
        self.title = title
        self.author = author
        self.pages = pages
        self.isbn = isbn


class User:
    """
    Класс User.
    Создает экземпляр по соответсвующим атрибутам.
    По умолчанию у нового пользователя списки busy_book и booked_book пустые,
    так как книги он еще не брал.
    """

    def __init__(self, name: str, surname: str) -> None:
        """
        Атрибуты экземпляра класса.
        :param name: имя пользователя.
        :param surname: фамилия пользователя.
        """
        self.name = name
        self.surname = surname
        self.busy_book = []
        self.booked_book = []

    @staticmethod
    def library_books(library) -> None:
        """
        Функция вывода всех существующих книг в библиотеке во всех их статусах.
        Выводит все существующие книги в списке library.
        :return: None.
        """
        print('| All library books:')
        for i in library:
            print(
                f'|| "{i.title}" - {i.author}, {i.pages} pages, '
                f'ISBN: {i.isbn}, status: {i.status}')
        print('\n')

    def take_book(self, book: str) -> None:
        """
        Функция получения книги.
        Взять можно тольку книги со статусом 'free'.
        Если проверка статуса книги прошла в функции check_book, то эта книга
        добавляется в список busy_book пользователя и меняет статус на 'busy'
        в библиотеке (список library).
        Из списка библиотеки книга не удаляется, только меняет статус.
        :param book: название книги.
        :return: None.
        """
        if check_book(book):
            active_book = check_book(book)
            print(f'| Did you take the book (Вы взяли книгу): '
                  f'"{active_book.title}" - {active_book.author}\n')
            active_book.status = 'busy'
            self.busy_book.append(active_book)

    def return_book(self, book: str) -> None:
        """
        Функция возврата книги.
        Вернуть книгу может только тот пользователь который ее взял.
        Проходит поиск книги которую хочет вернуть пользователь в списке этого
        пользователя busy_book.
        Если такая книга есть, она удаляется из списка пользователя busy_book
        и статус книги меняется на 'free' в библиотеке (список library).
        :param book: название книги.
        :return: None
        """
        for i in self.busy_book:
            if book == i.title:
                for j in library:
                    if book == j.title:
                        j.status = 'free'
                print(f'| You returned the book "{book}"\n')
                self.busy_book.remove(i)
                break
        else:
            print('| You didnt take this book (Вы не брали эту книгу).')

    def booking_book(self, book: str) -> None:
        """
        Функция бронирования книги.
        Если проверка статуса книги прошла в функции check_book, то эта книга
        добавляется пользователю в список booked_book и статус книги меняется
        на 'booked' в библиотеке (список library).
        Из списка библиотеки книга не удаляется, только меняет статус.
        :param book: название книги.
        :return: None.
        """
        if check_book(book):
            active_book = check_book(book)
            print(f'| Have you booked a book (Вы забронировали книгу): '
                  f'"{active_book.title}" - {active_book.author}')
            active_book.status = 'booked'
            self.booked_book.append(active_book)

    def user_status(self) -> None:
        """
        Функция статуса книг пользователя.
        Нужна для актуального вывода информации по книгам пользователя
        в его списках или их отсутсвие.
        :return: None.
        """
        if self.busy_book:
            print('| Books that you have (Книги у вас):')
            for i in self.busy_book:
                print(f'|| "{i.title}" - {i.author}')
        else:
            print(
                '| You dont have books (У вас нет книг которые вы взяли).')

        if self.booked_book:
            print('| Your booked books (Ваши забронированные книги):')
            for i in self.booked_book:
                print(f'|| "{i.title}" - {i.author}')
        else:
            print(
                '| You dont have books booked (У вас нет книг которые вы '
                'забронировали).\n')


"""
Созданы 4 экземпляра класса.
Добавлены в список library, это будет библиотекой.
"""
book1 = Book('It', 'Stephen King', 1138, '0-670-81302-8')
book2 = Book('Dreamcatcher', 'Stephen King', 620, '978-0-7432-1138-3')
book3 = Book('Wind-Up Bird Chronicle', 'Haruki Murakami', 607, '0-679-77543-9')
book4 = Book('A Wild Sheep Chase', 'Haruki Murakami', 299, '0-87011-905-2')
library = [book1, book2, book3, book4]
"""
Созданы 2 экземпляра класса.
Добавлены в список users, это пользователи которые уже зарегистрированы.
"""
user1 = User('Pavel', 'Zakhvatov')
users = [user1]


def add_users(name: str, surname: str) -> User:
    """
    Функция создания нового пользователя и добавления его
    в список пользователей.
    :param name: имя пользователя.
    :param surname: фамилия пользователя.
    :return: новый пользователь.
    """
    user = User(name, surname)
    users.append(user)
    return user


def user_name_verification(name: str, surname: str) -> User:
    """
    Функция верификации пользователя.
    Проверяет есть ли пользователя с именем и фамилией в списки уже
    существующих пользователей.
    Если есть то считаем что это старый пользователь и он становится активным,
    то есть он залогинился.
    Если такого пользщователя нет в списке существующих, то считаем его новым,
    вызывам функцию создания нового пользователя и он становится активным,
    то есть он зарегистрировался и сразу залогинился.
    Приветсвия у старого и нового пользователя отличаются.
    :param name: имя пользователя.
    :param surname: фамилия пользователя.
    :return: пользователь который совершает сейчас операции.
    """
    for i in users:
        if name + surname == i.name + i.surname:
            print(f'| You entered as {i.name} {i.surname}.')
            active_user = i
            break
    else:
        print(f'| Welcome to the library {name} {surname}.')
        active_user = add_users(name, surname)

    return active_user


def check_book(book: str):
    """
    Функция поиска и проверки книги.
    Проверка проходит по статусу книг.
    Если книга занята или забронирована вернет False.
    Если книги с таким названием нет в библиотеке вообще, вернет None.
    Если книга есть вернет ее.
    :param book:
    :return: может вернуть False, None или active_book.
    """
    for i in library:
        if i.title == book:
            if i.status == 'busy':
                print('| Book has already been taken (Книга уже занята).')
                return False
            elif i.status == 'booked':
                print('| Book is already booked (Книга уже забронирована).')
                return False
            elif i.status == 'free':
                active_book = i
                return active_book
    else:
        print('| The book is not in the library.')


def action(user_action: str, active_user: User) -> str:
    """
    Функция действий пользователя.
    Для каждого пункта меню вызывается соответсвующая функция.
    Если требуется ввод имени книги то запрашивается ввод перед вызовом.
    :param user_action: выбранный пункт меню.
    :param active_user: пользователь который совершает сейчас операции.
    :return:
    """
    if user_action == '1':
        active_user.library_books(library)
    elif user_action == '2':
        book = input('Enter book name: ')
        active_user.take_book(book)
    elif user_action == '3':
        book = input('Enter book name: ')
        active_user.booking_book(book)
    elif user_action == '4':
        book = input('Enter book name: ')
        active_user.return_book(book)

    elif user_action == '0':
        return user_action


def login() -> User:
    """
    Функция логина.
    Введеные пользователем имя и фамилия проверяются на существование
    пользователя, вызывая функцию user_name_verification.
    :return: пользователь который совершает сейчас операции.
    """
    print('| Login/Registration.\n')
    name = input('Enter name: ')
    surname = input('Enter surname: ')
    active_user = user_name_verification(name, surname)
    return active_user


def main(active_user: User) -> None:
    """
    Функция основного меню пользователя.
    Вызывается user_status - статус книг пользователя, для того чтобы
    информация по книгам пользователя была всегда в меню.
    Вызывается функция action - обработка выбранных пунктов меню.
    Позволяет прервать цикл - что является выходом пользователя, но не
    завершением выполнения программы.
    :param active_user: пользователь который совершает сейчас операции.
    :return: None.
    """
    while True:
        active_user.user_status()
        print(
            '| Actions:\n'
            '|| 1 - all library books\n|| 2 - take book\n'
            '|| 3 - booking book\n'
            '|| 4 - return book\n\n|| 0 - log out\n')
        user_action = input('Select action: ')
        action(user_action, active_user)
        if user_action == '0':
            break


def start() -> None:
    """
    Функция старт.
    Первое меню пользователя.
    Вызывает функцию main - основное меню пользователя.
    Функция для того чтобы не прерывая работу программы можно было входить,
    выходить за разных старых и новых пользователей и производить непрерывные
    манипуляции с книгами.
    :return: None.
    """
    while True:
        print('|Actions:\n|| 1 - login/registration\n|| 0 - quit\n')
        first_action = input('Select menu item: ')
        if first_action == '1':
            active_user = login()
            main(active_user)
        elif first_action == '0':
            break
        else:
            print('| Item not found.\n')


"""
Для запуска модуля самого по себе используется следующее условие.
Если модуль запускается сам по себе, то вызывается функция start.
"""
if __name__ == '__main__':
    start()
