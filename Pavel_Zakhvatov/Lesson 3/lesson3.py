import random
import math

# a и b меняем значения, b меняем знак
a, b = 10, 13
a, b = b, -a
print('Task 1: a =', a, ', b =', b, '\n')

# a увеличиваем в 3, b уменьшаяем на 3
a *= 3
b -= 3
print('Task 2: a =', a, ', b =', b, '\n')

# a преобразовываем в float, b в строку
a = float(a)
b = str(b)
print('Task 3: a =', type(a), a, ', b =', type(b), b, '\n')

# a делим на 11, результат 3 знака после запятой
print('Task 4: a =', round(a / 11, 3), '\n')

# b преобразовываем в float, записываем в c и возводим в 3 степень
b = float(b)
c = b
print('Task 5: c =', c ** 3, '\n')

# получаем случайное число кратное 3
print('Task 6: random =', random.randrange(0, 100, 3), '\n')

# получаем квадратный корень из 100 и возводим в 4 степень
print('Task 7: ответ =', (math.sqrt(100)) ** 4, '\n')

# выводим 'Hi guys' 4 раза и добавляем 'Today'
s = ('Hi guys' * 4) + 'Today'
print('Task 8:')
print(s, '\n')

# получаем длину строки
print('Task 9: длина строки =', len(s), '\n')

# выводим today в прямом и обратном порядке
s1 = s[-5::1]
s2 = s[-1:-6:-1]
print('Task 10:')
print(s1)
print(s2, '\n')

# выводим каждую вторую букву в прямом и обратном порядке
s = s.replace(" ", "")  # заменяем пробелы в строке на пустое значение
s3 = s[1::2]
s4 = s[-2::-2]
print('Task 11:')
print(s3)
print(s4, '\n')

# выводим результат через форматирование
s5 = f'Task 10: {s1}, {s2}, Task 11: {s3}, {s4}'
print('Task 12:')
print(s5, '\n')

# выводим строку, каждое слово с большой, в нижнем и верхнем регистре
print('Task 13:')
print(s5.title())
print(s5.lower())
print(s5.upper(), '\n')

# считаем скольтко раз встречается слово Task
print('Task 14: слово Task встречается', s5.count('Task'), "раза")
