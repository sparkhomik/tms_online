# Task 1. Проверка валидности карты:
def card_valid(card: str) -> bool:
    """
    Функция проверки номера карты по алгоритму Луна.
    :param card: строка, введенный номер карты.
    :return: значение True/False в зависисмости от валидности.
    """
    checsum = 0
    number = list(map(int, card))
    for indx, elem in enumerate(number):
        if indx % 2 == 0:
            elem_mylt = elem * 2
            if elem_mylt > 9:
                elem_mylt -= 9
            checsum += elem_mylt
        else:
            checsum += elem
    return checsum % 10 == 0


a = input('Введите номер карты:')
print(card_valid(a))
