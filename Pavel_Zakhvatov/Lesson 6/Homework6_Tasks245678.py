# Task 2. Подсчет количества букв:
def str_amount_let(str_orig: str) -> str:
    """
    Функия подсчета количества букв в строке.
    :param str_orig: строка.
    :return: строка вида: буква + количество.
    """
    str_dict = dict()
    final_str = ''
    for i in str_orig:
        str_dict[i] = str_dict.get(i, 0) + 1
    for key, value in str_dict.items():
        if value == 1:
            value = ''
        final_str = final_str + key + str(value)
    return final_str


print('Task 2:')
print(str_amount_let("abeehhhhhccced"), '\n')


# Task 4. Функция с изменяемым числом входных параметров:
def function(mpa: int, *apa: int, name=None, **ana: str) -> dict:
    """
    Функия с изменяемым числом входных параметров.
    :param mpa: обязательный позиционный аргумент.
    :param apa: переменное количество позиционных аргументов.
    :param name: обязательный именованный аргумент.
    :param ana: переменное количество именованных аргументов.
    :return: словарь, в котором ключи это описание переменных.
    """
    result = {'mandatory_position_argument': mpa,
              'additional_position_arguments': apa,
              'mandatory_named_argument': {'name': name},
              'additional_named_arguments': ana}

    return result


print('Task 4:')
print(function(1, 2, 3, name='test', surname='test2', some='something'), '\n')


# Task 5. Области видимости Changed_list:
def add_in_list(loc_my_list: list) -> list:
    """
    Функция добавления элемента в список.
    :param loc_my_list: список созданный на уровне модуля.
    :return: измененный список.
    """
    changed_list = loc_my_list
    changed_list.append('a')
    return changed_list


my_list = [1, 2, 3]
print('Task 5:')
print(my_list)
print(add_in_list(my_list), '\n')


# Task 6. Подсчет количества типов данных:
def types_amount(list_data: list) -> dict:
    """
    Функция подсчета количества данных по типу в списке.
    :param list_data: список.
    :return: словарь, где ключ - тип, значение - количество.
    """
    list_data_dict = dict()
    list_data_len = len(list_data)
    list_data_types = []
    for i in list(range(list_data_len)):
        list_data_types.append(type(list_data[i]))
    for j in list_data_types:
        list_data_dict[j] = list_data_dict.get(j, 0) + 1
    return list_data_dict


print('Task 6:')
print(types_amount([1, 2, 'a', (1, 2), 'b']), '\n')


# Task 7. Объекты с одинаковым hash, но разными id:
a = 419
b = 419.0
print('Task 7:')
print('id a:', id(a), 'id b:', id(b))
print('hash a:', hash(a), 'hash b:', hash(b))
print('a is b:', a is b)
print('a == b:', a == b, '\n')


# Task 8. Поиск в списке объекта который можно вызвать:
def callable_f(list_f: list) -> list:
    """
    Функция поиска возможных для вызова объектов списка.
    :param list_f: список.
    :return: список с объектами для вызова.
    """
    result_list = []
    list_f_len = len(list_f)
    for i in range(list_f_len):
        if callable(list_f[i]):
            result_list.append(list_f[i])
    return result_list


print('Task 8:')
print(callable_f([int, str, dict, 1, 2, 3]), '\n')
