from xml.etree import ElementTree


def get_books():
    """
    Функция get запроса для получения содержимого файла.
    :return: содержимое файла переведенное из xml формата.
    """
    file = open("library.xml", "r")
    content = ElementTree.fromstring(file.read())
    file.close()
    return content


def find_author(root_arg, name: str) -> list:
    """
    Функция поиска книг по имени/часть имени автора.
    :param root_arg: содержимое файла.
    :param name: имя автора/ часть имени.
    :return: список id книг.
    """
    books = []
    for book in root_arg:
        tag_author = book.find("author")
        if name in tag_author.text:
            books.append(book.attrib["id"])
    return books


def find_price(root_arg, price) -> list:
    """
    Функция поиска книг по цене.
    :param root_arg: содержимое файла.
    :param price: цена, может быть float или int.
    :return: список id книг.
    """
    books = []
    for book in root_arg:
        tag_price = book.find("price")
        if str(price) == tag_price.text:
            books.append(book.attrib["id"])
    return books


def find_title(root_arg, title: str) -> list:
    """
    Функция поиска книг по названию.
    :param root_arg: содержимое файла.
    :param title: название книги/часть названия.
    :return: список id книг.
    """
    books = []
    for book in root_arg:
        tag_title = book.find("title")
        if title in tag_title.text:
            books.append(book.attrib["id"])
    return books


def find_description(root_arg, description: str) -> list:
    """
    Функция поиска книг по описанию.
    :param root_arg: содержимое файла.
    :param description: описание книги/часть описания.
    :return: список id книг.
    """
    books = []
    for book in root_arg:
        tag_description = book.find("description")
        if description in tag_description.text:
            books.append(book.attrib["id"])
    return books


def list_print(list_arg: list) -> None:
    """
    Функция вывода содержимого списка.
    :param list_arg: список для вывода.
    :return: None.
    """
    for arg in list_arg:
        print(arg)


if __name__ == '__main__':
    # содержимое файла сохраняется в переменной.
    root = get_books()

    # вывод id книг при поиске по имени автора.
    print("find_author:")
    list_print(find_author(root, "a"))
    print("----")

    # вывод id книг при поиске по цене.
    print("find_price:")
    list_print(find_price(root, 44.95))
    print("----")

    # вывод id книг при поиске по назанию.
    print("find_title:")
    list_print(find_title(root, "Code"))
    print("----")

    # вывод id книг при поиске по описанию.
    print("find_description:")
    list_print(find_description(root, "to"))
