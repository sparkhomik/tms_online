import yaml
import json
from get_post_put_delete import get_all_authors


def get_order() -> dict:
    """
    Функция получения содержимого yaml файла.
    :return: словарь содержимого файла.
    """
    file = open("order.yaml", "r")
    content = yaml.safe_load(file)
    file.close()
    return content


def get_invoice_number(content: dict) -> str:
    """
    Функция получения номера заказа.
    :param content: словарь содержимого файла.
    :return: строка со значением ключа.
    """
    return content["invoice"]


def get_address(content: dict) -> dict:
    """
    Функция получения адреса.
    :param content: словарь содержимого файла.
    :return: словарь адрес.
    """
    return content["bill-to"]["address"]


def get_product(content: dict) -> tuple:
    """
    Функция получения списка содержимого и количества.
    :param content: словарь содержимого файла.
    :return: тапл (список содержимого, общее количество)
    """
    quantity = 0
    products = []
    for product in content["product"]:
        quantity += product["quantity"]
        products.append(product["sku"] + " " + product["description"])
    return products, quantity


def get_total(content: dict) -> float:
    """
    Функция получения стоимости.
    :param content: словарь содержимого файла.
    :return: стоимость.
    """
    return content["total"]


def yaml_to_json(content: dict) -> None:
    """
    Фукция конвертации yaml в json.
    :param content: словарь содержимого файла.
    :return: None.
    """
    file_json = open("order.json", "w+")
    # дата переведена в строку чтобы избежать ошибки.
    content["date"] = str(content["date"])
    json.dump(content, file_json)
    file_json.close()


def yaml_create_file(content: dict) -> None:
    """
    Функция создания yaml файла и записи в него содержимого content.
    :param content: словарь для записи в файл.
    :return: None.
    """
    file = open("test.yaml", "w+")
    yaml.dump(content, file)
    file.close()


if __name__ == '__main__':
    # содержимое файла сохраняется в переменной.
    content_arg = get_order()

    # вывод номера заказа.
    print("invoice_number:")
    print(get_invoice_number(content_arg))
    print("----")

    # вывод адреса.
    print("address:")
    for i in get_address(content_arg).values():
        print(i)
    print("----")

    # вывод списка содержимого.
    print("products:")
    print(get_product(content_arg))
    print("----")

    # вывод количества.
    print("quantity:")
    print(get_product(content_arg)[-1])
    print("----")

    # вывод стоимости.
    print("total:")
    print(get_total(content_arg))

    # конвертация yaml файла в json.
    yaml_to_json(content_arg)

    # создание и запись в файл содержимого по адресу.
    authors_address = "https://fakerestapi.azurewebsites.net/api/v1/Authors"
    yaml_content = get_all_authors(authors_address)
    yaml_create_file(yaml_content)
