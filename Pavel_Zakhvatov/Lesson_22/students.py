import json


def get_students() -> dict:
    """
    Функция получения содержимого json файла.
    :return: словарь содержимого файла.
    """
    file = open("students.json", "r")
    content_arg = json.loads(file.read())
    file.close()
    return content_arg


def filter_class(content_arg: dict, class_arg: str) -> list:
    """
    Функция фильтрации по классу.
    :param content_arg: словарь содержимого файла.
    :param class_arg: класс.
    :return: список словарей по фильтру.
    """
    students = []
    for student in content_arg:
        if student["Class"] == class_arg:
            students.append(student)
    return students


def filter_club(content_arg: dict, club: str) -> list:
    """
    Функция фильтрации по секции.
    :param content_arg: словарь содержимого файла.
    :param club: секция.
    :return: список словарей по фильтру.
    """
    students = []
    for student in content_arg:
        if student["Club"] == club:
            students.append(student)
    return students


def filter_gender(content_arg: dict, gender: str) -> list:
    """
    Функция фильтрации по полу.
    :param content_arg: словарь содержимого файла.
    :param gender: пол.
    :return: список словарей по фильтру.
    """
    students = []
    for student in content_arg:
        if student["Gender"] == gender:
            students.append(student)
    return students


def find_name(content_arg: dict, name: str) -> list:
    """
    Функция поиска по имени.
    :param content_arg: словарь содержимого файла.
    :param name: имя/часть имени.
    :return: список словарей по поиску.
    """
    students = []
    for student in content_arg:
        if name in student["Name"]:
            students.append(student)
    return students


def list_print(list_arg: list) -> None:
    """
    Функция вывода всодержимого списка.
    :param list_arg: список для вывода.
    :return: None.
    """
    for arg in list_arg:
        print(arg)


if __name__ == '__main__':
    # содержимое файла сохраняется в переменной.
    content = get_students()

    # вывод учеников отфильтрованных по классу.
    print("filter_class:")
    list_print(filter_class(content, "5a"))
    print("----")

    # вывод учеников отфильтрованных по секции.
    print("filter_club:")
    list_print(filter_club(content, "box"))
    print("----")

    # вывод учеников отфильтрованных по полу.
    print("filter_gender:")
    list_print(filter_gender(content, "M"))
    print("----")

    # вывод учеников отфильтрованных по имени.
    print("find_name:")
    list_print(find_name(content, "ki"))
