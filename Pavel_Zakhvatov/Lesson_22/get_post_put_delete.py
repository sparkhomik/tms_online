import requests
import json


def get_all_authors(arg_address: str) -> dict:
    """
    Функция get запроса для получения авторов.
    :param arg_address: адрес запроса.
    :return: словарь авторов переведеный из json формата.
    """
    response = requests.get(arg_address)
    return json.loads(response.content)


def get_all_authors_name(arg_authors: dict) -> list:
    """
    Функция получения списка авторов.
    :param arg_authors: словарь авторов.
    :return: список авторов.
    """
    authors_list = []
    for author in arg_authors:
        authors_list.append(author["firstName"] + " " + author["lastName"])
    return authors_list


def get_id_author(arg_authors: dict, arg_id: int) -> list:
    """
    Функция получения автора по id.
    :param arg_authors: словарь авторов.
    :param arg_id: id автора.
    :return: список firstName, lastName автора.
    """
    for author in arg_authors:
        if arg_id == author["id"]:
            return [author["firstName"], author["lastName"]]


def post_book(address: str, arg_id: int, title: str, description: str,
              page_count: int, excerpt: str, publish_date: str):
    """
    Функция post запроса для добавления книги.
    :param address: адрес запроса.
    :param arg_id: id книги.
    :param title: название книги.
    :param description: описание книги.
    :param page_count: количество страниц.
    :param excerpt: отрывок книги.
    :param publish_date: дата публикации.
    :return: ответ сервера.
    """
    response = requests.post(address, json={"id": arg_id, "title": title,
                                            "description": description,
                                            "pageCount": page_count,
                                            "excerpt": excerpt,
                                            "publishDate": publish_date})
    return response


def post_user(address: str, arg_id: int, user_name: str, password: str):
    """
    Функция post запроса для добавления пользователя.
    :param address: адрес запроса.
    :param arg_id: id пользователя.
    :param user_name: имя пользователя.
    :param password: пароль пользователя.
    :return: ответ сервера.
    """
    response = requests.post(address, json={"id": arg_id,
                                            "userName": user_name,
                                            "password": password})
    return response


def put_book(address: str, arg_id: int, title: str, description: str,
             page_count: int, excerpt: str, publish_date: str):
    """
    Функция put запроса для обновления данных книги.
    :param address: адрес запроса.
    :param arg_id: id книги.
    :param title: название книги.
    :param description: описание книги.
    :param page_count: количество страниц.
    :param excerpt: отрывок книги.
    :param publish_date: дата публикации.
    :return: ответ сервера.
    """
    actual_address = address + "/" + str(arg_id)
    response = requests.put(actual_address, json={"title": title,
                                                  "description": description,
                                                  "pageCount": page_count,
                                                  "excerpt": excerpt,
                                                  "publishDate": publish_date})
    return response


def delete_user(address: str, arg_id: int):
    """
    Функция delete запроса для удаления пользователя.
    :param address: адрес запроса.
    :param arg_id: id пользователя.
    :return: ответ сервера.
    """
    actual_address = address + "/" + str(arg_id)
    response = requests.delete(actual_address)
    return response


if __name__ == '__main__':
    # адреса запросов.
    authors_address = "https://fakerestapi.azurewebsites.net/api/v1/Authors"
    books_address = "https://fakerestapi.azurewebsites.net/api/v1/Books"
    users_address = "https://fakerestapi.azurewebsites.net/api/v1/Users"

    # вывод авторов.
    authors = get_all_authors(authors_address)
    for i in get_all_authors_name(authors):
        print(i)

    # вывод автора по id.
    print(*(get_id_author(authors, 5)))

    # вывод ответа сервера при добавлении книги.
    print(post_book(books_address, 12345, "test", "test", 6, "test",
                    "2021-10-08T13:33:34.799Z"))

    # вывод ответа сервера при добавлении пользователя.
    print(post_user(users_address, 12, "test", "123456"))

    # вывод ответа сервера при обновлении книги.
    print(put_book(books_address, 10, "test1", "test1", 0, "test1",
                   "2021-10-08T14:19:34.994Z"))

    # вывод ответа сервера при удалении пользователя.
    print(delete_user(users_address, 4))
