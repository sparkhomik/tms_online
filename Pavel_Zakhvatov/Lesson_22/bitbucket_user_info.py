import requests
import json


def authorization_user_info(access_token_arg: str, api_link_arg: str) -> tuple:
    """
    Функция get запроса для получения информации о пользователе.
    Токен для авторизации передается в заголовок запроса.
    Вывод информации о пользователе.
    :param access_token_arg: строка с токеном.
    :param api_link_arg: строка с api ссылкой.
    :return: тапл (словарь данные пользователя, uuid пользователя).
    """
    response_user = requests.get(f"{api_link_arg}/user",
                                 headers={"Authorization": access_token_arg})
    user_info = json.loads(response_user.content)
    print(f"USER: {user_info['username']}, name: {user_info['display_name']}, "
          f"status: {user_info['account_status']}")
    print("----")
    user_id = user_info["uuid"]
    return user_info, user_id


def repositories(user_info_arg: dict) -> None:
    """
    Функция get запроса для получения информации о репозиториях пользователя.
    Вывод информации о репозиториях.
    :return: None.
    """
    link = user_info_arg["links"]["repositories"]["href"]
    response_repositories = requests.get(link)
    repositories = json.loads(response_repositories.content)
    print("REPOSITORIES:")
    for i in repositories["values"]:
        print(
            f"full name: {i['full_name']}, uuid: {i['uuid']}, "
            f"branch: {i['mainbranch']['name']}")
    print("----")


def pullrequests_merged(user_id_arg: str, api_link_arg: str) -> None:
    """
    Функция get запроса для получения информации о пулреквестах пользователя
    со статусом merged.
    Вывод информации о пулреквестах.
    :param user_id_arg: uuid пользователя.
    :param api_link_arg: строка с api ссылкой.
    :return: None.
    """
    link = f"{api_link_arg}/pullrequests/{user_id_arg}" \
           f"?state=MERGED"
    response_pullrequests_merged = requests.get(link)
    pullrequests_merged = json.loads(response_pullrequests_merged.content)

    for i in pullrequests_merged["values"]:
        comments_link = i["links"]["comments"]["href"]
        response_comments = requests.get(f"{comments_link}")
        comments = json.loads(response_comments.content)
        print(
            f"{len(comments['values'])} comments in {i['id']} pullrequest, "
            f"branch: {i['source']['branch']['name']}, "
            f"status pullrequest: {i['state']}")

    print("MERGED:", len(pullrequests_merged["values"]))


def pullrequests_open(user_id_arg: str, api_link_arg: str) -> None:
    """
    Функция get запроса для получения информации о пулреквестах пользователя
    со статусом open.
    Вывод информации о пулреквестах.
    :param user_id_arg: uuid пользователя.
    :param api_link_arg: строка с api ссылкой.
    :return: None.
    """
    link = f"{api_link_arg}/pullrequests/{user_id_arg}" \
           f"?state=OPEN"
    response_pullrequests_open = requests.get(link)
    pullrequests_open = json.loads(response_pullrequests_open.content)

    for i in pullrequests_open["values"]:
        comments_link = i["links"]["comments"]["href"]
        response_comments = requests.get(f"{comments_link}")
        comments = json.loads(response_comments.content)
        print(
            f"{len(comments['values'])} comments in {i['id']} pullrequest, "
            f"branch: {i['source']['branch']['name']}, "
            f"status pullrequest: {i['state']}")

    print("OPEN:", len(pullrequests_open["values"]))


if __name__ == '__main__':
    """
    токен для доступа, рассматривается вариант когда токен уже известен.
    токен имеет срок действия, необходим актуальный.
    """
    access_token = "Bearer rDEqb-XGaOHPIVFEayfnZBBZ5ObZngjtasKB99II4z" \
                   "Nzgkcl_EQe98wItkfPhaCqDb_ipYdFEoVfbly7ireEVh" \
                   "Ixa_e364GiOPs_-y0x9KmrPD8yE03xSuve"

    api_link = "https://api.bitbucket.org/2.0"

    # содержимое о пользователе сохраняется в переменной.
    user_info = authorization_user_info(access_token, api_link)

    # вызов функции для вывода информации о репозиториях.
    repositories(user_info[0])

    # вызов функции для вывода информации о merged пулреквестах.
    pullrequests_merged(user_info[1], api_link)

    # вызов функции для вывода информации о open пулреквестах.
    pullrequests_open(user_info[1], api_link)
