"""
Ферма.
Абстрактный общий класс Fauna.
2 класса Animals и Birds (наследники абстрактного класса),
от которых унаследованы еще 4 класса, Pig, Goose, Chicken и Cow.
У общего класса 2 атрибута, имя и возраст(изменить нельзя, приватный).
У животных метод - ходить, у птиц - летать.
Метод у общего класса - стареть и метод голос.
Класс ферма - принимает список животных.
Каждое животное можно получить через цикл или вызвать по индексу.
Метод увеличения возраста всех животных на ферме.
"""
from abc import ABC, abstractmethod


class Fauna(ABC):
    """
    Класс Фауна.
    Является абстрактным.
    Содержит общие атрибуты для Животных и Птиц и общие методы.
    """
    def __init__(self, name, age):
        self.name = name
        self._age = age

    @abstractmethod
    def voice(self):
        """
        Абстрактный метод голос.
        Для переопредления в классе наследника под соответсвующий голос.
        """
        raise NotImplementedError

    def up_age(self) -> None:
        """
        Метод старения.
        Увеличивает возраст на 1.
        :return: None.
        """
        self._age += 1

    def __getattr__(self, item):
        """
        Метод проверки есть ли атрибуты класса которые можно вызвать.
        Если таких нет, то срабатает пользовательский exception.
        """
        raise NotExistException(item, self.__class__.__name__)


class Animals(Fauna, ABC):
    """
    Класс Животные.
    Наследник класса Фауна и абстрактного класса.
    """

    @staticmethod
    def walk() -> str:
        """
        Метод Ходит.
        :return: строка 'я хожу'
        """
        return 'I walk.'


class Birds(Fauna, ABC):
    """
    Класс Птицы.
    Наследник класса Фауна и абстрактного класса.
    """

    @staticmethod
    def fly() -> str:
        """
        Метод Летает.
        :return: строка 'Я летаю'.
        """
        return 'I fly.'


class Pig(Animals):
    """
    Класс Свинья.
    Наследует атрибуты класса Животные.
    """
    def __str__(self) -> str:
        return f'{Pig.__name__} by nickname {self.name}, ' \
               f'age: {self._age}, says "{self.voice()}"'

    def voice(self) -> str:
        """
        Переопределенный метод голос.
        :return: строка 'хрю'.
        """
        return 'хрю'


class Goose(Birds):
    """
    Класс Гусь.
    Наследует атрибуты класса Птицы.
    """
    def __str__(self):
        return f'{Goose.__name__} by nickname {self.name}, ' \
               f'age: {self._age}, says "{self.voice()}"'

    def voice(self) -> str:
        """
        Переопределенный метод голос.
        :return: строка 'гага'.
        """
        return 'гага'


class Chicken(Birds):
    """
    Класс Курица.
    Наследует атрибуты класса Птицы.
    """
    def __str__(self):
        return f'{Chicken.__name__} by nickname {self.name}, ' \
               f'age: {self._age}, says "{self.voice()}"'

    def voice(self) -> str:
        """
        Переопределенный метод голос.
        :return: строка 'кудах'.
        """
        return 'кудах'


class Cow(Animals):
    """
    Класс Корова.
    Наследует атрибуты класса Животные.
    """
    def __str__(self):
        return f'{Cow.__name__} by nickname {self.name}, ' \
               f'age: {self._age}, says "{self.voice()}"'

    def voice(self) -> str:
        """
        Переопределенный метод голос.
        :return: строка 'му'.
        """
        return 'му'


class Farm:
    """
    Класс Ферма.
    Атрибут animals_list, список экземпляров классов.
    """

    def __init__(self, animals_list):
        self.animals_list = animals_list

    def __iter__(self):
        return iter(self.animals_list)

    def __getitem__(self, item):
        return self.animals_list[item]

    def up_age(self) -> None:
        """
        Метод старения - увеличение возраста каждого животного через генератор
        списка.
        """
        [animal.up_age() for animal in self.animals_list]


class NotExistException(Exception):
    """
    Exception.
    Если атрибут экземпляра класса или класса не существует то будет
    вызвана ошибка с выводом сообщения которое сожержит вызываемый метод и
    класс.
    """

    def __init__(self, attr, name):
        self.message = f'Method/attribute "{attr}" does not exist in the' \
                       f' "{name}" class'
        super().__init__(self.message)


def all_animals(farm: Farm) -> None:
    """
    Функция получения всех животных.
    Выводит на печать каждый экземпляр класса из списка.
    """
    for animal in farm:
        print(animal)


def index_animal(farm: Farm, index: int) -> None:
    """
    Функция получения животного по индексу.
    Выводит на печать экземпляр класса полученый по индексу.
    """
    print(farm[index])


def up_age(farm: Farm) -> None:
    """
    Функия старения.
    Вызывает метод старения экземпляра класса Ферма.
    """
    farm.up_age()


def test():
    # Создаются экземпляры классов и записываются в список.
    pig = Pig('Хрюша', 2)
    chicken = Chicken('Курочка', 1)
    goose = Goose('Гус', 1)
    cow = Cow('Коровка', 4)

    animals = [pig, chicken, goose, cow]

    # Создается экземпляр класса Ферма из списка.
    farm = Farm(animals)

    # Вызывается функия получения всех животных.
    all_animals(farm)
    print('')

    # Вызывается функия получения животного по индексу.
    index_animal(farm, 2)

    # Вызывается функция старения.
    up_age(farm)
    print('')

    # Вызывается функия получения всех животных.
    all_animals(farm)
    print('')

    # Проверка вызова несуществующего метода, проверка ошибки.
    goose.Test()


if __name__ == '__main__':
    test()
