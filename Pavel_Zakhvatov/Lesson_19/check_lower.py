def check_lower(word: str) -> bool:
    """
    Функция проверки что все буквы в переданном слове в нижнем регистре.
    :param word: строка - проверяемое слово.
    :return: результат проверки в виде булевого значения.
    """
    return word.islower()


if __name__ == "__main__":
    # примеры вывода.
    print(check_lower("TeSt"))
    print(check_lower("test"))
    print(check_lower("TEST"))
