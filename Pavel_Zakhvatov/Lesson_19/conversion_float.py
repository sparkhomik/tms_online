class InputTypeError(Exception):
    """
    Пользовательская ошибка InputTypeError.
    В сообщении ошибки указывается какой передоваемый аргумент ее вызвал.
    """
    def __init__(self, arg):
        self.message = f"InputTypeError, argument '{arg}' is not a int!!"
        super().__init__(self.message)


def conversion_float(arg) -> float:
    """
    Функция перевода переданного аргумента в тип float.
    Если переданный аргумент не типа int, то вызывается пользовательская
    ошибка InputTypeError.
    :param arg: аргумент любого типа.
    :return: аргумент типа float.
    """
    if type(arg) != int:
        raise InputTypeError(arg)
    else:
        return float(arg)


if __name__ == "__main__":
    # примеры вывода.
    print(conversion_float(24))
    print(conversion_float('dfsdf'))
    print(conversion_float(-1))
