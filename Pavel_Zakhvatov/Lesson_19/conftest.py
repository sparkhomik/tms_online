import pytest
import time
import sys


@pytest.fixture(scope="session")
def time_start_stop_fixture():
    """
    Фикстура вывода времени запуска теста на этапе сетап и времени окончания
    теста на этапе тирдаун.
    Фикстура вызывается один раз за сессию.
    Формат вывод времени ЧЧ:ММ:СС.
    :return: Время старта сессии в секундах.
    """
    start_session_time = int(time.strftime("%S"))
    print("\n---- START TIME:", time.strftime("%X"), "----")
    yield start_session_time
    print("\n---- STOP TIME:", time.strftime("%X"), "----")


@pytest.fixture(scope="session")
def file_name_fixture():
    """
    Фикстура вывода имени файла из которого был запущен тест на этапе сетап.
    Фикстура вызывается один раз за сессию.
    Вывод только конечного имени, без полного пути.
    """
    print("\n---- TEST FILE NAME:", sys.argv[-1].split("/")[-1], "----")
    # print("\n---- TEST FILE NAME:", request.module, "----") - 2й вариант.


@pytest.fixture(scope="function", autouse=True)
def info_fixture(time_start_stop_fixture, request):
    """
    Фикстура вывода:
    1. на этапе сетап - имена используемых фикстур в тесте.
    Вывод имен вызываемых функций в которых содержится слово fixture.
    2. на этапе тирдаун - время между началом сессии и запуском теста.
    Время выводится в секундах. Из секунд времени на момент тирдаун
    вычитаются секунды времени на момент запуска модуля.
    Фикстура вызывается для каждой тестовой функции.
    Вывод:
    1. имена используемых фикстур.
    2. время между началом сессии и запуском теста.
    """
    for i in request.fixturenames:
        if "fixture" in i:
            print("\n---- START FIXTURE:", i, "----")
    yield
    # start_session_time = time_start_stop_fixture
    stop_test_time = int(time.strftime("%S"))
    print("\n---- TIME:", stop_test_time - time_start_stop_fixture, "sec ----")
    print("------------------------------------------------")
