def check_special_character(arg) -> bool:
    """
    Функция проверки наличия в переданном аргументе спецсимволов.
    :param arg: проверяемый аргумент любого типа.
    :return: результат проверки, если сивол есть, вернет True, если нет None.
    """
    for character in list(str(arg)):
        if character in "!@#$%^&*(){}<!№%:,.;[]":
            return True


if __name__ == "__main__":
    # примеры вывода.
    print(check_special_character(5))
    print(check_special_character("test"))
    print(check_special_character("!"))
    print(check_special_character("t@$t"))
