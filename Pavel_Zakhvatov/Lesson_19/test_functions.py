import pytest
from check_lower import check_lower
from conversion_float import conversion_float
from conversion_float import InputTypeError
from check_special_character import check_special_character
import time


@pytest.mark.usefixtures("time_start_stop_fixture", "file_name_fixture")
class TestClass:
    """
    Тестовый класс для объединения тестовых методов.
    Вызывает фикстуры time_start_stop_fixture и file_name_fixture.
    """

    @pytest.mark.parametrize("word, result", [("TEST", False), ("test", True),
                                              ("tEsT", False)])
    def test_check_lower(self, word: str, result: bool):
        """
        Тестовый метод проверки функции check_lower.
        Ставнивает проверяемое слово с ожидаемым результатом.
        :param word: проверяемое слово.
        :param result: ожидаемый результат проверки.
        """
        time.sleep(1)
        assert check_lower(word) == result

    @pytest.mark.parametrize("arg, result",
                             [(-23, -23.0), (24, 24.0), (1, 1.0)])
    def test_conversion_float(self, arg, result: float):
        """
        Тестовый метод проверки функции conversion_float.
        Сравнивает переданный агрумент с ожидаемым результатом.
        :param arg: переданный аргумент.
        :param result: ожидаемый результат перевода.
        """
        time.sleep(1)
        assert conversion_float(arg) == result

    @pytest.mark.parametrize("arg", ["test", "1", "-1"])
    def test_conversion_float_error(self, arg):
        """
        Тестовый метод проверки функции conversion_float.
        Проверяет вызывается ли пользовательская ошибка InputTypeError, если
        передан аргумент не типа int.
        :param arg: переданный аргумент.
        """
        time.sleep(1)
        with pytest.raises(InputTypeError):
            conversion_float(arg)

    @pytest.mark.parametrize("arg, result",
                             [(5, None), ("t@$t", True), ("test", None)])
    def test_check_special_character(self, arg, result):
        """
        Тестовый метод проверки функции check_special_character.

        :param arg: переданный аргумент.
        :param result: ожидаемый результат проверки.
        """
        time.sleep(1)
        assert check_special_character(arg) == result
