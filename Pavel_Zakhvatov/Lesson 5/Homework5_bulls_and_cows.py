# быки и коровы
import random

# получаем случайное 4х значное число из неповторяющихся цифр
bulls_cows_comp = ''.join(random.sample("0123456789", 4))
print(bulls_cows_comp)
'''
Далее в цикле реализовани ввод числа и три проверки:
если введенное угадано, то цикл ввода прерывается
и две проверки на ввод меньше 4х знаков и больше 4х знаков.
Сравнение проводил приводя строку к int, а проверки через сравнение
длинны строки с 4.
Далее после else выполдняется основная часть игры и как только
число совпадет цикл прервется.
'''
while True:
    bulls_cows_user = input('Введите загаданное 4х значное число:')
    if str(bulls_cows_user) == str(bulls_cows_comp):
        print('Угадали!!')
        break
    elif len(bulls_cows_user) < 4:
        print(f'Число {len(bulls_cows_user)} значное!! Не нарушайте правила!!')
    elif len(bulls_cows_user) > 4:
        print(f'Число {len(bulls_cows_user)} значное!! Не нарушайте правила!!')
    else:
        # получаем списки с преобразованием строки в числа
        bulls_cows_comp_change = str(bulls_cows_comp)
        bulls_cows_user = str(bulls_cows_user)
        bulls_cows_comp_change = list(int(i) for i in bulls_cows_comp_change)
        bulls_cows_user = list(int(i) for i in bulls_cows_user)

        cows = 0
        bulls = 0
        '''
        По умолчанию считаем, что вводится и загадывается число
        без повторяющихся цифр.
        Условие отработает корректно только для чисел без повторяющихся цифр.
        '''
        for x in bulls_cows_user:
            if x in bulls_cows_comp_change:
                if bool(bulls_cows_user.index(
                        x) == bulls_cows_comp_change.index(x)):
                    bulls += 1
                else:
                    cows += 1

        print('быков:', bulls)
        print('коров:', cows)
