# BuzzFuzz
i = 1
'''
Проходим каждое число на проверку что остаток от деления равен 0.
Сначала что число кратно и 3 и 5, затем 3 и 5 отдельно.
'''
while i < 101:
    if bool((i % 3 == 0) and (i % 5 == 0)):
        print('FuzzBuzz')
    elif bool(i % 3 == 0):
        print('Fuzz')
    elif bool(i % 5 == 0):
        print('Buzz')
    else:
        print(i)
    i += 1
