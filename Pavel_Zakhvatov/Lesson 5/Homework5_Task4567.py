# нумеруем строку
a = ['Один', 'Два', 'Три', 'Четыре', 'Пять']
b = []
print('Task 4:')
for i, it in enumerate(a):
    j = (str(i + 1) + ': ' + a[i])
    b.append(j)
print(b, '\n')

# проверка элементов
x = [1, 1, 1]
y = [1, 2, 1]
w = ['a', 'a', 'a']
z = []
print('Task 5:')
# если сам список возвращает True, то сравниваем элементы
if bool(x):
    if bool(x[0] == x[1] == x[2]):
        print(f'{x} == {True}')
    else:
        print(f'{x} == {False}')
else:
    print(f'{x} == {True}')

if bool(y):
    if bool(y[0] == y[1] == y[2]):
        print(f'{y} == {True}')
    else:
        print(f'{y} == {False}')
else:
    print(f'{y} == {True}')

if bool(w):
    if bool(w[0] == w[1] == w[2]):
        print(f'{w} == {True}')
    else:
        print(f'{w} == {False}')
else:
    print(f'{w} == {True}')

# если список пустой то элементы не сравниваем и выводим True
if bool(z):
    if bool(z[0] == z[1] == z[2]):
        print(f'{z} == {True}')
    else:
        print(f'{z} == {False}')
else:
    print(f'{z} == {True}', '\n')

# проверка все ли буквы в строчном
s1 = 'dogcat'
s2 = 'doGCat'
s1 = list(s1)
s2 = list(s2)
print('Task 6:')
i = 0
'''
Каждый элемент в списке, то есть каждую букву проверяем на соответсвие
нижнему регистру, если регистр нижний то из списка эту букву удаляем.
'''
while i < len(s1):
    if bool(s1[i] == s1[i].lower()):
        del s1[i]
    else:
        i += 1

while i < len(s2):
    if bool(s2[i] == s2[i].lower()):
        del s2[i]
    else:
        i += 1
'''
При выводе обновленных списков также проверяем на пустоту, если список
пустой, то выводим True.
Соответсвенно тот список где все буквы в нижнем регистре будет пустой, так как
все элементы = маленькой букве были удалены, если не пустой, то будет содержать
только большие буквы.
'''
print([bool(not s1), s1])
print([bool(not s2), s2], '\n')

# складываем числа в списке
# список позволяет вернуть сумму элементов, поэтому реализовал через sum
ss1 = []
ss2 = [1, 2, 3]
ss3 = [1.1, 2.2, 3.3]
ss4 = [4, 5, 6]
ss5 = range(101)
print('Task 7:')
print(sum(ss1))
print(sum(ss2))
print(sum(ss3))
print(sum(ss4))
print(sum(ss5))
