x = 6
y = -6

# 1
if x > 0:
    print("больше 0")

if y > 0:
    print("больше 0")

# 2
if x > 0:
    print("1")
else:
    print("-1")
if y > 0:
    print("1")
else:
    print("-1")

# 3
if x == 1:
    print("x = 1")
elif x == -1:
    print("x = -1")
elif x == y:
    print("x = y")
elif x > y:
    print("x > y")
else:
    print(x)
