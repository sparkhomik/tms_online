# like
names = input("Enter names/Введите имена:")
# алфавит для сравнения с именами
list_lang = 'абвгдеёжзийклмнопрстуфхцчшщъыьэюя'
lang = 'en'  # по умолчанию язык английский
'''
Еще один способ через пересечение:
list_lang = set(list_lang)
names_lang = set(names)
if names_lang.intersection(list_lang):
    lang = 'ru'
Оба способа не сработают если есть имена и на русском и на английском или есть
русские буквы, в любом случае будет считаться что язык русский если будет хоть
одно совпадение с русской буквой.
'''
for x in names:
    if x in list_lang:
        lang = 'ru'

# разбиваю строку по запятой и получаю список
names = names.split(',')
# получаю длинну строки, в данном случае количество строк в списке
len_names = len(names)
# получаю количество строк в списке без учета 2х имен
len_names_in_text = len(names) - 2
'''
5 условий, что имя в списке одно и не пустое,
что имен 2, 3 и более 3х, через сравнение с количеством строк в списке
и соответсвенно вывожу нужное сообщение через форматирование строки.
'''
if not (names == ['']) and len_names == 1 and (lang == 'en'):
    names = ''.join(names)
    print(f'{names} like this')
elif len_names == 2 and (lang == 'en'):
    print(f'{names[0]} and{names[1]} like this')
elif len_names == 3 and (lang == 'en'):
    print(f'{names[0]},{names[1]} and{names[2]} like this')
elif len_names > 3 and (lang == 'en'):
    print(f'{names[0]},{names[1]} and {len_names_in_text} others like this')
# блок если имена вводились на русском и lang приняло значение 'ru'
elif not (names == ['']) and len_names == 1 and (lang == 'ru'):
    names = ''.join(names)
    print(f'{names} лайкнул это')
elif len_names == 2 and (lang == 'ru'):
    print(f'{names[0]} и{names[1]} лайкнули это')
elif len_names == 3 and (lang == 'ru'):
    print(f'{names[0]},{names[1]} и{names[2]} лайкнули это')
elif len_names > 3 and (lang == 'ru'):
    print(f'{names[0]},{names[1]} и {len_names_in_text} других лайкнули это')
# без проверки языка так как язык определяется при вводе
else:
    print('No one likes this')
