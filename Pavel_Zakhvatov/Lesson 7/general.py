from cipher import caesar_cipher
from cipher import personal_cipher
from tic_tac_toe import tic_tac_toe
from random import randint

"""
Общий модуль.
Возможность выбора и запуска модулей 'крестики нолики', 'шифр Цезаря'
и 'персонального шифра'.
"""


def get_module() -> str:
    """
    Функция для выбора пункта меню.
    Цикл внутри функции отработает 3 раза при неверном вводе пункта меню,
    при вводе неверного пункта меню 4й раз вернется случайный пункт меню
    1, 2 или 3.
    :return: возвращает строку с выбранным пунктом меню.
    """
    print('1 - крестики нолики, 2 - шифр Цезаря, 3 - персональный шифр, '
          '4 - завершить')
    action = input('Выберите опцию: ')
    count = 3

    while action not in ('1', '2', '3', '4'):
        print('Такой операции нет.')
        action = input('Выберите опцию: ')
        count -= 1
        if count == 0:
            action = str(randint(1, 4))
    return action


def start() -> None:
    """
    Функция запуска модуля.
    Содержит основной цикл.
    Вызываем функцию get_module() и значение action присваиваем
    переменной user_module.
    Тем самым получаем какой пункт меню был выбран.
    Цикл отрабатывает до тех пор пока user_module != '4', либо пока один
    из модулей не отработает.
    Под условиями в цикле запускается соответсвующий модуль из нужного пакета.
    :return: None
    """
    user_module = get_module()

    while True:
        if user_module == '1':
            print('КРЕСТИКИ НОЛИКИ.')
            tic_tac_toe.start()
            break

        elif user_module == '2':
            print('ШИФР ЦЕЗАРЯ.')
            caesar_cipher.start()
            break

        elif user_module == '3':
            print('ПЕРСОНАЛЬНЫЙ ШИФР.')
            personal_cipher.start()
            break

        elif user_module == '4':
            break


"""
Для запуска модуля самого по себе используется следующее условие.
Если модуль запускается сам по себе, то вызывается функция start.
"""
if __name__ == '__main__':
    start()
