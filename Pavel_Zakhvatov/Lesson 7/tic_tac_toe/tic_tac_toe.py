"""
Модуль игры крестики нолики.
Играем против компьютера.
Рисуется пустое игровое поле и предлагается выбор команды за которую будем
играть.
Делаем первый ход путем ввода координат, далее ходит компьютер и
далее по очереди.
Игра продолжается до последнего хода или до победы одной из команд.
"""
from random import randint


def get_user_char() -> str:
    """
    Функция предлогает выбор команды для игры и пресваивает переменной
    user_char знак выбранной команды.
    :return: возвращает строку с выбранной командой для игры.
    """
    print('1 - крестики,', '2 - нолики.')
    user_char = input('Выберите команду: ')
    while user_char not in ('1', '2'):
        print('Такой команды нет, повторите выбор!!')
        user_char = input('Выберите команду: ')
    if user_char == '1':
        user_char = 'X'
    else:
        user_char = '0'
    print('Ваша команда:', user_char)
    return user_char


def show_field(field: list) -> None:
    """
    Функция отображения игрового поля.
    Двумерный список представляется в виде понятного поля для игры.
    :param field: двумерный список из полей куда ставится символ в игре.
    :return: None
    """
    print('  ' + field[0][0] + '  |  ' + field[0][1] + '  |  ' + field[0][
        2] + '  ')
    print('-----|-----|-----')
    print('  ' + field[1][0] + '  |  ' + field[1][1] + '  |  ' + field[1][
        2] + '  ')
    print('-----|-----|-----')
    print('  ' + field[2][0] + '  |  ' + field[2][1] + '  |  ' + field[2][
        2] + '  ')


def draw(field: list) -> bool:
    """
    Функция определения, что результат игры - ничья.
    Проходит провека что во всех строках игрового поля, то есть по y,
    не осталосб полей со значением пробела.
    Если остались то увеличивается счетчик, значение которого сравнивается с 0.
    Если полей с пробелом нет, то функция вернет True - значит ходов больше
    не осталось.
    :param field: двумерный список из полей куда ставится символ в игре.
    :return: возвращает значение bool.
    """
    count = 0
    for y in range(3):
        if ' ' in field[y]:
            count += 1
        else:
            count = 0
    return count == 0


def get_user_position(field: list) -> tuple:
    """
    Функция для перевода введенных координат в фактические индексы поля.
    Введенная строка с координатами преобразуется в тапл, введенные
    координаты уменьшаются на 1 для перевода в фактические индесы полей.
    Если введеные координаты не существуют(за пределами 3 на 3), выводится
    соответсвующее сообщение.
    :param field: двумерный список из полей куда ставится символ в игре.
    :return: возвращает тапл с фактическими индексами полей куда пользователь
    ставит свой символ.
    """
    real_x, real_y = 0, 0
    while True:
        coordinates = input('Введите поле (строка, столбец): ')
        coordinates = coordinates.split(',')
        y, x = coordinates[0], coordinates[1]
        middle_coord = (1, 2, 3)

        if int(x) not in middle_coord or int(y) not in middle_coord:
            print('Такого поля не существет.')
            continue

        real_x = int(x) - 1
        real_y = int(y) - 1
        if field[real_y][real_x] == ' ':
            break
        else:
            print('Поле уже занято.')

    return real_x, real_y


def get_comp_char(user_char: str) -> str:
    """
    Функция опредлдения команды компьютера.
    Команда компьютера определяется как противоположная команде пользователя.
    :param user_char: строка - символ команды пользователя.
    :return: возвращает строку - символ команды компьютера.
    """
    if user_char == 'X':
        return '0'
    else:
        return 'X'


def win(user_char: str, field: list) -> bool:
    """
    Функция проверки выигрышных комбинаций.
    Сначала проверяются строки - y.
    Далее колонки - x.
    Далее диагонали.
    Проверка проходит на отсутсвие пробела и присутсвие символа компьютера,
    если пробела в условии нет и отсутсвует символ компьютера,
    значит комбинация заполнена символами пользователя и функция вернет true.
    :param user_char: строка - символ команды пользователя.
    :param field: двумерный список из полей куда ставится символ в игре.
    :return: возвращает значение bool.
    """
    comp_char = get_comp_char(user_char)
    for y in range(3):
        if comp_char not in field[y] and ' ' not in field[y]:
            return True
    for x in range(3):
        columns = [field[0][x], field[1][x], field[2][x]]
        if comp_char not in columns and ' ' not in columns:
            return True
    diagonal = [field[0][0], field[1][1], field[2][2]]
    if comp_char not in diagonal and ' ' not in diagonal:
        return True
    diagonal = [field[0][2], field[1][1], field[2][0]]
    if comp_char not in diagonal and ' ' not in diagonal:
        return True

    return False


def get_comp_position(field: list) -> tuple:
    """
    Функция для опредления координат поля куда компьютер ставит свой символ.
    Координаты генерируются рандомно от 0 до 2.
    Происходит проверка не содержит ли поле по этим координатам символ пробел.
    Если не содержит то возвращаются координаты как ход компьютера.
    :param field: двумерный список из полей куда ставится символ в игре.
    :return: возвращает тапл с фактическими индексами полей куда компьютер
    ставит свой символ.
    """
    x = randint(0, 2)
    y = randint(0, 2)
    while field[y][x] != ' ':
        x = randint(0, 2)
        y = randint(0, 2)
    return x, y


def main() -> None:
    """
    Основная функция.
    Ничего не возвращает.
    Вызывает функции get_user_char и get_comp_char, символы команды
    пользователя и компьютера.
    В цикле вызывается игровое поле.
    Проходит проверка по функции draw на ничью.
    Далее провеки на выигрыш, если введеные координаты пользователем
    удовлетворяют условиям функции win.
    Проверка на проигрыш, если полученные координаты компьютера
    удовлетворяют условиям функции win.
    Если одно из условий вернет true, то цикл прерывается и выводится
    соответсвующее сообщение.
    :return: None
    """
    field = [[' ' for x in range(3)] for y in range(3)]
    user_char = get_user_char()
    comp_char = get_comp_char(user_char)

    while True:
        show_field(field)

        if draw(field):
            show_field(field)
            print('Ничья.')
            break

        x, y = get_user_position(field)
        field[y][x] = user_char
        if win(user_char, field):
            show_field(field)
            print('Победа.')
            break

        x, y = get_comp_position(field)
        field[y][x] = comp_char
        if win(comp_char, field):
            show_field(field)
            print('Поражение.')
            break


def get_action_user() -> str:
    """
    Функция для выбора действия пользователя после завершения основной функции
    игры.
    :return: возвращает строку с выбранным пунктом меню.
    """
    print('1 - сыграть еще, 2 - завершить')
    action = input('Выберите операцию: ')
    while action not in ('1', '2'):
        print('Такой операции нет.')
        action = input('Выберите операцию: ')
    return action


def start() -> None:
    """
    Функция запуска модуля.
    Содержит основной цикл.
    Вызывает основную функцию main.
    После завершения функции main вызывает функцию get_action_user и либо
    завершает цикл, либо запускает его выполнение повторно.
    Пока будет выбран пункт 1, функция main каждый раз будет запускаться
    повторно.
    :return: None
    """
    while True:
        main()
        action = get_action_user()
        if action == '2':
            break
        elif action == '1':
            continue


"""
Для запуска модуля самого по себе используется следующее условие.
Если модуль запускается сам по себе, то вызывается функция start.
"""
if __name__ == '__main__':
    start()
