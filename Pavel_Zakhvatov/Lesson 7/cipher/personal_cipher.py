"""
Модуль персонального шифра.
Принимает текст для зашифровки.
Каждый символ текста заменяется другим, на той же позиции из алфавита шифра.
"""

"""
Два алфавита, оригинальный английский и алфавит-шифр.
Длина алфавитов одинакова.
"""
alphabet_en = 'abcdefghijklmnopqrstuvwxyz'
alphabet_encrypted = '🐿🐮🐭🐫🐯🦏🦍🦊🦒🐀🐨🦘🐖🦇🦓🐰🦝🦔🦄🐆🐄🐷🦙🐽🦁🐻'


def get_action_user() -> str:
    """
    Функция для выбора пункта меню.
    Цикл внутри функции будет выполняться до тех пор пока не будет выбран
    существующий пункт меню.
    :return: возвращает строку с выбранным пунктом меню.
    """
    print('1 - зашифровать, 2 - расшифровать, 3 - завершить')
    action = input('Выберите операцию: ')
    while action not in ('1', '2', '3'):
        print('Такой операции нет.')
        action = input('Выберите операцию: ')
    return action


def get_encrypted(message: str) -> str:
    """
    Функция для зашифровки строки.
    Для каждого символа в строке определяется его позиция в
    оригинальном алфавите.
    Символ в соответсвующей позиции из алфавита-шифра добавляется в
    строку encrypted.
    Пробелы и знаки добавляюся в строку encrypted без изменений, тем самым
    сохраняются знаки оригинальной строки.
    :param message: строка для зашифровки.
    :return: возвращает зашифрованную строку с символами в обратном порядке.
    """
    encrypted = ''
    for char in message:
        if char in alphabet_en:
            t = alphabet_en.find(char)
            encrypted += alphabet_encrypted[t]
        else:
            encrypted += char
    return encrypted[::-1]


def get_decrypted(message: str) -> str:
    """
    Функция для расшифровки строки.
    Для каждого символа в строке определяется его позиция из алфавита-шифра.
    Символ в соответсвующей позиции из оригинального алфавита добавляется в
    строку decrypted.
    Пробелы и знаки добавляюся в строку encrypted без изменений.
    :param message: строка для расшифровки.
    :return: возвращает расшифрованную строку с символами в обратном порядке.
    """
    decrypted = ''
    for char in message:
        if char in alphabet_encrypted:
            t = alphabet_encrypted.find(char)
            decrypted += alphabet_en[t]
        else:
            decrypted += char
    return decrypted[::-1]


def main(action: str) -> None:
    """
    Основная функция.
    Ничего не возвращает.
    Сравнивает вернувшийся пункт меню action и вызывает функцию get_encrypted
    или get_decrypted соответсвенно, в которую передает сообщение message.
    :param action: строка, выбранный пункт меню.
    :return: None
    """
    while True:
        if action == '1':
            message = input('Введите строку(англ. буквы в нижнем регистре): ')
            print('Зашифрованная строка:', get_encrypted(message))
            break

        elif action == '2':
            message = input('Введите строку: ')
            print('Расшифрованная строка:', get_decrypted(message))
            break


def start() -> None:
    """
    Функция запуска модуля.
    Содержит основной цикл.
    Если вернувшийся пункт action это 3, то цикл прерывается и выполнение
    модуля завершается.
    В остальных случаях вызывается функция main().
    :return: None
    """
    while True:
        action = get_action_user()
        if action == '3':
            break
        else:
            main(action)


"""
Для запуска модуля самого по себе используется следующее условие.
Если модуль запускается сам по себе, то вызывается функция start.
"""
if __name__ == '__main__':
    start()
