"""
Модуль шифра Цезаря.
Принимает текст для зашифровки и ключ - смещение.
Каждый символ текста заменяется другим, отстоящим от него в алфавите
на фиксированное число позиций.
"""

"""
Два английским алфавита для букв в верхнем и нижнем регистре.
Число букв продублированно, чтобы смещение не выходило за пределы алфавита.
"""
alphabet_en_l = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'
alphabet_en_u = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ'


def get_action_user() -> str:
    """
    Функция для выбора пункта меню.
    Цикл внутри функции будет выполняться до тех пор пока не будет выбран
    существующий пункт меню.
    :return: возвращает строку с выбранным пунктом меню.
    """
    print('1 - зашифровать, 2 - расшифровать, 3 - завершить')
    action = input('Выберите операцию: ')

    while action not in ('1', '2', '3'):
        print('Такой операции нет.')
        action = input('Выберите операцию: ')
    return action


def get_encrypted(message: str, key: int) -> str:
    """
    Функция для зашифровки строки.
    Для каждого символа в строке определяется его позиция в алфавите.
    К позиции символа прибавляется ключ - смещение и символ с новой позиции
    добавляется в строку encrypted.
    Буквы в верхнем регистре и буквы в нижнем регистре проверяются в
    соответсвующих алфавитах.
    Пробелы и знаки добавляюся в строку encrypted без изменений.
    Тем самым сохраняется регистр букв и знаки оригинальной строки.
    Функция исользуется и для расшифровки сообщения, для этого
    нужно передать key со знаком минус.
    :param message: строка для зашифровки.
    :param key: ключ - смещение.
    :return: возвращает зашифрованную строку.
    """
    encrypted = ''
    for char in message:
        if char in alphabet_en_l:
            t = alphabet_en_l.find(char)
            new_key = t + key
            encrypted += alphabet_en_l[new_key]
        elif char in alphabet_en_u:
            t = alphabet_en_u.find(char)
            new_key = t + key
            encrypted += alphabet_en_u[new_key]
        else:
            encrypted += char
    return encrypted


def main(action: str) -> None:
    """
    Основная функция.
    Ничего не возвращает.
    Сравнивает вернувшийся пункт меню action и вызывает функцию get_encrypted,
    в которую передает сообщение message и ключ.
    Если выбран пункт расшифровки сообщения, то к переданному ключу key
    добавляется знак минус.
    :param action: строка, выбранный пункт меню.
    :return: None
    """
    while True:
        if action == '1':
            message = input('Введите строку(английские буквы): ')
            key = int(input('Введите ключ: '))
            print('Зашифрованная строка:', get_encrypted(message, key))
            break

        elif action == '2':
            message = input('Введите строку: ')
            key = int(input('Введите ключ: '))
            print('Расшифрованная строка:', get_encrypted(message, -key))
            break


def start() -> None:
    """
    Функция запуска модуля.
    Содержит основной цикл.
    Если вернувшийся пункт action это 3, то цикл прерывается и выполнение
    модуля завершается.
    В остальных случаях вызывается функция main().
    :return: None
    """
    while True:
        action = get_action_user()
        if action == '3':
            break
        else:
            main(action)
            continue


"""
Для запуска модуля самого по себе используется следующее условие.
Если модуль запускается сам по себе, то вызывается функция start.
"""
if __name__ == '__main__':
    start()
