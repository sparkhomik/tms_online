"""
Лексикографическое возрастание.
На вход подаётся некоторое количество (не больше сотни) разделённых пробелом
целых чисел (каждое не меньше 0 и не больше 19).
Выводим числа через пробел в порядке лексикографического возрастания названий
этих чисел в английском языке.
"""

number_names = {
    0: 'zero', 1: 'one', 2: 'two', 3: 'three', 4: 'four', 5: 'five', 6: 'six',
    7: 'seven', 8: 'eight', 9: 'nine', 10: 'ten', 11: 'eleven', 12: 'twelve',
    13: 'thirteen', 14: 'fourteen', 15: 'fifteen', 16: 'sixteen',
    17: 'seventeen', 18: 'eighteen', 19: 'nineteen'}


def conversion_verification(numbers: str) -> list:
    """
    Функция преобразования и проверки.
    Преобразования введеных чисел из строки в числа и добавления этих
    числел в список new_input_numbers.
    Проверка введеных чисел по условиям что каждое число больше 0 и меньше 19.
    Провека что введено чисел не более 100.
    :param numbers: строка введеных чисел.
    :return: список введеных чисел приведенных к int.
    """
    numbers = numbers.split(' ')
    new_input_numbers = [int(i) for i in numbers]

    if len(new_input_numbers) < 101:
        if all((i < 20) and (i > -1) for i in new_input_numbers):
            return new_input_numbers
        else:
            print('Есть числа меньше 0 или больше 19')
    else:
        print('Чисел более 100')


def translator(new_input_numbers: list, names: dict) -> list:
    """
    Функция переводчик.
    Сравнивает введеные числа со словарем number_names соответсвующих названий.
    Добавляет найденные названия чисел в новый список input_numbers_names.
    :param new_input_numbers: список введеных чисел приведенных к int.
    :param names: словарь названия чисел.
    :return: список из названий чисел.
    """
    input_numbers_names = []
    for i in names:
        for j in new_input_numbers:
            if i == j:
                input_numbers_names.append(names.get(i))

    return input_numbers_names


def lexicographic(input_numbers_names: list) -> None:
    """
    Функция лексикографического возрастания и вывода.
    Список переведенных чисел сортирует по длине слова и переводит в строку
    через пробел.
    Выводит полученную строку.
    :param input_numbers_names: список из названий чисел.
    :return: None.
    """
    print(' '.join(sorted(input_numbers_names, key=len)))


def start() -> None:
    """
    Функция запуска модуля.
    Введенные числа записываем строкой в переменную input_numbers.
    Вызываем функции по порядку:
    1. преобразование и проверка.
    2. перевод.
    3. возрастание и вывод.
    :return: None.
    """
    input_numbers = input('Введите числа:')

    if conversion_verification(input_numbers):
        new_input_numbers = conversion_verification(input_numbers)
        input_numbers_names = translator(new_input_numbers, number_names)
        lexicographic(input_numbers_names)


if __name__ == '__main__':
    start()
