"""
Декоратор типов.
Проверяет передоваемый тип функии и конвертирует в соответсвующий тип
данные.
"""
from functools import reduce


def typed_decorator(type: str):
    def converter(func):

        def wrapper(*args):
            """
            Функция обертка.
            В зависимости от пееданного аргумента type, преобразует аргументы
            в соответсвующий тип данных.
            :param args: аргументы, тип зависит от типа который передали.
            :return: возвращает функцию в которую передает преобразованные
            к соответсвующему типу аргументы.
            """
            if type == 'str':
                args = map(str, args)
                return func(*args)
            elif type == 'int':
                args = map(int, args)
                return func(*args)
            elif type == 'float':
                args = map(float, args)
                return func(*args)

        return wrapper

    return converter


"""
Оборачиваем функцию в декоратор typed_decorator.
Функция передает декоратору тип.
"""


def reducer_func(arg0, arg1):
    """
    Функия сложения 2х аргументов.
    :param arg0: тип зависит от типа передоваемого аргумента.
    :param arg1: тип зависит от типа передоваемого аргумента.
    :return: результат сложения, тип зависит от типа передоваемого аргумента.
    """
    return arg0 + arg1


@typed_decorator(type='int')
def add_symbols(*args):
    """
    Функция принимает аргументы и возвращает их сложение.
    В декоратор передается тип type.
    Тип аргументов будет приведен к типу type в декораторе.
    Сложение производится функцией reduce.
    :return: результа сложения аргументов, тип соответствует type.
    """
    return reduce(reducer_func, (args))


print(add_symbols('5', 1, 3))


@typed_decorator(type='str')
def add_symbols(*args):
    """
    Функция принимает аргументы и возвращает их сложение.
    В декоратор передается тип type.
    Тип аргументов будет приведен к типу type в декораторе.
    Сложение производится функцией reduce.
    :return: результа сложения аргументов, тип соответствует type.
    """
    return reduce(reducer_func, (args))


print(add_symbols('5', 'a'))


@typed_decorator(type='float')
def add_symbols(*args):
    """
    Функция принимает аргументы и возвращает их сложение.
    В декоратор передается тип type.
    Тип аргументов будет приведен к типу type в декораторе.
    Сложение производится функцией reduce.
    :return: результа сложения аргументов, тип соответствует type.
    """
    return reduce(reducer_func, (args))


print(add_symbols('5.5', 1.4, 3))
