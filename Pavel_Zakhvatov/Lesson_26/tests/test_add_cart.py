from pages.main_page import MainPage


def test_add_cart(driver):
    """
    Тестовая функция проверки сообщения при добавлении товара в корзину.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    add_message = main_page.add_product_in_cart()
    assert "Product successfully added to your shopping cart"\
           in add_message.text, "product not added"


def test_del_cart(driver):
    """
    Тестовая функция проверки сообщения после удаления товара из корзины и
    корзина становится пуста.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    cart_page = main_page.open_cart_page()
    cart_page.delete_product()
    assert cart_page.get_message().text == "Your shopping cart is empty.",\
        "incorrect cart page empty message"
