from pages.main_page import MainPage


def test_main_page_title(driver):
    """
    Тестовая функция проверки заголовка страницы.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    assert main_page.get_title() == "My Store", "incorrect main page title"


def test_main_page_logo(driver):
    """
    Тестовая функция проверки существования элемента logo.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    assert main_page.get_logo(), "logo absent"


def test_clickable_search_field(driver):
    """
    Тестовая функция проверки активности поля поиска.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    assert main_page.check_clickable_search_field(),\
        "search field not clickable"


def test_clickable_search_button(driver):
    """
    Тестовая функция проверка активности кнопки поиска.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    assert main_page.check_clickable_search_button(),\
        "search button not clickable"


def test_info(driver):
    """
    Тестовая функция проверки существования блока info.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    assert main_page.get_info()
