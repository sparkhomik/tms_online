from pages.main_page import MainPage


def test_login_page_title(driver):
    """
    Тестовая функция проверки заголовка страницы.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.get_title() == "Login - My Store",\
        "incorrect login page title"


def test_login_page_header(driver):
    """
    Тестовая функция проверки текста на странице.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.get_header().text == "Authentication",\
        "incorrect login page header"


def test_clickable_create_email_field(driver):
    """
    Тестовая функция проверки активности поля email регистрации.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.check_clickable_create_email_field(),\
        "create email field not clickable"


def test_clickable_email_field(driver):
    """
    Тестовая функция проверки активности поля email авторизации.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.check_clickable_email_field(),\
        "email field not clickable"


def test_clickable_password_field(driver):
    """
    Тестовая функция проверки активности поля пароль авторизации.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.check_clickable_password_field(),\
        "password field not clickable"


def test_clickable_create_button(driver):
    """
    Тестовая функция проверки активности кнопки регистрации.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.check_clickable_create_button(),\
        "create button not clickable"


def test_clickable_sign_in_button(driver):
    """
    Тестовая функция проверки активности кнопки авторизации.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    login_page = main_page.open_login_page()
    assert login_page.check_clickable_sign_in_button(),\
        "sign in button not clickable"
