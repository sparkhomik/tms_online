from pages.main_page import MainPage


def test_cart_page_title(driver):
    """
    Тестовая функция проверки заголовка страницы.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    cart_page = main_page.open_cart_page()
    assert cart_page.get_title() == "Order - My Store",\
        "incorrect cart page title"


def test_cart_page_message(driver):
    """
    Тестовая функция проверки сообщения когда корзина пуста.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    cart_page = main_page.open_cart_page()
    assert cart_page.get_message().text == "Your shopping cart is empty.",\
        "incorrect cart page empty message"


def test_cart_page_header(driver):
    """
    Тестовая функция проверки текста на странице.
    :param driver: драйвер.
    """
    main_page = MainPage(driver)
    main_page.open()
    cart_page = main_page.open_cart_page()
    assert "Shopping-cart summary" in cart_page.get_header().text,\
        "incorrect cart page header"
