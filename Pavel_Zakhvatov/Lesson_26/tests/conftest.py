import pytest
from selenium.webdriver import Safari


@pytest.fixture(scope="session")
def driver():
    """
    Фикстура страта и закрытия драйвера.
    :return: драйвер.
    """
    driver = Safari()
    yield driver
    driver.quit()
