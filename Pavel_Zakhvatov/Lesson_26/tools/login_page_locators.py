from selenium.webdriver.common.by import By


class LoginPageLocators:
    """
    Класс с локаторами элементов страницы login.
    """
    LOCATOR_LOGIN_HEADER = (By.XPATH, "//h1[@class='page-heading']")
    LOCATOR_EMAIL_CREATE_FIELD = (By.XPATH, "//input[@name='email_create']")
    LOCATOR_EMAIL_FIELD = (By.XPATH, "//input[@name='email']")
    LOCATOR_PASSWORD_FIELD = (By.XPATH, "//input[@name='passwd']")
    LOCATOR_CREATE_BUTTON = (By.XPATH, "//button[@name='SubmitCreate']")
    LOCATOR_SIGN_IN_BUTTON = (By.XPATH, "//button[@name='SubmitLogin']")
