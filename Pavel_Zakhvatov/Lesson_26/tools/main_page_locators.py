from selenium.webdriver.common.by import By


class MainPageLocators:
    """
    Класс с локаторами элементов главной страницы.
    """
    LOCATOR_SING_IN_BUTTON = (By.XPATH, "//a[@class='login']")
    LOCATOR_CART_BUTTON = (By.XPATH, "//a[@title='View my shopping cart']")
    LOCATOR_LOGO = (
        By.XPATH, "//img[@src='http://automationpractice.com/img/logo.jpg']")
    LOCATOR_SEARCH_FIELD = (By.XPATH, "//input[@id='search_query_top']")
    LOCATOR_SEARCH_BUTTON = (By.XPATH, "//button[@name='submit_search']")
    LOCATOR_INFO = (By.XPATH, "//section[@id='block_contact_infos']")
    LOCATOR_PRODUCT = (By.XPATH, "//img[@title='Blouse']")
    LOCATOR_ADD_BUTTON = (By.XPATH, "//p[@id='add_to_cart']/button")
    LOCATOR_ADD_MESSAGE = (By.XPATH, "//div[@class='clearfix']/div/h2")
