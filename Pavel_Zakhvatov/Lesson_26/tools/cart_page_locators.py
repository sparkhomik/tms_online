from selenium.webdriver.common.by import By


class CartPageLocators:
    """
    Класс с локаторами элементов страницы cart.
    """
    LOCATOR_CART_MESSAGE = (By.XPATH, "//p[@class='alert alert-warning']")
    LOCATOR_CART_HEADER = (By.XPATH, "//h1[@id='cart_title']")
    LOCATOR_TRASH_BUTTON = (By.XPATH, "//i[@class='icon-trash']")
