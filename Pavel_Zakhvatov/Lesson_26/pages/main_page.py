from pages.base_page import BasePage
from tools.main_page_locators import MainPageLocators
from pages.login_page import LoginPage
from pages.cart_page import CartPage


class MainPage(BasePage):
    """
    Класс главной страницы.
    """

    URL = "http://automationpractice.com/index.php"

    def __init__(self, driver):
        super().__init__(driver, self.URL)

    def get_title(self):
        """
        Метод получения заголовка страницы.
        :return: заголовок.
        """
        return self.driver.title

    def open_login_page(self):
        """
        Метод перехода на страницу авторизации.
        """
        login_link = self.find_element(MainPageLocators.LOCATOR_SING_IN_BUTTON)
        login_link.click()
        return LoginPage(self.driver, self.driver.current_url)

    def open_cart_page(self):
        """
        Метод перехода на страницу корзины.
        """
        cart_link = self.find_element(MainPageLocators.LOCATOR_CART_BUTTON)
        cart_link.click()
        return CartPage(self.driver, self.driver.current_url)

    def get_logo(self):
        """
        Метод получения элемента - лого.
        :return: элемент.
        """
        logo = self.find_element(MainPageLocators.LOCATOR_LOGO)
        return logo

    def check_clickable_search_field(self):
        """
        Метод проверки активности элемента - поле поиска.
        """
        return self.check_clickable_element(
            MainPageLocators.LOCATOR_SEARCH_FIELD)

    def check_clickable_search_button(self):
        """
        Метод проверки активности элемента - кнопка поиска.
        :return: элемент.
        """
        return self.check_clickable_element(
            MainPageLocators.LOCATOR_SEARCH_BUTTON)

    def get_info(self):
        """
        Метод получения элемента info.
        :return: элемент.
        """
        return self.find_element(MainPageLocators.LOCATOR_LOGO)

    def add_product_in_cart(self):
        """
        Метод добавления товара в корзину с главной странице.
        :return: элемент сообщения.
        """
        product = self.find_element(MainPageLocators.LOCATOR_PRODUCT)
        product.click()
        add_button = self.find_element(MainPageLocators.LOCATOR_ADD_BUTTON)
        add_button.click()
        add_message = self.visible_element(
            MainPageLocators.LOCATOR_ADD_MESSAGE)
        return add_message
