from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.common.exceptions import TimeoutException


class BasePage:
    """
    Базовый класс.
    """

    def __init__(self, driver, url):
        self.driver = driver
        self.url = url

    def open(self):
        """
        Метод открытия окна.
        """
        self.driver.get(self.url)

    def find_element(self, locator, timeout=15):
        """
        Метод поиска элемента.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: элемент если найден.
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(
                EC.presence_of_element_located(locator))
            return element
        except TimeoutException:
            return None

    def check_clickable_element(self, locator, timeout=15) -> bool:
        """
        Метод поиска и проверки активности элемента.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: bool значение отображающее активность элемента.
        """
        try:
            WebDriverWait(self.driver, timeout).until(
                EC.element_to_be_clickable(locator))
            return True
        except TimeoutException:
            return False

    def visible_element(self, locator, timeout=15):
        """
        Метод поиска и проверки видимости элемента.
        :param locator: локатор.
        :param timeout: время ожидания.
        :return: элемент если найден и видимый.
        """
        try:
            element = WebDriverWait(self.driver, timeout).until(
                EC.visibility_of_element_located(locator))
            return element
        except TimeoutException:
            return None
