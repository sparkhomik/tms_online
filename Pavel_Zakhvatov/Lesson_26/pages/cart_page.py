from pages.base_page import BasePage
from tools.cart_page_locators import CartPageLocators


class CartPage(BasePage):
    """
    Класс страницы cart.
    """

    def get_title(self):
        """
        Метод получения заголовка страницы.
        :return: заголовок страницы.
        """
        return self.driver.title

    def get_message(self):
        """
        Метод получения элемента сообщения корзины.
        :return: элемент сообщения.
        """
        return self.find_element(CartPageLocators.LOCATOR_CART_MESSAGE)

    def get_header(self):
        """
        Метод получения элемента текста на странице.
        :return: элемент текста.
        """
        return self.find_element(CartPageLocators.LOCATOR_CART_HEADER)

    def delete_product(self) -> None:
        """
        Метод удаления товара из корзины.
        :return: None.
        """
        self.find_element(CartPageLocators.LOCATOR_TRASH_BUTTON).click()
