from pages.base_page import BasePage
from tools.login_page_locators import LoginPageLocators


class LoginPage(BasePage):
    """
    Класс страницы login.
    """

    def get_title(self):
        """
        Метод получения заголовка страницы.
        :return: заголовок страницы.
        """
        return self.driver.title

    def get_header(self):
        """
        Метод получения элемента текста на странице.
        :return: элемент текста.
        """
        return self.find_element(LoginPageLocators.LOCATOR_LOGIN_HEADER)

    def check_clickable_create_email_field(self):
        """
        Метод проверки активности элемента - поле email регистрации.
        """
        return self.check_clickable_element(
            LoginPageLocators.LOCATOR_EMAIL_CREATE_FIELD)

    def check_clickable_email_field(self):
        """
        Метод проверки активности элемента - поле email авторизации.
        """
        return self.check_clickable_element(
            LoginPageLocators.LOCATOR_EMAIL_FIELD)

    def check_clickable_password_field(self):
        """
        Метод проверки активности элемента - поле пароль авторизации.
        """
        return self.check_clickable_element(
            LoginPageLocators.LOCATOR_PASSWORD_FIELD)

    def check_clickable_create_button(self):
        """
        Метод проверки активности элемента - кнопка регистрации.
        """
        return self.check_clickable_element(
            LoginPageLocators.LOCATOR_CREATE_BUTTON)

    def check_clickable_sign_in_button(self):
        """
        Метод проверки активности элемента - кнопка авторизации.
        """
        return self.check_clickable_element(
            LoginPageLocators.LOCATOR_SIGN_IN_BUTTON)
