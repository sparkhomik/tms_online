import unittest
from check_os import Os


class TestCheckOs(unittest.TestCase):
    """
    Тестовый класс.
    """

    def test_get_name(self):
        """
        Тестовый метод проверки на равенство имени ос.
        """
        self.assertEqual(Os.get_name(), 'Darwin')

    def test_get_release(self):
        """
        Тестовый метод проверки на равенство релиза ос.
        """
        self.assertEqual(Os.get_release(), '20.6.0')

    def test_get_version(self):
        """
        Тестовый метод проверки на равенство версии ос.
        """
        self.assertEqual(Os.get_version(), '11.5')

    @unittest.expectedFailure
    def test_get_name_fail(self):
        """
        Негативный тест.
        Тестовый метод проверки на равенство имени ос.
        """
        self.assertEqual(Os.get_name(), 'Windows')

    @unittest.expectedFailure
    def test_get_release_fail(self):
        """
        Негативный тест.
        Тестовый метод проверки на равенство релиза ос.
        """
        self.assertEqual(Os.get_release(), '11.1')

    @unittest.expectedFailure
    def test_get_version_fail(self):
        """
        Негативный тест.
        Тестовый метод проверки на равенство версии ос.
        """
        self.assertEqual(Os.get_version(), '11.1')
