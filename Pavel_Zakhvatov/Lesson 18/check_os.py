"""
Определение имени, релиза и версии текущей операционной системы.
"""
import platform


class Os:
    """
    Класс ОС.
    """

    @staticmethod
    def get_name() -> str:
        """
        Статический метод класса, получение имени ос.
        :return: строка, имя ос.
        """
        return platform.system()

    @staticmethod
    def get_release() -> str:
        """
        Статический метод класса, получение релиза ос.
        :return: строка, релиз ос.
        """
        return platform.release()

    @staticmethod
    def get_version() -> str:
        """
        Статический метод класса, получение версии ос.
        Сделана проверка по имени ос, если имя 'Darwin', то есть это MacOs,
        то используется метод получения версии для MacOs,
        иначе кросплатформенный метод получения версии ос.
        :return: строка, версия ос.
        """
        if platform.system() == 'Darwin':
            return platform.mac_ver()[0]
        else:
            return platform.version()


if __name__ == '__main__':

    os = Os()
    print(os.get_name())
    print(os.get_release())
    print(os.get_version())
