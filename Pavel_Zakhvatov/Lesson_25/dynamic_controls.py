from selenium.webdriver import Safari
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


def start_driver():
    """
    Функция старта драйвера.
    :return: драйвер.
    """
    return Safari()


def open_link(driver_arg, link_arg: str) -> None:
    """
    Функция открытия окна по ссылке.
    :param driver_arg: драйвер.
    :param link_arg: ссылка.
    :return: None.
    """
    driver_arg.get(link_arg)


def checkbox_find(driver_arg):
    """
    Функция поиска элемента checkbox.
    :param driver_arg: драйвер.
    :return: элемент checkbox.
    """
    return driver_arg.find_element_by_xpath("//input[@type='checkbox']")


def button_find(driver_arg) -> tuple:
    """
    Функция поиска двух элементов - кнопка.
    :param driver_arg: драйвер.
    :return: тапл с элементами кнопка.
    """
    return driver_arg.find_element_by_xpath(
        "//button[@onclick='swapCheckbox()']"
    ), driver_arg.find_element_by_xpath(
        "//button[@onclick='swapInput()']")


def click_button(button_arg) -> None:
    """
    Функция клика по кнопке.
    :param button_arg: элемент кнопка.
    :return: None.
    """
    button_arg.click()


def checkbox_message_wait(driver_arg) -> str:
    """
    Функция ожидания появления сообщения.
    :param driver_arg: драйвер.
    :return: текст элемента сообщения.
    """
    return WebDriverWait(driver_arg, 15).until(
        EC.visibility_of_element_located((
            By.XPATH, "//form[@id='checkbox-example']/p"))).text


def checkbox_invisible(driver_arg, checkbox_arg) -> str:
    """
    Функция проверки что элемент checkbox отсутсвует.
    :param driver_arg: драйвер.
    :param checkbox_arg: элемент checkbox.
    :return: строка с сообщением результат.
    """
    if WebDriverWait(driver_arg, 5).until(EC.invisibility_of_element_located(
            checkbox_arg)):
        return "checkbox invisible"


def input_find(driver_arg):
    """
    Функция поиска поля input.
    :param driver_arg: драйвер.
    :return: элемент поле input.
    """
    return driver_arg.find_element_by_xpath(
        "//form[@id='input-example']/input")


def input_disable(driver_arg) -> str:
    """
    Функция проверки что поле input неактивно.
    :param driver_arg: драйвер.
    :return: строка с сообщением результата.
    """
    if WebDriverWait(driver_arg, 5).until_not(EC.element_to_be_clickable(
            (By.XPATH, "//form[@id='input-example']/input"))) is False:
        return "input disable"


def input_message_wait(driver_arg) -> str:
    """
    Функция ожидания появления сообщения.
    :param driver_arg: драйвер.
    :return: текст элемента сообщения.
    """
    return WebDriverWait(driver_arg, 15).until(
        EC.visibility_of_element_located((
            By.XPATH, "//form[@id='input-example']/p"))).text


def input_enable(driver_arg) -> str:
    """
    Функция проверки что поле input активно.
    :param driver_arg: драйвер.
    :return: строка с сообщением результата.
    """
    if WebDriverWait(driver_arg, 5).until(EC.element_to_be_clickable((
            By.XPATH, "//form[@id='input-example']/input"))):
        return "input enable"


def stop_driver(driver_arg) -> None:
    """
    Функция закрытия окна.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.close()


if __name__ == '__main__':
    link = "http://the-internet.herokuapp.com/dynamic_controls"
    # старт драйвера.
    driver = start_driver()
    # открытие окна по ссылке.
    open_link(driver, link)
    # поиск элемента checkbox.
    checkbox = checkbox_find(driver)
    # поиск элемента кнопка для checkbox.
    button_checkbox = button_find(driver)[0]
    # клик на кнопку для checkbox.
    click_button(button_checkbox)
    # вывод текста сообщения.
    print(checkbox_message_wait(driver))
    # вывод результата проверки отсутсвия элемента checkbox.
    print(checkbox_invisible(driver, checkbox))

    # поиск элемента поле input.
    input_field = input_find(driver)
    # вывод результата проверки активности поля input.
    print(input_disable(driver))
    # поиск элемента кнопка для input.
    button_input = button_find(driver)[1]
    # клик на кнопку для input.
    click_button(button_input)
    # вывод текста сообщения.
    print(input_message_wait(driver))
    # вывод результата проверки активности поля input.
    print(input_enable(driver))
    # закрытие окна.
    stop_driver(driver)
