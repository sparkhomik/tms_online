from selenium.webdriver import Safari
from selenium.webdriver.support.ui import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC
from selenium.webdriver.common.by import By


def start_driver():
    """
    Функция старта драйвера.
    :return: драйвер.
    """
    return Safari()


def open_link(driver_arg, link_arg: str) -> None:
    """
    Функция открытия окна по ссылке.
    :param driver_arg: драйвер.
    :param link_arg: ссылка.
    :return: None.
    """
    driver_arg.get(link_arg)


def link_find(driver_arg):
    """
    Функция поиска ссылки.
    :param driver_arg: драйвер.
    :return: элемент ссылка.
    """
    return driver_arg.find_element_by_xpath("//a[@href='/iframe']")


def link_click(link_arg) -> None:
    """
    Функция клика по элементу ссылке.
    :param link_arg: элемент ссылка.
    :return: None.
    """
    link_arg.click()


def find_frame(driver_arg):
    """
    Функция поиска фрейма.
    :param driver_arg: драйвер.
    :return: элемент фрейм.
    """
    return WebDriverWait(driver_arg, 15).until(
        EC.visibility_of_element_located((By.XPATH, "//iframe")))


def switch_frame(driver_arg, frame_arg) -> None:
    """
    Функция переключения на фрейм.
    :param driver_arg: драйвер.
    :param frame_arg: элемент фрейм.
    :return: None.
    """
    driver_arg.switch_to.frame(frame_arg)


def find_text_frame(driver_arg):
    """
    Функция поиска элемента с текстом на фрейме.
    :param driver_arg: драйвер.
    :return: элемент с текстом.
    """
    return driver_arg.find_element_by_xpath("//p")


def stop_driver(driver_arg) -> None:
    """
    Функция закрытия окна.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.close()


if __name__ == '__main__':
    link = "http://the-internet.herokuapp.com/frames"
    # старт драйвера.
    driver = start_driver()
    # открытие окна по ссылке.
    open_link(driver, link)
    # поиск ссылки и клик на нее.
    link_click(link_find(driver))
    # поиск фрейма и переключение на него.
    switch_frame(driver, find_frame(driver))
    # вывод результата сравнения текста в элементе фрейма с ожидаемым.
    print(find_text_frame(driver).text == "Your content goes here.")
    # закрытие окна.
    stop_driver(driver)
