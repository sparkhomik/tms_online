import pytest
from selenium.webdriver import Safari


@pytest.fixture(scope="session")
def safari_driver():
    """
    Фикстура запуска и закрытия драйвера.
    """
    driver = Safari()
    yield driver
    driver.quit()


@pytest.fixture()
def driver_login(safari_driver):
    """
    Фикстура открытия окна логина.
    """
    safari_driver.get("http://the-internet.herokuapp.com/login")
    return safari_driver


@pytest.fixture()
def driver_checkboxes(safari_driver):
    """
    Фикстура открытия окна с чекбоксами.
    """
    safari_driver.get("http://the-internet.herokuapp.com/checkboxes")
    return safari_driver


@pytest.fixture()
def driver_windows(safari_driver):
    """
    Фикстура открытия окна с сылкой октрытия нового окна.
    """
    safari_driver.get("http://the-internet.herokuapp.com/windows")
    return safari_driver


@pytest.fixture()
def driver_elements(safari_driver):
    """
    Фикстура открытия окна с возможностью добавления/удаления элементов.
    """
    safari_driver.get("http://the-internet.herokuapp.com/add_remove_elements/")
    return safari_driver


@pytest.fixture(params=[("tomsmith", "123", "Your password is invalid!"),
                        ("123", "123", "Your username is invalid!"), (
                        "tomsmith", "SuperSecretPassword!",
                        "You logged into a secure area!")],
                ids=["invalid pass", "invalid username", "successful"])
def login(driver_login, request) -> tuple:
    """
    Фикстура логина.
    Ищет поле логин и пароль и вводит соответсвующие данные.
    Ищет кнопку login и нажимает на нее.
    Ищет сообщение о состоянии логина.
    Фикстура параметризированна и вводит 3 группы логин/пароль, для получения
    соответсвующих сообщений которых 3:
    1. правильное имя и неправильный пароль.
    2. неправильное имя и неправильный пароль.
    3. правильное имя и правильный пароль.
    :return: тапл (полученный результат, ожидаемый результат)
    """
    params = request.param
    driver_login.implicitly_wait(15)
    login_field = driver_login.find_element_by_xpath("//*[@id='username']")
    login_field.send_keys(params[0])
    pass_field = driver_login.find_element_by_xpath("//*[@id='password']")
    pass_field.send_keys(params[1])
    login_button = driver_login.find_element_by_css_selector(
        "button.radius")
    login_button.click()
    authentication_result = driver_login.find_element_by_xpath(
        "//*[@id='flash']")
    result = params[2]
    return authentication_result, result


@pytest.fixture()
def checkboxes(driver_checkboxes) -> list:
    """
    Фикстура выбора чекбоксов.
    Ищет чекбоксы и нажимает на каждый.
    :return: список состояния чекбоксов.
    """
    checkboxes = driver_checkboxes.find_elements_by_xpath(
        "//input[@type='checkbox']")
    result = []
    for checkbox in checkboxes:
        checkbox.click()
        result.append(checkbox.is_selected())
    return result


@pytest.fixture()
def windows(driver_windows) -> str:
    """
    Фикстура открытия нового окна.
    Ищет ссылку на новое окно и нажимает на нее.
    Закрывает текущее окно и переключает драйвер на новое открытое окно.
    :return: строка с заголовком окна.
    """
    link_new_window = driver_windows.find_element_by_xpath(
        "//*[@id='content']/div/a")
    link_new_window.click()
    driver_windows.close()
    driver_windows.switch_to.window(driver_windows.window_handles[0])
    text_new_window = driver_windows.title
    return text_new_window


@pytest.fixture()
def add_elements(driver_elements) -> list:
    """
    Фикстура добавления элементов.
    Находит кнопку добавления элемента и нажимает на нее.
    Находит созданные элементы.
    :return: список созданных элементов.
    """
    add_button = driver_elements.find_element_by_xpath(
        "//*[@id='content']/div/button")
    add_button.click()
    delete_button = driver_elements.find_elements_by_xpath(
        "//*[@id='elements']/button")
    return delete_button


@pytest.fixture()
def delete_elements(driver_elements, add_elements) -> list:
    """
    Фикстура удаления элементов.
    Нажимает на каждый созданный фикстурой add_elements элемент.
    Ищет созданные элементы.
    :param add_elements: список созданных фикстурой add_elements элементов.
    :return: список созданных элементов.
    """
    for button in add_elements:
        button.click()
    delete_button = driver_elements.find_elements_by_xpath(
        "//*[@id='elements']/button")
    return delete_button
