from selenium.webdriver import Safari


def driver_start():
    """
    Функция старта драйвера.
    :return: драйвер.
    """
    return Safari()


def open_link(link_arg: str, driver_arg) -> None:
    """
    Функция открытия окна.
    :param link_arg: ссылка.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.get(link_arg)


def button_click_xpath(driver_arg) -> None:
    """
    Функция поиска кнопки по xpath и нажатия на нее.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.find_element_by_xpath(
        "//a[@class='et_pb_button et_pb_button_4 et_pb_bg_layout_light']"
    ).click()


def button_click_css(driver_arg) -> None:
    """
    Функция поиска кнопки по css и нажатия на нее.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.find_element_by_css_selector(
        ".et_pb_button.et_pb_button_4.et_pb_bg_layout_light").click()


def button_click_class(driver_arg) -> None:
    """
    Функция поиска кнопки по class name и нажатия на нее.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.find_element_by_class_name(
        "et_pb_button.et_pb_button_4.et_pb_bg_layout_light").click()


def driver_stop(driver_arg) -> None:
    """
    Функция остановки драйвера.
    :param driver_arg: драйвер.
    :return: None.
    """
    driver_arg.close()


if __name__ == '__main__':
    link = "https://ultimateqa.com/complicated-page/"
    # вызов функции старта драйвера.
    driver = driver_start()
    # открытие ссылки.
    open_link(link, driver)
    # поиск 2й кнопки во 2м столбце, 3 способа и нажатие на кнопку.
    button_click_xpath(driver)
    button_click_css(driver)
    button_click_class(driver)
    # закрытие окна.
    driver_stop(driver)
