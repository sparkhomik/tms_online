import pytest
from selenium.webdriver import Safari


@pytest.fixture(scope="session")
def safari_driver():
    """
    Фикстура старта и закрытия драйвера.
    :return: драйвер.
    """
    driver = Safari()
    yield driver
    driver.quit()


@pytest.fixture()
def driver_open(safari_driver):
    """
    Фикстура открытия окна по ссылке.
    :param safari_driver: драйвер.
    :return: драйвер.
    """
    safari_driver.get("https://ultimateqa.com/filling-out-forms/")
    return safari_driver


@pytest.fixture(
    params=[("test_name", "", "Please, fill in the following fields:Message"),
            ("", "test_message", "Please, fill in the following fields:Name"),
            ("test_name", "test_message", "Thanks for contacting us")],
    ids=["no_message", "no_name", "successful"])
def form(driver_open, request) -> tuple:
    """
    Фикстура ввода name и message.
    Ищет поля name и message и вводит соответсвующие данные.
    Ищет кнопку submit и нажимает на нее.
    Фикстура параметризированна и вводит 3 группы name и message, для
    получения соответсвующих сообщений которых 3:
    1. отсутствует сообщение.
    2. отсутствует имя.
    3. имя и сообщение введены.
    :param driver_open: драйвер.
    :param request: request.
    :return: ожидаемый результат и фактический результат.
    """
    params = request.param
    driver_open.implicitly_wait(15)
    name_field = driver_open.find_element_by_xpath(
        "//input[@id='et_pb_contact_name_0']")
    message_field = driver_open.find_element_by_xpath(
        "//textarea[@id='et_pb_contact_message_0']")
    button_submit = driver_open.find_element_by_xpath(
        "//button[@name='et_builder_submit_button']")
    name_field.send_keys(params[0])
    message_field.send_keys(params[1])
    button_submit.click()
    """
    Если одно/оба поля пусты, то после нажатия кнопки, сообщение имеет пункты,
    указывающие какое поле требует ввода.
    Реализована проверка, если элементы доп сообщений найдены, то текст
    этого элемента добавляется к строке основного сообщения.
    """
    message = driver_open.find_element_by_xpath(
        "//div[@class='et-pb-contact-message']/p").text
    message_add = driver_open.find_elements_by_xpath(
        "//div[@class='et-pb-contact-message']/ul/li")
    if message_add:
        result = message + message_add[0].text
    else:
        result = message

    expected_result = params[2]
    return expected_result, result
