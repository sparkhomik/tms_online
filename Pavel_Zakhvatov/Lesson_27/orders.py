from mysql import connector


def connect_db(db):
    """
    Функция подключения к БД.
    """
    cursor = db.cursor()
    print("db connect")
    return cursor


def close_db(db):
    """
    Функция отключения от БД.
    """
    print("db close")
    db.close()


def create_table(table_name: str, cursor) -> None:
    """
    Функция создания таблицы.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    create_table = f"CREATE TABLE {table_name} " \
                   f"(ord_no INT(5), purch_amt FLOAT(7,2), ord_date DATE, " \
                   f"customer_id INT(4), salesman_id INT(4))"
    cursor.execute(create_table)
    print(f"create table: {table_name}")


def dell_table(table_name: str, cursor) -> None:
    """
    Функция удаления таблицы.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"DROP TABLE {table_name}")
    print(f"dell table: {table_name}")


def inset_in_table(table_name: str, db, cursor) -> None:
    """
    Функция заполнения таблицы.
    :param table_name: имя таблицы.
    :param db: db.
    :param cursor: cursor.
    :return: None.
    """
    for ord_no, purch_amt, ord_date, customer_id, salesman_id in [
        ("70001", "150.5", "2012-10-05", "3005", "5002"),
        ("70009", "270.65", "2012-09-10", "3001", "5005"),
        ("70002", "65.26", "2012-10-05", "3002", "5001"),
        ("70004", "110.5", "2012-08-17", "3009", "5003"),
        ("70007", "948.5", "2012-09-10", "3005", "5002"),
        ("70005", "2400.6", "2012-07-27", "3007", "5001"),
        ("70008", "5760", "2012-09-10", "3002", "5001"),
        ("70010", "1983.43", "2012-10-10", "3004", "5006"),
        ("70003", "2480.4", "2012-10-10", "3009", "5003"),
        ("70012", "250.45", "2012-06-27", "3008", "5002"),
        ("70011", "75.29", "2012-08-17", "3003", "5007"),
        ("70013", "3045.6", "2012-04-25", "3002", "5001")
    ]:
        inset_in_table = f"INSERT INTO {table_name} (" \
                         f"ord_no, purch_amt, ord_date, customer_id," \
                         f" salesman_id) VALUES (" \
                         f"'{ord_no}', '{purch_amt}', '{ord_date}'," \
                         f" '{customer_id}', '{salesman_id}')"
        cursor.execute(inset_in_table)
    db.commit()
    print("inset in table")


def print_all(table_name: str, cursor) -> None:
    """
    Функция вывода: дата заказа, id продавца, номер заказа, количество.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT ord_date, salesman_id, ord_no, purch_amt "
                   f"FROM {table_name}")
    for row in cursor.fetchall():
        print(row)


def print_salesman_id(table_name: str, cursor, id) -> None:
    """
    Функция вывода: номер заказа, дата заказа и количество по id продавца.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :param id: id продавца.
    :return: None.
    """
    cursor.execute(f"SELECT ord_no, ord_date, purch_amt "
                   f"FROM {table_name} where salesman_id = {id}")
    for row in cursor.fetchall():
        print(row)


def print_unique_id(table_name: str, cursor) -> None:
    """
    Функция вывода уникальных id продавца.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(
        f"SELECT DISTINCT salesman_id FROM {table_name}")
    for row in cursor.fetchall():
        print(row)


def print_between_ord(table_name: str, cursor, ord1: int, ord2: int) -> None:
    """
    Функция вывода заказов между двумя заказами.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :param ord1: id заказа больше.
    :param ord2: id заказа меньше.
    :return: None.
    """
    cursor.execute(
        f"SELECT * FROM {table_name} WHERE ord_no > {ord1} "
        f"AND ord_no < {ord2}")
    for row in cursor.fetchall():
        print(row)


if __name__ == '__main__':

    db_arg = connector.connect(
        host="localhost",
        user="root",
        password="",
        database="tms_test")

    # имя таблицы.
    table_arg = "orders"

    # подключение к БД.
    cursor_arg = connect_db(db_arg)

    # создание таблицы.
    create_table(table_arg, cursor_arg)

    # заполнение таблицы.
    inset_in_table(table_arg, db_arg, cursor_arg)

    # вывод: дата заказа, id продавца, номер заказа, количество.
    print_all(table_arg, cursor_arg)

    # вывод по id продавца = 5001.
    print_salesman_id(table_arg, cursor_arg, 5001)

    # вывод уникальных id продавца.
    print_unique_id(table_arg, cursor_arg)

    # вывод заказов между 70001 и 70007.
    print_between_ord(table_arg, cursor_arg, 70001, 70007)

    # удаление таблицы.
    dell_table(table_arg, cursor_arg)

    # отключение от БД.
    close_db(db_arg)
