from mysql import connector


def connect_db(db):
    """
    Функция подключения к БД.
    """
    cursor = db.cursor()
    print("db connect")
    return cursor


def close_db(db):
    """
    Функция отключения от БД.
    """
    print("db close")
    db.close()


def create_table(table_name: str, cursor) -> None:
    """
    Функция создания таблицы.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    create_table = f"CREATE TABLE {table_name} " \
                   f"(pro_id INT(3), pro_name VARCHAR(255), " \
                   f"pro_price INT(5), pro_com INT(2))"
    cursor.execute(create_table)
    print(f"create table: {table_name}")


def dell_table(table_name: str, cursor) -> None:
    """
    Функция удаления таблицы.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"DROP TABLE {table_name}")
    print(f"dell table: {table_name}")


def inset_in_table(table_name: str, db, cursor) -> None:
    """
    Функция заполнения таблицы.
    :param table_name: имя таблицы.
    :param db: db.
    :param cursor: cursor.
    :return: None.
    """
    for pro_id, pro_name, pro_price, pro_com in [
        ("101", "Mother Board", "3200", "15"),
        ("102", "Key Board", "450", "16"),
        ("103", "ZIP drive", "250", "14"),
        ("104", "Speaker", "550", "16"),
        ("105", "Monitor", "5000", "11"),
        ("106", "DVD drive", "900", "12"),
        ("107", "CD drive", "800", "2"),
        ("108", "Printer", "2600", "13"),
        ("109", "Refill cartridge", "350", "13"),
        ("110", "Mouse", "250", "12"),
    ]:
        inset_in_table = f"INSERT INTO {table_name} (" \
                         f"pro_id, pro_name, pro_price, pro_com) VALUES (" \
                         f"'{pro_id}', '{pro_name}', '{pro_price}'," \
                         f" '{pro_com}')"
        cursor.execute(inset_in_table)
    db.commit()
    print("inset in table")


def print_sum_price(table_name: str, cursor) -> None:
    """
    Функция вывода общей стоимости.
    :param table_name: имя таблицы.
    :param cursor: cursor
    :return: None.
    """
    cursor.execute(f"SELECT SUM(pro_price) FROM {table_name}")
    print(cursor.fetchall())


def print_avg_price(table_name: str, cursor) -> None:
    """
    Функция вывода средней стоимости.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT AVG(pro_price) FROM {table_name}")
    print(cursor.fetchall())


def print_between_price(table_name: str, cursor, price1: int, price2: int)\
        -> None:
    """
    Функция вывода строк с ценами между ценами.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :param price1: цена больше.
    :param price2: цена меньше.
    :return: None.
    """
    cursor.execute(
        f"SELECT * FROM {table_name} WHERE pro_price > {price1} "
        f"AND pro_price < {price2}")
    for row in cursor.fetchall():
        print(row)


def print_high_price(table_name: str, cursor) -> None:
    """
    Вывод pro_name с самой высокой ценой.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT pro_name FROM {table_name} WHERE pro_price = "
                   f"(SELECT MAX(pro_price) FROM {table_name})")
    print(cursor.fetchall())


def print_low_price(table_name: str, cursor) -> None:
    """
    Вывод pro_name с самой низкой ценой.
    :param table_name: имя таблицы.
    :param cursor: cursor.
    :return: None.
    """
    cursor.execute(f"SELECT pro_name FROM {table_name} WHERE pro_price = "
                   f"(SELECT MIN(pro_price) FROM {table_name})")
    print(cursor.fetchall())


if __name__ == '__main__':

    db_arg = connector.connect(
        host="localhost",
        user="root",
        password="",
        database="tms_test")

    # имя таблицы.
    table_arg = "pc"

    # подключение к БД.
    cursor_arg = connect_db(db_arg)

    # создание таблицы.
    create_table(table_arg, cursor_arg)

    # заполнение таблицы.
    inset_in_table(table_arg, db_arg, cursor_arg)

    # вывод общей стоимости.
    print_sum_price(table_arg, cursor_arg)

    # вывод средней стоимости.
    print_avg_price(table_arg, cursor_arg)

    # вывода с ценами между ценами.
    print_between_price(table_arg, cursor_arg, 200, 600)

    # вывод самой высокой ценой.
    print_high_price(table_arg, cursor_arg)

    # вывод самой низкой ценой.
    print_low_price(table_arg, cursor_arg)

    # удаление таблицы.
    dell_table(table_arg, cursor_arg)

    # отключение от БД.
    close_db(db_arg)
