from collections import OrderedDict

string_list = ["a", "b", "c"]
new_list = []
final_list = []
for i in enumerate(string_list):
    new_list.append(i)

od = OrderedDict(new_list)

for k, v in od.items():
    print(f"{k + 1}: {v}")
