# Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
initial_str = "dogcat"
modified_str = initial_str.lower()

initial_set = set(initial_str)
modified_set = set(modified_str)

wrong_letters = list(initial_set.difference(modified_set))

result = [initial_set == modified_set, wrong_letters]

print(result)
