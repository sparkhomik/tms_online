# Создайте список, состоящий из четырех строк. Затем, с помощью цикла for,
# выведите строки поочередно на экран.
cities = [
    "Gomel",
    "Minsk",
    "Grodno",
    "Mogilev"
]

for city in cities:
    print(city)

# Измените предыдущую программу так, чтобы в конце каждой буквы строки
# добавлялось тире
for city in cities:
    letters = list(city)
    for letter in letters:
        print(letter + "-")
    print()

# Напишите цикл, выводящий ряд четных чисел от 0 до 20.
# Затем, каждое третье число в ряде от -1 до -21.
# for i in range(0, 21):
#     if i % 2 == 0:
#         print(i)

# for i in range(-1, -21, -1):
#     if i % 3 != 0:
#         continue
#     print(i)
i = 0
while i <= 20:
    if i % 2 == 0:
        print(i)
    i += 1

j = -1
while j >= -21:
    if j % 3 == 0:
        print(j)
    j -= 1
