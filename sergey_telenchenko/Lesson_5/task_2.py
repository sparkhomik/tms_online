# Создайте программу, которая, принимая массив имён, возвращает строку
# описывающая количество лайков (как в Facebook).
print("Введите имена через запятую: ")
answer = input()
users = []
if answer != "":
    users = answer.split(", ")

message = "likes this"

if not users:
    print("No one", message)
elif len(users) <= 2:
    users_string = " and ".join(users)
    print(f"{users_string}", message)
elif len(users) == 3:
    first_user = users.pop(0)
    users_string = " and ".join(users)
    print(f"{first_user}, {users_string} {message}")
else:
    print(f"{users[0]}, {users[1]} and {len(users) - 2} others", message)
