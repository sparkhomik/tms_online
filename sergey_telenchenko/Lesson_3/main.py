import random
import math
# a = 10, b=23, поменять значения местами, чтобы в переменную “a”
# было записано значение “23”, в “b” - значение “-10”
a = 10
b = 23

a, b = b, -a

# значение переменной “a” увеличить в 3 раза, а значение “b” уменьшить на 3
a *= 3
b -= 3

# преобразовать значение “a” из целочисленного в число с плавающей точкой
# (float), а значение в переменной “b” в строку
a = float(a)
b = str(b)

# Разделить значение в переменной “a” на 11 и вывести результат с точностью
# 3 знака после запятой
a /= 11
print(round(a, 3))

# Преобразовать значение переменной “b” в число с плавающей точкой и записать
# в переменную “c”. Возвести полученное число в 3-ю степень.

c = float(b) ** 3

# Получить случайное число, кратное 3-м
print(random.randrange(0, 99, 3))

# Получить квадратный корень из 100 и возвести в 4 степень
print(math.sqrt(100) * 4)

# Строку “Hi guys” вывести 3 раза и в конце добавить “Today”
my_string = "Hi guys"
my_string *= 3
my_string += "Today"
print(my_string)

# Получить длину строки из предыдущего задания
print(len(my_string))

# Взять предыдущую строку и вывести слово “Today” в прямом и обратном порядке
today_index = my_string.index("Today")
today = my_string[today_index:]
today_reversed = today[::-1]
print(today)
print(today_reversed)

# “Hi guysHi guysHi guysToday” вывести каждую 2-ю букву
# в прямом и обратном порядке
every_second_forward = my_string[1::2]
every_second_backward = my_string[-2::-2]
print(every_second_forward)
print(every_second_backward)

# Используя форматирования подставить результаты из задания 10 и 11
# в следующую строку
# “Task 10: <в прямом>, <в обратном> Task 11: <в прямом>, <в обратном>”
modified_string = "Task 10: {today}, {today_reversed} Task 11: \
        {every_second_forward},\
        {every_second_backward}"\
        .format(today=today,
                today_reversed=today_reversed,
                every_second_backward=every_second_backward,
                every_second_forward=every_second_forward)

print(modified_string)

# Полученную строку в задании 12 вывести:
# а) Каждое слово с большой буквы
print(modified_string.title())
# б) все слова в нижнем регистре
print(modified_string.lower())
# в) все слова в верхнем регистре
print(modified_string.upper())

# Посчитать сколько раз слово “Task” встречается в строке из задания 12
print(modified_string.count("Task"))
