from collections import OrderedDict
from collections import Counter
import re

# Перевести строку в массив
str_1 = "Robin Singh"
str_1.split()

str_2 = "I love arrays they are my favorite"
str_2.split()

# Дан список: [‘Ivan’, ‘Ivanou’], и 2 строки: Minsk, Belarus
# Напечатайте текст: “Привет, Ivan Ivanou! Добро пожаловать в Minsk Belarus”

name = ['Ivan', 'Ivanou']
city = 'Minsk'
country = 'Belarus'

print(f"Привет, {name[0]} {name[1]}! Добро пожаловать в {city} {country}")

# Дан список ["I", "love", "arrays", "they", "are", "my", "favorite"]
# сделайте из него строку => "I love arrays they are my favorite"

array = ["I", "love", "arrays", "they", "are", "my", "favorite"]
string = " ".join(array)
print(string)

# Создайте список из 10 элементов, вставьте на 3-ю позицию новое значение,
# удалите элемент из списка под индексом 6
cars = ["Audi",
        "BMW",
        "Mercedes",
        "Opel",
        "VW",
        "Toyota",
        "Mazda",
        "Honda",
        "Kia",
        "Hyundai"
        ]
cars[2] = "Ford"
del cars[6]

# Есть 2 словаря
a = {'a': 1, 'b': 2, 'c': 3}
b = {'c': 3, 'd': 4, 'e': ""}

# Создайте словарь, который будет содержать в себе все элементы обоих словарей
c = {**a, **b}
# Обновите словарь “a” элементами из словаря “b”
a.update(b)
# Проверить что все значения в словаре “a” не пустые либо не равны нулю
print(all(a.values()))
# Проверить что есть хотя бы одно пустое значение
# (результат выполнения должен быть True)
print(not all(a.values()))
# Отсортировать словарь по алфавиту в обратном порядке
a = sorted(a.items(), reverse=True)
# Изменить значение под одним из ключей и вывести все значения
a[0] = ("e", 5)
reverse_ordered_dict = OrderedDict(a)
for k, v in reverse_ordered_dict.items():
    print(v)

# Создать список из элементов
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
# Вывести только уникальные значения и сохранить их в отдельную переменную
uniq_list_a = set(list_a)
# Добавить в полученный объект значение 22
uniq_list_a.add(22)
# Сделать list_a неизменяемым
list_a = tuple(list_a)
# Измерить его длину
print(len(list_a))


# Задачи на закрепление форматирования:
# Есть переменные
a = 10
b = 25

print(f"Summ is {a + b} and diff = {a - b}")
print("Summ is {sum} and diff = {sub}".format(sum=a + b, sub=a - b))
# Есть список
list_of_children = ["Sasha", "Vasia", "Nikalai"]

first_string = "First child is {sasha}, second is {vasia}, \
            and last one - {nikalai}"\
            .format(sasha=list_of_children[0],
                    vasia=list_of_children[1],
                    nikalai=list_of_children[2])
second_string = f"First child is {list_of_children[0]}, " \
                f"second is {list_of_children[1]}, " \
                f"and last one - {list_of_children[2]}"
print(first_string)
print(second_string)

# Вам передан массив чисел.
# Известно, что каждое число в этом массиве имеет пару, кроме одного
# Напишите программу, которая будет выводить уникальное число
arr = [1, 5, 2, 9, 2, 9, 1]
c = Counter(arr)
least_common = c.most_common()[-1]
print(least_common[0])

# Дан текст, который содержит различные английские буквы и знаки препинания.
# Вам необходимо найти самую частую букву в тексте.
# Результатом должна быть буква в нижнем регистре.
# При поиске самой частой буквы, регистр не имеет значения,
# так что при подсчете считайте, что "A" == "a".
# Убедитесь, что вы не считайте знаки препинания, цифры и пробелы,
# а только буквы.
# Если в тексте две и больше буквы с одинаковой частотой,
# тогда результатом будет буква, которая идет первой в алфавите.
s = "How do you do?"
# переводим строку в нижний регистр
s = s.lower()
# убираем все лишние символы кроме букв
s = re.sub('[^a-z]+', '', s)
# преобразуем строку в список и сортируем по алфавиту
s = sorted(list(s))
# подсчитываем наиболее частый символ
c = Counter(s)
print(c.most_common(1)[0][0])
