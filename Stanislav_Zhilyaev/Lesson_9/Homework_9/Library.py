class Book:
    all_books = dict()

    def __init__(self, title: str, author: str, pages: str, isbn: str) -> None:
        """
        Создание книги в библиотеке.
        :param title: имя книги
        :param author: автор
        :param pages: количество страниц
        :param isbn: ISBN
        """

        self.title = title
        self.author = author
        self.pages = pages
        self.isbn = isbn
        Book.all_books[title] = 'free'

    @staticmethod
    def check(atr: str) -> bool:
        """
        Проверка наличия книги в библиотеке.
        :param atr: наименование книги
        :return: книга в наличии или нет
        """
        return Book.all_books[atr] == 'free'


class User:

    def __init__(self, name: str) -> None:
        """
        Создание карточки читателя библиотеки.
        :param name: ФИО
        """
        self.name = name

    @staticmethod
    def take_book(atr: str) -> None:
        """
        Резервирование книги.
        :param atr: наименование книги
        :return: книга зарезервирована
        """
        Book.all_books[atr] = 'reserve'

    @staticmethod
    def return_book(atr: str) -> None:
        """
        Возврат книги.
        :param atr: наименование книги
        :return: книга вернулась в библиотеку и свободна для резервирования
        """
        Book.all_books[atr] = 'free'

    @staticmethod
    def reserve_book(atr: str) -> None:
        """
        Бронирование книги.
        :param atr: наименование книги
        :return: книга забронирована
        """
        Book.all_books[atr] = 'booked'


def book_title() -> str:
    """
    Получение названия книги от читателя.
    :return: название книги, с которой читатель хочет выполнить действие
    """
    a: str = ''
    b = Book.all_books
    while True:
        if a not in b.keys():
            a = input(f'Введите название книги из списка:\n{Book.all_books}\n')
            continue
        if not Book.check(a):
            print('Книга уже занята, выберите другую.')
            a = ''
        break
        return a


def book_operation() -> str:
    """
    Выбор операции, производимой читателем над выбранной книжкой.
    :return: номер операции, производимой читателем над выбранной книжкой
    """
    c = ''
    while True:
        if c not in ['1', '2', '3']:
            c = input('Введите номер операции, которую хотите '
                      'выполнить:\n1. Взять\n2. Вернуть\n'
                      '3. Забронировать\n')
            continue
        return c


def main_1() -> None:
    """
    создание библиотеки и произведение пользователем операции над выбранной
    книгой (резервирование, возврат или бронирование)
    """
    Book('title1', 'author1', '100', '101')
    Book('title2', 'author2', '200', '202')
    Book('title3', 'author3', '300', '303')
    a = book_title()
    c = book_operation()
    if c == '1':
        User.take_book(a)
    elif c == '2':
        User.return_book(a)
    else:
        User.reserve_book(a)
