class Bank:
    """
    Класс Bank.
    total_amount - сумма на конец вклада.
    profit - прибыль на конец вклада.
    """
    new_var = 0  # дополнительная переменная для уменьшения длины формулы.
    total_amount = 0
    profit = 0

    def __init__(self, amount: int, period: int, percent: int) -> None:
        """
        Атрибуты экземпляра класса.
        :param amount: сумма вклада.
        :param period: срок вклада.
        :param percent: годовой процент.
        """
        self.amount = amount
        self.period = period
        self.percent = percent
        self.deposit(amount, period, percent)

    def deposit(self, amount: int, period: int, percent: int) -> None:
        """
        Метод экземпляра класса.
        Функция для подсчета суммы вклада на конец срока и прибыли.
        Расчет производится по формуле ежемесячной капитализации:
        P * (1 + r / n) ** (n * t)
        Р – изначальная сумма.
        r – годовая процентная ставка.
        n – сколько раз в год учитывается процентная ставка.
        t – число лет.
        :param amount: сумма вклада.
        :param period: срок вклада.
        :param percent: годовой процент.
        :return: None.
        """
        self.new_var = amount * (1 + ((percent / 100) / 12)) ** (12 * period)
        self.total_amount = self.new_var
        self.profit = self.total_amount - amount


def start() -> None:
    """
    Основная функция.
    Содержит цикл для непрерывного ввода.
    :return: None.
    """
    while True:
        deposit = input('amount, period, percent: ')
        deposit = deposit.split(',')
        deposit = list(map(int, deposit))
        deposit = Bank(*deposit)

        print('total amount at the end of the period:',
              round(deposit.total_amount, 2))
        print('profit at the end of the period:', round(deposit.profit, 2))


if __name__ == '__main__':
    start()
