def test_buttons(complicated_page_driver, the_second_button,
                 click_on_the_second_button):
    """
    Проверка, что xpath, css selector и class name будет возвращена та же
    кнопка и клик по ней.
    """
    assert the_second_button[0] == the_second_button[1] == the_second_button[2]


def test_check_filling_the_form_result(filling_out_forms_driver,
                                       filling_the_name, filling_the_msg,
                                       click_on_the_button, result_msg):
    """
    Проверка наличия сообщения после отправки заполненной формы.
    """
    assert result_msg == 'Thanks for contacting us'


def test_submit_only_name(filling_out_forms_driver, filling_the_name,
                          click_on_the_button, result_msg):
    """
    Проверка наличия сообщения об ошибке при отправке заполненной формы, если
    было заполнено только поле Name.
    """
    assert 'Message' in result_msg


def test_submit_only_msg(filling_out_forms_driver, filling_the_msg,
                         click_on_the_button, result_msg):
    """
    Проверка наличия сообщения об ошибке при отправке заполненной формы, если
    было заполнено только поле Name.
    """
    assert 'Name' in result_msg
