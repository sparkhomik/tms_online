import pytest
from selenium.webdriver import Chrome


@pytest.fixture(scope='session', autouse=True)
def driver():
    """
    Запуск и закрытие драйвера Chrome в начале и конце сессии соответственно.
    :return: экземпляр класса драйвера Chrome
    """
    driver = Chrome(
        executable_path="C:/Users/stasi/Web_Driver/chromedriver.exe")
    yield driver
    driver.quit()


@pytest.fixture(scope='session')
def complicated_page_driver(driver):
    """
    Открытие страницы https://ultimateqa.com/complicated-page/ в браузере.
    :param driver: экземпляр класса драйвера Chrome
    :return: экземпляр класса драйвера Chrome
    """
    driver.get("https://ultimateqa.com/complicated-page/")
    yield driver
    driver.close()


@pytest.fixture(scope='session')
def filling_out_forms_driver(driver):
    """
    Открытие страницы https://ultimateqa.com/filling-out-forms/ в браузере.
    :param driver: экземпляр класса драйвера Chrome
    :return: экземпляр класса драйвера Chrome
    """
    driver.get("https://ultimateqa.com/filling-out-forms/")
    yield driver
    driver.close()


@pytest.fixture(scope='session')
def the_second_button(driver) -> list:
    """
    :param driver: экземпляр класса драйвера Chrome
    :return: список с кнопками, найденными разными способами
    """
    driver.implicitly_wait(15)
    b_css_selector = ".et_pb_button.et_pb_button_4"
    b_class_name = "et_pb_button.et_pb_button_4.et_pb_bg_layout_light"
    b_xpath = "//a[@class='et_pb_button et_pb_button_4 et_pb_bg_layout_light']"
    second_button_css = driver.find_element_by_css_selector(b_css_selector)
    second_button_xpath = driver.find_element_by_xpath(b_xpath)
    second_button_class = driver.find_element_by_class_name(b_class_name)
    button = [second_button_css, second_button_xpath, second_button_class]
    return button


@pytest.fixture(scope='session')
def click_on_the_second_button(driver, the_second_button) -> None:
    """
    Клик по кнопке.
    :param driver: экземпляр класса драйвера Chrome
    :param the_second_button: список с кнопками, найденными разными способами
    """
    the_second_button[1].click()


@pytest.fixture(scope='session')
def filling_the_name(driver):
    """
    Заполнение поля Name левой формы.
    :param driver: экземпляр класса драйвера Chrome
    """
    driver.implicitly_wait(15)
    name_css_selector = "div.et_pb_column_1 .et_pb_contact_field_0 .input"
    name = driver.find_element_by_css_selector(name_css_selector)
    name.send_keys("Test Name")


@pytest.fixture(scope='session')
def filling_the_msg(driver):
    """
    Заполнение поля Message левой формы.
    :param driver: экземпляр класса драйвера Chrome
    """
    msg_css_selector = "div.et_pb_column_1 .et_pb_contact_field_1 .input"
    message = driver.find_element_by_css_selector(msg_css_selector)
    message.send_keys("Test Message")


@pytest.fixture(scope='session')
def click_on_the_button(driver):
    """
    Нажатие кнопки Submit.
    :param driver: экземпляр класса драйвера Chrome
    """
    button_css_selector = "div.et_pb_column_1 .et_pb_button"
    submit_button = driver.find_element_by_css_selector(button_css_selector)
    submit_button.click()


@pytest.fixture(scope='session')
def result_msg(driver) -> str:
    """
    Получение сообщение об успешности отправки заполненной формы.
    :param driver: экземпляр класса драйвера Chrome
    :return: текст сообщения
    """
    result_css_selector = "div.et_pb_contact_form_0 .et-pb-contact-message"
    result = driver.find_element_by_css_selector(result_css_selector)
    return result.text
