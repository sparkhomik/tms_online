"""
Шифр Цезаря, который шифрует и расшифровывает символы, заменяя их на
другие, стоящие от них на определенное число позиций.
"""

"""
Два английским алфавита для букв в верхнем и нижнем регистре.
Число букв продублированно, чтобы смещение не выходило за пределы алфавита.
"""
alphabet_en_l = 'abcdefghijklmnopqrstuvwxyzabcdefghijklmnopqrstuvwxyz'
alphabet_en_u = 'ABCDEFGHIJKLMNOPQRSTUVWXYZABCDEFGHIJKLMNOPQRSTUVWXYZ'


def get_action_user() -> str:
    """
    Функция для выбора пункта меню.
    Выполняет функцию до тех пор, пока не будет выбран верный вариант.
    :return: возвращает строку с выбранным пунктом меню.
    """
    print('1 - зашифровать, 2 - расшифровать, 3 - завершить')
    action = input('Выберите операцию: ')
    while action not in ('1', '2', '3'):
        print('Такой операции нет.')
        action = input('Выберите операцию: ')
    return action


def get_encrypted(message: str, key: int) -> str:
    """
    Функция для зашифровки.
    :param message: строка для зашифровки.
    :param key: ключ - смещение.
    :return: возвращает зашифрованную строку.
    """
    encrypted = ''
    for char in message:
        if char in alphabet_en_l:
            t = alphabet_en_l.find(char)
            new_key = t + key
            encrypted += alphabet_en_l[new_key]
        elif char in alphabet_en_u:
            t = alphabet_en_u.find(char)
            new_key = t + key
            encrypted += alphabet_en_u[new_key]
        else:
            encrypted += char
    return encrypted


def main(action: str) -> None:
    """
    Основная функция.
    Сравнивает пункты.
    :param action: строка и выбранный пункт меню.
    :return: None
    """
    while True:
        if action == '1':
            message = input('Введите строку(английские буквы): ')
            key = int(input('Введите ключ: '))
            print('Зашифрованная строка:', get_encrypted(message, key))
            break
        elif action == '2':
            message = input('Введите строку: ')
            key = int(input('Введите ключ: '))
            print('Расшифрованная строка:', get_encrypted(message, -key))
            break


def start() -> None:
    """
    Функция запуска модуля.
    Если вернувшийся пункт action это 3, то цикл прерывается.
    В остальных случаях вызывается функция main().
    :return: None
    """
    while True:
        action = get_action_user()
        if action == '3':
            break
        else:
            main(action)
            continue


"""
Для запуска модуля самого по себе используется следующее условие.
Если модуль запускается сам по себе, то вызывается функция start.
"""
if __name__ == '__main__':
    start()
