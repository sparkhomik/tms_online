from random import randrange

"""
Модуль игры крестики-нолики находится в пакете Tic tac toe.
Пользователь выбирает позицию на доске от 1-9, затем наступает ход компьютера.
Игра длится до победы либо игрока, либо компьютера.
"""

board = [' ' for x in range(10)]


def let_pos(letter: str, pos: int):
    '''
    :param letter: буква
    :param pos:  позиция буквы
    '''
    board[pos] = letter


def freepos(pos) -> bool:
    '''
    Проверка свободной ячейки в board
    Возвращает True, если ячейка пустая
    :param pos: позиция
    :return: bool
    '''
    return board[pos] == ' '


def visual_board(board):
    '''
    Функция отрисовки самой доски
    '''
    print('   |   | ')
    print(' ' + board[1] + ' | ' + board[2] + ' | ' + board[3])
    print('   |   | ')
    print('-----------')
    print('   |   | ')
    print(' ' + board[4] + ' | ' + board[5] + ' | ' + board[6])
    print('   |   | ')
    print('-----------')
    print('   |   | ')
    print(' ' + board[7] + ' | ' + board[8] + ' | ' + board[9])
    print('   |   | ')


def checkwin(a, b) -> bool:
    '''
    Функция проверки победителя.
    Возвращает True, если игрок победил.
    :param a: ячейка
    :param b: значение
    '''
    return (a[7] == b and a[8] == b and a[9] == b) or \
           (a[4] == b and a[5] == b and a[6] == b) or \
           (a[1] == b and a[2] == b and a[3] == b) or \
           (a[1] == b and a[4] == b and a[7] == b) or \
           (a[2] == b and a[5] == b and a[8] == b) or \
           (a[3] == b and a[6] == b and a[9] == b) or \
           (a[1] == b and a[5] == b and a[9] == b) or \
           (a[3] == b and a[5] == b and a[7] == b)


def playermove() -> None:
    '''
    Функция хода игрока.
    Выбор позиции от 1 до 9.
    Проверка валидности хода.
    '''
    step = True
    while step:
        move = input('Выберите позицию для хода \'X\' (1-9): ')
        try:
            move = int(move)
            if 0 < move < 10:
                if freepos(move):
                    step = False
                    let_pos('X', move)
                else:
                    print('Ход невозможен: Эта ячейка занята.')
            else:
                print('Ошибка! Вводите числа от 1-9!')
        except ValueError:
            print('Вы можете ввести только цифры.')


def comp_move() -> int:
    '''
    Функция хода компьютера.
    '''
    possible_moves = [x for x, letter in enumerate(board) if
                      letter == ' ' and x != 0]
    move = 0

    for let in ['O', 'X']:
        for i in possible_moves:
            board_copy = board[:]
            board_copy[i] = let
            if checkwin(board_copy, let):
                move = i
                return move
    # Попытаемся занять один из углов, если они свободны
    corners_open = []
    for i in possible_moves:
        if i in [1, 3, 7, 9]:
            corners_open.append(i)

    if len(corners_open) > 0:
        move = selectrandom(corners_open)
        return move
        # Занимаем центр, если он свободен
    if 5 in possible_moves:
        move = 5
        return move
        # Занимаем остальные ячейки, если они свободны
        edges_open = []
        for i in possible_moves:
            if i in [2, 4, 6, 8]:
                edges_open.append(i)
        if len(edges_open) > 0:
            move = selectrandom(edges_open)
        return move


def selectrandom(li: list) -> int:
    """
    Функция рандомного выбора позиции
    :li: список позиций
    """
    ln = len(li)
    r = randrange(0, ln)
    return li[r]


def isboardfull(board) -> bool:
    """
    Функция проверки заполненности доски
    Возвращаем True, если все клетки на доске были заняты.
    """
    if board.count(' ') > 1:
        return False
    else:
        return True


def start() -> None:
    """
    Функция запуска игры и вывод итогов.
    """
    separator = '\n'
    print('Добро пожаловать в игру tic tac toe! {0}Расположение доски: {0}'
          ' 1 | 2 | 3 {0}---|---|--- {0} 4 | 5 | 6 {0}---|---|--- {0}'
          ' 7 | 8 | 9'.format(separator))
    while not (isboardfull(board)):
        if not (checkwin(board, 'O')):
            playermove()
            visual_board(board)
        else:
            print('Вы проиграли!')
            break
        if not (checkwin(board, 'X')):
            move = comp_move()
            if move == 0:
                print('Ничья!')
            else:
                move = comp_move()
                let_pos('O', move)
                print('Соперник поставил \'O\' на позицию', move, ':')
                visual_board(board)
        else:
            print('Вы выиграли!')
            break
    if isboardfull(board):
        print('Ничья!')


if __name__ == "__main__":
    start()
