from cipher import Caesar_cipher
from Tic_Tac_Toe import Tic_Tac_Toe
from random import randint

'''
Основной модуль из которого можно запустить наши программы.
'''


def get_module() -> str:
    '''
    Функция для выбора пункта меню.
    :return: возвращает строку с выбранным пунктом меню.
    '''
    print('1 - крестики нолики, 2 - шифр Цезаря, ' '3 - завершить')
    action = input('Выберите опцию: ')
    count = 3

    while action not in ('1', '2', '3', '4'):
        print('Такой операции нет.')
        action = input('Выберите опцию: ')
        count -= 1
        if count == 0:
            action = str(randint(1, 4))
    return action


def start() -> None:
    '''
    Функция запуска модуля.
    :return: None
    '''
    user_module = get_module()
    while True:
        if user_module == '1':
            print('Крестики-нолики.')
            Tic_Tac_Toe.start()
            break
        elif user_module == '2':
            print('Шифр Цезаря.')
            Caesar_cipher.start()
            break
        elif user_module == '4':
            break


if __name__ == "__main__":
    start()
