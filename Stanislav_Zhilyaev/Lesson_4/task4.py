
# 1 - Перевести строку в массив
a_1 = "Robin Singh"
print(a_1.split())

a_2 = "I love arrays they are my favorite"
print(a_2.split())

# 2 - Дан список: Напечатайте текст:
a_3 = ' '.join(['Ivan ''Ivanou'])
a_4, a_5 = 'Minsk', 'Belarus'
print(f'Привет, {a_3}! Добро пожаловать в {a_4} {a_5}')

# 3 - Дан список: Сделайте из него строку
a_6 = ["I", "love", "arrays", "they", "are", "my", "favorite"]
a_7 = ' '
print(a_7.join(a_6), "\n")

# 4 - Создайте список из 10 элементов, вставьте на 3-ю
# позицию новое значение, удалите элемент из списка под индексом 6
a_8 = list(range(10))
a_8[3] = 'What'
a_8.pop(6)
print(a_8)

# 5 - Есть 2 словаря:
dict_1 = {'a': 1, 'b': 2, 'c': 3}
dict_2 = {'c': 3, 'd': 4, 'e': ''}
# 5.1 - Создайте словарь, который будет
# содержать в себе все элементы обоих словарей:
dict_3 = {**dict_1, **dict_2}
print(dict_3)
# 5.2 - Обновите словарь “dict1” элементами из словаря “dict2”:
dict_4 = dict_1.copy()
dict_4.update(dict_2)
print(dict_4)
# 5.3 - Проверить что все значения в словаре “dict1” не
# пустые либо не равны нулю
print(all(dict_1))
# 5.4 - Проверить что есть хотя бы одно пустое значение
print(any(dict_4))
# 5.5 - Отсортировать словарь по алфавиту в обратном порядке
dict_5 = sorted(dict_4, reverse=True)
print(dict_5)
# 5.6 - Изменить значение под одним из ключей и вывести все значения
del dict_4['b']
print(dict_4)

# 6 - Создать список из элементов:
list_a = [1, 2, 3, 4, 4, 5, 2, 1, 8]
# 6.1 - Вывести только уникальные значения и
# сохранить их в отдельную переменную
list_b = set(list_a)
print(list_b)
# 6.2 - Добавить в полученный объект значение 22
list_b.add(22)
print(list_b)
# 6.3 - Сделать list_a неизменяемым
list_a = tuple(list_a)
print(list_a)
# 6.4 - Измерить его длинну
print(len(list_a))

# 7 Задачи на закрепление форматирования:
# 7.1 - Есть переменные:
# Вывести 'Summ is <сумма этих чисел> and diff = <их разница>.'
a = 10
b = 25
c = a + b
x = a - b
print(f'Summ is {c} and diff = {x}.')
print('Summ is {c_2} and diff = {x_2}.'.format(c_2=a + b, x_2=a - b))
# 7.2 - Есть список:
# Вывести “First child is <первое имя из списка>,
# second is “<второе>”, and last one – “<третье>””
lf = ['Sasha', 'Vasia', 'Nikalai']
print((f'First child is {lf[0]}, second is {lf[1]}, and last one – {lf[2]}'))
