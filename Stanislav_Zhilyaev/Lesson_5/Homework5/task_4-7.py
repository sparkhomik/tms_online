# 4
'''
Напишите код, который возьмет список строк и пронумерует их.
Нумерация начинается с 1, имеет “:” и пробел
[] => []
["a", "b", "c"] => ["1: a", "2: b", "3: c"]
'''
list = ["Почему", "это", "так", "сложно", "помогите"]
arr = []
if not list:
    print(list)
else:
    for i in range(len(list)):
        arr.append("%s: %s" % (i + 1, list[i]))
    print(arr)

# 5 - Проверить, все ли элементы одинаковые:

a = [1, 1, 1]
b = [1, 2, 1]
c = ['a', 'a', 'a']
d = []

print(all(i == a[0] for i in a))
print(all(i == b[0] for i in b))
print(all(i == c[0] for i in c))
print(all(i == c[0] for i in d))

# 6 - Проверка строки. В данной подстроке проверить все ли буквы в строчном
# регистре или нет и вернуть список не подходящих.
a = 'doGCat'

b = []
c = []

if a != a.lower():
    b.append('False')
    for i in a:
        if i != i.lower():
            c.append(i)
else:
    b.append('True')
b.append(c)
print(b)

# 7 - Сложите все числа в списке, они могут быть отрицательными,
# если список пустой вернуть 0
a = []
b = [1, 2, 3]
c = [1.1, 2.2, 3.3]
f = [4, 5, 6]
g = range(101)
print(sum(a))
print(sum(b))
print(sum(c))
print(sum(f))
print(sum(g))
