a = list(map(str, input("Add names: ").split(', ')))
if a[0] == '':
    print("No one likes this")
elif len(a) == 1:
    print("%s likes this" % a[0])
elif len(a) == 2:
    print("%s and %s like this" % (a[0], a[1]))
elif len(a) == 3:
    print("%s, %s and %s like this" % (a[0], a[1], a[2]))
elif len(a) >= 4:
    print("%s, %s and %s others like this" % (a[0], a[1], len(a) - 2))
