from base_page import BasePage
from tools.fiction_page_locators import FictionPageLocators


class FictionPage(BasePage):
    """
    Класс страницы Fiction.
    """

    def get_title(self):
        return self.driver.title

    def get_header_name(self):
        return self.find_element(FictionPageLocators.FICTION_HEADER_LOCATORS)

    def get_path_name(self):
        return self.find_element(FictionPageLocators.FICTION_PATH_LOCATORS)
