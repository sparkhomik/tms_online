from selenium.webdriver.common.by import By


class LoginPageLocators:
    """
    Класс локаторов страницы логина.
    """
    LOGIN_FORM_LOCATORS = (By.CSS_SELECTOR, '#login_form')
    REGISTER_FORM_LOCATORS = (By.CSS_SELECTOR, '#register_form')
    LOGIN_HEADER_LOCATORS = (By.CSS_SELECTOR, '.breadcrumb>.active')
