from base_page import BasePage
from tools.basket_page_locators import BasketPageLocators


class BasketPage(BasePage):
    """
    Класс корзины.
    """

    def get_title(self):
        """
        Получение заголовка окна.
        :return: заголовок окна
        """
        return self.driver.title

    def get_header_name(self):
        """
        Получение header.
        :return: header
        """
        return self.find_element(BasketPageLocators.BASKET_HEADER_LOCATORS)

    def get_path_name(self):
        """
        Получение пути открытой страницы.
        :return:
        """
        return self.find_element(BasketPageLocators.BASKET_PATH_LOCATORS)

    def empty_basket(self):
        """
        Получение надписи о том, что страница пустая.
        :return: надпись о том, что страница пустая
        """
        return self.find_element(BasketPageLocators.BASKET_IS_EMPTY)
