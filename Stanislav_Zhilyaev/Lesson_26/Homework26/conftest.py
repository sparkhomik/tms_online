import pytest
from selenium import webdriver


@pytest.fixture(scope="session")
def driver():
    """
    Запуск и закрытие драйвера Chrome.
    :return: экземпляр класса драйвера Chrome
    """
    driver = webdriver.Chrome(
        executable_path="'/Users/stasi/Web_Driver/chromedriver.exe'")
    yield driver
    driver.quit()
