from pages.main_page import MainPage


def test_books_page_title(driver):
    """
    Проверка, что заголовок страницы "Books | Oscar - Sandbox".
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    books_page = main_page.open_books_page()
    assert books_page.get_title() == "Books | Oscar - Sandbox", \
        "Incorrect books page title!"


def test_books_header_name(driver):
    """
    Проверка, что header равен "Books".
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    books_page = main_page.open_books_page()
    assert books_page.get_header_name().text == "Books", \
        "Incorrect books page header!"


def test_books_path_name(driver):
    """
    Проверка наличия "Books" в path.
    :param driver: экземпляр класса драйвера Chrome
    """
    main_page = MainPage(driver)
    main_page.open()
    books_page = main_page.open_books_page()
    assert books_page.get_path_name().text == "Books", \
        "Incorrect books page path!"
