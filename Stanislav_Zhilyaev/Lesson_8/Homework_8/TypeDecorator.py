from functools import reduce

'''
Декоратор типов. Проверяет передаваемый тип функции
и конвертирует данные в соответствующий тип.
'''


def typed_decorator(type: str):
    def converter(func):
        def wrapper(*args):
            """
            Функция врапер.
            В зависимости от переданного аргумента, преобразует их
            в соответсвующий тип данных.
            :param args: аргументы, тип зависит от типа который передали.
            :return: возвращает функцию в которую передает преобразованные
            к соответсвующему типу аргумента.
            """
            if type == 'str':
                args = map(str, args)
                return func(*args)
            elif type == 'int':
                args = map(int, args)
                return func(*args)
            elif type == 'float':
                args = map(float, args)
                return func(*args)

        return wrapper

    return converter


'''
Оборачиваем функцию в декоратор.Функция передачи типа декоратору.
'''


def reducer_func(arg0, arg1):
    '''
    Функция сложения двух аргументов.
    :param arg0: тип зависит от типа передаваемого аргумента.
    :param arg1: тип зависит от типа передаваемого аргумента.
    :return: результат.
    '''
    return arg0 + arg1


@typed_decorator(type='int')
def add_symbols(*args):
    '''
    Функция принимает аргументы и возвращает их сложение.
    Тип аргументов будет приведен к типу type в декораторе.
    Сложение производится функцией reduce.
    :return: результат сложения аргументов
    '''
    return reduce(reducer_func, (args))


print(add_symbols('1', 2, 3))


@typed_decorator(type='str')
def add_symbols(*args):
    """
    Функция принимает аргументы и возвращает их сложение.
    :return: результат сложения аргументов
    """
    return reduce(reducer_func, (args))


print(add_symbols('8', 'a'))


@typed_decorator(type='float')
def add_symbols(*args):
    """
    Функция принимает аргументы и возвращает их сложение.
    :return: результат сложения аргументов.
    """
    return reduce(reducer_func, (args))


print(add_symbols('1.2', 1.4, 8))
