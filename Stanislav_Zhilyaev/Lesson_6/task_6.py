def my_function(a: list) -> dict:
    """
    Функция подсчета количества данных по типу в списке.
    :param a: список.
    :return: словарь, где ключ - тип, значение - количество.
    """
    a_dict = {}
    a_types = []
    for i in a:
        a_types.append(type(i))
    for j in a_types:
        a_dict[j] = a_dict.get(j, 0) + 1
    return a_dict


my_list = [1, 2, 3, 'a', (1, 2), 'b']

print(my_function(my_list))
