
def add_in_list(loc_list: list) -> list:
    """
        Функция добавления элемента в список.
        :param loc_list: список созданный на уровне модуля.
        :return: измененный список.
    """
    new_list = loc_list
    new_list.append('abcd')
    return new_list


unchangeable_list = [1, 2, 3]

print('Task 5:')
print(unchangeable_list)
print(add_in_list(unchangeable_list), '\n')
