"""
Простейший калькулятор v0.1
Реализуйте программу, которая спрашивала у пользователя,
какую операцию он хочет произвести над числами, а затем
запрашивает два числа и выводит результат
Проверка деления на 0.
"""


def simple_calculator(action: str, num1: int, num2: int) -> None:
    """
        :param action: выбранное действие.
        :param num1: первое число.
        :param num2: второе число.
        :return: результат.
        """
    num1 = float(num1)
    num2 = float(num2)
    # в зависимости от номера операции даем выбор операции:
    if action == '1':
        result = num1 + num2
        result_code = (print('Сумма: ', result))
    elif action == '2':
        result = num1 - num1
        result_code = (print('Разность: ', result))
    elif action == '3':
        result = num1 * num1
        result_code = (print('Произведение: ', result))
    elif action == '4':
        if num2 == 0:
            result_code = (print('Вы пытаетесь делить на 0'))
        else:
            result1 = num1 // num1
            result2 = num1 % num2
            result_code = (print('Частное:', result1, 'Остаток:', result2))
    else:

        result_code = (print('Выбран неверный пункт меню'))

    return result_code


print('Выберите операцию:')
print('1: Сложение')
print('2: Вычитание')
print('3: Умножение')
print('4: Деление')
a = input('Введите номер пункта меню:')
b = int(input('Введите первое число:'))
c = int(input('Введите второе число:'))
simple_calculator(a, b, c)
