# написать функцию, которая проверяет есть ли в списке объект,
# которые можно вызвать
def call(list: list, result: bool) -> bool:
    """
        Функция поиска возможных для вызова объектов списка.
        :param list: список.
        :return: список с объектами для вызова.
        """
    for i in list:
        if callable(i):
            result = True
        break
    return result


list = ['Hello', type('Hello'), [5, 6]]
print(call(list, result=False))
