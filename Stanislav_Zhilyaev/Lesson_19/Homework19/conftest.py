import pytest
import time
import sys


@pytest.fixture(scope='session', autouse=True)
def take_time():
    """
    prints start time and time after all tests had been executed
    """
    session_time = time.time()
    print('\nSTART: {}'.format(time.strftime('%X %d %b')))
    yield session_time
    print('\nFINISH: {}'.format(time.strftime('%X %d %b')))


@pytest.fixture(scope='session')
def print_filename():
    """
    Prints filename of current module
    """
    yield
    print("\nFILENAME: %s\n" % sys.argv[-1])


@pytest.fixture(scope='function', autouse=True)
def fixture_name(take_time):
    """
    fixture_name prints time between session start and an actual test run
    """
    # printing info about fixture used in current test
    print(fixture_name.__doc__)
    yield
    # printing info about time between session start and actual test run
    stop = time.time()
    delta = stop - take_time
    print('\nTIME PASSED AFTER SESSION STARTED : {:0.3} seconds'.format(delta))
