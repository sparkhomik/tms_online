import re


def if_special_char(word):
    """
    Checks if the string is specialchar
    """

    special_char = re.compile('[@_!#$%^&*()<>?/|\n}{~:]')

    if special_char.search(word) is None:
        return False
    else:
        return True
