class InputTypeError(Exception):
    """
    Throws an error, if input is not type of int
    """

    def __init__(self, message="input should be numeric"):
        self.message = message

        super().__init__(self.message)


def to_float(nmbr):
    """
    Checks if input could be converted into float
    """
    if type(nmbr) != int:
        raise InputTypeError
    else:
        return float(nmbr)
