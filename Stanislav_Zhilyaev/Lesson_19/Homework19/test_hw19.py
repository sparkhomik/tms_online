import pytest
import time
from is_lower import is_lower
from if_special_char import if_special_char
from to_float import to_float
from to_float import InputTypeError


# class scope fixtures.

@pytest.mark.usefixtures("take_time", "print_filename", "fixture_name")
class TestClass:
    # parametrize tests for each function called three times

    time.sleep(0.5)

    @pytest.mark.parametrize('wrd, output', [("FDSd", False), ("fdFD", False),
                                             ("fdfdsf", True)])
    def test_is_lower(self, wrd, output):
        assert is_lower(wrd) == output

    @pytest.mark.parametrize('string, output',
                             [(45, 45.0), (2, 2.0), (84, 84.0)])
    def test_to_float(self, string, output):
        assert to_float(string) == output

    def test_to_float_raises(self):
        """
        Should raise an exception if InputTypeError haven't called
        """
        with pytest.raises(InputTypeError):
            time.sleep(0.3)

            to_float(nmbr=4.5)

    @pytest.mark.parametrize('schar, output', [("dfsdn%fsd", True),
                                               ("^^!!dfs", True),
                                               ("fsdfdsf", False)])
    def test_if_special_char(self, schar, output):
        assert if_special_char(schar) == output
