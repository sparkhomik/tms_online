from abc import ABC, abstractmethod
import random


class Animal(ABC):
    '''
    Класс всех животных, живущих на ферме.
    '''

    def __init__(self, name: str, age: int) -> None:
        '''
        Объект класса животные.
        :param name: имя животного.
        :param age: возраст животного.
        '''
        self.name = name
        self.__age = age

    class NotExistException(Exception):
        """
        custom exception
        """
        message = "NotExistException"

        def __init__(self, message):
            super().__init__(self.message)

    def __str__(self) -> str:
        '''
        Выводит информацию о животном.
        :return: имя и возраст животного.
        '''
        if self.__age <= 1:
            return f'{self.name} - {self.__age} year old'
        else:
            return f'{self.name} - {self.__age} year old'

    def age(self) -> int:
        '''
        Увеличение возраста животного на один год.
        :return: увеличенный на один год возраст животного
        '''
        self.__age += 1
        return self.__age

    @abstractmethod
    def voice(self) -> None:
        '''
        Абстрактный метод для предопределения в классах наследника.
        '''
        raise NotImplementedError


class Pets(Animal, ABC):
    '''
    Класс домашних животных, который содержит общий
    метод для всех домашнихживотных
    '''

    @staticmethod
    def go() -> str:
        '''
        Метод ходьбы.
        :return: ходьбу.
        '''
        return 'Yahhoo'


class Birds(Animal, ABC):
    '''
    Класс птиц.
    '''

    @staticmethod
    def fly() -> str:
        '''
        Метод полета.
        :return: полет.
        '''
        return 'Woooohoooo'


class Pig(Pets):
    '''
    Класс свинок. Содержит общий метод для всех свинок.
    '''

    def voice(self) -> str:
        '''
        Голос свинки.
        :return: звук свиньи.
        '''
        return 'Oink-oink'


class Cow(Pets):
    '''
    Класс коровки. Содержит общий метод для всех коровок.
    '''

    def voice(self) -> str:
        '''
        Голос коровки.
        :return: звук коровки.
        '''
        return 'Moooooooooo'


class Goose(Birds):
    '''
    Класс гуся. Содержит общий метод для всех гусей.
    '''

    def voice(self) -> str:
        '''
        Голос гуся.
        :return: звук гуся.
        '''
        return 'HONK'


class Chicken(Birds):
    '''
    Класс курочки. Содержит общий метод для всех курочек.
    '''

    def voice(self) -> str:
        '''
        Голос курочки.
        :return: звук курочки.
        '''
        return 'Chuk-chuk'


class Farm:
    '''
    Класс фермы, которая может содержать в себе животных.
    '''

    def __init__(self, *animals: object) -> None:
        '''
        Создаение объекта класса фермы.
        :param animals: животные с фермы
        '''
        self.index = 0
        self.animals = list(animals)

    def __getitem__(self, index: int) -> object:
        '''
        Определение животного с фермы по индексу
        :return: Индекс
        '''
        return self.animals[index]

    def __iter__(self) -> object:
        '''
        Итерирует ферму.
        :return: ферму-итератор
        '''
        return self

    def __next__(self) -> object:
        '''
        Возвращает следуещее животное с фермы.
        :return: следующее животное.
        '''
        try:
            item = self.animals[self.index]
        except IndexError:
            raise StopIteration()
        self.index += 1
        return item

    def __len__(self) -> int:
        '''
        Определение количества животных на ферме.
        :return: количество животных
        '''
        return len(self.animals)

    def all_animals(self) -> None:
        '''
        Вывод информации по всем животных на ферме, издаваемых ими
        звуков, возраст(увеличеный на год)
        '''
        print('My farm: ')
        for animal in self:
            animal.age()
            print(animal)
            print(animal.voice())

        def random_number(self) -> int:
            """
            Определение номера случайного животного на ферме.
            :return: номер случайного животного на ферме
            """
            number = random.randrange(0, len(self) - 1)
            return number

    class NotExistException(Exception):

        """
        Класс для определения ошибки, которая будет выведена пользователю,
        в случае вызова несуществующего метода или атрибута.
        """

        def __init__(self, func: str, name: str) -> None:
            """
            Определение сообщения об ошибке.
            :param func: имя метода или атрибута
            :param name: имя класса, для которого этот метод или
            атрибут был вызван
            """
            self.message = f'Для текущего класса {name}' \
 \
                           f' такой метод не существует – {func}.'

            super().__init__(self.message)

    def random_number(self):
        pass


def main() -> None:
    """
    Создание фермы с разнообразными животными.
    Вывод информации по всем животным на ферме, возвраст которых предварительно
    был увеличен на один год.
    Вывод звуков, издаваемых каждым животным на ферме.
    Вывод инфорамции по случайно выбранному животному на ферме.
    Попытка узнать возраст курицы.
    """

    pig1, pig2 = Pig('pig1', 3), Pig('pig2', 5)
    cow1 = Cow('cow1', 6)
    goose1, goose2 = Goose('goose1', 1), Goose('goose2', 1)
    chicken1 = Chicken('chicken1', 1)
    chicken2 = Chicken('chicken2', 1)
    chicken3 = Chicken('chicken3', 1)
    my_farm = Farm(pig1, pig2, cow1, goose1, goose2,
                   chicken1, chicken2, chicken3)
    my_farm.all_animals()


if __name__ == '__main__':
    main()
