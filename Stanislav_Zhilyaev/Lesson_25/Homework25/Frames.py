def test_text_frame(get_iframe_text):
    """
    Функция проверки текста
    """
    assert get_iframe_text == "Your content goes here."
