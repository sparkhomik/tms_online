def test_checkbox_true(get_checkbox):
    """
    Функция проверки наличия чекбокса
    """
    assert bool(get_checkbox) is True


def test_text_gone(get_text):
    """
    Функция проверки текста
    Сравнивает актуальный текст с ожидаемым
    """
    assert get_text[0] == "It's gone!"


def test_checkbox_false(get_text):
    """
    Функция проверки отсутствия чекбокса после клика на кнопку
    """
    assert bool(get_text[1]) is True


def test_disabled_input(get_disabled_input):
    """
    Функция проверки неактивности поля
    """
    assert get_disabled_input is False


def test_text_enabled(get_enabled_input):
    """
    Функция проверки текста.
    Сравнивает актуальный текст с ожидаемым
    """
    assert get_enabled_input[0] == "It's enabled!"


def test_enabled_input(get_enabled_input):
    """
    Функция проверки активности поля
    """
    assert bool(get_enabled_input[1]) is True
