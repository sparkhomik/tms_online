import pytest
from selenium.webdriver import Chrome
from selenium.webdriver.common.by import By
from selenium.webdriver.support.wait import WebDriverWait
from selenium.webdriver.support import expected_conditions as EC


@pytest.fixture(scope='session', autouse=True)
def safari_driver():
    """
    Запуск и закрытие драйвера Chrome в начале и конце сессии соответственно.
    :return: экземпляр класса драйвера Chrome
    """
    driver = Chrome(
        executable_path="C:/Users/stasi/Web_Driver/chromedriver.exe")
    yield driver
    driver.quit()


@pytest.fixture()
def driver_page(safari_driver):
    """
    Фикстура открытия заданной страницы
    """
    safari_driver.get("http://the-internet.herokuapp.com/dynamic_controls")
    return safari_driver


@pytest.fixture()
def get_checkbox(driver_page):
    """
    Фикстура поиска чекбокса
    :return: web-element
    """
    checkbox = driver_page.find_element(By.XPATH, "//input[@type='checkbox']")
    return checkbox


@pytest.fixture()
def get_text(driver_page) -> tuple:
    """
    Фикстура клика на кнопку и проверки наличия текста и отсутствия чекбокса
    :return: тапл текст и чекбокс
    """
    driver_page.find_element(By.XPATH, "//button[@type='button'][1]").click()
    message = WebDriverWait(driver_page, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "p#message"))
    )
    checkbox = WebDriverWait(driver_page, 10).until_not(
        EC.presence_of_element_located((By.XPATH, "//input[@type='checkbox']"))
    )
    return message.text, checkbox


@pytest.fixture()
def get_disabled_input(driver_page):
    """
    Фикстура проверки чекбокса.
    :return: True or False.
    """
    input = driver_page.find_element(By.CSS_SELECTOR, "input[type='text']")
    return input.is_enabled()


@pytest.fixture()
def get_enabled_input(driver_page) -> tuple:
    """
    Фикстура клика на кнопку и проверки наличия текста и проверки
    кликабельности поля
    :return: тапл текст и статус.
    """
    driver_page.find_element(By.XPATH, '(//button[@type="button"])[2]').click()
    message = WebDriverWait(driver_page, 10).until(
        EC.presence_of_element_located((By.CSS_SELECTOR, "p#message"))
    )
    status = WebDriverWait(driver_page, 5).until(EC.element_to_be_clickable(
        (By.XPATH, "//form[@id='input-example']/input")))
    return message.text, status


@pytest.fixture()
def get_iframe_text(safari_driver):
    """
    Фикстура проверки текста из iframe.
    :return: текст.
    """
    safari_driver.get('http://the-internet.herokuapp.com/frames')
    safari_driver.find_element(By.XPATH, "//a[@href='/iframe']").click()
    safari_driver.switch_to.frame(safari_driver.find_element(
        By.TAG_NAME, "iframe"))
    WebDriverWait(safari_driver, 30).until(EC.visibility_of_element_located(
        (By.CSS_SELECTOR, ".mce-content-body  p"),
    ))
    message = safari_driver.find_element(
        By.CSS_SELECTOR, ".mce-content-body p")
    return message.text
