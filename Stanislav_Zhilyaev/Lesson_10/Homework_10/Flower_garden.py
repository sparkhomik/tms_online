from operator import itemgetter


class Flowers:
    """
    Основной классс и его характеристики
    """
    name = None

    def __init__(self, length_of_stem, colour, fresh):
        """
        Атрибуты экземпляра класса Flowers
        :param length_of_stem: длинна стебля
        :param colour: цвет цветка
        :param fresh: свежесть
        """
        self.length_of_stem = length_of_stem
        self.colour = colour
        self.fresh = fresh

    def __str__(self):
        """
        Функция вывода цвета и названия цветка.
        :return: строку - цвет и название
        """
        return f'{self.colour} {self.name}'

    def __getitem__(self, item):
        """
        Функция доступа к элементам поиска
        :param item: элемент
        :return: возвращает элемент
        """
        return getattr(self, item)


class Rose(Flowers):
    name = "Rose"
    price = 7


class ChamomileBig(Flowers):
    name = "Chamomile"
    price = 9


class Tulips(Flowers):
    name = "Tulips"
    price = 5


class Violets(Flowers):
    name = "Violet"
    price = 6


class GetBouquet:
    """
    Класс для предметов декора букета
    """
    name = None

    def __init__(self, colour):
        """
        Атрибут экземпляра
        :param colour: цвет
        """
        self.colour = colour

    def __str__(self):
        """
        Метод, выводящий строку
        :return: цвет и название
        """
        return f"{self.colour} {self.name}"


class Paper(GetBouquet):
    """
    Класс бумага.
    """
    name = "Paper"
    price = 3


class Tape(GetBouquet):
    """
    Класс лента.
    """
    name = "Tape"
    price = 4


class Bouquet:
    """
    Класс букет.
    """

    def __init__(self, flowers: list, things):
        """
        Атрибуты экземпляра класса Bouquet
        :param flowers: список цветов
        :param things: список предметов декора
        """
        self.flowers = flowers
        self.things = things

    def __str__(self):
        """
        Метод, выводящий строку.
        :return: список цветов в букете.
        """
        flowers_1 = [str(flower).split(",") for flower in self.flowers]
        flowers_str = ''
        for flower in zip(*flowers_1):
            flowers_str = ", ".join(flower)
        return f"Список цветов: {flowers_str}"

    def flower_wilting_time(self) -> float:
        """
        Функция для определения среднего времени увядания букета.
        :return:  среднее время увядания букета.
        """
        wilting_time = [i.fresh for i in self.flowers]
        wilting_str = round(sum(wilting_time) / len(wilting_time), 1)
        return wilting_str

    def calculate_bouquet_price(self) -> int:
        """
        Функция для подсчета стоимости букета
        :return: стоимость букета
        """
        flowers_price = [i.price for i in self.flowers]
        accessories_price = [i.price for i in self.things]
        price = sum(flowers_price) + sum(accessories_price)
        return price

    def sort_property(self, proper) -> str:
        """
        Сортирует по заданному параметру
        :param proper: параметр
        :return: выводит результат сортировки
        """
        sorted_flowers = sorted(self.flowers, key=itemgetter(proper))
        sort_str = ", ".join(str(flower) for flower in sorted_flowers)
        return sort_str

    def check_existing(self, name) -> bool:
        """
        Функция, проверяющая наличие цветка в букете
        :param name: название
        :return: результат поиска,  True or False
        """
        flower_exist = any(flower.name == name for flower in self.flowers)
        return bool(flower_exist)

    def flower_index(self, index) -> str:
        """
        Функция для получения цветка по индексу
        :param index: индекс
        :return: цветок под индексом, строку
        """
        return self.flowers[index]

    def search_by_colour_and(self, colour=None, price=None) -> str:
        """
        Функция для поиска по цвету и/или стоимости
        :param colour: цвет
        :param price: прайс
        :return: результат поиска, строка
        """
        flower_list = []
        sorting_args_str = ''
        for flower in self.flowers:
            if (price is None or flower.price == price) and \
 \
                    (colour is None or flower.colour == colour):
                flower_list.append(flower)
        flower_list = [str(flower).split(',') for flower in flower_list]
        if len(flower_list):
            for flower in zip(*flower_list):
                sorting_args_str += ", ".join(flower)
            return sorting_args_str


def main() -> None:
    """
    Функция для запуска
    """
    # Создаем экземпляры класса Flowers
    rose = Rose(colour='Black', fresh=4, length_of_stem=70)
    rose1 = Rose(colour='Red', fresh=7, length_of_stem=80)
    rose2 = Rose(colour='White', fresh=5, length_of_stem=60)
    tulip = Tulips(colour="Blue", fresh=3, length_of_stem=30)
    tape = Tape(colour="Red")
    paper_1 = Paper(colour="Green")
    flowers = [rose, tulip, rose2, rose1]
    things = [tape, paper_1]
    new_bouquet = Bouquet(flowers, things)
    print(f'Букет: {new_bouquet}')
    # Сортировка по названию и цене
    print(f'Сортировка по названию:'
          f' {new_bouquet.sort_property(proper="name")}')
    print(f' Сортировка по цене: '
          f'{new_bouquet.sort_property(proper="price")}')
    # Проверка наличия цветка
    print(f' В букете есть цветок: '
          f'{new_bouquet.check_existing(name="Tulips")}')
    print(f' В букете есть цветок: '
          f'{new_bouquet.check_existing(name="Rose")}')
    print(f' В букете есть цветок: '
          f'{new_bouquet.check_existing(name="Violet")}')
    print(f' В букете есть цветок: '
          f'{new_bouquet.check_existing(name="Chamomile")}')
    # Рассчет стоимости букета
    print(f' Стоимость букета: {new_bouquet.calculate_bouquet_price()}')
    # Рассчет среднего времени увядания
    print(f' Время увядания: {new_bouquet.flower_wilting_time()}')
    # Принтуем цветок под заданными индексами
    print(f' Цветок под индексом [0]: {new_bouquet.flower_index(0)}')
    print(f' Цветок под индексом [1]: {new_bouquet.flower_index(1)}')
    # Исчет по параметрам
    print(f' Результат поиска по цвету "белый", цене (7):'
          f'{new_bouquet.search_by_colour_and(colour="White", price=7)}')
    print(f' Результат по цене 50:'
          f'{new_bouquet.search_by_colour_and(price=50)}')
    print(f' Результат поиска по  цвету "черный":'
          f'{new_bouquet.search_by_colour_and(colour="Black")}')


if __name__ == "__main__":
    main()
